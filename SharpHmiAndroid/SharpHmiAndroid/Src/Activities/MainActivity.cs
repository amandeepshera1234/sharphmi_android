﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using System.Timers;
using Android.Content;
using Android.Widget;
using Android.Text;
using Android.Support.V7.Preferences;
using System;
using Android;
using Android.Support.V4.App;
using Android.Content.PM;
using HmiApiLib.Controllers.UI.IncomingRequests;
using HmiApiLib.Common.Enums;
using Android.Speech.Tts;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using Android.Runtime;
using System.Collections.Generic;
using HmiApiLib.Common.Structs;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using HmiApiLib.Types;
using HmiApiLib.Utility;
using HmiApiLib;
using HmiApiLib.Handshaking;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using static HmiApiLib.InitialConnectionCommandConfig;
using Java.IO;
using SharpHmiAndroid.Src.Adapters;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Distribute;

namespace SharpHmiAndroid
{
    [Activity(Label = "SharpHmiAndroid", MainLauncher = true, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    public class MainActivity : AppCompatActivity, AppUiCallback, ActivityCompat.IOnRequestPermissionsResultCallback, TextToSpeech.IOnInitListener
    {
        NavigationView navigationView;
        DrawerLayout drawer;
        private AppSetting appSetting;
        private static string CONSOLE_FRAGMENT_TAG = "console_frag";
        private static string MAIN_FRAGMENT_TAG = "main_frag";
        static string HMI_FULL_FRAGMENT_TAG = "hmi_full_frag";
        private static string VLC_PLAYER_FRAGMENT_TAG = "vlc_player_frag";
        static string HMI_OPTIONS_MENU_FRAGMENT_TAG = "hmi_options_menu_frag";
        static string PERFORM_INTERACTION_FRAGMENT_TAG = "perfrom_interaction_frag";
        public const int REQUEST_STORAGE = 10;
        private static string APP_ID = "766f353c7dda4622adc2b614e2674bcf";
        AppInstanceManager.SelectionMode selectedMode = AppInstanceManager.SelectionMode.NONE;
		ModuleInfo moduleInfo = null;
		Grid grid = null;
		Grid gridLocation = null;
		Grid gridServicearea = null;
		Grid gridSeat = null;
		Handler mHandler;
        Action action;
		SeatLocation seatLocGrid=null;
		SeatLocationAdapter seatLocationadapter;
		List<SoftButtonCapabilities> btnSoftBtnCapList = null;
		List<SeatLocation> seatLocationList;
		private string RegisterButtonSubscription = ComponentPrefix.Buttons + "." + FunctionType.OnButtonSubscription;
        private string RegisterOnAppRegister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppRegistered;
        private string RegisterOnAppUnRegister = ComponentPrefix.BasicCommunication + "." + FunctionType.OnAppUnregistered;
        private string RegisterPutfile = ComponentPrefix.BasicCommunication + "." + FunctionType.OnPutFile;
        private string RegisterVideoStreaming = ComponentPrefix.Navigation + "." + FunctionType.OnVideoDataStreaming;
        private string RegisterAudioStreaming = ComponentPrefix.Navigation + "." + FunctionType.OnAudioDataStreaming;
        ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam = null;
		List<ImageField> imageFieldsList = null;
		List<ButtonCapabilities> BtnCapList = null;
		TextToSpeech textToSpeech;
        List<string> speechList = new List<string>();

        string[] resultCode = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
        string[] speechCapabilities = Enum.GetNames(typeof(SpeechCapabilities));
        string[] prerecordedSpeech = Enum.GetNames(typeof(PrerecordedSpeech));
        string[] languages = Enum.GetNames(typeof(Language));
        string[] buttonCapabilitiesArray = Enum.GetNames(typeof(ButtonName));
        string[] vehicleDataNotificationStatusArray = Enum.GetNames(typeof(VehicleDataNotificationStatus));
        string[] ECallConfirmationStatusArray = Enum.GetNames(typeof(ECallConfirmationStatus));
        string[] AmbientLightStatusArray = Enum.GetNames(typeof(AmbientLightStatus));
        String[] DeviceLevelStatusArray = Enum.GetNames(typeof(DeviceLevelStatus));
        String[] PrimaryAudioSourceArray = Enum.GetNames(typeof(PrimaryAudioSource));
        String[] IgnitionStableStatusArray = Enum.GetNames(typeof(IgnitionStableStatus));
        String[] IgnitionStatusArray = Enum.GetNames(typeof(IgnitionStatus));
        String[] VehicleDataEventStatusArray = Enum.GetNames(typeof(VehicleDataEventStatus));
        String[] WarningLightStatusArray = Enum.GetNames(typeof(WarningLightStatus));
        String[] SingleTireStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] CompassDirectionArray = Enum.GetNames(typeof(CompassDirection));
        String[] DimensionArray = Enum.GetNames(typeof(Dimension));
        String[] EmergencyEventTypeArray = Enum.GetNames(typeof(EmergencyEventType));
        String[] FuelCutoffStatusArray = Enum.GetNames(typeof(FuelCutoffStatus));
        String[] ComponentVolumeStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] PRNDLArray = Enum.GetNames(typeof(PRNDL));
        String[] TurnSignalArray = Enum.GetNames(typeof(TurnSignal));
        String[] WiperStatusArray = Enum.GetNames(typeof(WiperStatus));
        String[] VehicleDataStatusArray = Enum.GetNames(typeof(VehicleDataStatus));
        String[] PowerModeQualificationStatusArray = Enum.GetNames(typeof(PowerModeQualificationStatus));
        String[] CarModeStatusArray = Enum.GetNames(typeof(CarModeStatus));
        String[] PowerModeStatusArray = Enum.GetNames(typeof(PowerModeStatus));
        String[] RCDefrostZoneArray = Enum.GetNames(typeof(DefrostZone));
        String[] RCVentilationModeArray = Enum.GetNames(typeof(VentilationMode));
        string[] buttonNames = Enum.GetNames(typeof(ButtonName));
		string[] windowType = Enum.GetNames(typeof(WindowType));

		string[] electronicParkBrakeStatusArray = Enum.GetNames(typeof(ElectronicParkBrakeStatus));
        string[] videoStreamingProtocolArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VideoStreamingProtocol));
        string[] videoStreamingCodecArray = Enum.GetNames(typeof(HmiApiLib.Common.Enums.VideoStreamingCodec));

        AppInstanceManager theInstance;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Distribute.SetEnabledForDebuggableBuild(true);
            AppCenter.Start("APP_ID", typeof(Analytics), typeof(Crashes), typeof(Distribute));
            SetContentView(Resource.Layout.splash_screen);
            theInstance = AppInstanceManager.Instance;
            if ((AppInstanceManager.bRecycled == false) || (theInstance.getAppSetting() == null))
            { //Newly launched application.
                this.appSetting = new AppSetting(this);
                theInstance.setAppSetting(this.appSetting);
                CheckForUpdates();
            }
            else
            {
                this.appSetting = theInstance.getAppSetting();
            }

            theInstance.userMovedAwayFromMediaProj = false;

            if (appSetting != null)
            {
                selectedMode = appSetting.getSelectedMode();
            }
            if (!AppUtils.checkPermission(ApplicationContext, Manifest.Permission.WriteExternalStorage))
            {
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.WriteExternalStorage },
                                                  REQUEST_STORAGE);
            }
            else
            {
                selectMode();
            }
        }

        private void CheckForUpdates()
        {
            UpdateManager.Register(this, APP_ID);
        }

        private void UnregisterManagers()
        {
            UpdateManager.Unregister();
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, APP_ID);
            theInstance.setAppUiCallback(this);
        }

        protected override void OnPause()
        {
            base.OnPause();
            UnregisterManagers();
        }

        protected override void OnStop(){
            base.OnStop();
            theInstance.setAppUiCallback(null);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            UnregisterManagers();
        }

        void selectMode()
        {
            if (selectedMode == AppInstanceManager.SelectionMode.NONE)
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
                SetContentView(Resource.Layout.splash_selection_screen);
                Button debugModeButton = (Button)FindViewById(Resource.Id.splash_debug_view);
                Button basicModeButton = (Button)FindViewById(Resource.Id.splash_basic_view);

                debugModeButton.Click += (sender, e) =>
                {
                    selectedMode = AppInstanceManager.SelectionMode.DEBUG_MODE;
                    mainActivityInitialization();
                    prefs.Edit().PutInt(Const.PREFS_KEY_SELECTION_MODE, (int)selectedMode).Commit();
                };

                basicModeButton.Click += (sender, e) =>
                {
                    Toast.MakeText(this, "HMI Sample Coming Soon.", ToastLength.Long).Show();
                    return;
                  
                };
            }
            else
            {
                mainActivityInitialization();
            }
        }

        public void mainActivityInitialization()
        {
			if (selectedMode == AppInstanceManager.SelectionMode.BASIC_MODE)
			{
				SetContentView(Resource.Layout.main_basic);
				navigationView = FindViewById<NavigationView>(Resource.Id.nav_view_basic);
			}
			else if (selectedMode == AppInstanceManager.SelectionMode.DEBUG_MODE)
			{ try {
					SetContentView(Resource.Layout.Main);
					navigationView = FindViewById<NavigationView>(Resource.Id.nav_view_debug);
				}
				catch (Exception e)
				{ }
				}

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            Android.Support.V7.App.ActionBarDrawerToggle _actionBarDrawerToggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open,
            Resource.String.navigation_drawer_close);

            drawer.SetDrawerListener(_actionBarDrawerToggle);

            navigationView.NavigationItemSelected += OnNavigationItemSelected;

            MetricsManager.Register(Application, APP_ID);

            //Register to Feedback Manager.
            FeedbackManager.Register(this, APP_ID, null);
            if (SdlService.instance != null)
            {
                setConsoleFragment();
            }
            else
            {
                settingsDialog();
            }
        }

        public void showFeedbackActivity()
        {
            try
            {
                FeedbackManager.ShowFeedbackActivity(ApplicationContext);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Feedback Activity could not be displayed ", ToastLength.Long).Show();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            switch (requestCode)
            {
                case REQUEST_STORAGE:
                    {
                        if (grantResults.Length > 0 && (grantResults[0] == (int)Permission.Granted))
                        {
                            selectMode();
                        }
                        else
                        {
                            Toast.MakeText(this, "Permission Denied.", ToastLength.Long).Show();
                        }
                    }
                    break;
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.main, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        private void OnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var menuItem = e.MenuItem;
            menuItem.SetChecked(!menuItem.IsChecked);
            drawer.CloseDrawers();

            switch (menuItem.ItemId)
            {
                case Resource.Id.consoleLogs:
                    clearAllBackStackFragments();
                    break;

                case Resource.Id.vlcFragment:
                    setVlcPlayerFragment();
                    break;

                case Resource.Id.findApps:
                    setMainFragment();
                    break;

                case Resource.Id.feedbackActivity:
                    showFeedbackActivity();
                    break;

                case Resource.Id.nav_exit:
                    exitApp();
                    break;

                default:
                    Toast.MakeText(Application.Context, "Something is Wrong", ToastLength.Short).Show();
                    break;

            }
        }

        /** Closes the activity and stops the proxy service. */
        private void exitApp()
        {
            Finish();

            var timer = new Timer();
            //What to do when the time elapses
            timer.Elapsed += (sender, args) => ExitAppCallback();
            //How often (5 sec)
            timer.Interval = 50;
            //Start it!
            timer.Enabled = true;
        }

        private void ExitAppCallback()
        {
            Process.KillProcess(Process.MyPid());
        }

        /*Method for clearing out all backstack entries for fragments.*/
        public void clearAllBackStackFragments()
        {
            Android.App.FragmentManager fm = this.FragmentManager;
            fm.PopBackStackImmediate();
        }

        public void setMainFragment()
        {
            MainFragment mainFragment = getMainFragment();

            Android.App.FragmentManager fragmentManager = this.FragmentManager;

            if (mainFragment == null)
            {
                mainFragment = new MainFragment();
                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, mainFragment, MAIN_FRAGMENT_TAG).AddToBackStack(null).CommitAllowingStateLoss();
                fragmentManager.ExecutePendingTransactions();
                this.SetTitle(Resource.String.app_name);
            }
            SupportActionBar.Title = "Apps";
        }

        public void setConsoleFragment()
        {

            ConsoleFragment consoleFragment = getConsoleFragment();

            Android.App.FragmentManager fragmentManager = this.FragmentManager;

            if (consoleFragment == null)
            {
                consoleFragment = new ConsoleFragment();
                fragmentManager.BeginTransaction()
                               .Add(Resource.Id.frame_container, consoleFragment, CONSOLE_FRAGMENT_TAG).Commit();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        public void setVlcPlayerFragment()
        {

            VlcPlayer vlcPlayerFragment = getVlcPlayerFragment();

            Android.App.FragmentManager fragmentManager = this.FragmentManager;

            if (vlcPlayerFragment == null)
            {
                vlcPlayerFragment = new VlcPlayer();
                fragmentManager.BeginTransaction()
                               .Replace(Resource.Id.frame_container, vlcPlayerFragment, VLC_PLAYER_FRAGMENT_TAG).AddToBackStack(null).CommitAllowingStateLoss();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.settings:
                    settingsDialog();
                    return true;

                case Resource.Id.jsonParser:
                    jsonParser();
                    return true;

                case Resource.Id.appVersion:
                    showAppVersion();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private EditText txtLocalFileName;
        private string requiredPermission = null;

        public void jsonParser()
        {
            Android.Support.V7.App.AlertDialog.Builder showMoreDialogBuilder;
            Android.Support.V7.App.AlertDialog dlg;

            showMoreDialogBuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)ApplicationContext
                .GetSystemService(Context.LayoutInflaterService);
            View view = inflater.Inflate(Resource.Layout.jsonparser,
                    null);

            showMoreDialogBuilder.SetView(view);

            txtLocalFileName = (EditText)view.FindViewById(Resource.Id.jsonparser_local_file);

            TextView txtFileName = (TextView)view.FindViewById(Resource.Id.jsonparser_local_file_name);

            txtLocalFileName.TextChanged += (sender, e) =>
            {
                File tmpFile = new File(txtLocalFileName.Text);
                if (tmpFile.IsFile && tmpFile.Exists())
                {
                    string tmpFileName = tmpFile.Name;
                    txtFileName.Text = tmpFileName;
                }
                else
                {
                    txtFileName.Text = Resources.GetText(Resource.String.jsonparser_file_default);
                }
            };

            Button btnChooseFile = (Button)view.FindViewById(Resource.Id.jsonparser_select_file_button);

            btnChooseFile.Click += delegate
            {
                requiredPermission = Manifest.Permission.ReadExternalStorage;
                if (AppUtils.checkPermission(this, requiredPermission))
                {
                    Intent intent = new Intent(ApplicationContext, typeof(FileChooserActivity));
                    StartActivityForResult(intent, Const.REQUEST_FILE_CHOOSER);
                }
            };

            ImageView delay_minus = (ImageView)view.FindViewById(Resource.Id.jsonparser_delay_minus);
            ImageView delay_plus = (ImageView)view.FindViewById(Resource.Id.jsonparser_delay_plus);
            EditText delay_timer = (EditText)view.FindViewById(Resource.Id.jsonparser_delay_timer);

            delay_minus.Click += delegate
            {

                double timer = double.Parse(delay_timer.EditableText.ToString());
                if (timer > 0.0)
                {
                    timer = timer - 0.1;
                    delay_timer.Text = "" + timer;
                }
            };

            delay_plus.Click += delegate
            {
                double timer = double.Parse(delay_timer.EditableText.ToString());
                timer = timer + 0.1;
                delay_timer.Text = "" + timer;
            };

            delay_timer.TextChanged += (sender, e) =>
            {
                if (double.Parse(delay_timer.Text) <= 0.0)
                {
                    delay_minus.Enabled = false;
                }
                else
                {
                    delay_minus.Enabled = true;
                }
            };

            showMoreDialogBuilder
            .SetTitle("Select JSON File")
            .SetCancelable(false)
            .SetNegativeButton("Cancel", (senderAlert, args) =>
            {

            })
			.SetNeutralButton("Stop Tx", (senderAlert, args) =>
			{
                JsonParser.Instance.stopAutoParseJSONFile();
			})
           .SetPositiveButton("Auto TX", (senderAlert, args) =>
                {
                    if ((null != txtLocalFileName.Text && (txtLocalFileName.Text.Length > 0)))
                    {
                        File initialFile = new File(txtLocalFileName.Text);
                        System.IO.FileStream targetStream = new System.IO.FileStream(txtLocalFileName.Text, System.IO.FileMode.Open);
                        JsonParser.Instance.startAutoParseJSONFile(targetStream, double.Parse(delay_timer.Text));
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                    .SetTitle("Empty File")
                                    .SetMessage("File is empty, please choose another file.")
                                   .SetPositiveButton("OK", (sen, arg) => { }).Show();
                        txtLocalFileName.Text = "";
                    }
                });

            dlg = showMoreDialogBuilder.Create();
            dlg.Show();

            Button jsonPreview = dlg.GetButton((int)DialogButtonType.Neutral);
            jsonPreview.Enabled = true;

        }

        public void showAppVersion()
        {
            String appVersion = "App Ver: ";
            try
            {
                appVersion += PackageManager.GetPackageInfo(PackageName, 0).VersionName;
            }
            catch (Exception e)
            {
                appVersion += "Unknown";
            }

            String libVersion = "Proxy Lib Ver: ";

            if (AppInstanceManager.Instance.getProxyVersionInfo() != null)
            {
                libVersion += AppInstanceManager.Instance.getProxyVersionInfo();
            }
            else
            {
                libVersion += "Info not available";
            }

            new Android.Support.V7.App.AlertDialog.Builder(this).
                       SetTitle("SharpHmi Version").
                       SetMessage(appVersion + "\n\n" + libVersion + "\n")
                       .SetNeutralButton(Android.Resource.String.Ok, (senderAlert, args) =>
            {
            }).Create().Show();
        }

        public void settingsDialog()
        {
            Android.Support.V7.App.AlertDialog.Builder builder;
            Android.Support.V7.App.AlertDialog dlg;

            appSetting = AppInstanceManager.Instance.getAppSetting();

            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View layout = inflater.Inflate(Resource.Layout.setting_option, null);

            EditText serverAddressEditText = layout
                .FindViewById(Resource.Id.selectprotocol_ServerAddress) as EditText;
            EditText serverPortEditText = (EditText)layout
                .FindViewById(Resource.Id.selectprotocol_ServerPort);
            EditText videoStreamingPortEditText = (EditText)layout.FindViewById(Resource.Id.selectprotocol_VideoStreamingPort);
            EditText audioStreamingPortEditText = (EditText)layout.FindViewById(Resource.Id.selectprotocol_AudioStreamingPort);

            CheckBox registerUi = (CheckBox)layout.FindViewById(Resource.Id.register_ui);
            CheckBox registerBasicCommunication = (CheckBox)layout.FindViewById(Resource.Id.register_basic_communication);
            CheckBox registerButtons = (CheckBox)layout.FindViewById(Resource.Id.register_buttons);
            CheckBox registerVr = (CheckBox)layout.FindViewById(Resource.Id.register_vr);
            CheckBox registerTts = (CheckBox)layout.FindViewById(Resource.Id.register_tts);
            CheckBox registerNavigation = (CheckBox)layout.FindViewById(Resource.Id.register_navigation);
            CheckBox registerVehicleInfo = (CheckBox)layout.FindViewById(Resource.Id.register_vehicle_info);
            CheckBox registerRc = (CheckBox)layout.FindViewById(Resource.Id.register_rc);
            CheckBox registerSystemTime = (CheckBox)layout.FindViewById(Resource.Id.register_systemTime);

            CheckBox enableRemoteControl = (CheckBox)layout.FindViewById(Resource.Id.enable_remote_control);

            CheckBox buildBCOnReady = (CheckBox)layout.FindViewById(Resource.Id.build_bc_on_ready);
            CheckBox subscribeButtonSubscription = (CheckBox)layout.FindViewById(Resource.Id.subscribe_button_subscription);
            CheckBox subscribeAppRegister = (CheckBox)layout.FindViewById(Resource.Id.subscribe_app_register);
            CheckBox subscribeAppUnregister = (CheckBox)layout.FindViewById(Resource.Id.subscribe_app_unregister);
            CheckBox subscribePutfile = (CheckBox)layout.FindViewById(Resource.Id.subscribe_putfile);
            CheckBox subscribeVideoStreaming = (CheckBox)layout.FindViewById(Resource.Id.subscribe_video_streaming);
            CheckBox subscribeAudioStreaming = (CheckBox)layout.FindViewById(Resource.Id.subscribe_audio_streaming);

            CheckBox chkResponseBCMixAudio = (CheckBox)layout.FindViewById(Resource.Id.response_bc_mix_audio_support_chk);
            CheckBox chkResponseButtonsGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_button_get_capabilities_chk);
            CheckBox chkResponseNavigationIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_navigation_is_ready_chk);
            CheckBox chkResponseRCGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_rc_get_capabilities_chk);
            CheckBox chkResponseRCIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_rc_is_ready_chk);
            CheckBox chkResponseTTSGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_capabilities_chk);
            CheckBox chkResponseTTSGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_language_chk);
            CheckBox chkResponseTTSGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_tts_get_supported_language_chk);
            CheckBox chkResponseTTSIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_tts_is_ready_chk);
            CheckBox chkResponseUIGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_capabilities_chk);
            CheckBox chkResponseUIGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_language_chk);
            CheckBox chkResponseUIGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_ui_get_supported_language_chk);
            CheckBox chkResponseUIIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_ui_is_ready_chk);
            CheckBox chkResponseVIGetVehicleData = (CheckBox)layout.FindViewById(Resource.Id.response_vi_get_vehicle_data_chk);
            CheckBox chkResponseVIGetVehicleType = (CheckBox)layout.FindViewById(Resource.Id.response_vi_get_vehicle_type_chk);
            CheckBox chkResponseVIIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_vi_is_ready_chk);
            CheckBox chkResponseVRGetCapabilites = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_capabilities_chk);
            CheckBox chkResponseVRGetLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_language_chk);
            CheckBox chkResponseVRGetSupportedLanguage = (CheckBox)layout.FindViewById(Resource.Id.response_vr_get_supported_language_chk);
            CheckBox chkResponseVRIsReady = (CheckBox)layout.FindViewById(Resource.Id.response_vr_is_ready_chk);
            CheckBox chkResponseBCGetSystemInfo = (CheckBox)layout.FindViewById(Resource.Id.response_bc_get_system_info_chk);


            Button btnResponseBCMixAudio = (Button)layout.FindViewById(Resource.Id.response_bc_mix_audio_support_button);
            Button btnResponseButtonsGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_button_get_capabilities_button);
            Button btnResponseNavigationIsReady = (Button)layout.FindViewById(Resource.Id.response_navigation_is_ready_button);
            Button btnResponseRCGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_rc_get_capabilities_button);
            Button btnResponseRCIsReady = (Button)layout.FindViewById(Resource.Id.response_rc_is_ready_button);
            Button btnResponseTTSGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_tts_get_capabilities_button);
            Button btnResponseTTSGetLanguage = (Button)layout.FindViewById(Resource.Id.response_tts_get_language_button);
            Button btnResponseTTSGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_tts_get_supported_language_button);
            Button btnResponseTTSIsReady = (Button)layout.FindViewById(Resource.Id.response_tts_is_ready_button);
            Button btnResponseUIGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_ui_get_capabilities_button);
            Button btnResponseUIGetLanguage = (Button)layout.FindViewById(Resource.Id.response_ui_get_language_button);
            Button btnResponseUIGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_ui_get_supported_language_button);
            Button btnResponseUIIsReady = (Button)layout.FindViewById(Resource.Id.response_ui_is_ready_button);
            Button btnResponseVIGetVehicleData = (Button)layout.FindViewById(Resource.Id.response_vi_get_vehicle_data_button);
            Button btnResponseVIGetVehicleType = (Button)layout.FindViewById(Resource.Id.response_vi_get_vehicle_type_button);
            Button btnResponseVIIsReady = (Button)layout.FindViewById(Resource.Id.response_vi_is_ready_button);
            Button btnResponseVRGetCapabilites = (Button)layout.FindViewById(Resource.Id.response_vr_get_capabilities_button);
            Button btnResponseVRGetLanguage = (Button)layout.FindViewById(Resource.Id.response_vr_get_language_button);
            Button btnResponseVRGetSupportedLanguage = (Button)layout.FindViewById(Resource.Id.response_vr_get_supported_language_button);
            Button btnResponseVRIsReady = (Button)layout.FindViewById(Resource.Id.response_vr_is_ready_button);
            Button btnResponseBCGetSystemInfo = (Button)layout.FindViewById(Resource.Id.response_bc_get_system_info_button);

            chkResponseBCMixAudio.CheckedChange += (sender, e) => btnResponseBCMixAudio.Enabled = e.IsChecked;
            chkResponseButtonsGetCapabilites.CheckedChange += (sender, e) => btnResponseButtonsGetCapabilites.Enabled = e.IsChecked;
            chkResponseNavigationIsReady.CheckedChange += (sender, e) => btnResponseNavigationIsReady.Enabled = e.IsChecked;
            chkResponseRCGetCapabilites.CheckedChange += (sender, e) => btnResponseRCGetCapabilites.Enabled = e.IsChecked;
            chkResponseRCIsReady.CheckedChange += (sender, e) => btnResponseRCIsReady.Enabled = e.IsChecked;
            chkResponseTTSGetCapabilites.CheckedChange += (sender, e) => btnResponseTTSGetCapabilites.Enabled = e.IsChecked;
            chkResponseTTSGetLanguage.CheckedChange += (sender, e) => btnResponseTTSGetLanguage.Enabled = e.IsChecked;
            chkResponseTTSGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseTTSGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseTTSIsReady.CheckedChange += (sender, e) => btnResponseTTSIsReady.Enabled = e.IsChecked;
            chkResponseUIGetCapabilites.CheckedChange += (sender, e) => btnResponseUIGetCapabilites.Enabled = e.IsChecked;
            chkResponseUIGetLanguage.CheckedChange += (sender, e) => btnResponseUIGetLanguage.Enabled = e.IsChecked;
            chkResponseUIGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseUIGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseUIIsReady.CheckedChange += (sender, e) => btnResponseUIIsReady.Enabled = e.IsChecked;
            chkResponseVIGetVehicleData.CheckedChange += (sender, e) => btnResponseVIGetVehicleData.Enabled = e.IsChecked;
            chkResponseVIGetVehicleType.CheckedChange += (sender, e) => btnResponseVIGetVehicleType.Enabled = e.IsChecked;
            chkResponseVIIsReady.CheckedChange += (sender, e) => btnResponseVIIsReady.Enabled = e.IsChecked;
            chkResponseVRGetCapabilites.CheckedChange += (sender, e) => btnResponseVRGetCapabilites.Enabled = e.IsChecked;
            chkResponseVRGetLanguage.CheckedChange += (sender, e) => btnResponseVRGetLanguage.Enabled = e.IsChecked;
            chkResponseVRGetSupportedLanguage.CheckedChange += (sender, e) => btnResponseVRGetSupportedLanguage.Enabled = e.IsChecked;
            chkResponseVRIsReady.CheckedChange += (sender, e) => btnResponseVRIsReady.Enabled = e.IsChecked;
            chkResponseBCGetSystemInfo.CheckedChange += (sender, e) => btnResponseBCGetSystemInfo.Enabled = e.IsChecked;

            chkResponseBCMixAudio.Checked = appSetting.getBCMixAudioSupport();
            chkResponseButtonsGetCapabilites.Checked = appSetting.getButtonGetCapabilities();
            chkResponseNavigationIsReady.Checked = appSetting.getNavigationIsReady();
            chkResponseRCGetCapabilites.Checked = appSetting.getRCGetCapabilities();
            chkResponseRCIsReady.Checked = appSetting.getRCIsReady();
            chkResponseTTSGetCapabilites.Checked = appSetting.getTTSGetCapabilities();
            chkResponseTTSGetLanguage.Checked = appSetting.getTTSGetLanguage();
            chkResponseTTSGetSupportedLanguage.Checked = appSetting.getTTSGetSupportedLanguage();
            chkResponseTTSIsReady.Checked = appSetting.getTTSIsReady();
            chkResponseUIGetCapabilites.Checked = appSetting.getUIGetCapabilities();
            chkResponseUIGetLanguage.Checked = appSetting.getUIGetLanguage();
            chkResponseUIGetSupportedLanguage.Checked = appSetting.getUIGetSupportedLanguage();
            chkResponseUIIsReady.Checked = appSetting.getUIIsReady();
            chkResponseVIGetVehicleData.Checked = appSetting.getVIGetVehicleData();
            chkResponseVIGetVehicleType.Checked = appSetting.getVIGetVehicleType();
            chkResponseVIIsReady.Checked = appSetting.getVIIsReady();
            chkResponseVRGetCapabilites.Checked = appSetting.getVRGetCapabilities();
            chkResponseVRGetLanguage.Checked = appSetting.getVRGetLanguage();
            chkResponseVRGetSupportedLanguage.Checked = appSetting.getVRGetSupportedLanguage();
            chkResponseVRIsReady.Checked = appSetting.getVRIsReady();
            chkResponseBCGetSystemInfo.Checked = appSetting.getBCGetSystemInfo();

            enableRemoteControl.Checked = appSetting.isRCEnabled();

            btnResponseBCMixAudio.Click += (object sender, EventArgs e) => CreateBCResponseMixingAudioSupported();
            btnResponseButtonsGetCapabilites.Click += (object sender, EventArgs e) => CreateButtonsGetCapabilities();
            btnResponseNavigationIsReady.Click += (object sender, EventArgs e) => CreateNavigationResponseIsReady();
            btnResponseRCGetCapabilites.Click += (object sender, EventArgs e) => CreateRCResponseGetCapabilities();
            btnResponseRCIsReady.Click += (object sender, EventArgs e) => CreateRCResponseIsReady();
            btnResponseTTSGetCapabilites.Click += (object sender, EventArgs e) => CreateTTSResponseGetCapabilities();
            btnResponseTTSGetLanguage.Click += (object sender, EventArgs e) => CreateTTSResponseGetLanguage();
            btnResponseTTSGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateTTSResponseGetSupportedLanguages();
            btnResponseTTSIsReady.Click += (object sender, EventArgs e) => CreateTTSResponseIsReady();
            btnResponseUIGetCapabilites.Click += (object sender, EventArgs e) => CreateUIResponseGetCapabilities();
            btnResponseUIGetLanguage.Click += (object sender, EventArgs e) => CreateUIResponseGetLanguage();
            btnResponseUIGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateUIResponseGetSupportedLanguages();
            btnResponseUIIsReady.Click += (object sender, EventArgs e) => CreateUIResponseIsReady();
            btnResponseVIGetVehicleData.Click += (object sender, EventArgs e) => CreateVIResponseGetVehicleData();
            btnResponseVIGetVehicleType.Click += (object sender, EventArgs e) => CreateVIResponseGetVehicleType();
            btnResponseVIIsReady.Click += (object sender, EventArgs e) => CreateVIResponseIsReady();
            btnResponseVRGetCapabilites.Click += (object sender, EventArgs e) => CreateVRResponseGetCapabilities();
            btnResponseVRGetLanguage.Click += (object sender, EventArgs e) => CreateVRResponseGetLanguage();
            btnResponseVRGetSupportedLanguage.Click += (object sender, EventArgs e) => CreateVRResponseGetSupportedLanguages();
            btnResponseVRIsReady.Click += (object sender, EventArgs e) => CreateVRResponseIsReady();
            btnResponseBCGetSystemInfo.Click += (object sender, EventArgs e) => CreateBCResponseGetSystemInfo();

            string serverAddress = appSetting.getServerAddress();
            string serverPort = appSetting.getServerPort();
            string videoStreamingPort = appSetting.getVideoStreamingPort();
            string audioStreamingPort = appSetting.getAudioStreamingPort();

            InitialConnectionCommandConfig initConfig = appSetting.getInitialConnectionCommandConfig();

            if (initConfig != null)
            {
                buttonCapabilitiesResponseParam = initConfig.getButtonsCapabilitiesResponseParam();
                List<InterfaceType> registerComponents = initConfig.getRegisterComponents();
                if (registerComponents != null)
                {
                    for (int i = 0; i < registerComponents.Count; i++)
                    {
                        switch (registerComponents[i])
                        {
                            case InterfaceType.UI:
                                registerUi.Checked = true;
                                break;
                            case InterfaceType.BasicCommunication:
                                registerBasicCommunication.Checked = true;
                                break;
                            case InterfaceType.Buttons:
                                registerButtons.Checked = true;
                                break;
                            case InterfaceType.VR:
                                registerVr.Checked = true;
                                break;
                            case InterfaceType.TTS:
                                registerTts.Checked = true;
                                break;
                            case InterfaceType.Navigation:
                                registerNavigation.Checked = true;
                                break;
                            case InterfaceType.VehicleInfo:
                                registerVehicleInfo.Checked = true;
                                break;
                            case InterfaceType.RC:
                                registerRc.Checked = true;
                                break;
                        }
                    }
                }

                buildBCOnReady.Checked = initConfig.getIsBcOnReady();
                registerSystemTime.Checked = initConfig.getIsBcOnSystemTimeReady();

                List<PropertyName> subscribeNotifications = initConfig.getSubscribeNotifications();
                if (subscribeNotifications != null)
                {
                    for (int j = 0; j < subscribeNotifications.Count; j++)
                    {
                        if (subscribeNotifications[j].propertyName.Equals(RegisterButtonSubscription))
                        {
                            subscribeButtonSubscription.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterOnAppRegister))
                        {
                            subscribeAppRegister.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterOnAppUnRegister))
                        {
                            subscribeAppUnregister.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterPutfile))
                        {
                            subscribePutfile.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterVideoStreaming))
                        {
                            subscribeVideoStreaming.Checked = true;
                        }
                        else if (subscribeNotifications[j].propertyName.Equals(RegisterAudioStreaming))
                        {
                            subscribeAudioStreaming.Checked = true;
                        }
                    }
                }
            }

            builder = new Android.Support.V7.App.AlertDialog.Builder(this);

            serverAddressEditText.Text = serverAddress;
            serverPortEditText.Text = serverPort;
            videoStreamingPortEditText.Text = videoStreamingPort;
            audioStreamingPortEditText.Text = audioStreamingPort;

            string htmlString = "<b><u>SharpHmi Settings</u></b>";
            TextView title = new TextView(this);
            title.Gravity = GravityFlags.Center;
            title.Text = Html.FromHtml(htmlString).ToString();
            title.TextSize = 25;

            builder.SetCustomTitle(title);
            builder.SetPositiveButton("OK", (senderAlert, args) =>
            {
                if (selectedMode == AppInstanceManager.SelectionMode.DEBUG_MODE)
                {
                    setConsoleFragment();
                }
                else if (selectedMode == AppInstanceManager.SelectionMode.BASIC_MODE)
                {
                    setMainFragment();
                }

                List<InterfaceType> selectedEnumList = new List<InterfaceType>();
                List<PropertyName> propertyNameList = new List<PropertyName>();
                if (registerUi.Checked)
                {
                    selectedEnumList.Add(InterfaceType.UI);
                }
                if (registerBasicCommunication.Checked)
                {
                    selectedEnumList.Add(InterfaceType.BasicCommunication);
                }
                if (registerButtons.Checked)
                {
                    selectedEnumList.Add(InterfaceType.Buttons);
                }
                if (registerVr.Checked)
                {
                    selectedEnumList.Add(InterfaceType.VR);
                }
                if (registerTts.Checked)
                {
                    selectedEnumList.Add(InterfaceType.TTS);
                }
                if (registerNavigation.Checked)
                {
                    selectedEnumList.Add(InterfaceType.Navigation);
                }
                if (registerVehicleInfo.Checked)
                {
                    selectedEnumList.Add(InterfaceType.VehicleInfo);
                }
                if (registerRc.Checked)
                {
                    selectedEnumList.Add(InterfaceType.RC);
                }
                PropertyName propertyName = new PropertyName();
                if (subscribeButtonSubscription.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.Buttons, FunctionType.OnButtonSubscription);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeAppRegister.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppRegistered);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeAppUnregister.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppUnregistered);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribePutfile.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnPutFile);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeVideoStreaming.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnVideoDataStreaming);
                    propertyNameList.Add(propertyName);
                }
                propertyName = new PropertyName();
                if (subscribeAudioStreaming.Checked)
                {
                    propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnAudioDataStreaming);
                    propertyNameList.Add(propertyName);
                }
                if (serverAddressEditText.Length() != 0)
                {
                    appSetting.setServerAddress(serverAddressEditText.Text);
                }

                if (serverPortEditText.Length() != 0)
                {
                    appSetting.setServerPort(serverPortEditText.Text);
                }

                if (videoStreamingPortEditText.Length() != 0)
                {
                    appSetting.setVideoStreamingPort(videoStreamingPortEditText.Text);
                }

                if (audioStreamingPortEditText.Length() != 0)
                {
                    appSetting.setAudioStreamingPort(audioStreamingPortEditText.Text);
                }

                Boolean prevRCEnabled = appSetting.isRCEnabled();

                appSetting.setRCEnabled(enableRemoteControl.Checked);

                InitialConnectionCommandConfig initialConnectionCommandConfig = new InitialConnectionCommandConfig();
                initialConnectionCommandConfig.setRegisterComponents(selectedEnumList);
                initialConnectionCommandConfig.setIsBcOnReady(buildBCOnReady.Checked);
                initialConnectionCommandConfig.setIsBcOnSystemTimeReady(registerSystemTime.Checked);
                initialConnectionCommandConfig.setSubscribeNotifications(propertyNameList);
                initialConnectionCommandConfig.setButtonsCapabilitiesResponseParam(buttonCapabilitiesResponseParam);

                appSetting.setInitialConnectionCommandConfig(initialConnectionCommandConfig);

                string tmpServerAddress = appSetting.getServerAddress();
                string tmpServerPort = appSetting.getServerPort();

                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);

                String objStr = Newtonsoft.Json.JsonConvert.SerializeObject(initialConnectionCommandConfig);
                try
                {
                    prefs.Edit().PutString(Const.PREFS_KEY_SERVER_ADDRESS, appSetting.getServerAddress())
                         .PutInt(Const.PREFS_KEY_SERVER_PORT, int.Parse(appSetting.getServerPort()))
                         .PutInt(Const.PREFS_KEY_VIDEO_STREAMING_PORT, int.Parse(appSetting.getVideoStreamingPort()))
                         .PutInt(Const.PREFS_KEY_AUDIO_STREAMING_PORT, int.Parse(appSetting.getAudioStreamingPort()))
                         .PutString(Const.PREFS_KEY_INITIAL_CONFIG, objStr)
                         .PutBoolean(Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED, chkResponseBCMixAudio.Checked)
                         .PutBoolean(Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES, chkResponseButtonsGetCapabilites.Checked)
                         .PutBoolean(Const.PREFS_KEY_NAVIGATION_IS_READY, chkResponseNavigationIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_RC_GET_CAPABILITIES, chkResponseRCGetCapabilites.Checked)
                         .PutBoolean(Const.PREFS_KEY_RC_IS_READY, chkResponseRCIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_TTS_GET_CAPABILITIES, chkResponseTTSGetCapabilites.Checked)
                         .PutBoolean(Const.PREFS_KEY_TTS_GET_LANGUAGE, chkResponseTTSGetLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE, chkResponseTTSGetSupportedLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_TTS_IS_READY, chkResponseTTSIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_UI_GET_CAPABILITIES, chkResponseUIGetCapabilites.Checked)
                         .PutBoolean(Const.PREFS_KEY_UI_GET_LANGUAGE, chkResponseUIGetLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE, chkResponseUIGetSupportedLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_UI_IS_READY, chkResponseUIIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_DATA, chkResponseVIGetVehicleData.Checked)
                         .PutBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_TYPE, chkResponseVIGetVehicleType.Checked)
                         .PutBoolean(Const.PREFS_KEY_VI_IS_READY, chkResponseVIIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_VR_GET_CAPABILITIES, chkResponseVRGetCapabilites.Checked)
                         .PutBoolean(Const.PREFS_KEY_VR_GET_LANGUAGE, chkResponseVRGetLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE, chkResponseVRGetSupportedLanguage.Checked)
                         .PutBoolean(Const.PREFS_KEY_VR_IS_READY, chkResponseVRIsReady.Checked)
                         .PutBoolean(Const.PREFS_KEY_BC_GET_SYSTEM_INFO, chkResponseBCGetSystemInfo.Checked)
                         .PutBoolean(Const.PREFS_KEY_IS_RC_ENABLED, enableRemoteControl.Checked)
                         .Commit();
                }

                catch (Exception ex)
                {

                }
                if ((serverAddress != tmpServerAddress) || (serverPort != tmpServerPort) || (AppInstanceManager.currentState != AppInstanceManager.ConnectionState.CONNECTED))
                {   try
                    {
                        new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                        {
                            AppInstanceManager.Instance.setupConnection(appSetting.getServerAddress(), int.Parse(appSetting.getServerPort()), initialConnectionCommandConfig);
                        })).Start();
                    }
                    catch (Exception ex){

                    }
                }
                else
                {
                    if(prevRCEnabled != appSetting.isRCEnabled()){
                        sendRcOnRemoteControlSettings();
                    } else {
                        Toast.MakeText(this, "Changes will be effective only with New Conection", ToastLength.Long).Show();                        
                    }
                }

                builder.Dispose();
            });

            builder.SetNegativeButton("Cancel", (senderAlert, args) =>
            {
                builder.Dispose();
            });

            builder.SetCancelable(true);
            builder.SetView(layout);
            dlg = builder.Create();
            dlg.Show();
        }

        public ConsoleFragment getConsoleFragment()
        {
            return (ConsoleFragment)FragmentManager.FindFragmentByTag(CONSOLE_FRAGMENT_TAG);
        }

        public VlcPlayer getVlcPlayerFragment()
        {
            return (VlcPlayer)FragmentManager.FindFragmentByTag(VLC_PLAYER_FRAGMENT_TAG);
        }

        public MainFragment getMainFragment()
        {
            return (MainFragment)this.FragmentManager.FindFragmentByTag(MAIN_FRAGMENT_TAG);
        }

        public FullHmiFragment getFullHMiFragment()
        {
            return (FullHmiFragment)this.FragmentManager.FindFragmentByTag(HMI_FULL_FRAGMENT_TAG);
        }

        public OptionsMenuFragment getOptionsMenuFragment()
        {
            return (OptionsMenuFragment)this.FragmentManager.FindFragmentByTag(HMI_OPTIONS_MENU_FRAGMENT_TAG);
        }

        public ChoiceListFragment getChoiceListFragment()
        {
            return (ChoiceListFragment)this.FragmentManager.FindFragmentByTag(PERFORM_INTERACTION_FRAGMENT_TAG);
        }

        public void onBcAppRegisteredNotificationCallback(Boolean isNewAppRegistered)
        {
            if (isNewAppRegistered)
            {
                if (getMainFragment() is MainFragmentCallback)
                {
                    getMainFragment().onRefreshCallback();
                }
            }
            else
            {
                if (getFullHMiFragment() != null && getFullHMiFragment().IsVisible)
                {
                    RunOnUiThread(() => RemoveFullFragment());
                }
                else
                {
                    if (getMainFragment() is MainFragmentCallback)
                    {
                        getMainFragment().onRefreshCallback();
                    }
                }
            }
        }

        public void setHmiFragment(int appId, int windowId)
        {
			AppInstanceManager.Instance.sendOnAppActivatedNotification(appId, windowId);

			FullHmiFragment hmiFragment = getFullHMiFragment();

			Android.App.FragmentManager fragmentManager = FragmentManager;

			//if (hmiFragment == null)
			//{
			hmiFragment = new FullHmiFragment();

			Bundle bundle = new Bundle();
			bundle.PutInt(FullHmiFragment.sClickedAppID, appId);
			hmiFragment.Arguments = bundle;

			Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
			fragmentTransaction.Replace(Resource.Id.frame_container, hmiFragment, HMI_FULL_FRAGMENT_TAG).AddToBackStack(null).Commit();
			fragmentManager.ExecutePendingTransactions();
			SetTitle(Resource.String.app_name);
			//}
		}

        public void setOptionsFragment(int appID)
        {
            OptionsMenuFragment optionsMenuFragment = getOptionsMenuFragment();

            Android.App.FragmentManager fragmentManager = FragmentManager;

            if (optionsMenuFragment == null)
            {
                optionsMenuFragment = new OptionsMenuFragment();

                Bundle bundle = new Bundle();
                bundle.PutInt(FullHmiFragment.sClickedAppID, appID);
                optionsMenuFragment.Arguments = bundle;

                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, optionsMenuFragment, HMI_OPTIONS_MENU_FRAGMENT_TAG).AddToBackStack(null).Commit();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        private void RemoveFullFragment()
        {
            Android.App.FragmentManager fragmentManager = FragmentManager;
            fragmentManager.PopBackStack();
        }

        public void refreshOptionsMenu()
        {
            if ((getOptionsMenuFragment() != null) && (getOptionsMenuFragment().IsVisible))
            {
                getOptionsMenuFragment().onRefreshOptionsMenu();
            }
        }

        public void setDownloadedAppIcon()
        {
            if ((getMainFragment() != null) && (getMainFragment().IsVisible))
            {
                getMainFragment().onRefreshCallback();
            }
        }

        public override void OnBackPressed()
        {
            if ((getOptionsMenuFragment() != null) && (getOptionsMenuFragment().IsVisible))
            {
                if (getOptionsMenuFragment().OnBackPressed())
                {
                    base.OnBackPressed();
                }
            } else if(getVlcPlayerFragment() != null && getVlcPlayerFragment().IsVisible){
                theInstance.userMovedAwayFromMediaProj = true;
                base.OnBackPressed();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public void onUiShowRequestCallback(Show msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiShowRequestCallback(msg);
            }
        }

        public void onUiAlertRequestCallback(Alert msg)
        {
            RunOnUiThread(() => UpdateAlertUI(msg));
        }

        private void UpdateAlertUI(Alert msg)
        {
            Android.Support.V7.App.AlertDialog.Builder alertBuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = LayoutInflater;
            View view = inflater.Inflate(Resource.Layout.custom_alert_dialog, null);
            alertBuilder.SetView(view);

            TextView alertText1 = (TextView)view.FindViewById(Resource.Id.alert_text_1);
            TextView alertText2 = (TextView)view.FindViewById(Resource.Id.alert_text_2);
            TextView alertText3 = (TextView)view.FindViewById(Resource.Id.alert_text_3);

            Button softButton1 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_1);
            Button softButton2 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_2);
            Button softButton3 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_3);
            Button softButton4 = (Button)view.FindViewById(Resource.Id.alert_soft_btn_4);

            if ((msg.getAlertStrings() != null) && (msg.getAlertStrings().Count > 0))
            {
                for (int i = 0; i < msg.getAlertStrings().Count; i++)
                {
                    switch (msg.getAlertStrings()[i].fieldName)
                    {
                        case TextFieldName.alertText1:
                            alertText1.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        case TextFieldName.alertText2:
                            alertText2.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        case TextFieldName.alertText3:
                            alertText3.Text = msg.getAlertStrings()[i].fieldText;
                            break;
                        default:
                            break;
                    }
                }
            }

            if ((msg.getSoftButtons() != null) && (msg.getSoftButtons().Count > 0))
            {
                for (int i = 0; i < msg.getSoftButtons().Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            softButton1.Text = msg.getSoftButtons()[i].getText();
                            softButton1.Visibility = ViewStates.Visible;
                            break;
                        case 1:
                            softButton2.Text = msg.getSoftButtons()[i].getText();
                            softButton2.Visibility = ViewStates.Visible;
                            break;
                        case 2:
                            softButton3.Text = msg.getSoftButtons()[i].getText();
                            softButton3.Visibility = ViewStates.Visible;
                            break;
                        case 3:
                            softButton4.Text = msg.getSoftButtons()[i].getText();
                            softButton4.Visibility = ViewStates.Visible;
                            break;
                        default:
                            break;
                    }
                }
            }

            alertBuilder.SetPositiveButton("ok", (senderAlert, args) =>
            {
                alertBuilder.Dispose();
            });
            Android.Support.V7.App.AlertDialog ad = alertBuilder.Create();
            ad.Show();

            int? totalDuration = (int)msg.getDuration();
            if (totalDuration != null)
            {
                mHandler = new Handler(Looper.MainLooper);
                action = delegate
                {
                    ad.Cancel();
                };
                if (null != mHandler)
                    mHandler.PostDelayed(action, (long)totalDuration);
            }
        }

        public void onUiScrollableMessageRequestCallback(ScrollableMessage msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiScrollableMessageRequestCallback(msg);
            }
        }

        public void onUiPerformInteractionRequestCallback(PerformInteraction msg)
        {
            if ((getChoiceListFragment() != null) && (getChoiceListFragment().IsVisible))
            {
                getChoiceListFragment().onUiPerformInteractionRequestCallback(msg);
            }
            else
            {
                ChoiceListFragment choiceListFrag = getChoiceListFragment();

                Android.App.FragmentManager fragmentManager = FragmentManager;

                choiceListFrag = new ChoiceListFragment(msg);

                Android.App.FragmentTransaction fragmentTransaction = fragmentManager.BeginTransaction();
                fragmentTransaction.Replace(Resource.Id.frame_container, choiceListFrag, PERFORM_INTERACTION_FRAGMENT_TAG).AddToBackStack(null).Commit();
                fragmentManager.ExecutePendingTransactions();
                SetTitle(Resource.String.app_name);
            }
        }

        public void onUiSetMediaClockTimerRequestCallback(SetMediaClockTimer msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiSetMediaClockTimerRequestCallback(msg);
            }
        }

        public void OnButtonSubscriptionNotificationCallback(HmiApiLib.Controllers.Buttons.IncomingNotifications.OnButtonSubscription msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().OnButtonSubscriptionNotificationCallback(msg);
            }
        }

        public void onTtsSpeakRequestCallback(Speak msg)
        {
            if ((msg.getTtsChunkList() != null) && (msg.getTtsChunkList().Count > 0))
            {
                foreach (TTSChunk item in msg.getTtsChunkList())
                {
                    if (item.type == SpeechCapabilities.TEXT)
                    {
                        speechList.Add(item.text);
                    }
                }
            }
            RunOnUiThread(() => HandleSpeakRequest());
        }

        private void HandleSpeakRequest()
        {
            textToSpeech = new TextToSpeech(this, this);
        }

        public void OnInit([GeneratedEnum] OperationResult status)
        {
            if (status != OperationResult.Error)
            {
                textToSpeech.SetLanguage(Java.Util.Locale.Us);
                for (int i = 0; i < speechList.Count; i++)
                {
                    textToSpeech.Speak(speechList[i], QueueMode.Add, null, null);
                }
                speechList.Clear();
            }
        }

        public void onUiSliderRequestCallback(Slider msg)
        {
            if ((getFullHMiFragment() != null) && (getFullHMiFragment().IsVisible))
            {
                getFullHMiFragment().onUiSliderRequestCallback(msg);
            }
        }

        public void CreateBCResponseMixingAudioSupported()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox checkBoxAttenuatedSupported = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAllow = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);
			checkBoxAttenuatedSupported.CheckedChange += (s, e) =>
			{
				switchAllow.Enabled = e.IsChecked;
			};

			rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            var adapter1 = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter1;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)BuildDefaults.buildDefaultMessage(type, 0);
            }
            
			if (tmpObj != null)
			{

				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getAttenuatedSupported() == null)
				{
					checkBoxAttenuatedSupported.Checked = false;
					switchAllow.Enabled = false;
				}
				else
				{
					switchAllow.Checked = (bool)tmpObj.getAttenuatedSupported();
				}
			}

			rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAttenuatedSupported.Text = "AttenuatedSupported";

            rsltCodeCheckBox.Text = "Result Code";

            rpcAlertDialog.SetTitle("BC.MixingAudioSupported");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				bool? attenuatedSupported = null; ;
				if (checkBoxAttenuatedSupported.Checked)
					attenuatedSupported = switchAllow.Checked;
				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationMixingAudioSupportedResponse(BuildRpc.getNextId(), attenuatedSupported, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter1.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateNavigationResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = (View)inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("Nav.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            
			if (tmpObj != null)
			{

				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getAvailable() == null)
				{
					checkBoxAvailable.Checked = false;
					switchAvailable.Enabled = false;
					
				}
				else
				{
					switchAvailable.Checked = (bool)tmpObj.getAvailable();
				}
			}
			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAvailable.CheckedChange += (s, e) => {
				switchAvailable.Enabled = e.IsChecked;
				};

			rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCode.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

				
				bool? toggleState = null; 

				if (checkBoxAvailable.Checked)
				{
					toggleState = switchAvailable.Checked;
				}
				RpcResponse rpcMessage=rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.Navigation, toggleState, selectedResultCode);

				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.tts_get_capabilities, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("TTS.GetCapabilities");

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.speech_capabilities_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.speech_capabilities_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            bool[] speechCapabilitiesBoolArray = new bool[speechCapabilities.Length];

            CheckBox speechCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.speech_capabilities_cb);
            Button speechCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.speech_capabilities_btn);
            ListView ListViewSpeechCapabilities = (ListView)rpcView.FindViewById(Resource.Id.speech_capabilities_lv);

            List<SpeechCapabilities> speechCapabilitiesList = new List<SpeechCapabilities>();
            var speechCapabilitiesListAdapter = new SpeechCapabilitiesAdapter(this, speechCapabilitiesList);
            ListViewSpeechCapabilities.Adapter = speechCapabilitiesListAdapter;

            speechCapabilitiesCheckBox.CheckedChange += (s, e) =>
            {
                ListViewSpeechCapabilities.Enabled = e.IsChecked;
                speechCapabilitiesButton.Enabled = e.IsChecked;
            };

            speechCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder speechCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                speechCapabilitiesAlertDialog.SetTitle("SpeechCapabilities");

                speechCapabilitiesAlertDialog.SetMultiChoiceItems(speechCapabilities, speechCapabilitiesBoolArray, (sender, e) => speechCapabilitiesBoolArray[e.Which] = e.IsChecked);
                speechCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    speechCapabilitiesAlertDialog.Dispose();
                });

                speechCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    speechCapabilitiesList.Clear();
                    for (int i = 0; i < speechCapabilities.Length; i++)
                    {
                        if (speechCapabilitiesBoolArray[i])
                        {
                            speechCapabilitiesList.Add(((SpeechCapabilities)typeof(SpeechCapabilities).GetEnumValues().GetValue(i)));
                        }
                    }
                    speechCapabilitiesListAdapter.NotifyDataSetChanged();
                });
                speechCapabilitiesAlertDialog.Show();
            };

            bool[] prerecordedSpeechBoolArray = new bool[prerecordedSpeech.Length];

            CheckBox prerecordedSpeechCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.prerecorded_speech_cb);
            Button prerecordedSpeechButton = (Button)rpcView.FindViewById(Resource.Id.prerecorded_speech_btn);
            ListView ListViewPrerecordedSpeech = (ListView)rpcView.FindViewById(Resource.Id.prerecorded_speech_lv);

            List<PrerecordedSpeech> prerecordedSpeechList = new List<PrerecordedSpeech>();
            var prerecordedSpeechListAdapter = new PrerecordedSpeechAdapter(this, prerecordedSpeechList);
            ListViewPrerecordedSpeech.Adapter = prerecordedSpeechListAdapter;


            prerecordedSpeechCheckBox.CheckedChange += (s, e) =>
            {
                ListViewPrerecordedSpeech.Enabled = e.IsChecked;
                prerecordedSpeechButton.Enabled = e.IsChecked;
            };

            prerecordedSpeechButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder prerecordedSpeechAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                prerecordedSpeechAlertDialog.SetTitle("PrerecordedSpeech");

                prerecordedSpeechAlertDialog.SetMultiChoiceItems(prerecordedSpeech, prerecordedSpeechBoolArray, (sender, e) => prerecordedSpeechBoolArray[e.Which] = e.IsChecked);
                prerecordedSpeechAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    prerecordedSpeechAlertDialog.Dispose();
                });

                prerecordedSpeechAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    prerecordedSpeechList.Clear();
                    for (int i = 0; i < prerecordedSpeech.Length; i++)
                    {
                        if (prerecordedSpeechBoolArray[i])
                        {
                            prerecordedSpeechList.Add(((PrerecordedSpeech)typeof(PrerecordedSpeech).GetEnumValues().GetValue(i)));
                        }
                    }

                    prerecordedSpeechListAdapter.NotifyDataSetChanged();
                });
                prerecordedSpeechAlertDialog.Show();
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities>(adapter.Context, tmpObj.getMethod());

            if (tmpObj == null) //lets build the default values to populate our UI interface
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (tmpObj != null)
            {

                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getSpeechCapabilities() != null)
				{
					speechCapabilitiesList.AddRange(tmpObj.getSpeechCapabilities());
					speechCapabilitiesListAdapter.NotifyDataSetChanged();
				}
				else
					speechCapabilitiesCheckBox.Checked = false;

				if (tmpObj.getPrerecordedSpeechCapabilities() != null)
                {
                    prerecordedSpeechList.AddRange(tmpObj.getPrerecordedSpeechCapabilities());
                    prerecordedSpeechListAdapter.NotifyDataSetChanged();
                }
				else
					prerecordedSpeechCheckBox.Checked = false;
			}

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<SpeechCapabilities> speechCapList = null;
                List<PrerecordedSpeech> prerecordedSpchList = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                if (speechCapabilitiesCheckBox.Checked)
                    speechCapList = speechCapabilitiesList;

                if (prerecordedSpeechCheckBox.Checked)
                    prerecordedSpchList = prerecordedSpeechList;

                RpcResponse rpcResponse = BuildRpc.buildTTSGetCapabilitiesResponse(BuildRpc.getNextId(), rsltCode, speechCapList, prerecordedSpchList);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("TTS.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            languageCheckBox.Text = "Language";

            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            languageCheckBox.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnLanguage.SetSelection((int)tmpObj.getLanguage());
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getLanguage()==null)
					languageCheckBox.Checked = false;


			}

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? lang = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildTtsGetLanguageResponse(BuildRpc.getNextId(), lang, rsltCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View getSystemInfoRpcView = inflater.Inflate(Resource.Layout.get_support_languages, null);
            rpcAlertDialog.SetView(getSystemInfoRpcView);
            rpcAlertDialog.SetTitle("TTS.GetSupportedLanguages");

            CheckBox ResultCodeCheckBox = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_result_code_cb);
            Spinner spnResultCode = (Spinner)getSystemInfoRpcView.FindViewById(Resource.Id.get_supported_language_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            ResultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            bool[] languagesBoolArray = new bool[languages.Length];

            CheckBox languagesCheckBox = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_language_cb);
            ListView languagesListView = (ListView)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview);
            Button languagesButton = (Button)getSystemInfoRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview_btn);

            languagesCheckBox.CheckedChange += (s, e) =>
            {
                languagesButton.Enabled = e.IsChecked;
                languagesListView.Enabled = e.IsChecked;
            };

            List<Language> languagesList = new List<Language>();
            var languagesAdapter = new LanguagesAdapter(this, languagesList);
            languagesListView.Adapter = languagesAdapter;

            languagesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder languagesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                languagesAlertDialog.SetTitle("Language");

                languagesAlertDialog.SetMultiChoiceItems(languages, languagesBoolArray, (sender, e) => languagesBoolArray[e.Which] = e.IsChecked);

                languagesAlertDialog.SetNegativeButton("Add", (senderAlert, args) =>
                {
                    languagesList.Clear();
                    for (int i = 0; i < languagesBoolArray.Length; i++)
                    {
                        if (languagesBoolArray[i])
                        {
                            languagesList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    languagesAdapter.NotifyDataSetChanged();
                });

                languagesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    languagesAlertDialog.Dispose();
                });

                languagesAlertDialog.Show();
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {

                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getLanguages() != null)
				{
					languagesList.AddRange(tmpObj.getLanguages());
					languagesAdapter.NotifyDataSetChanged();
				}
				else
					languagesCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<Language> langList = null;

                if (ResultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (languagesCheckBox.Checked)
                    langList = languagesList;

                RpcResponse rpcResponse = BuildRpc.buildTTSGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, langList);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateTTSResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("TTS.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAvailable.CheckedChange += (s, e) => {
				if (e.IsChecked == false)
					switchAvailable.Checked = false;
				switchAvailable.Enabled = e.IsChecked;
				
			};

			HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            
			if (tmpObj != null)
			{

				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			 if (tmpObj.getAvailable() == null)
				{
					checkBoxAvailable.Checked = false;
					switchAvailable.Enabled = false;
				}
				else
				{
					switchAvailable.Checked = (bool)tmpObj.getAvailable();
				}
			}
			rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = "Available";
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
				bool? toggleState = null;

				if (checkBoxAvailable.Checked)
				{
					toggleState = switchAvailable.Checked;
				}
				RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.TTS, toggleState, rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });
            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("UI.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            languageCheckBox.Text = "Language";

            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            languageCheckBox.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = !spnResultCode.Enabled;

            HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnLanguage.SetSelection((int)tmpObj.getLanguage());

                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getLanguage() == null)
					languageCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? lang = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildUiGetLanguageResponse(BuildRpc.getNextId(), lang, rsltCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });
            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View getSupportedLanguagesRpcView = inflater.Inflate(Resource.Layout.get_support_languages, null);
            rpcAlertDialog.SetView(getSupportedLanguagesRpcView);
            rpcAlertDialog.SetTitle("UI.GetSupportedLanguages");

            CheckBox ResultCodeCheckBox = (CheckBox)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_result_code_cb);
            Spinner spnResultCode = (Spinner)getSupportedLanguagesRpcView.FindViewById(Resource.Id.get_supported_language_result_code_spn);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            ResultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            bool[] languagesBoolArray = new bool[languages.Length];

            CheckBox languagesCheckBox = (CheckBox)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_language_cb);
            ListView languagesListView = (ListView)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview);
            Button languagesButton = (Button)getSupportedLanguagesRpcView.FindViewById(Resource.Id.tts_get_supported_language_listview_btn);

            languagesCheckBox.CheckedChange += (s, e) =>
            {
                languagesButton.Enabled = e.IsChecked;
                languagesListView.Enabled = e.IsChecked;
            };

            List<Language> languagesList = new List<Language>();
            var languagesAdapter = new LanguagesAdapter(this, languagesList);
            languagesListView.Adapter = languagesAdapter;

            languagesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder languagesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                languagesAlertDialog.SetTitle("Language");

                languagesAlertDialog.SetMultiChoiceItems(languages, languagesBoolArray, (sender, e) => languagesBoolArray[e.Which] = e.IsChecked);

                languagesAlertDialog.SetNegativeButton("Add", (senderAlert, args) =>
                {
                    languagesList.Clear();
                    for (int i = 0; i < languagesBoolArray.Length; i++)
                    {
                        if (languagesBoolArray[i])
                        {
                            languagesList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    languagesAdapter.NotifyDataSetChanged();
                });

                languagesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    languagesAlertDialog.Dispose();
                });
                languagesAlertDialog.Show();
            };

            HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {

                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getLanguages() != null)
				{
					languagesList.AddRange(tmpObj.getLanguages());
					languagesAdapter.NotifyDataSetChanged();
				}
				else
					languagesCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                List<Language> langList = null;

                if (ResultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (languagesCheckBox.Checked)
                    langList = languagesList;

                RpcResponse rpcResponse = BuildRpc.buildUiGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, langList);
                AppUtils.savePreferenceValueForRpc(this, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        public void CreateUIResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("UI.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAvailable.CheckedChange += (s, e) =>
			{
				switchAvailable.Enabled = e.IsChecked;
			};

			HmiApiLib.Controllers.UI.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.IsReady>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            
			if (tmpObj != null)
			{

				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getAvailable() == null)
				{
					checkBoxAvailable.Checked = false;
					switchAvailable.Enabled = false;
				}
				else
				{
					switchAvailable.Checked = (bool)tmpObj.getAvailable();
				}
			}
			rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

				bool? toggleState = null;
				if (checkBoxAvailable.Checked)
				{
					toggleState = switchAvailable.Checked;

				}
				RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.UI, toggleState, rsltCode);




				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseGetVehicleType()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_vehicle_type, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox vehicleTypeChk = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_chk);
            CheckBox make_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_make_cb);
            EditText make_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_make_et);

            CheckBox model_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_model_cb);
            EditText model_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_model_et);

            CheckBox model_year_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_model_year_cb);
            EditText model_year_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_model_year_et);

            CheckBox trim_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_trim_cb);
            EditText trim_edittext = (EditText)rpcView.FindViewById(Resource.Id.vehicle_type_trim_et);

            CheckBox result_code_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vehicle_type_result_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.vehicle_type_result_code_spinner);

            rpcAlertDialog.SetTitle("VI.GetVehicleType");
            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            make_checkbox.CheckedChange += (sender, e) => make_edittext.Enabled = e.IsChecked;
            model_checkbox.CheckedChange += (sender, e) => model_edittext.Enabled = e.IsChecked;
            model_year_checkbox.CheckedChange += (sender, e) => model_year_edittext.Enabled = e.IsChecked;
            trim_checkbox.CheckedChange += (sender, e) => trim_edittext.Enabled = e.IsChecked;
            vehicleTypeChk.CheckedChange += (sender, e) =>
            {
                make_checkbox.Enabled = e.IsChecked;
                model_checkbox.Enabled = e.IsChecked;
                model_year_checkbox.Enabled = e.IsChecked;
                trim_checkbox.Enabled = e.IsChecked;
                make_edittext.Enabled = e.IsChecked && make_checkbox.Checked;
                model_edittext.Enabled = e.IsChecked && model_checkbox.Checked;
                model_year_edittext.Enabled = e.IsChecked && model_year_checkbox.Checked;
                trim_edittext.Enabled = e.IsChecked && trim_checkbox.Checked;
            };
            result_code_chk.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                VehicleType vehType = tmpObj.getVehicleType();

                if (vehType != null)
                {
                    if (null != vehType.getMake())
                        make_edittext.Text = vehType.getMake();
                    else
                        make_checkbox.Checked = false;

                    if (null != vehType.getModel())
                        model_edittext.Text = vehType.getModel();
                    else
                        model_checkbox.Checked = false;

                    if (null != vehType.getModelYear())
                        model_year_edittext.Text = vehType.getModelYear();
                    else
                        model_year_checkbox.Checked = false;

                    if (null != vehType.getTrim())
                        trim_edittext.Text = vehType.getTrim();
                    else
                        trim_checkbox.Checked = false;
                }
                else
                    vehicleTypeChk.Checked = false;
            }

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                VehicleType vehType = null;
                if (vehicleTypeChk.Checked)
                {
                    vehType = new VehicleType();
                    if (make_checkbox.Checked) vehType.make = make_edittext.Text;
                    if (model_checkbox.Checked) vehType.model = model_edittext.Text;
                    if (model_year_checkbox.Checked) vehType.modelYear = model_year_edittext.Text;
                    if (trim_checkbox.Checked) vehType.trim = trim_edittext.Text;
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_chk.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetVehicleTypeResponse(BuildRpc.getNextId(), rsltCode, vehType);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("VI.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
            Switch tglAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

			var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            checkBoxAvailable.CheckedChange += (s, e) => tglAvailable.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (sender, e) => resultCodeSpn.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (tmpObj != null)
            {
                if (null != tmpObj.getAvailable())
                    tglAvailable.Checked = (bool)tmpObj.getAvailable();
                else
                    checkBoxAvailable.Checked = false;
                
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
            }

            checkBoxAvailable.Text = "Available";

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                bool? avail = null;
                if (checkBoxAvailable.Checked)
                    avail = tglAvailable.Checked;

                RpcResponse rpcMessage = null;
                rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.VehicleInfo, avail, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.on_touch_event_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetCapabilities");

            CheckBox resultCodeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            resultCodeCheck.Text = "Result Code";

            var resultCodeAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            string[] vrCapabilities = Enum.GetNames(typeof(VrCapabilities));
            bool[] vrCapabilitiesBoolArray = new bool[vrCapabilities.Length];

            ListView vrCapabilitiesListView = rpcView.FindViewById<ListView>(Resource.Id.touch_event_listview);
            List<VrCapabilities> vrCapabilitiesList = new List<VrCapabilities>();
            var vrCapabilitiesAdapter = new VrCapabilitiesAdapter(this, vrCapabilitiesList);
            vrCapabilitiesListView.Adapter = vrCapabilitiesAdapter;

            CheckBox vrCapabilityButtonChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

            resultCodeCheck.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
            vrCapabilityButtonChk.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;

            createTouchEvent.Text = "Add VR Capabilities";
            createTouchEvent.Click += (sender, e1) =>
            {
                Android.Support.V7.App.AlertDialog.Builder vrCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                vrCapabilitiesAlertDialog.SetTitle("VR Capabilities");

                vrCapabilitiesAlertDialog.SetMultiChoiceItems(vrCapabilities, vrCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => vrCapabilitiesBoolArray[e.Which] = e.IsChecked);

                vrCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    vrCapabilitiesAlertDialog.Dispose();
                });

                vrCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    for (int i = 0; i < vrCapabilities.Length; i++)
                    {
                        if (vrCapabilitiesBoolArray[i])
                        {
                            vrCapabilitiesList.Add(((VrCapabilities)typeof(VrCapabilities).GetEnumValues().GetValue(i)));
                        }
                    }
                    vrCapabilitiesAdapter.NotifyDataSetChanged();
                });

                vrCapabilitiesAlertDialog.Show();
            };

            HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                if (tmpObj.getVrCapabilities() != null)
                {
                    vrCapabilityButtonChk.Checked = true;
                    vrCapabilitiesList.AddRange(tmpObj.getVrCapabilities());
                    vrCapabilitiesAdapter.NotifyDataSetChanged();
                }
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (resultCodeCheck.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                List<VrCapabilities> list = null;
                if (vrCapabilityButtonChk.Checked)
                {
                    list = vrCapabilitiesList;
                }
                RpcMessage rpcMessage = BuildRpc.buildVrGetCapabilitiesResponse(BuildRpc.getNextId(), selectedResultCode, list);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetLanguage()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_language, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetLanguage");

            CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_language_cb);
            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.tts_language_spn);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.tts_result_code_spn);

            languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnLanguage.Adapter = languageAdapter;

            string[] result = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
            var resultAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, result);
            spnResultCode.Adapter = resultAdapter;

            HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                if (null != tmpObj.getLanguage())
                {
                    languageCheckBox.Checked = true;
                    spnLanguage.SetSelection((int)tmpObj.getLanguage());
                }
                else
                {
                    languageCheckBox.Checked = false;
                    spnLanguage.Enabled = false;
                }
                    
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                Language? selectedLanguage = null;
                if (languageCheckBox.Checked)
                {
                    selectedLanguage = (Language)spnLanguage.SelectedItemPosition;
                }
                HmiApiLib.Common.Enums.Result? rstldCode = null;

                if (rsltCodeCheckBox.Checked)
                {
                    rstldCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                RpcMessage rpcMessage = BuildRpc.buildVrGetLanguageResponse(BuildRpc.getNextId(), selectedLanguage, rstldCode);
                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseGetSupportedLanguages()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.on_touch_event_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VR.GetSupportedLanguages");

            CheckBox resultCodeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            resultCodeCheck.Text = "Result Code";

            resultCodeCheck.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            var resultCodeAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            ListView listView = rpcView.FindViewById<ListView>(Resource.Id.touch_event_listview);
            List<Language> languageList = new List<Language>();
            var adapter = new LanguagesAdapter(this, languageList);
            listView.Adapter = adapter;

            bool[] langBoolArray = new bool[languages.Length];

            CheckBox buttonCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

            buttonCheck.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;
            createTouchEvent.Text = "Add Languages";

            HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getLanguages() != null)
                {
                    buttonCheck.Checked = true;
                    languageList.AddRange(tmpObj.getLanguages());
                    adapter.NotifyDataSetChanged();
                }
            }

            createTouchEvent.Click += (sender, e1) =>
            {
                Android.Support.V7.App.AlertDialog.Builder languageAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                languageAlertDialog.SetTitle("Languages");

                languageAlertDialog.SetMultiChoiceItems(languages, langBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => langBoolArray[e.Which] = e.IsChecked);

                languageAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    languageAlertDialog.Dispose();
                });

                languageAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                {
                    for (int i = 0; i < languages.Length; i++)
                    {
                        if (langBoolArray[i])
                        {
                            languageList.Add(((Language)typeof(Language).GetEnumValues().GetValue(i)));
                        }
                    }
                    adapter.NotifyDataSetChanged();
                });
                languageAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheck.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                List<Language> language = null;
                if (buttonCheck.Checked)
                    language = languageList;

                RpcResponse rpcResponse = BuildRpc.buildVrGetSupportedLanguagesResponse(BuildRpc.getNextId(), rsltCode, language);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateBCResponseGetSystemInfo()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.get_system_info, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("BC.GetSystemInfo");

            CheckBox checkBoxCCPUVesrion = (CheckBox)rpcView.FindViewById(Resource.Id.ccpu_version_cb);
            EditText editTextCCPUVesrion = (EditText)rpcView.FindViewById(Resource.Id.ccpu_version);

            CheckBox checkBoxLanguage = (CheckBox)rpcView.FindViewById(Resource.Id.language_cb);
            Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.language);

            CheckBox checkBoxCountryCode = (CheckBox)rpcView.FindViewById(Resource.Id.wers_country_code_cb);
            EditText editTextCountryCode = (EditText)rpcView.FindViewById(Resource.Id.wersCountryCode);

            CheckBox checkBoxResultCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_getSystemInfo_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_code_getSystemInfo);

            checkBoxCCPUVesrion.CheckedChange += (s, e) => editTextCCPUVesrion.Enabled = e.IsChecked;
            checkBoxLanguage.CheckedChange += (s, e) => spnLanguage.Enabled = e.IsChecked;
            checkBoxCountryCode.CheckedChange += (s, e) => editTextCountryCode.Enabled = e.IsChecked;

            checkBoxResultCode.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            var languageAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            spnLanguage.Adapter = languageAdapter;

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)BuildDefaults.buildDefaultMessage(type, 0);
            }
			else
			{
				editTextCCPUVesrion.Text = tmpObj.getCcpuVersion();

				spnLanguage.SetSelection((int)tmpObj.getLanguage());
				editTextCountryCode.Text = tmpObj.getWersCountryCode();
				spnResultCode.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getCcpuVersion() == null)
					checkBoxCCPUVesrion.Checked = false;
				if (tmpObj.getLanguage() == null)
					checkBoxLanguage.Checked = false;
				if (tmpObj.getWersCountryCode() == null)
					checkBoxCountryCode.Checked = false;
			}

			rpcAlertDialog.SetNeutralButton("CANCEL", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            

            rpcAlertDialog.SetNegativeButton("TX_LATER", (senderAlert, args) =>
            {
                String countryCode = null;
                String ccpuVersion = null;

                if (checkBoxCCPUVesrion.Checked)
                {
                    ccpuVersion = editTextCCPUVesrion.Text;
                }

                if (checkBoxCountryCode.Checked)
                {
                    countryCode = editTextCountryCode.Text;
                }

                HmiApiLib.Common.Enums.Result? resltCode = null;
                if (checkBoxResultCode.Checked)
                {
                    resltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                Language? language = null;
                if (checkBoxLanguage.Checked)
                {
                    language = (Language)spnLanguage.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildBasicCommunicationGetSystemInfoResponse(BuildRpc.getNextId(), resltCode, ccpuVersion, language, countryCode);
                AppUtils.savePreferenceValueForRpc(spnLanguage.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("RESET", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(spnLanguage.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVRResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("VR.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
            Switch tglAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAvailable.CheckedChange += (s, e) =>
			{
				tglAvailable.Enabled = e.IsChecked;
			};
				
			

            HmiApiLib.Controllers.VR.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                if (null != tmpObj.getAvailable())
                {
                    checkBoxAvailable.Checked = true;
                    tglAvailable.Checked = (bool)tmpObj.getAvailable();
                }
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                bool? avail = null;

                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (checkBoxAvailable.Checked)
                    avail = tglAvailable.Checked;

                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.VR, avail, rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateUIResponseGetCapabilities(){
			grid = null;
			seatLocGrid = null;
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
			LayoutInflater layoutInflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
			View rpcView = layoutInflater.Inflate(Resource.Layout.ui_get_capabilities, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("UI.GetCapabilities");

            HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities) AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities);
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities) BuildDefaults.buildDefaultMessage(type, 0);
	}

	Button addDisplayCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilities_add_display_capabilities_btn);
	CheckBox displayCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.ui_get_capability_display_capability_chk);

	Button addAudioPassThruCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilitiess_add_audio_pass_thru_capabilities_btn);
	CheckBox audioPassThruCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.ui_get_capability_audio_pass_thru_capability_chk);

	Button addSystemCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.add_system_capabilities_btn);
	CheckBox systemCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.system_capabilities_chk);

	CheckBox hmiZoneCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_hmi_zone_capabilities_cb);
	Spinner hmiZoneCapabilitiesSpinner = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_hmi_zone_capabilities_spn);

	CheckBox addSoftButtonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_soft_button_capabilities_chk);
	Button addSoftButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.get_capabilities_add_soft_button_capabilities_btn);
	ListView softButtonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.get_capabilities_soft_button_capabilities_listview);

	
			CheckBox HmiCapabilitiesCheck = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_add_hmi_capabilities_chk);

			CheckBox HmiCapabilitiesNavigationCheck = (CheckBox)rpcView.FindViewById(Resource.Id.hmi_capabilities_navigation_checkbox);
			Switch HmiCapabilitiesNavigationTgl = (Switch)rpcView.FindViewById(Resource.Id.hmi_capabilities_navigation_tgl);

			CheckBox HmiCapabilitiesPhonecallCheck = (CheckBox)rpcView.FindViewById(Resource.Id.hmi_capabilities_phone_call_checkbox);
			Switch HmiCapabilitiesPhonecallTgl = (Switch)rpcView.FindViewById(Resource.Id.hmi_capabilities_phone_call_tgl);

			HmiCapabilitiesCheck.CheckedChange += (sender, e) =>
            {
                HmiCapabilitiesNavigationCheck.Enabled = e.IsChecked;
                HmiCapabilitiesPhonecallCheck.Enabled = e.IsChecked;
                HmiCapabilitiesNavigationTgl.Enabled = e.IsChecked && HmiCapabilitiesNavigationCheck.Checked;
                HmiCapabilitiesPhonecallTgl.Enabled = e.IsChecked && HmiCapabilitiesPhonecallCheck.Checked;
            };

            HmiCapabilitiesNavigationCheck.CheckedChange += (s, e) => HmiCapabilitiesNavigationTgl.Enabled = e.IsChecked;
            HmiCapabilitiesPhonecallCheck.CheckedChange += (s, e) => HmiCapabilitiesPhonecallTgl.Enabled = e.IsChecked;

            if (tmpObj != null )
            {
                if (tmpObj.getHMICapabilities() != null)
				{
					HmiCapabilitiesCheck.Checked = true;
					if (null != tmpObj.getHMICapabilities().getNavigation())
                        HmiCapabilitiesNavigationTgl.Checked = (bool) tmpObj.getHMICapabilities().getNavigation();
					else
						HmiCapabilitiesNavigationCheck.Checked = false;

					if (null != tmpObj.getHMICapabilities().getPhoneCall())
                        HmiCapabilitiesPhonecallTgl.Checked = (bool) tmpObj.getHMICapabilities().getPhoneCall();
					else
						HmiCapabilitiesPhonecallCheck.Checked = false;
				}
				else
					HmiCapabilitiesCheck.Checked = false;
			}

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_cb);
			Spinner rsltCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_spn);

			string[] hmiZoneCapabilities = Enum.GetNames(typeof(HmiZoneCapabilities));
			var hmiZoneCapabilitiesadapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, hmiZoneCapabilities);
			hmiZoneCapabilitiesSpinner.Adapter = hmiZoneCapabilitiesadapter;

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			rsltCodeSpinner.Adapter = resultCodeAdapter;
			 displayCapabilitiesCheck.CheckedChange += (s, e) => addDisplayCapabilitiesButton.Enabled = e.IsChecked;
            audioPassThruCapabilitiesCheck.CheckedChange += (s, e) => addAudioPassThruCapabilitiesButton.Enabled = e.IsChecked;
            systemCapabilitiesCheck.CheckedChange += (s, e) => addSystemCapabilitiesButton.Enabled = e.IsChecked;
            hmiZoneCapabilitiesCheckBox.CheckedChange += (s, e) => hmiZoneCapabilitiesSpinner.Enabled = e.IsChecked;
            rsltCodeCheckBox.CheckedChange += (s, e) => rsltCodeSpinner.Enabled = e.IsChecked;
            addSoftButtonCapabilitiesCheckBox.CheckedChange += (sender, e) =>

            {
                addSoftButtonCapabilitiesButton.Enabled = e.IsChecked;
                softButtonCapabilitiesListView.Enabled = e.IsChecked;
            };

            AudioPassThruCapabilities audioPassThruCap = null;
			DisplayCapabilities dspCap = null;
			SystemCapabilities systemCapabilities = null;

            if (tmpObj != null)
            {
                dspCap = tmpObj.getDisplayCapabilities();
                audioPassThruCap = tmpObj.getAudioPassThruCapabilities();
                systemCapabilities = tmpObj.getSystemCapabilities();
				if (tmpObj.getDisplayCapabilities() == null)
					displayCapabilitiesCheck.Checked= false;
				if (tmpObj.getAudioPassThruCapabilities() == null)
					audioPassThruCapabilitiesCheck.Checked = false;
                if (tmpObj.getSystemCapabilities() == null)
                    systemCapabilitiesCheck.Checked = false;
				rsltCodeSpinner.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getHmiZoneCapabilities() == null)
					hmiZoneCapabilitiesCheckBox.Checked = false;
				else
					hmiZoneCapabilitiesSpinner.SetSelection((int)tmpObj.getHmiZoneCapabilities());
			}

            if (audioPassThruCap == null)
            {
                audioPassThruCap = new AudioPassThruCapabilities();
            }
            if (dspCap == null)
            {
                dspCap = new DisplayCapabilities();
            }
            if (systemCapabilities == null)
            {
                systemCapabilities = new SystemCapabilities();
            }

            addDisplayCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder displayCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View displayCapabilitiesView = layoutInflater.Inflate(Resource.Layout.display_capabilities, null);
				displayCapabilitiesAlertDialog.SetView(displayCapabilitiesView);
                displayCapabilitiesAlertDialog.SetTitle("Display Capabilities");

                CheckBox checkBoxDisplayType = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_type_checkbox);
				Spinner spnButtonDisplayType = (Spinner)displayCapabilitiesView.FindViewById(Resource.Id.display_type_spinner);

				string[] displayType = Enum.GetNames(typeof(DisplayType));
				var displayTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, displayType);

				spnButtonDisplayType.Adapter = displayTypeAdapter;
				if (dspCap.getDisplayType() != null)
					spnButtonDisplayType.SetSelection((int) dspCap.getDisplayType());
				else
					checkBoxDisplayType.Checked = false;

				List<TextField> textFieldsList = null;
				ListView textFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_text_fields_listview);
				Button textFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_btn);

				CheckBox addTextFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_chk);
				
				var textFieldAdapter = new TextFieldAdapter(this, null, ViewStates.Visible, null);
				if (textFieldsList == null)
				{
					textFieldsList = new List<TextField>();
					textFieldsList.AddRange(BuildDefaults.buildDefaultTextField());
				}
				

                List<ImageField> imageFieldsList =null;
				ListView imageFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_fields_listview);

				Button imageFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_btn);
				CheckBox imageFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_chk);
				
				var imageFieldAdapter = new ImageFieldAdapter(this, null, ViewStates.Visible, null);
				if (imageFieldsList == null)
				{
					imageFieldsList = new List<ImageField>();
					imageFieldsList.AddRange(BuildDefaults.buildDefaultImageField());
				}
				
                List<MediaClockFormat> mediaClockFormatsList = new List<MediaClockFormat>();
				Button mediaClockFormatsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_btn);

				CheckBox mediaClockFormatsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_chk);
				if (dspCap.getMediaClockFormats() != null)
				{
					mediaClockFormatsList.AddRange(dspCap.getMediaClockFormats());
				}
				else
					mediaClockFormatsChk.Checked = false;
				
				var mediaClockFormatAdapter = new MediaClockFormatAdapter(this, mediaClockFormatsList);
				

                List<ImageType> imageTypeList = new List<ImageType>();
				CheckBox imageTypeCheckbox = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_chk);

				Button addImageTypeButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_btn);
				if (dspCap.getImageCapabilities() != null)
				{
					imageTypeList.AddRange(dspCap.getImageCapabilities());
				}
				else
					imageTypeCheckbox.Checked = false;

				var imageTypeAdapter = new ImageTypeAdapter(this, imageTypeList);
				

                CheckBox screenParamsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_chk);
				Button screenParamsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_btn);

				CheckBox checkBoxGraphicSupported = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_graphic_supported_checkbox);
                if (null != dspCap.getGraphicSupported())
                    checkBoxGraphicSupported.Checked = (bool) dspCap.getGraphicSupported();

				else
					checkBoxGraphicSupported.Checked = false;

				CheckBox checkBoxTemplatesAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_checkbox);
				EditText editTextTemplatesAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_edittext);
				if (dspCap.getTemplatesAvailable() != null)
				{
					String availableTemplates = null;
					for (int i = 0; i<dspCap.getTemplatesAvailable().Count; i++)
					{
						if (i == 0)
						{
							availableTemplates = dspCap.getTemplatesAvailable()[i];
						}
						else
						{
							availableTemplates = availableTemplates + "," + dspCap.getTemplatesAvailable()[i];
						}
					}
					editTextTemplatesAvailable.Text = availableTemplates;
				}
					else
					checkBoxTemplatesAvailable.Checked = false;


				checkBoxTemplatesAvailable.CheckedChange += (s, e) => editTextTemplatesAvailable.Enabled = e.IsChecked;
                screenParamsChk.CheckedChange += (s, e) => screenParamsButton.Enabled = e.IsChecked;
                checkBoxDisplayType.CheckedChange += (s, e) => spnButtonDisplayType.Enabled = e.IsChecked;
                addTextFieldsChk.CheckedChange += (s, e) =>
                {
                    textFieldsButton.Enabled = e.IsChecked;
                    textFieldsListView.Enabled = e.IsChecked;
                };
                imageFieldsChk.CheckedChange += (s, e) =>
                {
                    imageFieldsButton.Enabled = e.IsChecked;
                    imageFieldsListView.Enabled = e.IsChecked;
                };
                mediaClockFormatsChk.CheckedChange += (sender, e) =>
                {
                    mediaClockFormatsButton.Enabled = e.IsChecked;
                };

                textFieldsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder textFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View textFieldsView = layoutInflater.Inflate(Resource.Layout.text_field, null);
					textFieldsAlertDialog.SetView(textFieldsView);
                    textFieldsAlertDialog.SetTitle("TextField");
					ListView viewTextField = (ListView)textFieldsView.FindViewById(Resource.Id.text_fields_listview);
					Button textFieldsAdd = (Button)textFieldsView.FindViewById(Resource.Id.add);

					textFieldAdapter = new TextFieldAdapter(this, textFieldsList, ViewStates.Visible, viewTextField);
					viewTextField.Adapter = textFieldAdapter;
					Utility.setListViewHeightBasedOnChildren(viewTextField);

					CheckBox checkBoxName = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_name_checkbox);
					Spinner spnName = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_name_spinner);

					string[] textFieldNames = Enum.GetNames(typeof(TextFieldName));
					var textFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, textFieldNames);
					spnName.Adapter = textFieldNamesAdapter;

                    CheckBox checkBoxCharacterSet = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_checkbox);
					Spinner spnCharacterSet = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_spinner);

					string[] characterSet = Enum.GetNames(typeof(CharacterSet));
					var characterSetAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, characterSet);
					spnCharacterSet.Adapter = characterSetAdapter;

                    CheckBox checkBoxWidth = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_width_checkbox);
					EditText editTextWidth = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_width_edittext);

					CheckBox checkBoxRow = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_row_checkbox);
					EditText editTextRow = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_row_edittext);

					checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
										checkBoxCharacterSet.CheckedChange += (s, e) => spnCharacterSet.Enabled = e.IsChecked;
										checkBoxWidth.CheckedChange += (s, e) => editTextWidth.Enabled = e.IsChecked;
										checkBoxRow.CheckedChange += (s, e) => editTextRow.Enabled = e.IsChecked;
										TextField txtField = new TextField();
					textFieldsAdd.Click += delegate
					{


						if (checkBoxName.Checked)
						{
							txtField.name = (TextFieldName)spnName.SelectedItemPosition;
						}
						if (checkBoxCharacterSet.Checked)
						{
							txtField.characterSet = (CharacterSet)spnCharacterSet.SelectedItemPosition;
						}
						if (checkBoxWidth.Checked)
						{
							if (editTextWidth.Text != null && editTextWidth.Text.Length > 0)
								txtField.width = Java.Lang.Integer.ParseInt(editTextWidth.Text);
						}
						if (checkBoxRow.Checked)
						{
							if (editTextRow.Text != null && editTextRow.Text.Length > 0)
								txtField.rows = Java.Lang.Integer.ParseInt(editTextRow.Text);
						}
						textFieldsList.Add(txtField);
						textFieldAdapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(viewTextField);
					};
						textFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
					{


                    });
					
					
                    textFieldsAlertDialog.Show();
                };

                imageFieldsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder imageFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View imageFieldsView = layoutInflater.Inflate(Resource.Layout.image_field, null);
					imageFieldsAlertDialog.SetView(imageFieldsView);
                    imageFieldsAlertDialog.SetTitle("Image Field");

                    CheckBox checkBoxName = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_name_checkbox);
					Spinner spnName = (Spinner)imageFieldsView.FindViewById(Resource.Id.image_field_name_spinner);
					ListView viewImageField = (ListView)imageFieldsView.FindViewById(Resource.Id.image_fields_listview);
					Button imageFieldsAdd = (Button)imageFieldsView.FindViewById(Resource.Id.add);
					

					imageFieldAdapter = new ImageFieldAdapter(this, imageFieldsList, ViewStates.Visible, viewImageField);
					viewImageField.Adapter = imageFieldAdapter;
					Utility.setListViewHeightBasedOnChildren(viewImageField);

					string[] imageFieldNames = Enum.GetNames(typeof(ImageFieldName));
					var imageFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageFieldNames);
					spnName.Adapter = imageFieldNamesAdapter;

                    List<FileType> fileTypeList = new List<FileType>();
					ListView fileTypeListView = (ListView)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_listview);
					var fileTypeAdapter = new FileTypeAdapter(this, fileTypeList);
					fileTypeListView.Adapter = fileTypeAdapter;

                    string[] fileTypes = Enum.GetNames(typeof(FileType));
					bool[] fileTypeBoolArray = new bool[fileTypes.Length];

					CheckBox fileTypeChk = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_chk);
					Button fileTypeButton = (Button)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_btn);

					checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                    fileTypeChk.CheckedChange += (sender, e) => fileTypeButton.Enabled = e.IsChecked;

                    fileTypeButton.Click += (sender, e1) =>
                    {
                        Android.Support.V7.App.AlertDialog.Builder fileTypeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
						fileTypeAlertDialog.SetTitle("FileType");

                        fileTypeAlertDialog.SetMultiChoiceItems(fileTypes, fileTypeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => fileTypeBoolArray[e.Which] = e.IsChecked);

                        fileTypeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                        {
                            fileTypeAlertDialog.Dispose();
                        });

                        fileTypeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                        {
							fileTypeList.Clear();

							for (int i = 0; i<fileTypes.Length; i++)
                            {
                                if (fileTypeBoolArray[i])
                                {
                                    fileTypeList.Add(((FileType)typeof(FileType).GetEnumValues().GetValue(i)));
                                }
                            }
                            fileTypeAdapter.NotifyDataSetChanged();
                            Utility.setListViewHeightBasedOnChildren(fileTypeListView);
                        });
                        fileTypeAlertDialog.Show();
                    };

                    CheckBox checkBoxImageResolution = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_resolution_checkbox);

					CheckBox checkBoxResolutionWidth = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_checkbox);
					EditText editTextResolutionWidth = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_edit_text);

					CheckBox checkBoxResolutionHeight = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_checkbox);
					EditText editTextResolutionHeight = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_edit_text);

				checkBoxImageResolution.CheckedChange += (sender, e) =>
                    {
                        checkBoxResolutionWidth.Enabled = e.IsChecked;
                        editTextResolutionWidth.Enabled = e.IsChecked;
                        checkBoxResolutionHeight.Enabled = e.IsChecked;
                        editTextResolutionHeight.Enabled = e.IsChecked;
                    };
                    checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                    checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;
					ImageField imgField = new ImageField();
					imageFieldsAdd.Click += delegate
					{
						if (checkBoxName.Checked)
						{
							imgField.name = (ImageFieldName)spnName.SelectedItemPosition;
						}
						if (fileTypeChk.Checked)
						{
							imgField.imageTypeSupported = fileTypeList;
						}

						ImageResolution resolution = null;
						if (checkBoxImageResolution.Checked)
						{
							resolution = new ImageResolution();

							if (checkBoxResolutionWidth.Checked)
							{
								if (editTextResolutionWidth.Text.Equals(""))
									resolution.resolutionWidth = 0;
								else
									resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
							}
							if (checkBoxResolutionHeight.Checked)
							{
								if (editTextResolutionHeight.Text.Equals(""))
									resolution.resolutionHeight = 0;
								else
									resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
							}
						}
						imgField.imageResolution = resolution;

						imageFieldsList.Add(imgField);
						imageFieldAdapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(viewImageField);
					};
						imageFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {


                       
                    });
					
					

					imageFieldsAlertDialog.Show();
                };

                string[] mediaClockFormats = Enum.GetNames(typeof(MediaClockFormat));
				bool[] mediaClockFormatsBoolArray = new bool[mediaClockFormats.Length];
				for (int i = 0; i < mediaClockFormats.Length; i++)
				{
					mediaClockFormatsBoolArray[i] = true;
				}
				mediaClockFormatsButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder mediaClockFormatsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
					mediaClockFormatsAlertDialog.SetTitle("MediaClockFormats");

                    mediaClockFormatsAlertDialog.SetMultiChoiceItems(mediaClockFormats, mediaClockFormatsBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => mediaClockFormatsBoolArray[e.Which] = e.IsChecked);

                  
					
					mediaClockFormatsAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
						mediaClockFormatsList.Clear();
						for (int i = 0; i<mediaClockFormats.Length; i++)
                        {
                            if (mediaClockFormatsBoolArray[i])
                            {
                                mediaClockFormatsList.Add(((MediaClockFormat)typeof(MediaClockFormat).GetEnumValues().GetValue(i)));
                            }
                        }
                       
                    });

                    mediaClockFormatsAlertDialog.Show();
                };

                string[] imageCapabilities = Enum.GetNames(typeof(ImageType));
				bool[] imageCapabilitiesBoolArray = new bool[imageCapabilities.Length];
				for (int i = 0; i < imageCapabilities.Length; i++)
				{
					imageCapabilitiesBoolArray[i] = true;
				}
				addImageTypeButton.Click += (sender, e1) =>
                {
                    Android.Support.V7.App.AlertDialog.Builder imageCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
					imageCapabilitiesAlertDialog.SetTitle("Image Capabilities");

                    imageCapabilitiesAlertDialog.SetMultiChoiceItems(imageCapabilities, imageCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => imageCapabilitiesBoolArray[e.Which] = e.IsChecked);

                    

                    imageCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
						imageTypeList.Clear();

						for (int i = 0; i<imageCapabilities.Length; i++)
                        {
                            if (imageCapabilitiesBoolArray[i])
                            {
                                imageTypeList.Add(((ImageType)typeof(ImageType).GetEnumValues().GetValue(i)));
                            }
                        }
                       
                    });

                    imageCapabilitiesAlertDialog.Show();
                };

                ScreenParams scrnParam = dspCap.getScreenParams();
                if (null == scrnParam)
                {
                    scrnParam = new ScreenParams();
                }
                screenParamsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder screenParamsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View screenParamsView = layoutInflater.Inflate(Resource.Layout.screen_param, null);
					screenParamsAlertDialog.SetView(screenParamsView);
                    screenParamsAlertDialog.SetTitle("Screen Params");

                    CheckBox checkBoxImageResolution = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_image_resolution_checkbox);
					CheckBox checkBoxResolutionWidth = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_checkbox);
					EditText editTextResolutionWidth = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_edit_text);

					CheckBox checkBoxResolutionHeight = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_checkbox);
					EditText editTextResolutionHeight = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_edit_text);

					CheckBox checkTouchEventCapabilities = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_touch_event_capability_checkbox);
					CheckBox checkBoxpressAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_press_available_checkbox);
					CheckBox checkBoxMultiTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_multi_touch_available_checkbox);
					CheckBox checkBoxDoubleTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_double_press_available_checkbox);

					checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                    checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                    checkBoxImageResolution.CheckedChange += (sender, e) =>
                    {
                        checkBoxResolutionWidth.Enabled = e.IsChecked;
                        editTextResolutionWidth.Enabled = e.IsChecked;
                        checkBoxResolutionHeight.Enabled = e.IsChecked;
                        editTextResolutionHeight.Enabled = e.IsChecked;
                    };

                    checkTouchEventCapabilities.CheckedChange += (sender, e) =>

                    {
                        checkBoxpressAvailable.Enabled = e.IsChecked;
                        checkBoxMultiTouchAvailable.Enabled = e.IsChecked;
                        checkBoxDoubleTouchAvailable.Enabled = e.IsChecked;
                    };

					if (scrnParam.getResolution() != null)
					{
						checkBoxImageResolution.Checked = true;
						editTextResolutionWidth.Text = scrnParam.getResolution().getResolutionWidth().ToString();
						editTextResolutionHeight.Text = scrnParam.getResolution().getResolutionHeight().ToString();
					}
					else
						checkBoxImageResolution.Checked = false;

					if (scrnParam.getTouchEventAvailable() != null)
                    {
                        checkTouchEventCapabilities.Checked = true;
						if (null != scrnParam.getTouchEventAvailable().getPressAvailable())
							checkBoxpressAvailable.Checked = (bool) scrnParam.getTouchEventAvailable().getPressAvailable();
						else
							checkBoxpressAvailable.Checked = false;

						if (null != scrnParam.getTouchEventAvailable().getMultiTouchAvailable())
							checkBoxMultiTouchAvailable.Checked = (bool) scrnParam.getTouchEventAvailable().getMultiTouchAvailable();
						else
							checkBoxMultiTouchAvailable.Checked = false;

						if (null != scrnParam.getTouchEventAvailable().getDoublePressAvailable())
							checkBoxDoubleTouchAvailable.Checked = (bool) scrnParam.getTouchEventAvailable().getDoublePressAvailable();
						else
							checkBoxDoubleTouchAvailable.Checked = false;

					}
					else
						checkTouchEventCapabilities.Checked= false;

					screenParamsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {
                        ImageResolution imgResolution = null;
                        if (checkBoxImageResolution.Checked)
                        {
                            imgResolution = new ImageResolution();
                            if (checkBoxResolutionWidth.Checked)
                            {
                                if (editTextResolutionWidth.Text != null && editTextResolutionWidth.Text.Length > 0)
                                    imgResolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                            }
                            if (checkBoxResolutionHeight.Checked)
                            {
                                if (editTextResolutionHeight.Text != null && editTextResolutionHeight.Text.Length > 0)
                                    imgResolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                            }
                        }
                        scrnParam.resolution = imgResolution;

                        TouchEventCapabilities touchEventCapabilities = null;
                        if (checkTouchEventCapabilities.Checked)
                        {
                            touchEventCapabilities = new TouchEventCapabilities();

							touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                            touchEventCapabilities.multiTouchAvailable = checkBoxMultiTouchAvailable.Checked;
                            touchEventCapabilities.doublePressAvailable = checkBoxDoubleTouchAvailable.Checked;
                        }
                        scrnParam.touchEventAvailable = touchEventCapabilities;
                    });

                    screenParamsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        screenParamsAlertDialog.Dispose();
                    });
                    screenParamsAlertDialog.Show();
                };

                CheckBox checkBoxNumCustomPresetsAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_checkbox);
				EditText editTextNumCustomPresetsAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_edittext);

				checkBoxNumCustomPresetsAvailable.CheckedChange += (s, e) => editTextNumCustomPresetsAvailable.Enabled = e.IsChecked;

                editTextNumCustomPresetsAvailable.Text = dspCap.getNumCustomPresetsAvailable().ToString();

				if (dspCap.getNumCustomPresetsAvailable() == null)
					checkBoxNumCustomPresetsAvailable.Checked = false;


				displayCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    dspCap = new DisplayCapabilities();
                    if (checkBoxDisplayType.Checked)
                    {
                        dspCap.displayType = (DisplayType) spnButtonDisplayType.SelectedItemPosition;
                    }

                    if (addTextFieldsChk.Checked)
                    {
                        dspCap.textFields = textFieldsList;
                    }

                    if (imageFieldsChk.Checked)
                    {
                        dspCap.imageFields = imageFieldsList;
                    }

                    if (mediaClockFormatsChk.Checked)
                    {
                        dspCap.mediaClockFormats = mediaClockFormatsList;
                    }

                    if (imageTypeCheckbox.Checked)
                    {
                        dspCap.imageCapabilities = imageTypeList;
                    }

                    dspCap.graphicSupported = checkBoxGraphicSupported.Checked;
                    if (checkBoxTemplatesAvailable.Checked)
                    {
                        List<String> templatesAvailable = new List<string>();
						templatesAvailable.AddRange(editTextTemplatesAvailable.Text.Split(','));

                        dspCap.templatesAvailable = templatesAvailable;
                    }

                    if (screenParamsChk.Checked)
                    {
                        dspCap.screenParams = scrnParam;
                    }

                    if (checkBoxNumCustomPresetsAvailable.Checked)
                    {
                        if (editTextNumCustomPresetsAvailable.Text != null && editTextNumCustomPresetsAvailable.Text.Length > 0)
                        {
                            try
                            {
                                dspCap.numCustomPresetsAvailable = Java.Lang.Integer.ParseInt(editTextNumCustomPresetsAvailable.Text);
                            }
                            catch (Exception e) { }
                        }
                    }
                });

                displayCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    displayCapabilitiesAlertDialog.Dispose();
                });
                displayCapabilitiesAlertDialog.Show();
            };

            addAudioPassThruCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder audioPassThruCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View audioPassThruCapabilitiesView = layoutInflater.Inflate(Resource.Layout.audio_pass_thru_capabilities, null);
				audioPassThruCapabilitiesAlertDialog.SetView(audioPassThruCapabilitiesView);
                audioPassThruCapabilitiesAlertDialog.SetTitle("AudioPassThruCapabilities");

                CheckBox samplingRateCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_cap_samlping_rate_cb);
				Spinner samplingRateSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_cap_samlping_rate_spn);

				string[] samplingRate = Enum.GetNames(typeof(SamplingRate));
				var samplingRateAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, samplingRate);
				samplingRateSpn.Adapter = samplingRateAdapter;

                CheckBox bitsPerSampleCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_bits_per_sample_cb);
				Spinner bitsPerSampleSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_bits_per_sample_spn);

				string[] bitsPerSample = Enum.GetNames(typeof(BitsPerSample));
				var bitsPerSampleAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, bitsPerSample);
				bitsPerSampleSpn.Adapter = bitsPerSampleAdapter;

                CheckBox audioTypeCheckBox = (CheckBox)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_audio_type_cb);
				Spinner audioTypeSpn = (Spinner)audioPassThruCapabilitiesView.FindViewById(Resource.Id.audio_pass_thru_audio_type_spn);

				string[] audioType = Enum.GetNames(typeof(AudioType));
				var audioTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, audioType);
				audioTypeSpn.Adapter = audioTypeAdapter;

                samplingRateCheckBox.CheckedChange += (s, e) => samplingRateSpn.Enabled = e.IsChecked;
                bitsPerSampleCheckBox.CheckedChange += (s, e) => bitsPerSampleSpn.Enabled = e.IsChecked;

                audioTypeCheckBox.CheckedChange += (s, e) => audioTypeSpn.Enabled = e.IsChecked;
				if(audioPassThruCap.getSamplingRate()!=null)
				samplingRateSpn.SetSelection((int) audioPassThruCap.getSamplingRate());
				if (audioPassThruCap.getBitsPerSample() != null)
				bitsPerSampleSpn.SetSelection((int) audioPassThruCap.getBitsPerSample());
				if (audioPassThruCap.getAudioType() != null)
					audioTypeSpn.SetSelection((int) audioPassThruCap.getAudioType());

                audioPassThruCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    if (samplingRateCheckBox.Checked)
						audioPassThruCap.samplingRate = (SamplingRate) samplingRateSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.samplingRate = SamplingRate.RATE_8KHZ;

                    if (bitsPerSampleCheckBox.Checked)
						audioPassThruCap.bitsPerSample = (BitsPerSample) bitsPerSampleSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.bitsPerSample = BitsPerSample.RATE_8_BIT;

                    if (audioTypeCheckBox.Checked)
						audioPassThruCap.audioType = (AudioType) audioTypeSpn.SelectedItemPosition;
                    else
                        audioPassThruCap.audioType = AudioType.PCM;
                });

                audioPassThruCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    audioPassThruCapabilitiesAlertDialog.Dispose();
                });
				if (audioPassThruCap != null)
				{
					if (audioPassThruCap.getSamplingRate() == null)
						samplingRateCheckBox.Checked = false;
					if (audioPassThruCap.getBitsPerSample() == null)
						bitsPerSampleCheckBox.Checked = false;
					if (audioPassThruCap.getAudioType() == null)
						audioTypeCheckBox.Checked = false;
					

				}
				audioPassThruCapabilitiesAlertDialog.Show();
            };



            addSystemCapabilitiesButton.Click += delegate
            {

				Android.Support.V7.App.AlertDialog.Builder systemCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View systemCapabilitiesView = layoutInflater.Inflate(Resource.Layout.system_capabilities, null);
				systemCapabilitiesAlertDialog.SetView(systemCapabilitiesView);
                systemCapabilitiesAlertDialog.SetTitle("System Capabilities");

                CheckBox checkBoxNavigationCapability = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.navigation_capability_checkbox);
				CheckBox checkBoxSendLocationEnabled = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.send_location_enabled_checkbox);
				Switch switchsendLocationEnabled = (Switch)systemCapabilitiesView.FindViewById(Resource.Id.send_location_enabled_switch);

                

                CheckBox checkBoxGetWayPointsEnabled = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.get_way_points_enabled_checkbox);
				Switch switchGetWayPointsEnabled = (Switch)systemCapabilitiesView.FindViewById(Resource.Id.get_way_points_enabled_switch);
               

                checkBoxNavigationCapability.CheckedChange += (s, e) =>
                {
                    checkBoxSendLocationEnabled.Enabled = e.IsChecked;
                    switchsendLocationEnabled.Enabled = e.IsChecked;

                    checkBoxGetWayPointsEnabled.Enabled = e.IsChecked;
                    switchGetWayPointsEnabled.Enabled = e.IsChecked;
                };
                checkBoxSendLocationEnabled.CheckedChange += (s, e) =>
                {
                    switchsendLocationEnabled.Enabled = e.IsChecked;
                };
                checkBoxGetWayPointsEnabled.CheckedChange += (s, e) =>
                {
                    switchGetWayPointsEnabled.Enabled = e.IsChecked;
                };


				CheckBox checkBoxPhoneCapability = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.phone_capability_checkbox);
				CheckBox checkBoxDialNumberEnabled = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.dial_number_enabled_checkbox);
				Switch switchDialNumberEnabled = (Switch)systemCapabilitiesView.FindViewById(Resource.Id.dial_number_enabled_switch);

				

                checkBoxPhoneCapability.CheckedChange += (s, e) =>
                {
                    checkBoxDialNumberEnabled.Enabled = e.IsChecked;
                    switchDialNumberEnabled.Enabled = e.IsChecked;
                };
                checkBoxDialNumberEnabled.CheckedChange += (s, e) =>
                {
                    switchDialNumberEnabled.Enabled = e.IsChecked;
                };

                CheckBox checkBoxVideoStreamingCapability = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.video_streaming_capability_checkbox);
				CheckBox checkBoxImageResolution = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.image_resolution_checkbox);
				CheckBox checkBoxResolutionWidth = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.resolution_width_checkbox);
				EditText editTextResolutionWidth = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.resolution_width_edit_text);
                
				CheckBox checkBoxResolutionHeight = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.resolution_height_checkbox);
				EditText editTextResolutionHeight = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.resolution_height_edit_text);
               
				CheckBox checkBoxMaxBitrate = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.max_bitrate_checkbox);
				EditText editTextMaxBitrate = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.max_bitrate_edit_text);

				CheckBox seatLocationCapability = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.seat_location_capability);
				CheckBox rows = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.rows);
				EditText rowsEditText = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.rows_edit_text);
				CheckBox col = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.col);
				EditText colEditText = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.col_edit_text);
				CheckBox level = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.level);
				EditText levelEditText = (EditText)systemCapabilitiesView.FindViewById(Resource.Id.level_edit_text);
				CheckBox seatChk = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.seat_chk);
				CheckBox gridChk = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.grid_chk);
				ListView seatLocationListview = (ListView)systemCapabilitiesView.FindViewById(Resource.Id.seat_location_listview);

				Button gridBtn = (Button)systemCapabilitiesView.FindViewById(Resource.Id.grid_btn);

				CheckBox checkBoxHapticSpatialDataSupported = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.haptic_spatial_data_supported_checkbox);
				Switch switchHapticSpatialDataSupported = (Switch)systemCapabilitiesView.FindViewById(Resource.Id.haptic_spatial_data_supported_switch); 
                

                CheckBox addSupportedFormatsCheckBox = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.supported_formats_chk);
				Button addSupportedFormatsButton = (Button)systemCapabilitiesView.FindViewById(Resource.Id.supported_formats_btn);
				ListView supportedFormatsListView = (ListView)systemCapabilitiesView.FindViewById(Resource.Id.supported_formats_listview);

				CheckBox addDisplayCapCheckBox = (CheckBox)systemCapabilitiesView.FindViewById(Resource.Id.display_capabilites_chk);
				Button addDisplayCapButton = (Button)systemCapabilitiesView.FindViewById(Resource.Id.display_capabilites_btn);

				if (systemCapabilities != null)
				{
					if (systemCapabilities.getSeatCapability() != null)
					{
						if (systemCapabilities.getSeatCapability().getlevels() != null)
						{
							levelEditText.Text = systemCapabilities.getSeatCapability().getlevels().ToString();
						}
						if (systemCapabilities.getSeatCapability().getcolumns() != null)
						{
							colEditText.Text = systemCapabilities.getSeatCapability().getcolumns().ToString();
						}
						if (systemCapabilities.getSeatCapability().getrows() != null)
						{
							rowsEditText.Text = systemCapabilities.getSeatCapability().getrows().ToString();
						}
					}if (systemCapabilities.getNavigationCapability() != null)
					{
						if (systemCapabilities.getNavigationCapability().getGetWayPointsEnabled() != null)
						{
							switchGetWayPointsEnabled.Checked = (bool)systemCapabilities.getNavigationCapability().getGetWayPointsEnabled();
						}
						if (systemCapabilities.getNavigationCapability().getSendLocationEnabled() != null)
						{
							switchsendLocationEnabled.Checked = (bool)systemCapabilities.getNavigationCapability().getSendLocationEnabled();
						}
					}
					if (systemCapabilities.getVideoStreamingCapability() != null)
					{
						if (systemCapabilities.getVideoStreamingCapability().getImageResolution().getResolutionHeight() != null)
						{
							editTextResolutionHeight.Text = systemCapabilities.getVideoStreamingCapability().getImageResolution().getResolutionHeight().ToString();
						}
						if (systemCapabilities.getVideoStreamingCapability().getImageResolution().getResolutionWidth() != null)
						{
							editTextResolutionWidth.Text = systemCapabilities.getVideoStreamingCapability().getImageResolution().getResolutionWidth().ToString();
						}
						if (systemCapabilities.getVideoStreamingCapability().getMaxBitrate() != null)
						{
							editTextMaxBitrate.Text = systemCapabilities.getVideoStreamingCapability().getMaxBitrate().ToString();
						}
						if (systemCapabilities.getVideoStreamingCapability().getHapticSpatialDataSupported() != null)
						{
							switchHapticSpatialDataSupported.Checked = (bool)systemCapabilities.getVideoStreamingCapability().getHapticSpatialDataSupported();
						}
					}
					if (systemCapabilities.getPhoneCapability()!=null && systemCapabilities.getPhoneCapability().getDialNumberEnabled() != null)
					{
						switchDialNumberEnabled.Checked = (bool)systemCapabilities.getPhoneCapability().getDialNumberEnabled();
					}
					
				}

				List<DisplayCapability> dispCapability = new List<DisplayCapability>();
				if (systemCapabilities != null && systemCapabilities.getDisplayCapability() != null)
				{
					dispCapability.AddRange(systemCapabilities.getDisplayCapability());

				}
				else
				{
					addDisplayCapCheckBox.Checked = false;
					addDisplayCapButton.Enabled = false;
				}

				

				

				var displayCapAdapter = new DisplayCapabilityAdapter(this, dispCapability,null);
				

				seatLocationList = new List<SeatLocation>();
				if (systemCapabilities != null && systemCapabilities.getSeatCapability() != null)
				{
					if (systemCapabilities.getSeatCapability().getSeats() != null)
					{
						seatLocationList.AddRange(systemCapabilities.getSeatCapability().getSeats());

					}
					else
					{
						seatChk.Checked = false;
						gridChk.Enabled = false;
						gridBtn.Enabled = false;


					}
				}
				else
				{
					seatLocationCapability.Checked = false;
					rows.Enabled = false;

					rowsEditText.Enabled = false;
					col.Enabled = false;

					colEditText.Enabled = false;
					level.Enabled = false;


					levelEditText.Enabled = false;
					seatChk.Enabled = false;
					gridChk.Enabled = false;

					gridBtn.Enabled = false;
				}
				seatLocationadapter = new SeatLocationAdapter(this, seatLocationList,ViewStates.Visible);
				seatLocationListview.Adapter = seatLocationadapter;
				Utility.setListViewHeightBasedOnChildren(seatLocationListview);


				addDisplayCapCheckBox.CheckedChange += (s, e) =>
				{
					addDisplayCapButton.Enabled= e.IsChecked;

				};
				seatLocationCapability.CheckedChange += (s, e) =>
                {
					rows.Enabled = e.IsChecked;

					rowsEditText.Enabled = e.IsChecked;
                    col.Enabled = e.IsChecked;

                    colEditText.Enabled = e.IsChecked;
                    level.Enabled = e.IsChecked;


                    levelEditText.Enabled = e.IsChecked;
                    seatChk.Enabled = e.IsChecked;
					gridChk.Enabled = e.IsChecked;

					gridBtn.Enabled = e.IsChecked;
                    
                };
				rows.CheckedChange += (s, e) => rowsEditText.Enabled = e.IsChecked;
				col.CheckedChange += (s, e) => colEditText.Enabled = e.IsChecked;
				level.CheckedChange += (s, e) => levelEditText.Enabled = e.IsChecked;
				seatChk.CheckedChange += (s, e) => {
					gridBtn.Enabled = e.IsChecked;
					gridChk.Enabled = e.IsChecked;
				};



				checkBoxVideoStreamingCapability.CheckedChange += (s, e) =>
				{
					checkBoxImageResolution.Enabled = e.IsChecked;

					checkBoxResolutionWidth.Enabled = e.IsChecked;
					editTextResolutionWidth.Enabled = e.IsChecked;

					checkBoxResolutionHeight.Enabled = e.IsChecked;
					editTextResolutionHeight.Enabled = e.IsChecked;


					checkBoxMaxBitrate.Enabled = e.IsChecked;
					editTextMaxBitrate.Enabled = e.IsChecked;

					checkBoxHapticSpatialDataSupported.Enabled = e.IsChecked;
					switchHapticSpatialDataSupported.Enabled = e.IsChecked;

					addSupportedFormatsCheckBox.Enabled = e.IsChecked;
					addSupportedFormatsButton.Enabled = e.IsChecked;
					supportedFormatsListView.Enabled = e.IsChecked;
				};


				checkBoxImageResolution.CheckedChange += (s, e) =>
                {
                    checkBoxResolutionWidth.Enabled = e.IsChecked;
                    editTextResolutionWidth.Enabled = e.IsChecked;

                    checkBoxResolutionHeight.Enabled = e.IsChecked;
                    editTextResolutionHeight.Enabled = e.IsChecked;

                };
                checkBoxResolutionWidth.CheckedChange += (s, e) =>
                {
                    editTextResolutionWidth.Enabled = e.IsChecked;
                };
                checkBoxResolutionHeight.CheckedChange += (s, e) =>
                {
                    editTextResolutionHeight.Enabled = e.IsChecked;
                };
                checkBoxMaxBitrate.CheckedChange += (s, e) =>
                {
                    editTextMaxBitrate.Enabled = e.IsChecked;
                };
                checkBoxHapticSpatialDataSupported.CheckedChange += (s, e) =>
                {
                    switchHapticSpatialDataSupported.Enabled = e.IsChecked;
                };
                addSupportedFormatsCheckBox.CheckedChange += (s, e) =>
                {
                    addSupportedFormatsButton.Enabled = e.IsChecked;
                    supportedFormatsListView.Enabled = e.IsChecked;
                };


                List<VideoStreamingFormat> supportedFormatsList = new List<VideoStreamingFormat>();
                if (systemCapabilities.getVideoStreamingCapability()!=null && systemCapabilities.getVideoStreamingCapability().getSupportedFormats() != null)
                {
                    supportedFormatsList.AddRange(systemCapabilities.getVideoStreamingCapability().getSupportedFormats());
                }

                var supportedFormatsAdapter = new VideoStreamingAdapter(this, supportedFormatsList);
				supportedFormatsListView.Adapter = supportedFormatsAdapter;
				Utility.setListViewHeightBasedOnChildren(supportedFormatsListView);


				gridBtn.Click += (object sender, EventArgs e) => 
			{
				gridSeat = null;
				GridDialogue(layoutInflater,"seat", seatLocationListview);


			};
				
				addSupportedFormatsButton.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder supportedFormatsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View supportedFormatsView = layoutInflater.Inflate(Resource.Layout.video_streaming_format, null);
					supportedFormatsAlertDialog.SetView(supportedFormatsView);
                    supportedFormatsAlertDialog.SetTitle("Video Streaming Format");

                    CheckBox protocolCheckBox = (CheckBox)supportedFormatsView.FindViewById(Resource.Id.video_streaming_protocol_chk);
					Spinner protocolSpinner = (Spinner)supportedFormatsView.FindViewById(Resource.Id.video_streaming_protocol_spn);

					CheckBox codecCheckBox = (CheckBox)supportedFormatsView.FindViewById(Resource.Id.video_streaming_codec_chk);
					Spinner codecSpinner = (Spinner)supportedFormatsView.FindViewById(Resource.Id.video_streaming_codec_spn);

					var protocolAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, videoStreamingProtocolArray);
					protocolSpinner.Adapter = protocolAdapter;

                    var codecAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, videoStreamingCodecArray);
					codecSpinner.Adapter = codecAdapter;

                    protocolCheckBox.CheckedChange += (s, e) => protocolSpinner.Enabled = e.IsChecked;
                    codecCheckBox.CheckedChange += (s, e) => codecSpinner.Enabled = e.IsChecked;

                    supportedFormatsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                    {
                        VideoStreamingFormat videoStreamingFormat = new VideoStreamingFormat();
                        if (protocolCheckBox.Checked)
                        {
                            videoStreamingFormat.protocol = (VideoStreamingProtocol) protocolSpinner.SelectedItemPosition;
                        }
                        if (codecCheckBox.Checked)
                        {
                            videoStreamingFormat.codec = (VideoStreamingCodec) codecSpinner.SelectedItemPosition;
                        }

                        supportedFormatsList.Add(videoStreamingFormat);
                        supportedFormatsAdapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(supportedFormatsListView);

					});

                    supportedFormatsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        supportedFormatsAlertDialog.Dispose();
                    });

                    supportedFormatsAlertDialog.Show();

                };
				
			addDisplayCapButton.Click += delegate
			{
				Android.Support.V7.App.AlertDialog.Builder displayCapabilityAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View displayCapability = layoutInflater.Inflate(Resource.Layout.display_capability, null);
				displayCapabilityAlertDialog.SetView(displayCapability);
				displayCapabilityAlertDialog.SetTitle("Display Capability");

				CheckBox displayCapabilityDisplayNamechk = (CheckBox)displayCapability.FindViewById(Resource.Id.display_name_checkbox);
				EditText displayCapabilityDisplayNameEt = (EditText)displayCapability.FindViewById(Resource.Id.display_name_et);
				CheckBox displayCapabilityWindowTypeSupportedChk = (CheckBox)displayCapability.FindViewById(Resource.Id.display_capability_window_type_supported_chk);
				CheckBox displayCapabilityWindowCapabilitiesChk = (CheckBox)displayCapability.FindViewById(Resource.Id.display_capability_windowCapabilities_chk);
				CheckBox windowtypeCapabilitiesTypeCheckbox = (CheckBox)displayCapability.FindViewById(Resource.Id.windowtype_capabilities_type_checkbox);
				Spinner windowtypeCapabilitiesTypeSpinner = (Spinner)displayCapability.FindViewById(Resource.Id.windowtype_capabilities_type_spinner);
				CheckBox windowtypeCapabilitiesTypeMaxwindowsCheckbox = (CheckBox)displayCapability.FindViewById(Resource.Id.windowtype_capabilities_type_maxwindows_checkbox);
				EditText windowtypeCapabilitiesTypeMaxwindowsEt = (EditText)displayCapability.FindViewById(Resource.Id.windowtype_capabilities_type_maxwindows_et);

				Button displayCapabilityWindowCapabilitiesBtn = (Button)displayCapability.FindViewById(Resource.Id.display_capability_windowCapabilities_btn);
				
				var typeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, windowType);
				windowtypeCapabilitiesTypeSpinner.Adapter = typeAdapter;
					
				displayCapabilityDisplayNamechk.CheckedChange += (s, e) => displayCapabilityDisplayNameEt.Enabled = e.IsChecked;
				displayCapabilityWindowTypeSupportedChk.CheckedChange += (s, e) =>
				{
					windowtypeCapabilitiesTypeCheckbox.Enabled = e.IsChecked;
					windowtypeCapabilitiesTypeSpinner.Enabled = e.IsChecked;
					windowtypeCapabilitiesTypeMaxwindowsCheckbox.Enabled = e.IsChecked;
					windowtypeCapabilitiesTypeMaxwindowsEt.Enabled = e.IsChecked;
				};
				windowtypeCapabilitiesTypeMaxwindowsEt.Text = BuildDefaults.buildDefaultDisplayCapability().getWindowTypeSupported().getMaxNumberOfWindows().ToString();
				windowtypeCapabilitiesTypeMaxwindowsCheckbox.CheckedChange += (s, e) => windowtypeCapabilitiesTypeMaxwindowsEt.Enabled = e.IsChecked;
				windowtypeCapabilitiesTypeCheckbox.CheckedChange += (s, e) => windowtypeCapabilitiesTypeSpinner.Enabled = e.IsChecked;
				displayCapabilityDisplayNameEt.Text = BuildDefaults.buildDefaultDisplayCapability().getDisplayName();
				

				displayCapabilityWindowCapabilitiesChk.CheckedChange += (s, e) => displayCapabilityWindowCapabilitiesBtn.Enabled = e.IsChecked;
				List<WindowCapability> windowCapabilityList =null;

				ListView displayCapabilityView = (ListView)displayCapability.FindViewById(Resource.Id.display_capability_lv);
				ListView windowCapabilityView = (ListView)displayCapability.FindViewById(Resource.Id.window_cap_list_view);
				displayCapAdapter = new DisplayCapabilityAdapter(this, dispCapability, null);
				Button displayCap_Add = (Button)displayCapability.FindViewById(Resource.Id.add);
				

				displayCap_Add.Click += delegate
				{
					DisplayCapability dispCap = new DisplayCapability();
					if (displayCapabilityDisplayNamechk.Checked)
					{

						if (!displayCapabilityDisplayNameEt.Text.Equals(""))
							dispCap.displayName = displayCapabilityDisplayNameEt.Text.ToString();
						
					}
					
					
					WindowTypeCapabilities windowTypeCap = null;
					if (displayCapabilityWindowTypeSupportedChk.Checked)
					{
					windowTypeCap = new WindowTypeCapabilities();
					if (windowtypeCapabilitiesTypeCheckbox.Checked)

					{
					windowTypeCap.type = (WindowType)windowtypeCapabilitiesTypeSpinner.SelectedItemPosition;
					}
					if (windowtypeCapabilitiesTypeMaxwindowsCheckbox.Checked)
					{
					windowTypeCap.maxNumberOfWindows = null;
					if (!windowtypeCapabilitiesTypeMaxwindowsEt.Text.Equals(""))
					windowTypeCap.maxNumberOfWindows = Java.Lang.Integer.ParseInt(windowtypeCapabilitiesTypeMaxwindowsEt.Text);
					}
					dispCap.windowTypeSupported = windowTypeCap;
					}
					dispCap.windowCapabilities = null;
					if (displayCapabilityWindowCapabilitiesChk.Checked)
					{
						dispCap.windowCapabilities = windowCapabilityList;
					}
					if (dispCap != null )
					{
						

						dispCapability.Add(dispCap);
					}
					displayCapAdapter = new DisplayCapabilityAdapter(this, dispCapability, displayCapabilityView);
					displayCapabilityView.Adapter = displayCapAdapter;
					displayCapAdapter.NotifyDataSetChanged();
					Utility.setListViewHeightBasedOnChildren(displayCapabilityView);


				};
				displayCapabilityView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
				{
				var listView = sender as ListView;
				var t = dispCapability[e.Position];
				if (t.getDisplayName() != null)
				{
						displayCapabilityDisplayNameEt.Text = t.getDisplayName().ToString();
				}
					if (t.getWindowTypeSupported().getMaxNumberOfWindows() != null || t.getWindowTypeSupported().getMaxNumberOfWindows().ToString() != "")
					{
						windowtypeCapabilitiesTypeMaxwindowsEt.Text = t.getWindowTypeSupported().getMaxNumberOfWindows().ToString();
					}
					else
						windowtypeCapabilitiesTypeMaxwindowsEt.Text = "";
				if (t.getWindowTypeSupported().GetType() != null)
				{
						if (t.getWindowTypeSupported().getType().ToString().Equals("MAIN"))

							windowtypeCapabilitiesTypeSpinner.SetSelection(0);
						else
							windowtypeCapabilitiesTypeSpinner.SetSelection(1);

				}
				
				};
				if (windowCapabilityList == null)
				{
					windowCapabilityList = new List<WindowCapability>();
					windowCapabilityList.Add(BuildDefaults.buildDefaultWindowCapability());
				}
				displayCapabilityWindowCapabilitiesBtn.Click += delegate
				{

				Android.Support.V7.App.AlertDialog.Builder windowCapabilityAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View windowCapability = layoutInflater.Inflate(Resource.Layout.window_capability, null);
				windowCapabilityAlertDialog.SetView(windowCapability);
				windowCapabilityAlertDialog.SetTitle("Window Capability");

				CheckBox windowCapabilityWindowidcheckbox = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_windowid_checkbox);
				EditText windowCapabilityWindowidEt = (EditText)windowCapability.FindViewById(Resource.Id.window_capability_windowid_et);
				CheckBox windowCapabilityTextFieldsChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_add_text_fields_chk);
				CheckBox windowCapabilityImageFieldsChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_image_fields_chk);
				CheckBox windowCapabilityImageTypesChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_image_types_chk);
				CheckBox windowCapabilityTemplatesAvailableCheckbox = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_templates_available_checkbox);
				EditText windowCapabilityTemplatesAvailableEdittext = (EditText)windowCapability.FindViewById(Resource.Id.window_capability_templates_available_edittext);
				CheckBox windowCapabilityButtonCapChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_button_cap_chk);
				CheckBox windowCapabilitySoftbuttonChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_softbutton_chk);
				CheckBox windowCapabilityNumCustomPresetsAvailableCheckbox = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_num_custom_presets_available_checkbox);
				EditText windowCapabilityNumCustomPresetsAvailableEdittext = (EditText)windowCapability.FindViewById(Resource.Id.window_capability_num_custom_presets_available_edittext);
				Button windowCapabilityAddTextfieldsBtn = (Button)windowCapability.FindViewById(Resource.Id.window_capability_add_text_fields_btn);
				Button windowCapabilityAddImageFieldsBtn = (Button)windowCapability.FindViewById(Resource.Id.window_capability_image_fields_btn);
				Button windowCapabilityButtonCapBtn = (Button)windowCapability.FindViewById(Resource.Id.window_capability_button_cap_btn);
				Button windowCapabilitiesSoftbuttonBtn = (Button)windowCapability.FindViewById(Resource.Id.window_capability_softbutton_btn);
				List<TextField> tempTextFieldsList =null;
				List<TextField> textFieldsList = new List<TextField>();

					windowCapabilityWindowidEt.Text = BuildDefaults.buildDefaultWindowCapability().getWindowID().ToString();
					var textFieldAdapter = new TextFieldAdapter(this, null, ViewStates.Visible,null);
					if (tempTextFieldsList == null)
					{
						tempTextFieldsList = new List<TextField>();
						tempTextFieldsList.AddRange(BuildDefaults.buildDefaultTextField());
					}

					windowCapabilityAddTextfieldsBtn.Click += delegate
				{
					
					
					Android.Support.V7.App.AlertDialog.Builder textFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View textFieldsView = layoutInflater.Inflate(Resource.Layout.text_field, null);
					textFieldsAlertDialog.SetView(textFieldsView);
					textFieldsAlertDialog.SetTitle("TextField");

					CheckBox checkBoxName = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_name_checkbox);
					Spinner spnName = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_name_spinner);
					string[] textFieldNames = Enum.GetNames(typeof(TextFieldName));
					var textFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, textFieldNames);
					spnName.Adapter = textFieldNamesAdapter;
					CheckBox checkBoxCharacterSet = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_checkbox);
					Spinner spnCharacterSet = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_spinner);
					string[] characterSet = Enum.GetNames(typeof(CharacterSet));
					var characterSetAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, characterSet);
					spnCharacterSet.Adapter = characterSetAdapter;
					CheckBox checkBoxWidth = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_width_checkbox);
					EditText editTextWidth = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_width_edittext);
					CheckBox checkBoxRow = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_row_checkbox);
					EditText editTextRow = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_row_edittext);
					checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
					checkBoxCharacterSet.CheckedChange += (s, e) => spnCharacterSet.Enabled = e.IsChecked;
					checkBoxWidth.CheckedChange += (s, e) => editTextWidth.Enabled = e.IsChecked;
					checkBoxRow.CheckedChange += (s, e) => editTextRow.Enabled = e.IsChecked;
					ListView viewTextField = (ListView)textFieldsView.FindViewById(Resource.Id.text_fields_listview);
					Button textFieldsAdd = (Button)textFieldsView.FindViewById(Resource.Id.add);
					textFieldAdapter = new TextFieldAdapter(this, tempTextFieldsList, ViewStates.Visible, viewTextField);
					viewTextField.Adapter = textFieldAdapter;
					Utility.setListViewHeightBasedOnChildren(viewTextField);
					
					textFieldsAdd.Click += delegate
					{
						TextField txtField = new TextField();

						if (checkBoxName.Checked)
						{
							txtField.name = (TextFieldName)spnName.SelectedItemPosition;
						}
						if (checkBoxCharacterSet.Checked)
						{
							txtField.characterSet = (CharacterSet)spnCharacterSet.SelectedItemPosition;
						}
						if (checkBoxWidth.Checked)
						{
							if (editTextWidth.Text != null && editTextWidth.Text.Length > 0)
								txtField.width = Java.Lang.Integer.ParseInt(editTextWidth.Text);
						}
						if (checkBoxRow.Checked)
						{
							if (editTextRow.Text != null && editTextRow.Text.Length > 0)
								txtField.rows = Java.Lang.Integer.ParseInt(editTextRow.Text);
						}
						tempTextFieldsList.Add(txtField);
						
						textFieldAdapter.NotifyDataSetChanged();

						Utility.setListViewHeightBasedOnChildren(viewTextField);
					};
					
					textFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
					{
			


					});

					
					textFieldsAlertDialog.SetCancelable(false);

					textFieldsAlertDialog.Show();
				};

					
				Button imageFieldsButton = (Button)windowCapability.FindViewById(Resource.Id.window_capability_image_fields_btn);
				CheckBox imageFieldsChk = (CheckBox)windowCapability.FindViewById(Resource.Id.window_capability_image_fields_chk);
				var imageFieldAdapter = new ImageFieldAdapter(this, null, ViewStates.Visible,null);

					if (imageFieldsList == null)
					{
						imageFieldsList = new List<ImageField>();
						imageFieldsList.AddRange(BuildDefaults.buildDefaultImageField());
					}

					windowCapabilityAddImageFieldsBtn.Click += delegate
				{

					imageFieldsList = new List<ImageField>();
					imageFieldsList.AddRange(BuildDefaults.buildDefaultImageField());
					

					Android.Support.V7.App.AlertDialog.Builder imageFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
					View imageFieldsView = layoutInflater.Inflate(Resource.Layout.image_field, null);
					imageFieldsAlertDialog.SetView(imageFieldsView);
					imageFieldsAlertDialog.SetTitle("Image Field");

					CheckBox checkBoxName = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_name_checkbox);
					Spinner spnName = (Spinner)imageFieldsView.FindViewById(Resource.Id.image_field_name_spinner);

					string[] imageFieldNames = Enum.GetNames(typeof(ImageFieldName));
					var imageFieldNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageFieldNames);
					spnName.Adapter = imageFieldNamesAdapter;

					List<FileType> fileTypeList = new List<FileType>();
					ListView fileTypeListView = (ListView)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_listview);
					var fileTypeAdapter = new FileTypeAdapter(this, fileTypeList);
					fileTypeListView.Adapter = fileTypeAdapter;

					string[] fileTypes = Enum.GetNames(typeof(FileType));
					bool[] fileTypeBoolArray = new bool[fileTypes.Length];

					CheckBox fileTypeChk = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_chk);
					Button fileTypeButton = (Button)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_btn);


					checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
					fileTypeChk.CheckedChange += (sender, e) => fileTypeButton.Enabled = e.IsChecked;

					fileTypeButton.Click += (sender, e1) =>
					{
						Android.Support.V7.App.AlertDialog.Builder fileTypeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
						fileTypeAlertDialog.SetTitle("FileType");

						fileTypeAlertDialog.SetMultiChoiceItems(fileTypes, fileTypeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => fileTypeBoolArray[e.Which] = e.IsChecked);

						fileTypeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
						{
						fileTypeAlertDialog.Dispose();
						});


						fileTypeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
						{
							fileTypeList.Clear();
							for (int i = 0; i < fileTypes.Length; i++)
							{
								if (fileTypeBoolArray[i])
								{
									fileTypeList.Add(((FileType)typeof(FileType).GetEnumValues().GetValue(i)));
								}
							}
							fileTypeAdapter.NotifyDataSetChanged();
							Utility.setListViewHeightBasedOnChildren(fileTypeListView);
					});
						fileTypeAlertDialog.SetCancelable(false);

						fileTypeAlertDialog.Show();
				};


				CheckBox checkBoxImgResolution = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_resolution_checkbox);
				CheckBox checkBoxResWidth = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_checkbox);
				EditText editTextResWidth = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_edit_text);
				CheckBox checkBoxResHeight = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_checkbox);
				EditText editTextResHeight = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_edit_text);


				checkBoxImgResolution.CheckedChange += (sender, e) =>
				{
				    checkBoxResWidth.Enabled = e.IsChecked;
					editTextResWidth.Enabled = e.IsChecked;
					checkBoxResHeight.Enabled = e.IsChecked;
					editTextResHeight.Enabled = e.IsChecked;
				};
				checkBoxResWidth.CheckedChange += (s, e) => editTextResWidth.Enabled = e.IsChecked;
				checkBoxResHeight.CheckedChange += (s, e) => editTextResHeight.Enabled = e.IsChecked;
				ListView viewImageField = (ListView)imageFieldsView.FindViewById(Resource.Id.window_capability_image_fields_listview);
				Button imageFieldsAdd = (Button)imageFieldsView.FindViewById(Resource.Id.add);
				imageFieldsAdd.Visibility = ViewStates.Visible;

					imageFieldAdapter = new ImageFieldAdapter(this, imageFieldsList, ViewStates.Visible, viewImageField);
					viewImageField.Adapter = imageFieldAdapter;
					Utility.setListViewHeightBasedOnChildren(viewImageField);
					imageFieldsAdd.Click += delegate
				{
								
					ImageField imgField = new ImageField();
					if (checkBoxName.Checked)
					{
					 imgField.name = (ImageFieldName)spnName.SelectedItemPosition;
					}
					if (fileTypeChk.Checked)
					{
					 imgField.imageTypeSupported = fileTypeList;
					}


					ImageResolution resolution = null;
					if (checkBoxImageResolution.Checked)
					{
						resolution = new ImageResolution();
						if (checkBoxResolutionWidth.Checked)
						{
							if (editTextResolutionWidth.Text.Equals(""))
								resolution.resolutionWidth = 0;
							else
								resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
						}

						if (checkBoxResolutionHeight.Checked)
						{
							if (editTextResolutionHeight.Text.Equals(""))
								resolution.resolutionHeight = 0;
							else
								resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
						}
					}
					imgField.imageResolution = resolution;
					imageFieldsList.Add(imgField);

					imageFieldAdapter.NotifyDataSetChanged();
					Utility.setListViewHeightBasedOnChildren(viewImageField);


				};
				imageFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				{
					
								

				});

					imageFieldsAlertDialog.SetCancelable(false);

					imageFieldsAlertDialog.Show();
			};

						List<ImageType> imageTypeList = new List<ImageType>();

						Button windowCapabilityImageTypesBtn = (Button)windowCapability.FindViewById(Resource.Id.window_capability_image_types_btn);
						if (dspCap.getImageCapabilities() != null)
						{
							imageTypeList.AddRange(dspCap.getImageCapabilities());
						}
						else
							windowCapabilityImageTypesChk.Checked = false;

						var imageTypeAdapter = new ImageTypeAdapter(this, imageTypeList);
					

						string[] imageCapabilities = Enum.GetNames(typeof(ImageType));
						bool[] imageCapabilitiesBoolArray = new bool[imageCapabilities.Length];
						for (int i = 0; i < imageCapabilities.Length; i++)
						{
						imageCapabilitiesBoolArray[i]=true;
						}
						


					windowCapabilityImageTypesBtn.Click += (sender, e1) =>
						{

							Android.Support.V7.App.AlertDialog.Builder imageCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
							imageCapabilitiesAlertDialog.SetTitle("Image Capabilities");

							imageCapabilitiesAlertDialog.SetMultiChoiceItems(imageCapabilities, imageCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => imageCapabilitiesBoolArray[e.Which] = e.IsChecked);

							

							imageCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
							{
								imageTypeList.Clear();
								for (int i = 0; i < imageCapabilities.Length; i++)
								{
									if (imageCapabilitiesBoolArray[i])
									{
										imageTypeList.Add(((ImageType)typeof(ImageType).GetEnumValues().GetValue(i)));
									}
								}
								imageTypeAdapter.NotifyDataSetChanged();
							
							});
							imageCapabilitiesAlertDialog.SetCancelable(false);

							imageCapabilitiesAlertDialog.Show();
						};

					

					var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, null, ViewStates.Visible,null);


					if (BtnCapList == null)
					{
						BtnCapList = new List<ButtonCapabilities>();
						BtnCapList.AddRange(BuildDefaults.buildDefaultButtonCapsList());
					}


					windowCapabilityButtonCapBtn.Click += delegate
						{
							
							Android.Support.V7.App.AlertDialog.Builder btnCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
							View btnCapabilitiesView = layoutInflater.Inflate(Resource.Layout.button_capabilities, null);
							btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
							btnCapabilitiesAlertDialog.SetTitle("Button Capabilities");

							CheckBox chkButtonName = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);
							Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);

							var namesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
							spnButtonNames.Adapter = namesAdapter;

							CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);
							Switch checkBoxShortPressAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_tgl);

							CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);
							Switch checkBoxLongPressAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_tgl);

							CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);
							Switch checkBoxUpDownAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_tgl);
							checkBoxShortPressAvailable.CheckedChange += (sender, e) => {
								checkBoxShortPressAvailable_tgl.Enabled = e.IsChecked;
							};
							checkBoxLongPressAvailable.CheckedChange += (sender, e) => {
								checkBoxLongPressAvailable_tgl.Enabled = e.IsChecked;
							};
							checkBoxUpDownAvailable.CheckedChange += (sender, e) => {
								checkBoxUpDownAvailable_tgl.Enabled = e.IsChecked;
							};
							ListView btnCapabilitiesListView = (ListView)btnCapabilitiesView.FindViewById(Resource.Id.btn_capabilities_list_view);
							buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, BtnCapList, ViewStates.Visible, btnCapabilitiesListView);
							btnCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;
							Utility.setListViewHeightBasedOnChildren(btnCapabilitiesListView);


							chkButtonName.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;
							checkBoxShortPressAvailable.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;
							checkBoxLongPressAvailable.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;
							checkBoxUpDownAvailable.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;

							Button btnCapAdd = (Button)btnCapabilitiesView.FindViewById(Resource.Id.add);
							btnCapAdd.Visibility = ViewStates.Visible;

							btnCapAdd.Click += delegate
							{
								ButtonCapabilities btn = new ButtonCapabilities();
								if (chkButtonName.Checked)
								{
									btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;
								}
								if (checkBoxShortPressAvailable.Checked)
									btn.shortPressAvailable = checkBoxShortPressAvailable_tgl.Checked;
								if (checkBoxLongPressAvailable.Checked)
									btn.longPressAvailable = checkBoxLongPressAvailable_tgl.Checked;
								if (checkBoxUpDownAvailable.Checked)
									btn.upDownAvailable = checkBoxUpDownAvailable_tgl.Checked;



								BtnCapList.Add(btn);


								buttonCapabilitiesAdapter.NotifyDataSetChanged();
								Utility.setListViewHeightBasedOnChildren(btnCapabilitiesListView);

							};
							btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
							{

							
							});

							
							btnCapabilitiesAlertDialog.SetCancelable(false);

							btnCapabilitiesAlertDialog.Show();
						};
					
					 
					
					var softBtnAdapter = new SoftButtonCapabilitiesAdapter(this, null, ViewStates.Invisible,null);
					
					if (btnSoftBtnCapList == null)
					{

						btnSoftBtnCapList = new List<SoftButtonCapabilities>();
						btnSoftBtnCapList.AddRange(BuildDefaults.buildDefaultSoftButtonCapabilities());
					}

					windowCapabilitiesSoftbuttonBtn.Click += delegate
						{


							Android.Support.V7.App.AlertDialog.Builder softButtonCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
							View softButtonCapabilitiesView = layoutInflater.Inflate(Resource.Layout.soft_button_capabilities, null);
							softButtonCapabilitiesAlertDialog.SetView(softButtonCapabilitiesView);
							softButtonCapabilitiesAlertDialog.SetTitle("SoftButtonCapabilities");
							ListView softbtnCapabilitiesListView = (ListView)softButtonCapabilitiesView.FindViewById(Resource.Id.softbutton_capabilities_list_view);
							
							softBtnAdapter = new SoftButtonCapabilitiesAdapter(this, btnSoftBtnCapList, ViewStates.Invisible, softbtnCapabilitiesListView);
							
							softbtnCapabilitiesListView.Adapter = softBtnAdapter;
							Utility.setListViewHeightBasedOnChildren(softbtnCapabilitiesListView);

							CheckBox checkBoxShortPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_short_press_available_checkbox);
							CheckBox checkBoxLongPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_long_press_available_checkbox);
							CheckBox checkBoxUpDownAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_up_down_available_checkbox);
							CheckBox checkBoxTextSupported = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_text_supported_checkbox);
							TextView temp = (TextView)softButtonCapabilitiesView.FindViewById(Resource.Id.temp);

							CheckBox checkBoxImageSupported = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_image_supported_checkbox);
							Button softButtonFieldsAdd = (Button)softButtonCapabilitiesView.FindViewById(Resource.Id.add);
							softButtonFieldsAdd.Visibility = ViewStates.Visible;
							softButtonFieldsAdd.Click += delegate
							{
								SoftButtonCapabilities btn = new SoftButtonCapabilities();
								btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
								btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
								btn.upDownAvailable = checkBoxUpDownAvailable.Checked;
								btn.imageSupported = checkBoxImageSupported.Checked;
								btn.textSupported = checkBoxTextSupported.Checked;

								btnSoftBtnCapList.Add(btn);

								softBtnAdapter.NotifyDataSetChanged();
								Utility.setListViewHeightBasedOnChildren(softbtnCapabilitiesListView);


							};
								softButtonCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
							{   
							});

						
							softButtonCapabilitiesAlertDialog.SetCancelable(false);

							softButtonCapabilitiesAlertDialog.Show();

						};



						if (dspCap.getTemplatesAvailable() != null)
						{
							String availableTemplates = null;
							for (int i = 0; i < dspCap.getTemplatesAvailable().Count; i++)
							{
								if (i == 0)
								{
									availableTemplates = dspCap.getTemplatesAvailable()[i];
								}
								else
								{
									availableTemplates = availableTemplates + "," + dspCap.getTemplatesAvailable()[i];
								}
							}
							windowCapabilityTemplatesAvailableEdittext.Text = availableTemplates;
						}
						else {
							windowCapabilityTemplatesAvailableCheckbox.Checked = false;
						}

						windowCapabilityTemplatesAvailableCheckbox.CheckedChange += (s, e) => windowCapabilityTemplatesAvailableEdittext.Enabled = e.IsChecked;
						windowCapabilityTextFieldsChk.CheckedChange += (s, e) =>
						{
							windowCapabilityAddTextfieldsBtn.Enabled = e.IsChecked;
						};
						windowCapabilityWindowidcheckbox.CheckedChange += (s, e) =>
						{
							windowCapabilityWindowidEt.Enabled = e.IsChecked;
						};
						windowCapabilityImageFieldsChk.CheckedChange += (s, e) =>
						{
							windowCapabilityAddImageFieldsBtn.Enabled = e.IsChecked;
						};
						windowCapabilityButtonCapChk.CheckedChange += (s, e) =>
						{
							windowCapabilityButtonCapBtn.Enabled = e.IsChecked;
						};
						windowCapabilitySoftbuttonChk.CheckedChange += (s, e) =>
						{
							windowCapabilitiesSoftbuttonBtn.Enabled = e.IsChecked;
						};
						windowCapabilityImageTypesChk.CheckedChange += (s, e) =>
						{
							windowCapabilityImageTypesBtn.Enabled = e.IsChecked;
						};
						windowCapabilityNumCustomPresetsAvailableCheckbox.CheckedChange += (s, e) =>
						{
							windowCapabilityNumCustomPresetsAvailableEdittext.Enabled = e.IsChecked;

						};
						ListView windowCapability_Listview = (ListView)windowCapability.FindViewById(Resource.Id.cap_list_view);
						Button add = (Button)windowCapability.FindViewById(Resource.Id.add);
						var windowCapAdapter = new WindowCapabilityAdapter(this, windowCapabilityList,null);

						

					windowCapability_Listview.Adapter = windowCapAdapter;
						windowCapability_Listview.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
						{
							var listView = sender as ListView;
							var t = windowCapabilityList[e.Position];
							if (t.getWindowID() != null)
								windowCapabilityWindowidEt.Text = t.getWindowID().ToString();
							if (t.getNumCustomPresetsAvailable() != null)
								windowCapabilityNumCustomPresetsAvailableEdittext.Text = t.getNumCustomPresetsAvailable().ToString();
							else
								windowCapabilityNumCustomPresetsAvailableEdittext.Text = "";
							
							
							
							if (t.getTemplatesAvailable() != null)
							{
								
									String availableTemplates = null;
									for (int i = 0; i < t.getTemplatesAvailable().Count; i++)
									{
										if (i == 0)
										{
											availableTemplates = t.getTemplatesAvailable()[i];
										}
										else
										{
											availableTemplates = availableTemplates + "," + t.getTemplatesAvailable()[i];
										}
									}
									windowCapabilityTemplatesAvailableEdittext.Text = availableTemplates;
								
								


							}
							else
								windowCapabilityTemplatesAvailableCheckbox.Checked = false;

						};

						
						add.Click += delegate
						{
							WindowCapability winCap = new WindowCapability();
							if (windowCapabilityWindowidcheckbox.Checked ){
								if(windowCapabilityWindowidEt.Text.ToString() != "")
							{
								winCap.windowID = Java.Lang.Integer.ParseInt(windowCapabilityWindowidEt.Text);

								
							}
								else
								{
									windowCapabilityWindowidEt.RequestFocus();
									windowCapabilityWindowidEt.SetError(GetString(Resource.String.error_field_required), null);
									return;
								}
							}

							if (windowCapabilityTextFieldsChk.Checked )
							{

								
								List<TextField> finalList = new List<TextField>();
								finalList.AddRange(tempTextFieldsList);
								winCap.textFields = finalList;
								


							}
							if (windowCapabilityImageFieldsChk.Checked)
							{
								List<ImageField> finalList = new List<ImageField>();
								finalList.AddRange(imageFieldsList);
								winCap.imageFields = finalList;
							}
							winCap.imageTypeSupported = null;
							if (windowCapabilityImageTypesChk.Checked)
							{
								winCap.imageTypeSupported = imageTypeList;
							}
							winCap.buttonCapabilities = null; 
							if (windowCapabilityButtonCapChk.Checked)
							{
								List<ButtonCapabilities> finalList = new List<ButtonCapabilities>();
								finalList.AddRange(BtnCapList);
								winCap.buttonCapabilities = finalList;
							}
							winCap.softButtonCapabilities = null;
							if (windowCapabilitySoftbuttonChk.Checked)
							{
								List<SoftButtonCapabilities> finalList = new List<SoftButtonCapabilities>();
								finalList.AddRange(btnSoftBtnCapList);
								winCap.softButtonCapabilities = finalList;
							}

							
							winCap.templatesAvailable = null;
							if (windowCapabilityTemplatesAvailableCheckbox.Checked)
							{
								List<String> templatesAvailable = new List<string>();
								templatesAvailable.AddRange(windowCapabilityTemplatesAvailableEdittext.Text.Split(','));

								winCap.templatesAvailable = templatesAvailable;
							}


							winCap.numCustomPresetsAvailable = null;
							if (windowCapabilityNumCustomPresetsAvailableCheckbox.Checked)
							{
								if (windowCapabilityNumCustomPresetsAvailableEdittext.Text != null && windowCapabilityNumCustomPresetsAvailableEdittext.Text.Length > 0)
								{
									try
									{
										winCap.numCustomPresetsAvailable = Java.Lang.Integer.ParseInt(windowCapabilityNumCustomPresetsAvailableEdittext.Text);
									}
									catch (Exception e) { }
								}
							}

								windowCapabilityList.Add(winCap);

								windowCapAdapter = new WindowCapabilityAdapter(this, windowCapabilityList, windowCapability_Listview);
								windowCapability_Listview.Adapter = windowCapAdapter;
								windowCapAdapter.NotifyDataSetChanged();
							Utility.setListViewHeightBasedOnChildren(windowCapability_Listview);

						};
					if (windowCapabilityList != null) {
						windowCapAdapter = new WindowCapabilityAdapter(this, windowCapabilityList, windowCapability_Listview);
						windowCapability_Listview.Adapter = windowCapAdapter;
						Utility.setListViewHeightBasedOnChildren(windowCapability_Listview);
					}
					windowCapabilityAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>

						{
							
							
						});

					
					windowCapabilityAlertDialog.SetCancelable(false);

					windowCapabilityAlertDialog.Show();

					};


				if (dispCapability != null) {
					displayCapAdapter = new DisplayCapabilityAdapter(this, dispCapability, displayCapabilityView);
					displayCapabilityView.Adapter = displayCapAdapter;
					Utility.setListViewHeightBasedOnChildren(displayCapabilityView);
				}
					
				displayCapabilityAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
					{
					
						
						
						
					});
				displayCapabilityAlertDialog.SetCancelable(false);

				displayCapabilityAlertDialog.Show();



				};

			systemCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
            {
                if (checkBoxNavigationCapability.Checked)
                {
                    NavigationCapability navCap = new NavigationCapability();
                    if (checkBoxSendLocationEnabled.Checked)
						navCap.sendLocationEnabled = switchsendLocationEnabled.Checked;

                    if (checkBoxGetWayPointsEnabled.Checked)
						navCap.getWayPointsEnabled = switchGetWayPointsEnabled.Checked;
					systemCapabilities.navigationCapability = navCap;
                }

                if (checkBoxPhoneCapability.Checked)
                {
                    PhoneCapability phoneCap = new PhoneCapability();
                    if (checkBoxSendLocationEnabled.Checked)
						phoneCap.dialNumberEnabled = switchDialNumberEnabled.Checked;

					systemCapabilities.phoneCapability = phoneCap;
                }
				systemCapabilities.seatLocationCapability = null;
				if (seatLocationCapability.Checked)
					{
					SeatLocationCapability seatLocationCap = new SeatLocationCapability();


					if (rows.Checked)
						{
							if (rowsEditText.Text.Equals(""))
								seatLocationCap.rows = 1;
							else
							seatLocationCap.rows= Java.Lang.Integer.ParseInt(rowsEditText.Text);
						}

						if (col.Checked)
						{
							if (colEditText.Text.Equals(""))
								seatLocationCap.columns = 1;
							else
								seatLocationCap.columns = Java.Lang.Integer.ParseInt(colEditText.Text);
						}
						if (level.Checked)
						{
							if (levelEditText.Text.Equals(""))
								seatLocationCap.levels = 1;
							else
								seatLocationCap.levels = Java.Lang.Integer.ParseInt(levelEditText.Text);
						}
						if (seatChk.Checked)
						{
							seatLocationCap.seats = seatLocationList;
						}
						systemCapabilities.seatLocationCapability = seatLocationCap;

					}

					if (checkBoxVideoStreamingCapability.Checked)
                    {
                        VideoStreamingCapability videoStreamingCapability = new VideoStreamingCapability();
                        if (checkBoxImageResolution.Checked)
                        {
                            ImageResolution resolution = null;
                            if (checkBoxImageResolution.Checked)
                            {
                                resolution = new ImageResolution();

                                if (checkBoxResolutionWidth.Checked)
                                {
                                    if (editTextResolutionWidth.Text.Equals(""))
                                        resolution.resolutionWidth = 0;
                                    else
                                        resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                                }
                                if (checkBoxResolutionHeight.Checked)
                                {
                                    if (editTextResolutionHeight.Text.Equals(""))
                                        resolution.resolutionHeight = 0;
                                    else
                                        resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                                }

                            }

                            int? maxBitRate = null;
                            if (checkBoxMaxBitrate.Checked)
                            {
                                if (!editTextResolutionHeight.Text.Equals(""))
                                    maxBitRate = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                            }

                            if (checkBoxHapticSpatialDataSupported.Checked)
                            {
                                videoStreamingCapability.hapticSpatialDataSupported = switchHapticSpatialDataSupported.Checked;
                            }

                            if(addSupportedFormatsCheckBox.Checked)
                            {
                                videoStreamingCapability.supportedFormats =supportedFormatsList;
                            }

                            videoStreamingCapability.preferredResolution = resolution;
                            videoStreamingCapability.maxBitrate = maxBitRate;
                            videoStreamingCapability.supportedFormats = supportedFormatsList;
                        }

                        systemCapabilities.videoStreamingCapability = videoStreamingCapability;

                    }
				systemCapabilities.displaycapability = null;
				if (addDisplayCapCheckBox.Checked)
				{

					systemCapabilities.displaycapability = dispCapability;

				}
				
				
			});


            systemCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
            {
               systemCapabilitiesAlertDialog.Dispose();
            });
               
            systemCapabilitiesAlertDialog.Show();
        };


			List<SoftButtonCapabilities> btnSoftButtonCapList = null;
			
			var adapter = new SoftButtonCapabilitiesAdapter(this, null, ViewStates.Invisible, null);

			if (btnSoftButtonCapList == null)
			{

				btnSoftButtonCapList = new List<SoftButtonCapabilities>();
				if (tmpObj.getSoftButtonCapabilities() != null)
					btnSoftButtonCapList.AddRange(BuildDefaults.buildDefaultSoftButtonCapabilities());

				else
				{
					addSoftButtonCapabilitiesButton.Enabled = false;
					addSoftButtonCapabilitiesCheckBox.Checked = false;
				}
			}


			addSoftButtonCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder softButtonCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View softButtonCapabilitiesView = layoutInflater.Inflate(Resource.Layout.soft_button_capabilities, null);
				softButtonCapabilitiesAlertDialog.SetView(softButtonCapabilitiesView);
                softButtonCapabilitiesAlertDialog.SetTitle("SoftButtonCapabilities");
				Button softButtonFieldsAdd = (Button)softButtonCapabilitiesView.FindViewById(Resource.Id.add);
				ListView softbtnCapabilitiesListView = (ListView)softButtonCapabilitiesView.FindViewById(Resource.Id.softbutton_capabilities_list_view);
				 adapter = new SoftButtonCapabilitiesAdapter(this, btnSoftButtonCapList, ViewStates.Invisible, softbtnCapabilitiesListView);
				softbtnCapabilitiesListView.Adapter = adapter;
				Utility.setListViewHeightBasedOnChildren(softbtnCapabilitiesListView);
				CheckBox checkBoxShortPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_short_press_available_checkbox);
				CheckBox checkBoxLongPressAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_long_press_available_checkbox);
				CheckBox checkBoxUpDownAvailable = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_up_down_available_checkbox);
				CheckBox checkBoxTextSupported = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_text_supported_checkbox);
				CheckBox checkBoxImageSupported = (CheckBox)softButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_image_supported_checkbox);
				softButtonFieldsAdd.Click += delegate
				{
					SoftButtonCapabilities btn = new SoftButtonCapabilities();
					btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
					btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
					btn.upDownAvailable = checkBoxUpDownAvailable.Checked;
					btn.imageSupported = checkBoxImageSupported.Checked;
					btn.textSupported = checkBoxTextSupported.Checked;


					btnSoftButtonCapList.Add(btn);
					adapter.NotifyDataSetChanged();
					Utility.setListViewHeightBasedOnChildren(softbtnCapabilitiesListView);

				};
					softButtonCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    
                });
				
				softButtonCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    softButtonCapabilitiesAlertDialog.Dispose();
                });
                softButtonCapabilitiesAlertDialog.Show();

            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HMICapabilities hmiCap = null;
                if (HmiCapabilitiesCheck.Checked)
                {
                    hmiCap = new HMICapabilities();
                    if (HmiCapabilitiesNavigationCheck.Checked)
						hmiCap.navigation = HmiCapabilitiesNavigationTgl.Checked;
                    if (HmiCapabilitiesPhonecallCheck.Checked)
						hmiCap.phoneCall = HmiCapabilitiesPhonecallTgl.Checked;
                }

                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result) rsltCodeSpinner.SelectedItemPosition;
                }

                DisplayCapabilities displayCap = null;
                if (displayCapabilitiesCheck.Checked)
                {
                    displayCap = dspCap;
                }

                AudioPassThruCapabilities audioCap = null;
                if (audioPassThruCapabilitiesCheck.Checked)
                {
                    audioCap = audioPassThruCap;
                }
                SystemCapabilities sysCap = null;
                if(systemCapabilitiesCheck.Checked)
                {
                    sysCap = systemCapabilities;
                }
				
				HmiZoneCapabilities? selectedHmiZoneCap = null;
                if (hmiZoneCapabilitiesCheckBox.Checked)
                {
                    selectedHmiZoneCap = (HmiZoneCapabilities) hmiZoneCapabilitiesSpinner.SelectedItemPosition;
                }

                List<SoftButtonCapabilities> softBtnCapList = null;
                if (addSoftButtonCapabilitiesCheckBox.Checked)
                {
                    softBtnCapList = btnSoftButtonCapList;
                }

                RpcResponse rpcMessage = BuildRpc.buildUiGetCapabilitiesResponse(BuildRpc.getNextId(), selectedResultCode, displayCap, audioCap, selectedHmiZoneCap, softBtnCapList, hmiCap, sysCap);
				AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
			});

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
}
            });
            rpcAlertDialog.Show();}

        public void CreateButtonsGetCapabilities()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.button_get_capabilities_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("Buttons.GetCapabilities");

            List<ButtonCapabilities> btnCap = new List<ButtonCapabilities>();

            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.get_capabilities_list_view);
            var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, btnCap, ViewStates.Invisible, buttonCapabilitiesListView);
            buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;

            Button addCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.add_capabilities_button);
            CheckBox checkBoxOnScreenPresetsAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.on_screen_presets_available);
			Switch switchAllow = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox addButtonCapabilitiesChk = (CheckBox)rpcView.FindViewById(Resource.Id.add_button_capabilities_chk);
            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_tv);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.get_capabilities_result_code_spn);

            resultCodeCheckbox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxOnScreenPresetsAvailable.CheckedChange += (s, e) =>
			{
				switchAllow.Enabled = e.IsChecked;
			};


			addButtonCapabilitiesChk.CheckedChange += (sender, e) =>
            {
                addCapabilitiesButton.Enabled = e.IsChecked;
                buttonCapabilitiesListView.Enabled = e.IsChecked;
            };

            var resultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities);
                tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
				if (tmpObj.getButtonCapabilities() != null)
				{
					btnCap.AddRange(tmpObj.getButtonCapabilities());
					buttonCapabilitiesAdapter.NotifyDataSetChanged();
				}
				else
					addButtonCapabilitiesChk.Checked = false;

				if (tmpObj.getPresetBankCapabilities() != null && tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable() != null)
				{
					checkBoxOnScreenPresetsAvailable.Checked = true;
					switchAllow.Checked = (bool)tmpObj.getPresetBankCapabilities().getOnScreenPresetsAvailable();
				}
				else
				{
					checkBoxOnScreenPresetsAvailable.Checked = false;
					switchAllow.Enabled = false;
				}
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }
			
			
			addCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder btnCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View btnCapabilitiesView = inflater.Inflate(Resource.Layout.button_capabilities, null);
                btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
                btnCapabilitiesAlertDialog.SetTitle("Button Capabilities");

                CheckBox chkButtonName = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);
                Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);

                var namesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
                spnButtonNames.Adapter = namesAdapter;

                CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);
                CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);
                CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);

                chkButtonName.CheckedChange += (sender, e) => spnButtonNames.Enabled = e.IsChecked;

                btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    ButtonCapabilities btn = new ButtonCapabilities();
                    if (chkButtonName.Checked)
                    {
                        btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;
                    }

                    btn.shortPressAvailable = checkBoxShortPressAvailable.Checked;
                    btn.longPressAvailable = checkBoxLongPressAvailable.Checked;
                    btn.upDownAvailable = checkBoxUpDownAvailable.Checked;

                    btnCap.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                });

                btnCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    btnCapabilitiesAlertDialog.Dispose();
                });
                btnCapabilitiesAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                List<ButtonCapabilities> capabilities = null;
                HmiApiLib.Common.Enums.Result? resultCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    resultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                if (addButtonCapabilitiesChk.Checked)
                {
                    capabilities = btnCap;
                }

                PresetBankCapabilities presetCap = new PresetBankCapabilities();
				presetCap.onScreenPresetsAvailable = null;
				if (checkBoxOnScreenPresetsAvailable.Checked)
				{
					presetCap.onScreenPresetsAvailable = switchAllow.Checked;
					
				}
				RpcResponse rpcMessage = BuildRpc.buildButtonsGetCapabilitiesResponse(BuildRpc.getNextId(), capabilities, presetCap, resultCode);
				


                AppUtils.savePreferenceValueForRpc(this, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void CreateVIResponseGetVehicleData()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.vi_get_vehicle_data, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("VI.GetVehicleData");

            CheckBox result_code_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_chk);
            Spinner result_code_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_spinner);

            CheckBox gps_data_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_chk);
            Button gps_data_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_btn);

            CheckBox speed_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_chk);
            EditText speed_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_et);

            CheckBox rpm_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_chk);
            EditText rpm_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_et);

            CheckBox fuel_level_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_chk);
            EditText fuel_level_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_et);

            CheckBox fuel_level_state_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_chk);
            Spinner fuel_level_state_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_spinner);

            CheckBox instant_fuel_consumption_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_chk);
            EditText instant_fuel_consumption_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_et);

            CheckBox external_temp_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_chk);
            EditText external_temp_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_et);

            CheckBox vin_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_chk);
            EditText vin_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_et);

            CheckBox prndl_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_chk);
            Spinner prndl_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_spinner);

            CheckBox turnSignal_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_turnSignal_chk);
            Spinner turnSignal_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_turnSignal_spinner);

            CheckBox tire_pressure_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_chk);
            Button tire_pressure_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_btn);

            CheckBox odometer_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_chk);
            EditText odometer_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_et);

            CheckBox belt_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_chk);
            Button belt_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_button);

            CheckBox body_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_chk);
            Button body_info_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_button);

            CheckBox device_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_chk);
            Button device_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_button);

            CheckBox driver_braking_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_chk);
            Spinner driver_braking_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_spinner);

            CheckBox wiper_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_chk);
            Spinner wiper_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_spinner);

            CheckBox head_lamp_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_chk);
            Button head_lamp_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_btn);

            CheckBox engine_torque_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_chk);
            EditText engine_torque_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_et);

            CheckBox acc_padel_position_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_chk);
            EditText acc_padel_position_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_et);

            CheckBox steering_wheel_angle_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_chk);
            EditText steering_wheel_angle_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_et);

            CheckBox ecall_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_chk);
            Button ecall_info_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_btn);

            CheckBox airbag_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_chk);
            Button airbag_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_btn);

            CheckBox emergency_event_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_chk);
            Button emergency_event_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_btn);

            CheckBox cluster_modes_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_chk);
            Button cluster_modes_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_btn);

            CheckBox my_key_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_chk);
            Spinner my_key_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_spinner);

			CheckBox electronic_park_brake_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_chk);
			Spinner electronic_park_brake_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_spinner);

            CheckBox engine_oil_life_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_chk);
            EditText engine_oil_life_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_et);

            CheckBox fuel_range_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_chk);
            Button fuel_range_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_btn);
            ListView fuel_range_lv = (ListView)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_lv);
            List<FuelRange> fuelRangeList = null;

            var ElectronicParkBrakeStatus = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, electronicParkBrakeStatusArray);
            electronic_park_brake_status_spinner.Adapter = ElectronicParkBrakeStatus;

            var ResultCodeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            result_code_spinner.Adapter = ResultCodeAdapter;

            var ComponentVolumeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, ComponentVolumeStatusArray);
            fuel_level_state_spinner.Adapter = ComponentVolumeStatusAdapter;

            var PRNDLAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PRNDLArray);
            prndl_spinner.Adapter = PRNDLAdapter;

			var TurnSignalAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, TurnSignalArray);
			turnSignal_spinner.Adapter = TurnSignalAdapter;

            var VehicleDataEventStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataEventStatusArray);
            driver_braking_spinner.Adapter = VehicleDataEventStatusAdapter;

            var WiperStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, WiperStatusArray);
            wiper_status_spinner.Adapter = WiperStatusAdapter;

            var VehicleDataStatus = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataStatusArray);

			my_key_spinner.Adapter = WiperStatusAdapter;

			result_code_chk.CheckedChange += (s, e) => result_code_spinner.Enabled = e.IsChecked;
			gps_data_chk.CheckedChange += (s, e) => gps_data_btn.Enabled = e.IsChecked;
			speed_chk.CheckedChange += (s, e) => speed_et.Enabled = e.IsChecked;
			rpm_chk.CheckedChange += (s, e) => rpm_et.Enabled = e.IsChecked;
			fuel_level_chk.CheckedChange += (s, e) => fuel_level_et.Enabled = e.IsChecked;
			fuel_level_state_chk.CheckedChange += (s, e) => fuel_level_state_spinner.Enabled = e.IsChecked;
			instant_fuel_consumption_chk.CheckedChange += (s, e) => instant_fuel_consumption_et.Enabled = e.IsChecked;
			external_temp_chk.CheckedChange += (s, e) => external_temp_et.Enabled = e.IsChecked;
			vin_chk.CheckedChange += (s, e) => vin_et.Enabled = e.IsChecked;
			prndl_chk.CheckedChange += (s, e) => prndl_spinner.Enabled = e.IsChecked;
            turnSignal_chk.CheckedChange += (s, e) => turnSignal_spinner.Enabled = e.IsChecked;
			tire_pressure_chk.CheckedChange += (s, e) => tire_pressure_btn.Enabled = e.IsChecked;
			odometer_chk.CheckedChange += (s, e) => odometer_et.Enabled = e.IsChecked;
			belt_status_chk.CheckedChange += (s, e) => belt_status_button.Enabled = e.IsChecked;
			body_info_chk.CheckedChange += (s, e) => body_info_button.Enabled = e.IsChecked;
			device_status_chk.CheckedChange += (s, e) => device_status_button.Enabled = e.IsChecked;
			driver_braking_chk.CheckedChange += (s, e) => driver_braking_spinner.Enabled = e.IsChecked;
			wiper_status_chk.CheckedChange += (s, e) => wiper_status_spinner.Enabled = e.IsChecked;
			head_lamp_status_chk.CheckedChange += (s, e) => head_lamp_status_btn.Enabled = e.IsChecked;
			engine_torque_chk.CheckedChange += (s, e) => engine_torque_et.Enabled = e.IsChecked;
			acc_padel_position_chk.CheckedChange += (s, e) => acc_padel_position_et.Enabled = e.IsChecked;
			steering_wheel_angle_chk.CheckedChange += (s, e) => steering_wheel_angle_et.Enabled = e.IsChecked;
			ecall_info_chk.CheckedChange += (s, e) => ecall_info_btn.Enabled = e.IsChecked;
			airbag_status_chk.CheckedChange += (s, e) => airbag_status_btn.Enabled = e.IsChecked;
			emergency_event_chk.CheckedChange += (s, e) => emergency_event_btn.Enabled = e.IsChecked;
			cluster_modes_chk.CheckedChange += (s, e) => cluster_modes_btn.Enabled = e.IsChecked;
			my_key_chk.CheckedChange += (s, e) => my_key_spinner.Enabled = e.IsChecked;
			electronic_park_brake_status_chk.CheckedChange += (s, e) => electronic_park_brake_status_spinner.Enabled = e.IsChecked;
            engine_oil_life_chk.CheckedChange += (s, e) => engine_oil_life_et.Enabled = e.IsChecked;
            fuel_range_chk.CheckedChange += (s, e) =>
            {
                fuel_range_btn.Enabled = e.IsChecked;
                fuel_range_lv.Enabled = e.IsChecked;
            };

			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();

            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(this, tmpObj.getMethod());

            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }

            GPSData viGPSData = null;
            TireStatus viTireStatus = null;
            BeltStatus viBeltStatus = null;
            BodyInformation viBodyInformation = null;
            DeviceStatus viDeviceStatus = null;
            HeadLampStatus viHeadLampStatus = null;
            ECallInfo viECallInfo = null;
            AirbagStatus viAirbagStatus = null;
            EmergencyEvent viEmergencyEvent = null;
            ClusterModeStatus viClusterModeStatus = null;

            if (tmpObj != null)
            {
                result_code_spinner.SetSelection((int)tmpObj.getResultCode());
                viGPSData = tmpObj.getGps();
                if (null == viGPSData)
                {
                    gps_data_btn.Enabled = false;
                    gps_data_chk.Checked = false;
                }
                if (tmpObj.getSpeed() != null)
                {
                    speed_et.Text = tmpObj.getSpeed().ToString();
                }
                else
                {
                    speed_chk.Checked = false;
                    speed_et.Enabled = false;
                }
                if (tmpObj.getRpm() != null)
                {
                    rpm_et.Text = tmpObj.getRpm().ToString();
                }
                else
                {
                    rpm_et.Enabled = false;
                    rpm_chk.Checked = false;
                }
                if (tmpObj.getFuelLevel() != null)
                {
                    fuel_level_et.Text = tmpObj.getFuelLevel().ToString();
                }
                else
                {
                    fuel_level_et.Enabled = false;
                    fuel_level_chk.Checked = false;
                }
                if (tmpObj.getFuelLevel_State() != null)
                {
                    fuel_level_state_spinner.SetSelection((int)tmpObj.getFuelLevel_State());
                }
                else
                {
                    fuel_level_state_spinner.Enabled = false;
                    fuel_level_state_chk.Checked = false;
                }
                if (tmpObj.getInstantFuelConsumption() != null)
                {
                    instant_fuel_consumption_et.Text = tmpObj.getInstantFuelConsumption().ToString();
                }
                else
                {
                    instant_fuel_consumption_et.Enabled = false;
                    instant_fuel_consumption_chk.Checked = false;
                }
                if (tmpObj.getExternalTemperature() != null)
                {
                    external_temp_et.Text = tmpObj.getExternalTemperature().ToString();
                }
                else
                {
                    external_temp_et.Enabled = false;
                    external_temp_chk.Checked = false;
                }
                if (tmpObj.getVin() != null)
                {
                    vin_et.Text = tmpObj.getVin();
                }
                else
                {
                    vin_et.Enabled = false;
                    vin_chk.Checked = false;
                }

                if (tmpObj.getPrndl() != null)
                {
                    prndl_spinner.SetSelection((int)tmpObj.getPrndl());
                }
                else
                {
                    prndl_spinner.Enabled = false;
                    prndl_chk.Checked = false;
                }
                viTireStatus = tmpObj.getTirePressure();
                if (null == viTireStatus)
                {
                    tire_pressure_btn.Enabled = false;
                    tire_pressure_chk.Checked = false;
                }
                if (tmpObj.getTurnSignal() != null)
				{
					turnSignal_spinner.SetSelection((int)tmpObj.getTurnSignal());
				}
				else
				{
					turnSignal_spinner.Enabled = false;
					turnSignal_chk.Checked = false;
				}
                if (tmpObj.getOdometer() != null)
                {
                    odometer_et.Text = tmpObj.getOdometer().ToString();
                }
                else
                {
                    odometer_et.Enabled = false;
                    odometer_chk.Checked = false;
                }
                viBeltStatus = tmpObj.getBeltStatus();
                if (null == viBeltStatus)
                {
                    belt_status_button.Enabled = false;
                    belt_status_chk.Checked = false;
                }
                viBodyInformation = tmpObj.getBodyInformation();
                if (null == viBodyInformation)
                {
                    body_info_button.Enabled = false;
                    body_info_chk.Checked = false;
                }
                viDeviceStatus = tmpObj.getDeviceStatus();
                if (null == viDeviceStatus)
                {
                    device_status_button.Enabled = false;
                    device_status_chk.Checked = false;
                }
                if (tmpObj.getDriverBraking() != null)
                {
                    driver_braking_spinner.SetSelection((int)tmpObj.getDriverBraking());
                }
                else
                {
                    driver_braking_spinner.Enabled = false;
                    driver_braking_chk.Checked = false;
                }

                if (tmpObj.getWiperStatus() != null)
                {
                    wiper_status_spinner.SetSelection((int)tmpObj.getWiperStatus());
                }
                else
                {
                    wiper_status_spinner.Enabled = false;
                    wiper_status_chk.Checked = false;
                }
                viHeadLampStatus = tmpObj.getHeadLampStatus();
                if (null == viHeadLampStatus)
                {
                    head_lamp_status_btn.Enabled = false;
                    head_lamp_status_chk.Checked = false;
                }
                if (tmpObj.getEngineTorque() != null)
                {
                    engine_torque_et.Text = tmpObj.getEngineTorque().ToString();
                }
                else
                {
                    engine_torque_et.Enabled = false;
                    engine_torque_chk.Checked = false;
                }
                if (tmpObj.getAccPedalPosition() != null)
                {
                    acc_padel_position_et.Text = tmpObj.getAccPedalPosition().ToString();
                }
                else
                {
                    acc_padel_position_et.Enabled = false;
                    acc_padel_position_chk.Checked = false;
                }
                if (tmpObj.getSteeringWheelAngle() != null)
                {
                    steering_wheel_angle_et.Text = tmpObj.getSteeringWheelAngle().ToString();
                }
                else
                {
                    steering_wheel_angle_et.Enabled = false;
                    steering_wheel_angle_chk.Checked = false;
                }
                viECallInfo = tmpObj.getECallInfo();
                if (null == viECallInfo)
                {
                    ecall_info_btn.Enabled = false;
                    ecall_info_chk.Checked = false;
                }
                viAirbagStatus = tmpObj.getAirbagStatus();
                if (null == viAirbagStatus)
                {
                    airbag_status_btn.Enabled = false;
                    airbag_status_chk.Checked = false;
                }
                viEmergencyEvent = tmpObj.getEmergencyEvent();
                if (null == viEmergencyEvent)
                {
                    emergency_event_btn.Enabled = false;
                    emergency_event_chk.Checked = false;
                }
                viClusterModeStatus = tmpObj.getClusterModes();
                if (null == viClusterModeStatus)
                {
                    cluster_modes_btn.Enabled = false;
                    cluster_modes_chk.Checked = false;
                }
                if (tmpObj.getMyKey() != null)
                {
                    my_key_spinner.SetSelection((int)tmpObj.getMyKey().getE911Override());
                }
                else
                {
                    my_key_spinner.Enabled = false;
                    my_key_chk.Checked = false;
                }
                if (tmpObj.getElectronicParkBrakeStatus() != null)
                {
                    electronic_park_brake_status_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus());
                }
                else
                {
                    electronic_park_brake_status_spinner.Enabled = false;
                    electronic_park_brake_status_chk.Checked = false;
                }
                if (tmpObj.getEngineOilLife() != null)
                {
                    engine_oil_life_et.Text = ((float)tmpObj.getEngineOilLife()).ToString();
                }
                else
                {
                    engine_oil_life_et.Enabled = false;
                    engine_oil_life_chk.Checked = false;
                }
                fuelRangeList = tmpObj.getFuelRange();
			}

            if (fuelRangeList == null)
            {
                fuelRangeList = new List<FuelRange>();
                fuel_range_btn.Enabled = false;
                fuel_range_chk.Checked = false;
            }

            var fuelRangeAdapter = new FuelRangeAdapter(this, fuelRangeList);
            fuel_range_lv.Adapter = fuelRangeAdapter;
            Utility.setListViewHeightBasedOnChildren(fuel_range_lv);

            gps_data_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder gpsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View gpsView = inflater.Inflate(Resource.Layout.vi_gps_data, null);
                gpsAlertDialog.SetView(gpsView);
                gpsAlertDialog.SetTitle("GPS Data");

                CheckBox chkLongitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_chk);
                EditText etLongitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_et);

                CheckBox chkLatitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_chk);
                EditText etLatitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_et);

                CheckBox chkUtcYear = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_chk);
                EditText etUtcYear = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_et);

                CheckBox chkUtcMonth = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_chk);
                EditText etUtcMonth = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_et);

                CheckBox chkUtcDay = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_chk);
                EditText etUtcDay = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_et);

                CheckBox chkUtcHours = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_chk);
                EditText etUtcHours = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_et);

                CheckBox chkUtcMinutes = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_chk);
                EditText etUtcMinutes = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_et);

                CheckBox chkUtcSeconds = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_chk);
                EditText etUtcSeconds = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_et);

                CheckBox chkCompassDirection = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_chk);
                Spinner spnCompassDirection = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_spinner);

                CheckBox chkpdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_chk);
                EditText etpdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_et);

                CheckBox chkhdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_chk);
                EditText ethdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_et);

                CheckBox chkvdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_chk);
                EditText etvdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_et);

                CheckBox chkActual = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_actual_chk);

                CheckBox chkSatellites = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_chk);
                EditText etSatellites = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_et);

                CheckBox chkDimension = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_chk);
                Spinner spnDimension = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_spinner);

                CheckBox chkAltitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_chk);
                EditText etAltitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_et);

                CheckBox chkHeading = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_chk);
                EditText etHeading = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_et);

                CheckBox chkSpeed = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_chk);
                EditText etSpeed = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_et);
				CheckBox chkShifted = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_shifted_chk);
				Switch tglShifted = (Switch)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_shifted_tgl);

				var CompassDirectionAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, CompassDirectionArray);
                spnCompassDirection.Adapter = CompassDirectionAdapter;

                var DimensionAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, DimensionArray);
                spnDimension.Adapter = DimensionAdapter;

                chkLongitude.CheckedChange += (sender1, e1) => etLongitude.Enabled = e1.IsChecked;
                chkLatitude.CheckedChange += (sender1, e1) => etLatitude.Enabled = e1.IsChecked;
                chkUtcYear.CheckedChange += (sender1, e1) => etUtcYear.Enabled = e1.IsChecked;
                chkUtcMonth.CheckedChange += (sender1, e1) => etUtcMonth.Enabled = e1.IsChecked;
                chkUtcDay.CheckedChange += (sender1, e1) => etUtcDay.Enabled = e1.IsChecked;
                chkUtcHours.CheckedChange += (sender1, e1) => etUtcHours.Enabled = e1.IsChecked;
                chkUtcMinutes.CheckedChange += (sender1, e1) => etUtcMinutes.Enabled = e1.IsChecked;
                chkUtcSeconds.CheckedChange += (sender1, e1) => etUtcSeconds.Enabled = e1.IsChecked;
                chkCompassDirection.CheckedChange += (sender1, e1) => spnCompassDirection.Enabled = e1.IsChecked;
                chkpdop.CheckedChange += (sender1, e1) => etpdop.Enabled = e1.IsChecked;
                chkhdop.CheckedChange += (sender1, e1) => ethdop.Enabled = e1.IsChecked;
                chkvdop.CheckedChange += (sender1, e1) => etvdop.Enabled = e1.IsChecked;
                chkSatellites.CheckedChange += (sender1, e1) => etSatellites.Enabled = e1.IsChecked;
                chkDimension.CheckedChange += (sender1, e1) => spnDimension.Enabled = e1.IsChecked;
                chkAltitude.CheckedChange += (sender1, e1) => etAltitude.Enabled = e1.IsChecked;
                chkHeading.CheckedChange += (sender1, e1) => etHeading.Enabled = e1.IsChecked;
                chkSpeed.CheckedChange += (sender1, e1) => etSpeed.Enabled = e1.IsChecked;
				chkShifted.CheckedChange += (sender1, e1) => tglShifted.Enabled = e1.IsChecked;

				if (viGPSData != null)
                {
                    if (null != viGPSData.getLongitudeDegrees())
                    {
                        etLongitude.Text = viGPSData.getLongitudeDegrees().ToString();
                    }
                    else
                    {
                        etLongitude.Enabled = false;
                        chkLongitude.Checked = false;
                    }
                    if (null != viGPSData.getLatitudeDegrees())
                    {
                        etLatitude.Text = viGPSData.getLatitudeDegrees().ToString();
                    }
                    else
                    {
                        etLatitude.Enabled = false;
                        chkLatitude.Checked = false;
                    }
                    if (null != viGPSData.getUtcYear())
                    {
                        etUtcYear.Text = viGPSData.getUtcYear().ToString();
                    }
                    else
                    {
                        etUtcYear.Enabled = false;
                        chkUtcYear.Checked = false;
                    }
                    if (null != viGPSData.getUtcMonth())
                    {
                        etUtcMonth.Text = viGPSData.getUtcMonth().ToString();
                    }
                    else
                    {
                        etUtcMonth.Enabled = false;
                        chkUtcMonth.Checked = false;
                    }
                    if (null != viGPSData.getUtcDay())
                    {
                        etUtcDay.Text = viGPSData.getUtcDay().ToString();
                    }
                    else
                    {
                        etUtcDay.Enabled = false;
                        chkUtcDay.Checked = false;
                    }
                    if (null != viGPSData.getUtcHours())
                    {
                        etUtcHours.Text = viGPSData.getUtcHours().ToString();
                    }
                    else
                    {
                        etUtcHours.Enabled = false;
                        chkUtcHours.Checked = false;
                    }
                    if (null != viGPSData.getUtcMinutes())
                    {
                        etUtcMinutes.Text = viGPSData.getUtcMinutes().ToString();
                    }
                    else
                    {
                        etUtcMinutes.Enabled = false;
                        chkUtcMinutes.Checked = false;
                    }
                    if (null != viGPSData.getUtcSeconds())
                    {
                        etUtcSeconds.Text = viGPSData.getUtcSeconds().ToString();
                    }
                    else
                    {
                        etUtcSeconds.Enabled = false;
                        chkUtcSeconds.Checked = false;
                    }
                    if (null != viGPSData.getCompassDirection())
                    {
                        spnCompassDirection.SetSelection((int)viGPSData.getCompassDirection());
                    }
                    else
                    {
                        spnCompassDirection.Enabled = false;
                        chkCompassDirection.Checked = false;
                    }
                    if (null != viGPSData.getPdop())
                    {
                        etpdop.Text = viGPSData.getPdop().ToString();
                    }
                    else
                    {
                        etpdop.Enabled = false;
                        chkpdop.Checked = false;
                    }
                    if (null != viGPSData.getHdop())
                    {
                        ethdop.Text = viGPSData.getHdop().ToString();
                    }
                    else
                    {
                        ethdop.Enabled = false;
                        chkhdop.Checked = false;
                    }
                    if (null != viGPSData.getVdop())
                    {
                        etvdop.Text = viGPSData.getVdop().ToString();
                    }
                    else
                    {
                        etvdop.Enabled = false;
                        chkvdop.Checked = false;
                    }
                    if (null != viGPSData.getActual())
                        chkActual.Checked = (bool)viGPSData.getActual();
                    if (null != viGPSData.getSatellites())
                    {
                        etSatellites.Text = viGPSData.getSatellites().ToString();
                    }
                    else
                    {
                        etSatellites.Enabled = false;
                        chkSatellites.Checked = false;
                    }
                    if (null != viGPSData.getDimension())
                    {
                        spnDimension.SetSelection((int)viGPSData.getDimension());
                    }
                    else
                    {
                        spnDimension.Enabled = false;
                        chkDimension.Checked = false;
                    }
                    if (null != viGPSData.getAltitude())
                    {
                        etAltitude.Text = viGPSData.getAltitude().ToString();
                    }
                    else
                    {
                        etAltitude.Enabled = false;
                        chkAltitude.Checked = false;
                    }
                    if (null != viGPSData.getHeading())
                    {
                        etHeading.Text = viGPSData.getHeading().ToString();
                    }
                    else
                    {
                        etHeading.Enabled = false;
                        chkHeading.Checked = false;
                    }
                    if (null != viGPSData.getSpeed())
                    {
                        etSpeed.Text = viGPSData.getSpeed().ToString();
                    }
                    else
                    {
                        etSpeed.Enabled = false;
                        chkSpeed.Checked = false;
                    }
					if (null != viGPSData.getShifted())
					{
						tglShifted.Checked = (bool)viGPSData.getShifted();
					}
					else
					{
						tglShifted.Enabled = false;
						chkShifted.Checked = false;
					}
				}
                gpsAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    gpsAlertDialog.Dispose();
                });
                gpsAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viGPSData = new GPSData();
                    if (chkLongitude.Checked && etLongitude.Text != null && etLongitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.longitudeDegrees = Java.Lang.Float.ParseFloat(etLongitude.Text);
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                    if (chkLatitude.Checked && etLatitude.Text != null && etLatitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.latitudeDegrees = Java.Lang.Float.ParseFloat(etLatitude.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcYear.Checked && etUtcYear.Text != null && etUtcYear.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcYear = Java.Lang.Integer.ParseInt(etUtcYear.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcMonth.Checked && etUtcMonth.Text != null && etUtcMonth.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcMonth = Java.Lang.Integer.ParseInt(etUtcMonth.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcDay.Checked && etUtcDay.Text != null && etUtcDay.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcDay = Java.Lang.Integer.ParseInt(etUtcDay.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcHours.Checked && etUtcHours.Text != null && etUtcHours.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcHours = Java.Lang.Integer.ParseInt(etUtcHours.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcMinutes.Checked && etUtcMinutes.Text != null && etUtcMinutes.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcMinutes = Java.Lang.Integer.ParseInt(etUtcMinutes.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkUtcSeconds.Checked && etUtcSeconds.Text != null && etUtcSeconds.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcSeconds = Java.Lang.Integer.ParseInt(etUtcSeconds.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkCompassDirection.Checked)
                    {
                        viGPSData.compassDirection = (CompassDirection)spnCompassDirection.SelectedItemPosition;
                    }
                    if (chkpdop.Checked && etpdop.Text != null && etpdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.pdop = Java.Lang.Float.ParseFloat(etpdop.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkhdop.Checked && ethdop.Text != null && ethdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.hdop = Java.Lang.Float.ParseFloat(ethdop.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkvdop.Checked && etvdop.Text != null && etvdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.vdop = Java.Lang.Float.ParseFloat(etvdop.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    viGPSData.actual = chkActual.Checked;
                    if (chkSatellites.Checked && etSatellites.Text != null && etSatellites.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.satellites = Java.Lang.Integer.ParseInt(etSatellites.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkDimension.Checked)
                    {
                        viGPSData.dimension = (Dimension)spnDimension.SelectedItemPosition;
                    }
                    if (chkAltitude.Checked && etAltitude.Text != null && etAltitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.altitude = Java.Lang.Float.ParseFloat(etAltitude.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkHeading.Checked && etHeading.Text != null && etHeading.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.heading = Java.Lang.Float.ParseFloat(etHeading.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (chkSpeed.Checked && etSpeed.Text != null && etSpeed.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.speed = Java.Lang.Float.ParseFloat(etSpeed.Text);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
					if (chkShifted.Checked)
					{
						try
						{
							viGPSData.shifted = tglShifted.Checked;
						}
						catch (Exception ex) { }
					}
				});
                gpsAlertDialog.Show();
            };

            tire_pressure_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder tirePressureAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View tirePressureView = inflater.Inflate(Resource.Layout.vi_tire_status, null);
                tirePressureAlertDialog.SetView(tirePressureView);
                tirePressureAlertDialog.SetTitle("Tire Pressure");

                CheckBox pressure_tell_tale_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_chk);
                Spinner pressure_tell_tale_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_spinner);

                CheckBox left_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_chk);
                Spinner left_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_spinner);

                CheckBox right_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_chk);
                Spinner right_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_spinner);

                CheckBox left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_chk);
                Spinner left_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_spinner);

                CheckBox right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_chk);
                Spinner right_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_spinner);

                CheckBox inner_left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_chk);
                Spinner inner_left_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_spinner);

                CheckBox inner_right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_chk);
                Spinner inner_right_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_spinner);

                var WarningLightStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, WarningLightStatusArray);
                pressure_tell_tale_spn.Adapter = WarningLightStatusAdapter;

                var SingleTireStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, SingleTireStatusArray);
                left_front_spn.Adapter = SingleTireStatusAdapter;
                right_front_spn.Adapter = SingleTireStatusAdapter;
                left_rear_spn.Adapter = SingleTireStatusAdapter;
                right_rear_spn.Adapter = SingleTireStatusAdapter;
                inner_left_rear_spinner.Adapter = SingleTireStatusAdapter;
                inner_right_rear_spinner.Adapter = SingleTireStatusAdapter;

                pressure_tell_tale_chk.CheckedChange += (sender1, e1) => pressure_tell_tale_spn.Enabled = e1.IsChecked;
                left_front_chk.CheckedChange += (sender1, e1) => left_front_spn.Enabled = e1.IsChecked;
                right_front_chk.CheckedChange += (sender1, e1) => right_front_spn.Enabled = e1.IsChecked;
                left_rear_chk.CheckedChange += (sender1, e1) => left_rear_spn.Enabled = e1.IsChecked;
                right_rear_chk.CheckedChange += (sender1, e1) => right_rear_spn.Enabled = e1.IsChecked;
                inner_left_rear_chk.CheckedChange += (sender1, e1) => inner_left_rear_spinner.Enabled = e1.IsChecked;
                inner_right_rear_chk.CheckedChange += (sender1, e1) => inner_right_rear_spinner.Enabled = e1.IsChecked;

                if (viTireStatus != null)
                {
                    if (null != viTireStatus.getPressureTelltale())
                    {
                        pressure_tell_tale_spn.SetSelection((int)viTireStatus.getPressureTelltale());
                    }
                    else
                    {
                        pressure_tell_tale_spn.Enabled = false;
                        pressure_tell_tale_chk.Checked = false;
                    }

                    if (viTireStatus.getleftFront() != null && (null != viTireStatus.getleftFront().getStatus()))
                    {
                        left_front_spn.SetSelection((int)viTireStatus.getleftFront().getStatus());
                    }
                    else
                    {
                        left_front_spn.Enabled = false;
                        left_front_chk.Checked = false;
                    }
                    if (viTireStatus.getRightFront() != null && (null != viTireStatus.getRightFront().getStatus()))
                    {
                        right_front_spn.SetSelection((int)viTireStatus.getRightFront().getStatus());
                    }
                    else
                    {
                        right_front_spn.Enabled = false;
                        right_front_chk.Checked = false;
                    }
                    if (viTireStatus.getLeftRear() != null && (null != viTireStatus.getLeftRear().getStatus()))
                    {
                        left_rear_spn.SetSelection((int)viTireStatus.getLeftRear().getStatus());
                    }
                    else
                    {
                        left_rear_spn.Enabled = false;
                        left_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getRightRear() != null && (null != viTireStatus.getRightRear().getStatus()))
                    {
                        right_rear_spn.SetSelection((int)viTireStatus.getRightRear().getStatus());
                    }
                    else
                    {
                        right_rear_spn.Enabled = false;
                        right_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getInnerLeftRear() != null && (null != viTireStatus.getInnerLeftRear().getStatus()))
                    {
                        inner_left_rear_spinner.SetSelection((int)viTireStatus.getInnerLeftRear().getStatus());
                    }
                    else
                    {
                        inner_left_rear_spinner.Enabled = false;
                        inner_left_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getInnerRightRear() != null && (null != viTireStatus.getInnerRightRear().getStatus()))
                    {
                        inner_right_rear_spinner.SetSelection((int)viTireStatus.getInnerRightRear().getStatus());
                    }
                    else
                    {
                        inner_right_rear_spinner.Enabled = false;
                        inner_right_rear_chk.Checked = false;
                    }
                }

                tirePressureAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    tirePressureAlertDialog.Dispose();
                });
                tirePressureAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viTireStatus = new TireStatus();
                    if (pressure_tell_tale_chk.Checked)
                    {
                        viTireStatus.pressureTelltale = (WarningLightStatus)pressure_tell_tale_spn.SelectedItemPosition;
                    }
                    if (left_front_chk.Checked)
                    {
                        SingleTireStatus leftFront = new SingleTireStatus();
                        leftFront.status = (ComponentVolumeStatus)left_front_spn.SelectedItemPosition;
                        viTireStatus.leftFront = leftFront;
                    }
                    if (right_front_chk.Checked)
                    {
                        SingleTireStatus right_front = new SingleTireStatus();
                        right_front.status = (ComponentVolumeStatus)right_front_spn.SelectedItemPosition;
                        viTireStatus.rightFront = right_front;
                    }
                    if (left_rear_chk.Checked)
                    {
                        SingleTireStatus left_rear = new SingleTireStatus();
                        left_rear.status = (ComponentVolumeStatus)left_rear_spn.SelectedItemPosition;
                        viTireStatus.leftRear = left_rear;
                    }
                    if (right_rear_chk.Checked)
                    {
                        SingleTireStatus right_rear = new SingleTireStatus();
                        right_rear.status = (ComponentVolumeStatus)right_rear_spn.SelectedItemPosition;
                        viTireStatus.rightRear = right_rear;
                    }
                    if (inner_left_rear_chk.Checked)
                    {
                        SingleTireStatus inner_left_rear = new SingleTireStatus();
                        inner_left_rear.status = (ComponentVolumeStatus)inner_left_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerLeftRear = inner_left_rear;
                    }
                    if (inner_right_rear_chk.Checked)
                    {
                        SingleTireStatus inner_right_rear = new SingleTireStatus();
                        inner_right_rear.status = (ComponentVolumeStatus)inner_right_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerRightRear = inner_right_rear;
                    }
                });
                tirePressureAlertDialog.Show();
            };

            belt_status_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder beltStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View beltStatusView = inflater.Inflate(Resource.Layout.vi_belt_status, null);
                beltStatusAlertDialog.SetView(beltStatusView);
                beltStatusAlertDialog.SetTitle("Belt Status");

                CheckBox driver_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_chk);
                Spinner driver_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_spinner);

                CheckBox passenger_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_chk);
                Spinner passenger_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_spinner);

                CheckBox passenger_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_chk);
                Spinner passenger_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_spinner);

                CheckBox driver_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_chk);
                Spinner driver_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_spinner);

                CheckBox left_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_chk);
                Spinner left_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_spinner);

                CheckBox passenger_child_detected_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_chk);
                Spinner passenger_child_detected_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_spinner);

                CheckBox right_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_chk);
                Spinner right_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_spinner);

                CheckBox middle_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_chk);
                Spinner middle_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_spinner);

                CheckBox middle_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_chk);
                Spinner middle_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_spinner);

                CheckBox left_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_chk);
                Spinner left_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_spinner);

                CheckBox right_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_chk);
                Spinner right_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_spinner);

                CheckBox left_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_chk);
                Spinner left_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_spinner);

                CheckBox right_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_chk);
                Spinner right_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_spinner);

                CheckBox middle_row_1_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_chk);
                Spinner middle_row_1_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_spinner);

                CheckBox middle_row_1_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_chk);
                Spinner middle_row_1_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_spinner);

                driver_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_child_detected_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                left_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                right_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_1_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                middle_row_1_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;

                if (viBeltStatus != null)
                {
                    if (null != viBeltStatus.getDriverBeltDeployed())
                    {
                        driver_belt_deployed_spinner.SetSelection((int)viBeltStatus.getDriverBeltDeployed());
                    }
                    else
                    {
                        driver_belt_deployed_spinner.Enabled = false;
                        driver_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerBeltDeployed())
                    {
                        passenger_belt_deployed_spinner.SetSelection((int)viBeltStatus.getPassengerBeltDeployed());
                    }
                    else
                    {
                        passenger_belt_deployed_spinner.Enabled = false;
                        passenger_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerBuckleBelted())
                    {
                        passenger_buckle_belted_spinner.SetSelection((int)viBeltStatus.getPassengerBuckleBelted());
                    }
                    else
                    {
                        passenger_buckle_belted_spinner.Enabled = false;
                        passenger_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getDriverBuckleBelted())
                    {
                        driver_buckle_belted_spinner.SetSelection((int)viBeltStatus.getDriverBuckleBelted());
                    }
                    else
                    {
                        driver_buckle_belted_spinner.Enabled = false;
                        driver_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRow2BuckleBelted())
                    {
                        left_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow2BuckleBelted());
                    }
                    else
                    {
                        left_row_2_buckle_belted_spinner.Enabled = false;
                        left_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerChildDetected())
                    {
                        passenger_child_detected_spinner.SetSelection((int)viBeltStatus.getPassengerChildDetected());
                    }
                    else
                    {
                        passenger_child_detected_spinner.Enabled = false;
                        passenger_child_detected_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRow2BuckleBelted())
                    {
                        right_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow2BuckleBelted());
                    }
                    else
                    {
                        right_row_2_buckle_belted_spinner.Enabled = false;
                        right_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow2BuckleBelted())
                    {
                        middle_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow2BuckleBelted());
                    }
                    else
                    {
                        middle_row_2_buckle_belted_spinner.Enabled = false;
                        middle_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow3BuckleBelted())
                    {
                        middle_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow3BuckleBelted());
                    }
                    else
                    {
                        middle_row_3_buckle_belted_spinner.Enabled = false;
                        middle_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRow3BuckleBelted())
                    {
                        left_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow3BuckleBelted());
                    }
                    else
                    {
                        left_row_3_buckle_belted_spinner.Enabled = false;
                        left_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRow3BuckleBelted())
                    {
                        right_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow3BuckleBelted());
                    }
                    else
                    {
                        right_row_3_buckle_belted_spinner.Enabled = false;
                        right_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRearInflatableBelted())
                    {
                        left_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getLeftRearInflatableBelted());
                    }
                    else
                    {
                        left_rear_inflatable_belted_spinner.Enabled = false;
                        left_rear_inflatable_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRearInflatableBelted())
                    {
                        right_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getRightRearInflatableBelted());
                    }
                    else
                    {
                        right_rear_inflatable_belted_spinner.Enabled = false;
                        right_rear_inflatable_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow1BeltDeployed())
                    {
                        middle_row_1_belt_deployed_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BeltDeployed());
                    }
                    else
                    {
                        middle_row_1_belt_deployed_spinner.Enabled = false;
                        middle_row_1_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow1BuckleBelted())
                    {
                        middle_row_1_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BuckleBelted());
                    }
                    else
                    {
                        middle_row_1_buckle_belted_spinner.Enabled = false;
                        middle_row_1_buckle_belted_chk.Checked = false;
                    }
                }

                driver_belt_deployed_chk.CheckedChange += (sender1, e1) => driver_belt_deployed_spinner.Enabled = e1.IsChecked;
                passenger_belt_deployed_chk.CheckedChange += (sender1, e1) => passenger_belt_deployed_spinner.Enabled = e1.IsChecked;
                passenger_buckle_belted_chk.CheckedChange += (sender1, e1) => passenger_buckle_belted_spinner.Enabled = e1.IsChecked;
                driver_buckle_belted_chk.CheckedChange += (sender1, e1) => driver_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                passenger_child_detected_chk.CheckedChange += (sender1, e1) => passenger_child_detected_spinner.Enabled = e1.IsChecked;
                right_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                middle_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
                middle_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                right_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
                left_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => left_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
                right_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => right_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
                middle_row_1_belt_deployed_chk.CheckedChange += (sender1, e1) => middle_row_1_belt_deployed_spinner.Enabled = e1.IsChecked;
                middle_row_1_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_1_buckle_belted_spinner.Enabled = e1.IsChecked;

                beltStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    beltStatusAlertDialog.Dispose();
                });
                beltStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viBeltStatus = new BeltStatus();
                    if (driver_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.driverBeltDeployed = (VehicleDataEventStatus)driver_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.passengerBeltDeployed = (VehicleDataEventStatus)passenger_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.passengerBuckleBelted = (VehicleDataEventStatus)passenger_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (driver_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.driverBuckleBelted = (VehicleDataEventStatus)driver_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.leftRow2BuckleBelted = (VehicleDataEventStatus)left_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (passenger_child_detected_chk.Checked)
                    {
                        viBeltStatus.passengerChildDetected = (VehicleDataEventStatus)passenger_child_detected_spinner.SelectedItemPosition;
                    }
                    if (right_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.rightRow2BuckleBelted = (VehicleDataEventStatus)right_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_2_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow2BuckleBelted = (VehicleDataEventStatus)middle_row_2_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow3BuckleBelted = (VehicleDataEventStatus)middle_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.leftRow3BuckleBelted = (VehicleDataEventStatus)left_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (right_row_3_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.rightRow3BuckleBelted = (VehicleDataEventStatus)right_row_3_buckle_belted_spinner.SelectedItemPosition;
                    }
                    if (left_rear_inflatable_belted_chk.Checked)
                    {
                        viBeltStatus.leftRearInflatableBelted = (VehicleDataEventStatus)left_rear_inflatable_belted_spinner.SelectedItemPosition;
                    }
                    if (right_rear_inflatable_belted_chk.Checked)
                    {
                        viBeltStatus.rightRearInflatableBelted = (VehicleDataEventStatus)right_rear_inflatable_belted_spinner.SelectedItemPosition;
                    }
                    if (middle_row_1_belt_deployed_chk.Checked)
                    {
                        viBeltStatus.middleRow1BeltDeployed = (VehicleDataEventStatus)middle_row_1_belt_deployed_spinner.SelectedItemPosition;
                    }
                    if (middle_row_1_buckle_belted_chk.Checked)
                    {
                        viBeltStatus.middleRow1BuckleBelted = (VehicleDataEventStatus)middle_row_1_buckle_belted_spinner.SelectedItemPosition;
                    }
                });
                beltStatusAlertDialog.Show();
            };

            body_info_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder bodyInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View bodyInfoView = inflater.Inflate(Resource.Layout.vi_body_information, null);
                bodyInfoAlertDialog.SetView(bodyInfoView);
                bodyInfoAlertDialog.SetTitle("Body Info");

                CheckBox park_brake_active = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active);
                Switch park_brake_active_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active_tgl);

                CheckBox ignition_stable_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_chk);
                Spinner ignition_stable_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_spinner);

                CheckBox ignition_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_chk);
                Spinner ignition_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_spinner);

                CheckBox driver_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar);
                Switch driver_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar_tgl);

                CheckBox passenger_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar);
                Switch passenger_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar_tgl);

                CheckBox rear_left_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar);
                Switch rear_left_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar_tgl);

                CheckBox rear_right_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar);
                Switch rear_right_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar_tgl);

                var IgnitionStableStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStableStatusArray);
                ignition_stable_status_spinner.Adapter = IgnitionStableStatusAdapter;

                var IgnitionStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStatusArray);
                ignition_status_spinner.Adapter = IgnitionStatusAdapter;

				park_brake_active.CheckedChange += (sender1, e1) =>
				{
					park_brake_active_tgl.Enabled = e1.IsChecked;
				};
				ignition_stable_status_chk.CheckedChange += (sender1, e1) =>ignition_stable_status_spinner.Enabled = e1.IsChecked;
				
				ignition_status_chk.CheckedChange += (sender1, e1) =>ignition_status_spinner.Enabled = e1.IsChecked;
				
				driver_door_ajar.CheckedChange += (sender1, e1) =>
				{
					driver_door_ajar_tgl.Enabled = e1.IsChecked;
				};
				passenger_door_ajar.CheckedChange += (sender1, e1) =>
				{
					passenger_door_ajar_tgl.Enabled = e1.IsChecked;
				};
				rear_left_door_ajar.CheckedChange += (sender1, e1) =>
				{
					rear_left_door_ajar_tgl.Enabled = e1.IsChecked;
				};
				rear_right_door_ajar.CheckedChange += (sender1, e1) =>
				{
					rear_right_door_ajar_tgl.Enabled = e1.IsChecked;
				};

                if (viBodyInformation != null)
                {
                    if (null != viBodyInformation.getParkBrakeActive())
                    {
                        park_brake_active_tgl.Checked = (bool)viBodyInformation.getParkBrakeActive();
                    }
                    else
                    {
                        park_brake_active.Checked = false;
                        park_brake_active_tgl.Enabled = false;
                    }
                        
                    if (null != viBodyInformation.getIgnitionStableStatus())
                    {
                        ignition_stable_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStableStatus());
                    }
                    else
                    {
                        ignition_stable_status_chk.Checked = false;
                        ignition_stable_status_spinner.Enabled = false;
                    }
                        
                    if (null != viBodyInformation.getIgnitionStatus())
                    {
                        ignition_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStatus());
                    }
                    else
                    {
                        ignition_status_spinner.Enabled = false;
                        ignition_status_chk.Checked = false;
                    }
                        
                    if (null != viBodyInformation.getDriverDoorAjar())
                    {
                        driver_door_ajar_tgl.Checked = (bool)viBodyInformation.getDriverDoorAjar();
                    }
                    else
                    {
                        driver_door_ajar.Checked = false;
                        driver_door_ajar_tgl.Enabled = false;
                    }
                        
                    if (null != viBodyInformation.getPassengerDoorAjar())
                    {
                        passenger_door_ajar_tgl.Checked = (bool)viBodyInformation.getPassengerDoorAjar();
                    }
                    else
                    {
                        passenger_door_ajar.Checked = false;
                        passenger_door_ajar_tgl.Enabled = false;
                    }

                        
                    if (null != viBodyInformation.getRearLeftDoorAjar())
                    {
                        rear_left_door_ajar_tgl.Checked = (bool)viBodyInformation.getRearLeftDoorAjar();
                    }
                    else
                    {
                        rear_left_door_ajar.Checked = false;
                        rear_left_door_ajar_tgl.Enabled = false;
                    }
                        
                    if (null != viBodyInformation.getRearRightDoorAjar())
                    {
                        rear_right_door_ajar_tgl.Checked = (bool)viBodyInformation.getRearRightDoorAjar();
                    }
                    else
                    {
                        rear_right_door_ajar.Checked = false;
                        rear_right_door_ajar_tgl.Enabled = false;
                    }    
                }

                bodyInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    bodyInfoAlertDialog.Dispose();
                });
                bodyInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viBodyInformation = new BodyInformation();
                    if (park_brake_active.Checked)
                        viBodyInformation.parkBrakeActive = park_brake_active_tgl.Checked;
                    if (ignition_stable_status_chk.Checked)
                    {
                        viBodyInformation.ignitionStableStatus = (IgnitionStableStatus)ignition_stable_status_spinner.SelectedItemPosition;
                    }
                    if (ignition_status_chk.Checked)
                    {
                        viBodyInformation.ignitionStatus = (IgnitionStatus)ignition_status_spinner.SelectedItemPosition;
                    }
                    if (driver_door_ajar.Checked)
                        viBodyInformation.driverDoorAjar = driver_door_ajar_tgl.Checked;
                    if (passenger_door_ajar.Checked)
                        viBodyInformation.passengerDoorAjar = passenger_door_ajar_tgl.Checked;
                    if (rear_left_door_ajar.Checked)
                        viBodyInformation.rearLeftDoorAjar = rear_left_door_ajar_tgl.Checked;
                    if (rear_right_door_ajar.Checked)
                        viBodyInformation.rearRightDoorAjar = rear_right_door_ajar_tgl.Checked;
                });
                bodyInfoAlertDialog.Show();
            };

            device_status_button.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder deviceStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View deviceStatusView = inflater.Inflate(Resource.Layout.vi_device_status, null);
                deviceStatusAlertDialog.SetView(deviceStatusView);
                deviceStatusAlertDialog.SetTitle("Device Status");

                CheckBox voice_rec_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on);
                Switch voice_rec_on_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on_tgl);

                CheckBox bt_icon_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on);
                Switch bt_icon_on_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on_tgl);

                CheckBox call_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_call_active);
                Switch call_active_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_call_active_tgl);

                CheckBox phone_roaming = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming);
                Switch phone_roaming_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming_tgl);

                CheckBox text_msg_available = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available);
                Switch text_msg_available_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available_tgl);

                CheckBox batt_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_chk);
                Spinner batt_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_spinner);

                CheckBox stereo_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted);
                Switch stereo_audio_output_muted_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted_tgl);

                CheckBox mono_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted);
                Switch mono_audio_output_muted_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted_tgl);

                CheckBox signal_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_chk);
                Spinner signal_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_spinner);

                CheckBox primary_audio_source_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_chk);
                Spinner primary_audio_source_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_spinner);

                CheckBox ecall_event_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active);
                Switch ecall_event_active_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active_tgl);

                var battLevelStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, DeviceLevelStatusArray);
                batt_level_status_spinner.Adapter = battLevelStatusAdapter;
                signal_level_status_spinner.Adapter = battLevelStatusAdapter;

                var PrimaryAudioSourceAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PrimaryAudioSourceArray);
                primary_audio_source_spinner.Adapter = PrimaryAudioSourceAdapter;

                voice_rec_on.CheckedChange += (sender1, e1) => voice_rec_on_tgl.Enabled = e1.IsChecked;
                bt_icon_on.CheckedChange += (sender1, e1) => bt_icon_on_tgl.Enabled = e1.IsChecked;
                call_active.CheckedChange += (sender1, e1) => call_active_tgl.Enabled = e1.IsChecked;
                phone_roaming.CheckedChange += (sender1, e1) => phone_roaming_tgl.Enabled = e1.IsChecked;
                text_msg_available.CheckedChange += (sender1, e1) => text_msg_available_tgl.Enabled = e1.IsChecked;
                batt_level_status_chk.CheckedChange += (sender1, e1) => batt_level_status_spinner.Enabled = e1.IsChecked;
                stereo_audio_output_muted.CheckedChange += (sender1, e1) => stereo_audio_output_muted_tgl.Enabled = e1.IsChecked;
                mono_audio_output_muted.CheckedChange += (sender1, e1) => mono_audio_output_muted_tgl.Enabled = e1.IsChecked;
                signal_level_status_chk.CheckedChange += (sender1, e1) => signal_level_status_spinner.Enabled = e1.IsChecked;
                primary_audio_source_chk.CheckedChange += (sender1, e1) => primary_audio_source_spinner.Enabled = e1.IsChecked;
                ecall_event_active.CheckedChange += (sender1, e1) => ecall_event_active_tgl.Enabled = e1.IsChecked;

                if (viDeviceStatus != null)
                {
                    if (null != viDeviceStatus.voiceRecOn)
                        voice_rec_on_tgl.Checked = (bool)viDeviceStatus.voiceRecOn;
                    else
                        voice_rec_on.Checked = false;

                    if (null != viDeviceStatus.btIconOn)
                        bt_icon_on_tgl.Checked = (bool)viDeviceStatus.btIconOn;
                    else
                        bt_icon_on.Checked = false;

                    if (null != viDeviceStatus.callActive)
                        call_active_tgl.Checked = (bool)viDeviceStatus.callActive;
                    else
                        call_active.Checked = false;

                    if (null != viDeviceStatus.phoneRoaming)
                        phone_roaming_tgl.Checked = (bool)viDeviceStatus.phoneRoaming;
                    else
                        phone_roaming.Checked = false;

                    if (null != viDeviceStatus.textMsgAvailable)
                        text_msg_available_tgl.Checked = (bool)viDeviceStatus.textMsgAvailable;
                    else
                        text_msg_available.Checked = false;

                    if (null != viDeviceStatus.getBattLevelStatus())
                        batt_level_status_spinner.SetSelection((int)viDeviceStatus.getBattLevelStatus());
                    else
                        batt_level_status_chk.Checked = false;

                    if (null != viDeviceStatus.stereoAudioOutputMuted)
                        stereo_audio_output_muted_tgl.Checked = (bool)viDeviceStatus.stereoAudioOutputMuted;
                    else
                        stereo_audio_output_muted.Checked = false;

                    if (null != viDeviceStatus.monoAudioOutputMuted)
                        mono_audio_output_muted_tgl.Checked = (bool)viDeviceStatus.monoAudioOutputMuted;
                    else
                        mono_audio_output_muted.Checked = false;

                    if (null != viDeviceStatus.getSignalLevelStatus())
                        signal_level_status_spinner.SetSelection((int)viDeviceStatus.getSignalLevelStatus());
                    else
                        signal_level_status_chk.Checked = false;

                    if (null != viDeviceStatus.getPrimaryAudioSource())
                        primary_audio_source_spinner.SetSelection((int)viDeviceStatus.getPrimaryAudioSource());
                    else
                        primary_audio_source_chk.Checked = false;

                    if (null != viDeviceStatus.eCallEventActive)
                        ecall_event_active_tgl.Checked = (bool)viDeviceStatus.eCallEventActive;
                    else
                        ecall_event_active.Checked = false;
                }

                deviceStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    deviceStatusAlertDialog.Dispose();
                });
                deviceStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viDeviceStatus = new DeviceStatus();

                    if (voice_rec_on.Checked)
                        viDeviceStatus.voiceRecOn = voice_rec_on_tgl.Checked;
                    if (bt_icon_on.Checked)
                        viDeviceStatus.btIconOn = bt_icon_on_tgl.Checked;
                    if (call_active.Checked)
                        viDeviceStatus.callActive = call_active_tgl.Checked;
                    if (phone_roaming.Checked)
                        viDeviceStatus.phoneRoaming = phone_roaming_tgl.Checked;
                    if (text_msg_available.Checked)
                        viDeviceStatus.textMsgAvailable = text_msg_available_tgl.Checked;
                    if (batt_level_status_chk.Checked)
                        viDeviceStatus.battLevelStatus = (DeviceLevelStatus)batt_level_status_spinner.SelectedItemPosition;
                    if (stereo_audio_output_muted.Checked)
                        viDeviceStatus.stereoAudioOutputMuted = stereo_audio_output_muted_tgl.Checked;
                    if (mono_audio_output_muted.Checked)
                        viDeviceStatus.monoAudioOutputMuted = mono_audio_output_muted_tgl.Checked;
                    if (signal_level_status_chk.Checked)
                        viDeviceStatus.signalLevelStatus = (DeviceLevelStatus)signal_level_status_spinner.SelectedItemPosition;
                    if (primary_audio_source_chk.Checked)
                        viDeviceStatus.primaryAudioSource = (PrimaryAudioSource)primary_audio_source_spinner.SelectedItemPosition;
                    if (ecall_event_active.Checked)
                        viDeviceStatus.eCallEventActive = ecall_event_active_tgl.Checked;
                });
                deviceStatusAlertDialog.Show();
            };

            head_lamp_status_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder headLampAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View headLampView = inflater.Inflate(Resource.Layout.vi_head_lamp_status, null);
                headLampAlertDialog.SetView(headLampView);
                headLampAlertDialog.SetTitle("Head Lamp Status");

                CheckBox low_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on_chk);
                CheckBox high_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on_chk);

                Switch low_beams_on_tgl = (Switch)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on_tgl);
                Switch high_beams_on_tgl = (Switch)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on_tgl);

                CheckBox ambient_light_sensor_status_chk = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_chk);
                Spinner ambient_light_sensor_status_spinner = (Spinner)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_spinner);

                var AmbientLightStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, AmbientLightStatusArray);
                ambient_light_sensor_status_spinner.Adapter = AmbientLightStatusAdapter;

				low_beams_on.CheckedChange += (sender1, e1) =>
				{
					low_beams_on_tgl.Enabled = e1.IsChecked;
				};
				high_beams_on.CheckedChange += (sender1, e1) =>
				{
					high_beams_on_tgl.Enabled = e1.IsChecked;
				};
                ambient_light_sensor_status_chk.CheckedChange += (sender1, e1) => ambient_light_sensor_status_spinner.Enabled = e1.IsChecked;

                if (viHeadLampStatus != null)
                {
                    if (null != viHeadLampStatus.lowBeamsOn)
                        low_beams_on_tgl.Checked = (bool)viHeadLampStatus.lowBeamsOn;
                    else
                        low_beams_on.Checked = false;

                    if (null != viHeadLampStatus.highBeamsOn)
                        high_beams_on_tgl.Checked = (bool)viHeadLampStatus.highBeamsOn;
                    else
                        high_beams_on.Checked = false;
                    
                    if (null != viHeadLampStatus.getAmbientLightSensorStatus())
                        ambient_light_sensor_status_spinner.SetSelection((int)viHeadLampStatus.getAmbientLightSensorStatus());
                    else
                        ambient_light_sensor_status_chk.Checked = false;
                }

                headLampAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    headLampAlertDialog.Dispose();
                });
                headLampAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viHeadLampStatus = new HeadLampStatus();
                    if (low_beams_on.Checked)
                        viHeadLampStatus.lowBeamsOn = low_beams_on_tgl.Checked;
                    if (high_beams_on.Checked)
                        viHeadLampStatus.highBeamsOn = high_beams_on_tgl.Checked;
                    if (ambient_light_sensor_status_chk.Checked)
                    {
                        viHeadLampStatus.ambientLightSensorStatus = (AmbientLightStatus)ambient_light_sensor_status_spinner.SelectedItemPosition;
                    }
                });
                headLampAlertDialog.Show();
            };

            ecall_info_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder eCallInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View eCallInfoView = inflater.Inflate(Resource.Layout.vi_ecall_info, null);
                eCallInfoAlertDialog.SetView(eCallInfoView);
                eCallInfoAlertDialog.SetTitle("ECall Info");

                CheckBox ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_chk);
                Spinner ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_spinner);

                CheckBox aux_ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_chk);
                Spinner aux_ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_spinner);

                CheckBox ecall_confirmation_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_chk);
                Spinner ecall_confirmation_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_spinner);

                ecall_notification_status_chk.CheckedChange += (sender1, e1) => ecall_notification_status_spinner.Enabled = e1.IsChecked;
                aux_ecall_notification_status_chk.CheckedChange += (sender1, e1) => aux_ecall_notification_status_spinner.Enabled = e1.IsChecked;
                ecall_confirmation_status_chk.CheckedChange += (sender1, e1) => ecall_confirmation_status_spinner.Enabled = e1.IsChecked;

                var vehicleDataNotificationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataNotificationStatusArray);
                ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;
                aux_ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;

                var ECallConfirmationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, ECallConfirmationStatusArray);
                ecall_confirmation_status_spinner.Adapter = ECallConfirmationStatusAdapter;

                if (null != viECallInfo)
                {
                    if (null != viECallInfo.getECallNotificationStatus())
                        ecall_notification_status_spinner.SetSelection((int)viECallInfo.getECallNotificationStatus());
                    else
                        ecall_notification_status_chk.Checked = false;

                    if (null != viECallInfo.getAuxECallNotificationStatus())
                        aux_ecall_notification_status_spinner.SetSelection((int)viECallInfo.getAuxECallNotificationStatus());
                    else
                        aux_ecall_notification_status_chk.Checked = false;

                    if (null != viECallInfo.getECallConfirmationStatus())
                        ecall_confirmation_status_spinner.SetSelection((int)viECallInfo.getECallConfirmationStatus());
                    else
                        ecall_confirmation_status_chk.Checked = false;
                }

                eCallInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    eCallInfoAlertDialog.Dispose();
                });
                eCallInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viECallInfo = new ECallInfo();
                    if (ecall_notification_status_chk.Checked)
                        viECallInfo.eCallNotificationStatus = (VehicleDataNotificationStatus)ecall_notification_status_spinner.SelectedItemPosition;
                    if (aux_ecall_notification_status_chk.Checked)
                        viECallInfo.auxECallNotificationStatus = (VehicleDataNotificationStatus)aux_ecall_notification_status_spinner.SelectedItemPosition;
                    if (ecall_confirmation_status_chk.Checked)
                        viECallInfo.eCallConfirmationStatus = (ECallConfirmationStatus)ecall_confirmation_status_spinner.SelectedItemPosition;
                });
                eCallInfoAlertDialog.Show();
            };

            airbag_status_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder airbagStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View airbagStatusView = inflater.Inflate(Resource.Layout.vi_airbag_status, null);
                airbagStatusAlertDialog.SetView(airbagStatusView);
                airbagStatusAlertDialog.SetTitle("Airbag Status");

                CheckBox driver_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_chk);
                Spinner driver_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_spinner);

                CheckBox driver_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_chk);
                Spinner driver_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_spinner);

                CheckBox driver_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_chk);
                Spinner driver_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_spinner);

                CheckBox passenger_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_chk);
                Spinner passenger_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_spinner);

                CheckBox passenger_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_chk);
                Spinner passenger_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_spinner);

                CheckBox driver_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_chk);
                Spinner driver_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_spinner);

                CheckBox passenger_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_chk);
                Spinner passenger_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_spinner);

                CheckBox passenger_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_chk);
                Spinner passenger_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_spinner);

                driver_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                driver_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
                passenger_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;

                driver_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
                driver_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
                passenger_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;

                if (null != viAirbagStatus)
                {
                    if (null != viAirbagStatus.getDriverAirbagDeployed())
                    {
                        driver_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverAirbagDeployed());
                    }
                    else
                    {
                        driver_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getDriverSideAirbagDeployed())
                    {
                        driver_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverSideAirbagDeployed());
                    }
                    else
                    {
                        driver_side_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getDriverCurtainAirbagDeployed())
                    {
                        driver_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverCurtainAirbagDeployed());
                    }
                    else
                    {
                        driver_curtain_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getPassengerAirbagDeployed())
                    {
                        passenger_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerAirbagDeployed());
                    }
                    else
                    {
                        passenger_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getPassengerCurtainAirbagDeployed())
                    {
                        passenger_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerCurtainAirbagDeployed());
                    }
                    else
                    {
                        passenger_curtain_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getDriverKneeAirbagDeployed())
                    {
                        driver_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverKneeAirbagDeployed());
                    }
                    else
                    {
                        driver_knee_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getPassengerSideAirbagDeployed())
                    {
                        passenger_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerSideAirbagDeployed());
                    }
                    else
                    {
                        passenger_side_airbag_deployed_chk.Checked = false;
                    }
                        
                    if (null != viAirbagStatus.getPassengerKneeAirbagDeployed())
                    {
                        passenger_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerKneeAirbagDeployed());
                    }
                    else
                    {
                        passenger_knee_airbag_deployed_chk.Checked = false;
                    }
                }

                airbagStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    airbagStatusAlertDialog.Dispose();
                });
                airbagStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viAirbagStatus = new AirbagStatus();
                    if (driver_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverAirbagDeployed = (VehicleDataEventStatus)driver_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_side_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverSideAirbagDeployed = (VehicleDataEventStatus)driver_side_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_curtain_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverCurtainAirbagDeployed = (VehicleDataEventStatus)driver_curtain_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerAirbagDeployed = (VehicleDataEventStatus)passenger_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_curtain_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerCurtainAirbagDeployed = (VehicleDataEventStatus)passenger_curtain_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (driver_knee_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.driverKneeAirbagDeployed = (VehicleDataEventStatus)driver_knee_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_side_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerSideAirbagDeployed = (VehicleDataEventStatus)passenger_side_airbag_deployed_spinner.SelectedItemPosition;
                    }
                    if (passenger_knee_airbag_deployed_chk.Checked)
                    {
                        viAirbagStatus.passengerKneeAirbagDeployed = (VehicleDataEventStatus)passenger_knee_airbag_deployed_spinner.SelectedItemPosition;
                    }
                });
                airbagStatusAlertDialog.Show();
            };

            emergency_event_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder emergencyEventAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View emergencyEventView = inflater.Inflate(Resource.Layout.vi_emergency_event, null);
                emergencyEventAlertDialog.SetView(emergencyEventView);
                emergencyEventAlertDialog.SetTitle("Emergency Event");

                CheckBox emergency_event_type_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_chk);
                Spinner emergency_event_type_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_spinner);

                CheckBox fuel_cutoff_status_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_chk);
                Spinner fuel_cutoff_status_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_spinner);

                CheckBox rollover_event_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_chk);
                Spinner rollover_event_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_spinner);

                CheckBox maximum_change_velocity_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_chk);
                Spinner maximum_change_velocity_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_spinner);

                CheckBox multiple_events_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_chk);
                Spinner multiple_events_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_spinner);

                var EmergencyEventTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, EmergencyEventTypeArray);
                emergency_event_type_spinner.Adapter = EmergencyEventTypeAdapter;

                var FuelCutoffStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, FuelCutoffStatusArray);
                fuel_cutoff_status_spinner.Adapter = FuelCutoffStatusAdapter;

                rollover_event_spinner.Adapter = VehicleDataEventStatusAdapter;
                maximum_change_velocity_spinner.Adapter = VehicleDataEventStatusAdapter;
                multiple_events_spinner.Adapter = VehicleDataEventStatusAdapter;

                emergency_event_type_chk.CheckedChange += (sender1, e1) => emergency_event_type_spinner.Enabled = e1.IsChecked;
                fuel_cutoff_status_chk.CheckedChange += (sender1, e1) => fuel_cutoff_status_spinner.Enabled = e1.IsChecked;
                rollover_event_chk.CheckedChange += (sender1, e1) => rollover_event_spinner.Enabled = e1.IsChecked;
                maximum_change_velocity_chk.CheckedChange += (sender1, e1) => maximum_change_velocity_spinner.Enabled = e1.IsChecked;
                multiple_events_chk.CheckedChange += (sender1, e1) => multiple_events_spinner.Enabled = e1.IsChecked;

                if (null != viEmergencyEvent)
                {
                    if (null != viEmergencyEvent.getEmergencyEventType())
                    {
                        emergency_event_type_spinner.SetSelection((int)viEmergencyEvent.getEmergencyEventType());
                    }
                    else
                    {
                        emergency_event_type_chk.Checked = false;
                    }
                        
                    if (null != viEmergencyEvent.getFuelCutoffStatus())
                    {
                        fuel_cutoff_status_spinner.SetSelection((int)viEmergencyEvent.getFuelCutoffStatus());
                    }
                    else
                    {
                        fuel_cutoff_status_chk.Checked = false;
                    }
                        
                    if (null != viEmergencyEvent.getRolloverEvent())
                    {
                        rollover_event_spinner.SetSelection((int)viEmergencyEvent.getRolloverEvent());
                    }
                    else
                    {
                        rollover_event_chk.Checked = false;
                    }
                        
                    if (null != viEmergencyEvent.getMaximumChangeVelocity())
                    {
                        maximum_change_velocity_spinner.SetSelection((int)viEmergencyEvent.getMaximumChangeVelocity());
                    }
                    else
                    {
                        maximum_change_velocity_chk.Checked = false;
                    }
                        
                    if (null != viEmergencyEvent.getMultipleEvents())
                    {
                        multiple_events_spinner.SetSelection((int)viEmergencyEvent.getMultipleEvents());
                    }
                    else
                    {
                        multiple_events_chk.Checked = false;
                    }
                }

                emergencyEventAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    emergencyEventAlertDialog.Dispose();
                });
                emergencyEventAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viEmergencyEvent = new EmergencyEvent();
                    if (emergency_event_type_chk.Checked)
                    {
                        viEmergencyEvent.emergencyEventType = (EmergencyEventType)emergency_event_type_spinner.SelectedItemPosition;
                    }
                    if (fuel_cutoff_status_chk.Checked)
                    {
                        viEmergencyEvent.fuelCutoffStatus = (FuelCutoffStatus)fuel_cutoff_status_spinner.SelectedItemPosition;
                    }
                    if (rollover_event_chk.Checked)
                    {
                        viEmergencyEvent.rolloverEvent = (VehicleDataEventStatus)rollover_event_spinner.SelectedItemPosition;
                    }
                    if (maximum_change_velocity_chk.Checked)
                    {
                        viEmergencyEvent.maximumChangeVelocity = (VehicleDataEventStatus)maximum_change_velocity_spinner.SelectedItemPosition;
                    }
                    if (multiple_events_chk.Checked)
                    {
                        viEmergencyEvent.multipleEvents = (VehicleDataEventStatus)multiple_events_spinner.SelectedItemPosition;
                    }
                });
                emergencyEventAlertDialog.Show();
            };

            cluster_modes_btn.Click += (sender, e) =>
            {
                Android.Support.V7.App.AlertDialog.Builder clusterModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View clusterModeView = inflater.Inflate(Resource.Layout.vi_cluster_mode_status, null);
                clusterModeAlertDialog.SetView(clusterModeView);
                clusterModeAlertDialog.SetTitle("Cluster Mode Status");

                CheckBox power_mode_active = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active);
                Switch power_mode_active_tgl = (Switch)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active_tgl);

                CheckBox power_mode_qualification_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_chk);
                Spinner power_mode_qualification_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_spinner);

                CheckBox car_mode_status_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_chk);
                Spinner car_mode_status_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_spinner);

                CheckBox power_mode_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_chk);
                Spinner power_mode_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_spinner);

                var PowerModeQualificationStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeQualificationStatusArray);
                power_mode_qualification_status_spinner.Adapter = PowerModeQualificationStatusAdapter;

                var CarModeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, CarModeStatusArray);
                car_mode_status_status_spinner.Adapter = CarModeStatusAdapter;

                var PowerModeStatusAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeStatusArray);
                power_mode_status_spinner.Adapter = PowerModeStatusAdapter;

				power_mode_active.CheckedChange += (sender1, e1) =>
				{
					power_mode_active_tgl.Enabled = e1.IsChecked;
				};
                power_mode_qualification_status_chk.CheckedChange += (sender1, e1) => power_mode_qualification_status_spinner.Enabled = e1.IsChecked;
                car_mode_status_status_chk.CheckedChange += (sender1, e1) => car_mode_status_status_spinner.Enabled = e1.IsChecked;
                power_mode_status_chk.CheckedChange += (sender1, e1) => power_mode_status_spinner.Enabled = e1.IsChecked;

                if (null != viClusterModeStatus)
                {
                    if (null != viClusterModeStatus.powerModeActive)
                        power_mode_active_tgl.Checked = (bool)viClusterModeStatus.powerModeActive;
                    else
                        power_mode_active.Checked = false;
                        
                    if (null != viClusterModeStatus.getPowerModeQualificationStatus())
                    {
                        power_mode_qualification_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeQualificationStatus());
                    }
                    else
                    {
                        power_mode_qualification_status_chk.Checked = false;
                    }
                        
                    if (null != viClusterModeStatus.getCarModeStatus())
                    {
                        car_mode_status_status_spinner.SetSelection((int)viClusterModeStatus.getCarModeStatus());
                    }
                    else
                    {
                        car_mode_status_status_chk.Checked = false;
                    }
                        
                    if (null != viClusterModeStatus.getPowerModeStatus())
                    {
                        power_mode_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeStatus());
                    }
                    else
                    {
                        power_mode_status_chk.Checked = false;
                    }
                }

                clusterModeAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
                {
                    clusterModeAlertDialog.Dispose();
                });
                clusterModeAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viClusterModeStatus = new ClusterModeStatus();
                    if (power_mode_active.Checked)
                        viClusterModeStatus.powerModeActive = power_mode_active_tgl.Checked;
                    
                    if (power_mode_qualification_status_chk.Checked)
                    {
                        viClusterModeStatus.powerModeQualificationStatus = (PowerModeQualificationStatus)power_mode_qualification_status_spinner.SelectedItemPosition;
                    }
                    if (car_mode_status_status_chk.Checked)
                    {
                        viClusterModeStatus.carModeStatus = (CarModeStatus)car_mode_status_status_spinner.SelectedItemPosition;
                    }
                    if (power_mode_status_chk.Checked)
                    {
                        viClusterModeStatus.powerModeStatus = (PowerModeStatus)power_mode_status_spinner.SelectedItemPosition;
					}
				});
				clusterModeAlertDialog.Show();
			};

            fuel_range_btn.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder fuelRangeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View fuelRangeView = inflater.Inflate(Resource.Layout.add_fuel_range, null);
                fuelRangeAlertDialog.SetView(fuelRangeView);
                fuelRangeAlertDialog.SetTitle("Add Fuel Range");

                CheckBox checkBoxName = (CheckBox)fuelRangeView.FindViewById(Resource.Id.fuel_type_check);
                Spinner spnName = (Spinner)fuelRangeView.FindViewById(Resource.Id.fuel_type_spinner);

                string[] fuelTypeNames = Enum.GetNames(typeof(FuelType));
                var fuelTypeAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, fuelTypeNames);
                spnName.Adapter = fuelTypeAdapter;

                CheckBox rangeChk = (CheckBox)fuelRangeView.FindViewById(Resource.Id.range_check);
                EditText rangeET = (EditText)fuelRangeView.FindViewById(Resource.Id.range_et);

                checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                rangeChk.CheckedChange += (sender, e) => rangeET.Enabled = e.IsChecked;

                fuelRangeAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    FuelRange fuelRange = new FuelRange();

                    if (checkBoxName.Checked)
                    {
                        fuelRange.type = (FuelType)spnName.SelectedItemPosition;
                    }
                    if (rangeChk.Checked)
                    {
                        try
                        {
                            fuelRange.range = Java.Lang.Float.ParseFloat(rangeET.Text);
                        }
                        catch (Exception e)
                        {
                            fuelRange.range = 0;
                        }
                    }

                    fuelRangeList.Add(fuelRange);
                    fuelRangeAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(fuel_range_lv);
                });

                fuelRangeAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    fuelRangeAlertDialog.Dispose();
                });
                fuelRangeAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_chk.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;
                }

                GPSData gpsdata = null;
                if (gps_data_chk.Checked)
                {
                    gpsdata = viGPSData;
                }

                float? speed = null;
                if (speed_chk.Checked && speed_et.Text != null && speed_et.Text.Length > 0)
                {
                    speed = Java.Lang.Float.ParseFloat(speed_et.Text);
                }

                int? rpm = null;
                if (rpm_chk.Checked && rpm_et.Text != null && rpm_et.Text.Length > 0)
                {
                    rpm = Java.Lang.Integer.ParseInt(rpm_et.Text);
                }

                float? fuelLevel = null;
                if (fuel_level_chk.Checked && fuel_level_et.Text != null && fuel_level_et.Text.Length > 0)
                {
                    fuelLevel = Java.Lang.Float.ParseFloat(fuel_level_et.Text);
                }

                ComponentVolumeStatus? fuelLevel_State = null;
                if (fuel_level_state_chk.Checked)
                {
                    fuelLevel_State = (ComponentVolumeStatus)fuel_level_state_spinner.SelectedItemPosition;
                }

                float? instantFuelConsumption = null;
                if (instant_fuel_consumption_chk.Checked && instant_fuel_consumption_et.Text != null && instant_fuel_consumption_et.Text.Length > 0)
                {
                    instantFuelConsumption = Java.Lang.Float.ParseFloat(instant_fuel_consumption_et.Text);
                }

                float? externalTemperature = null;
                if (external_temp_chk.Checked && external_temp_et.Text != null && external_temp_et.Text.Length > 0)
                {
                    externalTemperature = Java.Lang.Float.ParseFloat(external_temp_et.Text);
                }

                string vin = null;
                if (vin_chk.Checked)
                {
                    vin = vin_et.Text;
                }

                PRNDL? prndl = null;
                if (prndl_chk.Checked)
                {
                    prndl = (PRNDL)prndl_spinner.SelectedItemPosition;
                }

				TurnSignal? turnSignal = null;
				if (turnSignal_chk.Checked)
				{
					turnSignal = (TurnSignal)turnSignal_spinner.SelectedItemPosition;
				}

                TireStatus tirePressure = null;
                if (tire_pressure_chk.Checked)
                {
                    tirePressure = viTireStatus;
                }

                int? odometer = null;
                if (odometer_chk.Checked && odometer_et.Text != null && odometer_et.Text.Length > 0)
                {
                    odometer = Java.Lang.Integer.ParseInt(odometer_et.Text);
                }

                BeltStatus beltStatus = null;
                if (belt_status_chk.Checked)
                {
                    beltStatus = viBeltStatus;
                }

                BodyInformation bodyInformation = null;
                if (body_info_chk.Checked)
                {
                    bodyInformation = viBodyInformation;
                }

                DeviceStatus deviceStatus = null;
                if (device_status_chk.Checked)
                {
                    deviceStatus = viDeviceStatus;
                }

                VehicleDataEventStatus? driverBraking = null;
                if (driver_braking_chk.Checked)
                {
                    driverBraking = (VehicleDataEventStatus)driver_braking_spinner.SelectedItemPosition;
                }

                WiperStatus? wiperStatus = null;
                if (wiper_status_chk.Checked)
                {
                    wiperStatus = (WiperStatus)wiper_status_spinner.SelectedItemPosition;
                }

                HeadLampStatus headLampStatus = null;
                if (head_lamp_status_chk.Checked)
                {
                    headLampStatus = viHeadLampStatus;
                }

                float? engineTorque = null;
                if (engine_torque_chk.Checked && engine_torque_et.Text != null && engine_torque_et.Text.Length > 0)
                {
                    engineTorque = Java.Lang.Float.ParseFloat(engine_torque_et.Text);
                }

                float? accPedalPosition = null;
                if (acc_padel_position_chk.Checked && acc_padel_position_et.Text != null && acc_padel_position_et.Text.Length > 0)
                {
                    accPedalPosition = Java.Lang.Float.ParseFloat(acc_padel_position_et.Text);
                }

                float? steeringWheelAngle = null;
                if (steering_wheel_angle_chk.Checked && steering_wheel_angle_et.Text != null && steering_wheel_angle_et.Text.Length > 0)
                {
                    steeringWheelAngle = Java.Lang.Float.ParseFloat(steering_wheel_angle_et.Text);
                }

                ECallInfo eCallInfo = null;
                if (ecall_info_chk.Checked)
                {
                    eCallInfo = viECallInfo;
                }

                AirbagStatus airbagStatus = null;
                if (airbag_status_chk.Checked)
                {
                    airbagStatus = viAirbagStatus;
                }

                EmergencyEvent emergencyEvent = null;
                if (emergency_event_chk.Checked)
                {
                    emergencyEvent = viEmergencyEvent;
                }

                ClusterModeStatus clusterModeStatus = null;
                if (cluster_modes_chk.Checked)
                {
                    clusterModeStatus = viClusterModeStatus;
                }

                MyKey myKey = null;
                if (my_key_chk.Checked)
                {
                    myKey = new MyKey();
                    myKey.e911Override = (VehicleDataStatus)my_key_spinner.SelectedItemPosition;
                }

                ElectronicParkBrakeStatus? electronicParkBrakeStatus = null;
                if (electronic_park_brake_status_chk.Checked)
                {
                    electronicParkBrakeStatus = (ElectronicParkBrakeStatus)electronic_park_brake_status_spinner.SelectedItemPosition;
                }

                float? engineOilLife = null;
                if (engine_oil_life_chk.Checked)
                {
                    try
                    {
                        engineOilLife = Java.Lang.Float.ParseFloat(engine_oil_life_et.Text);
                    }
                    catch (Exception e)
                    {
                        engineOilLife = null;
                    }
                }

                List<FuelRange> fuelRange = null;
                if (fuel_range_chk.Checked)
                {
                    fuelRange = fuelRangeList;
                }

				RpcResponse rpcMessage = BuildRpc.buildVehicleInfoGetVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gpsdata, speed, rpm, fuelLevel,
                                                                                         fuelLevel_State, instantFuelConsumption, externalTemperature, vin,
                                                                                        prndl, turnSignal, tirePressure, odometer, beltStatus, bodyInformation,
                                                                                        deviceStatus, driverBraking, wiperStatus, headLampStatus,
                                                                                        engineTorque, accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus,
                                                                                        emergencyEvent, clusterModeStatus, myKey, electronicParkBrakeStatus, engineOilLife, fuelRange);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void dialogClosable(DialogInterface dialogInterface, Boolean close)
        {
            try
            {
                Java.Lang.Reflect.Field field = dialogInterface.Class.Superclass.GetDeclaredField("mShowing");
                field.Accessible = true;
                field.Set(dialogInterface, close);
            }
            catch (Exception e)
            {

            }
        }

        protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switch (requestCode)
            {
                case 45:
                    //int REQUEST_FILE_CHOOSER = 45;
                    if (resultCode == Android.App.Result.Ok)
                    {
                        String localFilePath = (String)IntentHelper.
                                getObjectForKey(Const.INTENTHELPER_KEY_FILECHOOSER_FILE);
                        if (localFilePath != null)
                        {
                            txtLocalFileName.SetText(localFilePath, TextView.BufferType.Editable);
                        }
                    }
                    IntentHelper.removeObjectForKey(Const.INTENTHELPER_KEY_FILECHOOSER_FILE);
                    break;
            }
        }

		public void GridDialogue(LayoutInflater inflater,String type,ListView listview)
		{
			
				Android.Support.V7.App.AlertDialog.Builder gridAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View gridView = inflater.Inflate(Resource.Layout.grid, null);
				gridAlertDialog.SetView(gridView);
				gridAlertDialog.SetTitle("Grid");
				CheckBox colCheck = (CheckBox)gridView.FindViewById(Resource.Id.col_check);
				EditText colEt = (EditText)gridView.FindViewById(Resource.Id.col_et);

				CheckBox rowCheck = (CheckBox)gridView.FindViewById(Resource.Id.row_check);
				EditText rowEt = (EditText)gridView.FindViewById(Resource.Id.row_et);

				CheckBox levelCheck = (CheckBox)gridView.FindViewById(Resource.Id.level_check);
				EditText levelEt = (EditText)gridView.FindViewById(Resource.Id.level_et);

			CheckBox colSpanCheck = (CheckBox)gridView.FindViewById(Resource.Id.colspan_check);
			EditText colSpanEt = (EditText)gridView.FindViewById(Resource.Id.colspan_et);
			CheckBox rowSpanCheck = (CheckBox)gridView.FindViewById(Resource.Id.rowspan_check);
			EditText rowSpanEt = (EditText)gridView.FindViewById(Resource.Id.rowspan_et);

			CheckBox levelSpanCheck = (CheckBox)gridView.FindViewById(Resource.Id.levelspan_check);
			EditText levelSpanEt = (EditText)gridView.FindViewById(Resource.Id.levelspan_et);

			levelSpanCheck.CheckedChange += (sender, e) =>
				{
					levelSpanEt.Enabled = e.IsChecked;
				};
			rowSpanCheck.CheckedChange += (sender, e) =>
				{
					rowSpanEt.Enabled = e.IsChecked;
				};
			colSpanCheck.CheckedChange += (sender, e) =>
				{
					colSpanEt.Enabled = e.IsChecked;
				};
			levelCheck.CheckedChange += (sender, e) =>
				{
					levelEt.Enabled = e.IsChecked;
				};
			rowCheck.CheckedChange += (sender, e) =>
			{
				rowEt.Enabled = e.IsChecked;
			};
			colCheck.CheckedChange += (sender, e) =>
			{
				colEt.Enabled = e.IsChecked;
			};
			if (grid != null)
			{
				if (grid.getCol() != null)
					colEt.Text = grid.getCol().ToString();
				else
				{
					colCheck.Checked = false;
					colEt.Enabled = false;
				}
				if (grid.getRow() != null)
					rowEt.Text = grid.getRow().ToString();
				else
				{
					rowCheck.Checked = false;
					rowEt.Enabled = false;
				}
				if (grid.getLevel() != null)
					levelEt.Text = grid.getLevel().ToString();
				else
				{
					levelCheck.Checked = false;
					levelEt.Enabled = false;
				}
				if (grid.getColSpan() != null)
					colSpanEt.Text = grid.getColSpan().ToString();
				else
				{
					colSpanCheck.Checked = false;
					colSpanEt.Enabled = false;
				}
				if (grid.getRowSpan() != null)
					rowSpanEt.Text = grid.getRowSpan().ToString();
				else
				{
					rowSpanCheck.Checked = false;
					rowSpanEt.Enabled = false;
				}
				if (grid.getLevelSpan() != null)
					levelSpanEt.Text = grid.getLevelSpan().ToString();
				else
				{
					levelSpanCheck.Checked = false;
					levelSpanEt.Enabled = false;
				}
			}
			gridAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
				{
					gridAlertDialog.Dispose();
				});

			gridAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				{
					grid = new Grid();

					if (colCheck.Checked && colEt.Text != "")
					{
						if (colEt.Text.Equals(""))
							grid.col = -1;
						else
						grid.col = Java.Lang.Integer.ParseInt(colEt.Text);

					}
					if (rowCheck.Checked && rowEt.Text != "")
						grid.row = Java.Lang.Integer.ParseInt(rowEt.Text); 
					if (levelCheck.Checked)
					{
						if (levelEt.Text == "")
							grid.level = 0;
						else
						grid.level = Java.Lang.Integer.ParseInt(levelEt.Text);
					}
					if (colSpanCheck.Checked  )
					{
						if (colSpanEt.Text == "")
							grid.colspan = 0;
						else
							grid.colspan = Java.Lang.Integer.ParseInt(colSpanEt.Text);
					}
					if (rowSpanCheck.Checked )
					{
						if (rowSpanEt.Text == "")
							grid.rowSpan = 0;
						else
							grid.rowSpan = Java.Lang.Integer.ParseInt(rowSpanEt.Text);
					}
					if (colSpanCheck.Checked )
					{
						if (colSpanEt.Text == "")
							grid.levelSpan = 0;
						else
							grid.levelSpan = Java.Lang.Integer.ParseInt(levelSpanEt.Text);
					}
					
					if (type.Equals("Location"))
						gridLocation = grid;
					else if (type.Equals("Servicearea"))
						gridServicearea = grid;
					else
					{
						gridSeat = grid;
						seatLocGrid = new SeatLocation();
						seatLocGrid.grid = gridSeat;
						seatLocationList.Add(seatLocGrid);

						seatLocationadapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(listview);

					}

				});

			gridAlertDialog.Show();

			
			





		}
		public void ModuleInfoDialogue(LayoutInflater inflater) {
			Android.Support.V7.App.AlertDialog.Builder moduleInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
			View moduleInfoView = inflater.Inflate(Resource.Layout.module_info, null);
			moduleInfoAlertDialog.SetView(moduleInfoView);
			moduleInfoAlertDialog.SetTitle("ModuleInfo");
			CheckBox module_info_location_module_id_cb = (CheckBox)moduleInfoView.FindViewById(Resource.Id.Module_info_location_module_id_cb);
			EditText module_info_location_module_id_edittext = (EditText)moduleInfoView.FindViewById(Resource.Id.Module_info_location_module_id_edittext);

			CheckBox module_info_location_cb = (CheckBox)moduleInfoView.FindViewById(Resource.Id.Module_info_location_cb);
			Button module_info_location_cb_btn = (Button)moduleInfoView.FindViewById(Resource.Id.Module_info_location_cb_btn);

			CheckBox module_info_location_servicearea_cb = (CheckBox)moduleInfoView.FindViewById(Resource.Id.Module_info_location_servicearea_cb);
			Button module_info_location_servicearea_btn = (Button)moduleInfoView.FindViewById(Resource.Id.Module_info_location_servicearea_btn);

			CheckBox multipleAccess_chk = (CheckBox)moduleInfoView.FindViewById(Resource.Id.MultipleAccess_chk);
			Switch multipleAccess_tgl = (Switch)moduleInfoView.FindViewById(Resource.Id.MultipleAccess_tgl);
			module_info_location_module_id_edittext.Text = BuildDefaults.buildDefaultModulInfo().getModuleId().ToString();
			multipleAccess_chk.CheckedChange += (sender, e) =>
			{
				multipleAccess_tgl.Enabled = e.IsChecked;
			};
			module_info_location_servicearea_cb.CheckedChange += (sender, e) =>
			{
				module_info_location_servicearea_btn.Enabled = e.IsChecked;
			};
			module_info_location_cb.CheckedChange += (sender, e) =>
			{
				module_info_location_cb_btn.Enabled = e.IsChecked;
			};
			module_info_location_module_id_cb.CheckedChange += (sender, e) =>
			{
				module_info_location_module_id_edittext.Enabled = e.IsChecked;
			};
			module_info_location_cb_btn.Click += (object sender, EventArgs e) => {
				gridLocation = null;
				 GridDialogue(inflater,"Location",null); };
			module_info_location_servicearea_btn.Click += (object sender, EventArgs e) => {
				gridServicearea = null;
				  GridDialogue(inflater,"Servicearea",null); };

			moduleInfoAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
			{
				moduleInfoAlertDialog.Dispose();
			});
			if (moduleInfo != null)
			{
				if (null != moduleInfo.getallowMultipleAccess())
					multipleAccess_tgl.Checked = (bool)moduleInfo.getallowMultipleAccess();
				else
				{
					multipleAccess_chk.Checked = false;
					multipleAccess_tgl.Checked = false;
				}
				if (moduleInfo.getserviceArea() != null)
					gridServicearea = moduleInfo.getserviceArea();
				else
				{
					module_info_location_servicearea_btn.Enabled = false;
					module_info_location_servicearea_cb.Checked = false;
				}
				if (moduleInfo.getLocation() != null)
					gridLocation = moduleInfo.getLocation();
				else {
					module_info_location_cb_btn.Enabled = false;
					module_info_location_cb.Checked = false;
				}
				if (moduleInfo.getModuleId() != null)
					module_info_location_module_id_edittext.Text = moduleInfo.getModuleId().ToString();
				else {
					module_info_location_module_id_edittext.Enabled = false;
					module_info_location_module_id_cb.Checked = false;
				}
			}
			moduleInfoAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
			{
				moduleInfo = new ModuleInfo();

				if (module_info_location_module_id_cb.Checked)
					moduleInfo.moduleId = module_info_location_module_id_edittext.Text.ToString();
				if (module_info_location_cb.Checked)
					moduleInfo.location = gridLocation;
				if (module_info_location_servicearea_cb.Checked)
					moduleInfo.serviceArea = gridServicearea;
				moduleInfo.allowMultipleAccess = null;
				if (multipleAccess_chk.Checked)
					moduleInfo.allowMultipleAccess = multipleAccess_tgl.Checked;
				
			});
			moduleInfoAlertDialog.Show();


		}
        public void CreateRCResponseGetCapabilities()
        {

			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.rc_get_capabilities, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle("RC.GetCapabilities");

            CheckBox remoteControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_remote_control_capabilities_cb);

            CheckBox climateControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_cb);
            Button addClimateControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_btn);
            ListView climateControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_climate_control_capabilities_lv);
            List<ClimateControlCapabilities> climateControlCapabilitiesList = new List<ClimateControlCapabilities>();

            CheckBox radioControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_cb);
            Button addRadioControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_btn);
            ListView radioControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_radio_control_capabilities_lv);
            List<RadioControlCapabilities> radioControlCapabilitiesList = new List<RadioControlCapabilities>();

            CheckBox buttonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_button_capabilities_cb);
            Button addButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_button_capabilities_btn);
            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_button_capabilities_lv);
            List<ButtonCapabilities> btnCapList = new List<ButtonCapabilities>();

            CheckBox seatControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_seat_control_capabilities_cb);
            Button addSeatControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_seat_control_capabilities_btn);
            ListView seatControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_seat_control_capabilities_lv);
            List<SeatControlCapabilities> seatCapList = new List<SeatControlCapabilities>();

            CheckBox audioControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_audio_control_capabilities_cb);
            Button addAudioControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_audio_control_capabilities_btn);
            ListView audioControlCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.rc_audio_control_capabilities_lv);
            List<AudioControlCapabilities> audioCapList = new List<AudioControlCapabilities>();

            CheckBox hmiSettingControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_hmi_setting_control_capabilities_cb);
            Button hmiSettingControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_hmi_setting_control_capabilities_btn);

            CheckBox lightControlCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.rc_light_control_capabilities_cb);
            Button lightControlCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.rc_light_control_capabilities_btn);

            CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.rc_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.rc_result_code_spn);

            var climateControlCapabilitiesAdapter = new ClimateControlCapabilitiesAdapter(this, climateControlCapabilitiesList);
            climateControlCapabilitiesListView.Adapter = climateControlCapabilitiesAdapter;

            var radioControlCapabilitiesAdapter = new RadioControlCapabilitiesAdapter(this, radioControlCapabilitiesList);
            radioControlCapabilitiesListView.Adapter = radioControlCapabilitiesAdapter;

            var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(this, btnCapList, ViewStates.Invisible, buttonCapabilitiesListView);
            buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;

            var seatControlCapabilitiesAdapter = new SeatControlCapabilitiesAdapter(this, seatCapList);
            seatControlCapabilitiesListView.Adapter = seatControlCapabilitiesAdapter;

            var audioControlCapabilitiesAdapter = new AudioControlCapabilitiesAdapter(this, audioCapList);
            audioControlCapabilitiesListView.Adapter = audioControlCapabilitiesAdapter;

            resultCodeSpn.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);

            remoteControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                climateControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				addClimateControlCapabilitiesButton.Enabled = evn.IsChecked && climateControlCapabilitiesCheckBox.Checked;

				radioControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				addRadioControlCapabilitiesButton.Enabled = evn.IsChecked && radioControlCapabilitiesCheckBox.Checked;

				buttonCapabilitiesCheckBox.Enabled = evn.IsChecked;
				addButtonCapabilitiesButton.Enabled = evn.IsChecked && buttonCapabilitiesCheckBox.Checked;

				seatControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				addSeatControlCapabilitiesButton.Enabled = evn.IsChecked && seatControlCapabilitiesCheckBox.Checked;
				audioControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				addAudioControlCapabilitiesButton.Enabled = evn.IsChecked && audioControlCapabilitiesCheckBox.Checked;

				hmiSettingControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				hmiSettingControlCapabilitiesButton.Enabled = evn.IsChecked && hmiSettingControlCapabilitiesCheckBox.Checked;

				lightControlCapabilitiesCheckBox.Enabled = evn.IsChecked;
				lightControlCapabilitiesButton.Enabled= evn.IsChecked && lightControlCapabilitiesCheckBox.Checked;
			};

            climateControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addClimateControlCapabilitiesButton.Enabled = evn.IsChecked;
                climateControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            radioControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addRadioControlCapabilitiesButton.Enabled = evn.IsChecked;
                radioControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            buttonCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addButtonCapabilitiesButton.Enabled = evn.IsChecked;
                buttonCapabilitiesListView.Enabled = evn.IsChecked;
            };
            seatControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addSeatControlCapabilitiesButton.Enabled = evn.IsChecked;
                seatControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            audioControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                addAudioControlCapabilitiesButton.Enabled = evn.IsChecked;
                audioControlCapabilitiesListView.Enabled = evn.IsChecked;
            };
            hmiSettingControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
            {
                hmiSettingControlCapabilitiesButton.Enabled = evn.IsChecked;
            };
			lightControlCapabilitiesCheckBox.CheckedChange += (sen, evn) =>
			{
				lightControlCapabilitiesButton.Enabled = evn.IsChecked;
			};
			resultCodeCb.CheckedChange += (sen, evn) => resultCodeSpn.Enabled = evn.IsChecked;

            HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities);
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)BuildDefaults.buildDefaultMessage(type, 0);
            }

            HMISettingsControlCapabilities hmis = null;
            LightControlCapabilities lightCap = null;

            if (tmpObj != null && tmpObj.getRemoteControlCapabilities() != null)
            {
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities() != null)
				{
					climateControlCapabilitiesList.AddRange(tmpObj.getRemoteControlCapabilities().getClimateControlCapabilities());
					climateControlCapabilitiesAdapter.NotifyDataSetChanged();
				}
				else
					climateControlCapabilitiesCheckBox.Checked = false;

				if (tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities() != null)
				{
					radioControlCapabilitiesList.AddRange(tmpObj.getRemoteControlCapabilities().getRadioControlCapabilities());
					radioControlCapabilitiesAdapter.NotifyDataSetChanged();
				}
				else
					radioControlCapabilitiesCheckBox.Checked = false;

				if (tmpObj.getRemoteControlCapabilities().getButtonCapabilities() != null)
				{
					btnCapList.AddRange(tmpObj.getRemoteControlCapabilities().getButtonCapabilities());
					buttonCapabilitiesAdapter.NotifyDataSetChanged();
				}
				else
					buttonCapabilitiesCheckBox.Checked = false;

				if (tmpObj.getRemoteControlCapabilities().getSeatControlCapabilities() != null)
				{
					seatCapList.AddRange(tmpObj.getRemoteControlCapabilities().getSeatControlCapabilities());
					seatControlCapabilitiesAdapter.NotifyDataSetChanged();
				}
				else
					seatControlCapabilitiesCheckBox.Checked = false;

				if (tmpObj.getRemoteControlCapabilities().getAudioControlCapabilities() != null)
                {
                    audioCapList.AddRange(tmpObj.getRemoteControlCapabilities().getAudioControlCapabilities());
                    audioControlCapabilitiesAdapter.NotifyDataSetChanged();
                }
				else
					audioControlCapabilitiesCheckBox.Checked = false;

				hmis = tmpObj.getRemoteControlCapabilities().getHmiSettingsControlCapabilities();
                if (hmis == null)
                {
                    hmiSettingControlCapabilitiesCheckBox.Checked = false;
                }

				lightCap = tmpObj.getRemoteControlCapabilities().getLightControlCapabilities();
				if (null == lightCap)
				{
					lightControlCapabilitiesCheckBox.Checked = false;
				}
			}
            Utility.setListViewHeightBasedOnChildren(climateControlCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(radioControlCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(seatControlCapabilitiesListView);
            Utility.setListViewHeightBasedOnChildren(audioControlCapabilitiesListView);

            addClimateControlCapabilitiesButton.Click += delegate
			{
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder climateControlCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View climateControlCapabilitiesView = inflater.Inflate(Resource.Layout.climate_control_capabilities, null);
                climateControlCapabilitiesAlertDialog.SetView(climateControlCapabilitiesView);
                climateControlCapabilitiesAlertDialog.SetTitle("ClimateControlCapabilities");

				CheckBox moduleNameCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_module_name_cb);
				EditText moduleNameEt = (EditText)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_module_name_et);
				CheckBox currentTempAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_current_temp_available_cb);
				Switch currentTempAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_current_temp_available_tgl);
				currentTempAvailableCb.CheckedChange += (sender, e) => {
						currentTempAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox moduleInfoCheckBox = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)climateControlCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);
				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};
				CheckBox fanSpeedAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_fan_speed_available_cb);
				Switch fanSpeedAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_fan_speed_available_tgl);
				fanSpeedAvailableCb.CheckedChange += (sender, e) => {
						fanSpeedAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox desiredTemperatureAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_desired_temperature_available_cb);
				Switch desiredTemperatureAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_desired_temperature_available_tgl);
				desiredTemperatureAvailableCb.CheckedChange += (sender, e) => {
						desiredTemperatureAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox acEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_enable_available_cb);
				Switch acEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_enable_available_tgl);
				acEnableAvailableCb.CheckedChange += (sender, e) => {
						acEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox acMaxEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_max_enable_available_cb);
				Switch acMaxEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ac_max_enable_available_tgl);
				acMaxEnableAvailableCb.CheckedChange += (sender, e) => {
					acMaxEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox circulateAirEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_circulate_air_enable_available_cb);
				Switch circulateAirEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_circulate_air_enable_available_tgl);
				circulateAirEnableAvailableCb.CheckedChange += (sender, e) => {
						circulateAirEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox autoModeEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_auto_mode_enable_available_cb);
				Switch autoModeEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_auto_mode_enable_available_tgl);

				autoModeEnableAvailableCb.CheckedChange += (sender, e) => {
						autoModeEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox dualModeEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_dual_mode_enable_available_cb);
				Switch dualModeEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_dual_mode_enable_available_tgl);
				dualModeEnableAvailableCb.CheckedChange += (sender, e) => {
						dualModeEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox defrostZoneAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_available_cb);
				Switch defrostZoneAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_available_tgl);
				defrostZoneAvailableCb.CheckedChange += (sender, e) => {
						defrostZoneAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox defrostZoneCheckBox = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_cb);
				Button addDefrostZoneButton = (Button)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_btn);
				ListView defrostZoneListView = (ListView)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_defrost_zone_lv);
				List<DefrostZone> defrostZoneList = new List<DefrostZone>();


				CheckBox ventilationModeAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_available_cb);
				Switch ventilationModeAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_available_tgl);
				ventilationModeAvailableCb.CheckedChange += (sender, e) => {
						ventilationModeAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox ventilationModeCheckBox = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_cb);
				Button addVentilationModeButton = (Button)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_btn);
				ListView ventilationModeListView = (ListView)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_ventilation_mode_lv);
				List<VentilationMode> ventilationModeList = new List<VentilationMode>();

				CheckBox heatedSteeringWheelAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_steering_wheel_available_cb);
				Switch heatedSteeringWheelAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_steering_wheel_available_tgl);
				heatedSteeringWheelAvailableCb.CheckedChange += (sender, e) => {
						heatedSteeringWheelAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox heatedWindshieldAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_windshield_available_cb);
				Switch heatedWindshieldAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_windshield_available_tgl);
				heatedWindshieldAvailableCb.CheckedChange += (sender, e) => {
						heatedWindshieldAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox heatedRearWindowAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_rear_window_available_cb);
				Switch heatedRearWindowAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_rear_window_available_tgl);
				heatedRearWindowAvailableCb.CheckedChange += (sender, e) => {
						heatedRearWindowAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox heatedMirrorsAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_mirrors_available_cb);
				Switch heatedMirrorsAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_control_capabilities_heated_mirrors_available_tgl);
				heatedMirrorsAvailableCb.CheckedChange += (sender, e) => {
						heatedMirrorsAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox climateEnableAvailableCb = (CheckBox)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_enable_available_cb);

				Switch climateEnableAvailable_tgl = (Switch)climateControlCapabilitiesView.FindViewById(Resource.Id.climate_enable_available_tgl);
				


				moduleNameCb.CheckedChange += (sen, evn) => moduleNameEt.Enabled = evn.IsChecked;
                defrostZoneCheckBox.CheckedChange += (sen, evn) =>
                {
                    addDefrostZoneButton.Enabled = evn.IsChecked;
                    defrostZoneListView.Enabled = evn.IsChecked;
                };

                ventilationModeCheckBox.CheckedChange += (sen, evn) =>
                {
                    addVentilationModeButton.Enabled = evn.IsChecked;
                    ventilationModeListView.Enabled = evn.IsChecked;
                };

                var defrostZoneAdapter = new DefrostZoneAdapter(this, defrostZoneList);
                defrostZoneListView.Adapter = defrostZoneAdapter;

				

				var ventilationModeAdapter = new VentilationModeAdapter(this, ventilationModeList);
                ventilationModeListView.Adapter = ventilationModeAdapter;

				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);



				climateEnableAvailableCb.CheckedChange += (sender, e) =>
				{
					climateEnableAvailable_tgl.Enabled = e.IsChecked;
				};


				bool[] defrostZoneBoolArray = new bool[RCDefrostZoneArray.Length];
                addDefrostZoneButton.Click += (sender, e1) =>
                {
					Android.Support.V7.App.AlertDialog.Builder defrostZoneAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    defrostZoneAlertDialog.SetTitle("DefrostZone");

                    defrostZoneAlertDialog.SetMultiChoiceItems(RCDefrostZoneArray, defrostZoneBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => defrostZoneBoolArray[e.Which] = e.IsChecked);

                    defrostZoneAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        defrostZoneAlertDialog.Dispose();
                    });

                    defrostZoneAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
						defrostZoneList.Clear();

						for (int i = 0; i < RCDefrostZoneArray.Length; i++)
						{
							if (defrostZoneBoolArray[i])
							{
								defrostZoneList.Add(((DefrostZone)typeof(DefrostZone).GetEnumValues().GetValue(i)));
							}
						}
						defrostZoneAdapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(defrostZoneListView);
					});
                    defrostZoneAlertDialog.Show();
                };

                bool[] ventilationModeBoolArray = new bool[RCVentilationModeArray.Length];
                addVentilationModeButton.Click += (sender, e1) =>
                {
					Android.Support.V7.App.AlertDialog.Builder ventilationModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                    ventilationModeAlertDialog.SetTitle("VentilationMode");

                    ventilationModeAlertDialog.SetMultiChoiceItems(RCVentilationModeArray, ventilationModeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => ventilationModeBoolArray[e.Which] = e.IsChecked);

                    ventilationModeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                    {
                        ventilationModeAlertDialog.Dispose();
                    });

                    ventilationModeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                    {
						ventilationModeList.Clear();
						for (int i = 0; i < RCVentilationModeArray.Length; i++)
						{
							if (ventilationModeBoolArray[i])
							{
								ventilationModeList.Add(((VentilationMode)typeof(VentilationMode).GetEnumValues().GetValue(i)));
							}
						}
						ventilationModeAdapter.NotifyDataSetChanged();
						Utility.setListViewHeightBasedOnChildren(ventilationModeListView);
					});
                    ventilationModeAlertDialog.Show();
                };


                climateControlCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				{
					ClimateControlCapabilities climateControlCapabilities = new ClimateControlCapabilities();

					if (moduleNameCb.Checked)
                        climateControlCapabilities.moduleName = moduleNameEt.Text;
					if (moduleInfoCheckBox.Checked )
					{
						climateControlCapabilities.moduleInfo = moduleInfo;
					}
					if (currentTempAvailableCb.Checked)
						climateControlCapabilities.currentTemperatureAvailable = currentTempAvailable_tgl.Checked;
					if (fanSpeedAvailableCb.Checked)
						climateControlCapabilities.fanSpeedAvailable = fanSpeedAvailable_tgl.Checked;
					if (desiredTemperatureAvailableCb.Checked)
						climateControlCapabilities.desiredTemperatureAvailable = desiredTemperatureAvailable_tgl.Checked;

					if (circulateAirEnableAvailableCb.Checked)
						climateControlCapabilities.circulateAirEnableAvailable = circulateAirEnableAvailable_tgl.Checked;
					if (acMaxEnableAvailableCb.Checked)
						climateControlCapabilities.acMaxEnableAvailable = acMaxEnableAvailable_tgl.Checked;
					if (acEnableAvailableCb.Checked)
						climateControlCapabilities.acEnableAvailable = acEnableAvailable_tgl.Checked;

					if (autoModeEnableAvailableCb.Checked)
						climateControlCapabilities.autoModeEnableAvailable = autoModeEnableAvailable_tgl.Checked;
					if (dualModeEnableAvailableCb.Checked)
						climateControlCapabilities.dualModeEnableAvailable = dualModeEnableAvailable_tgl.Checked;
					if (defrostZoneAvailableCb.Checked)
						climateControlCapabilities.defrostZoneAvailable = defrostZoneAvailable_tgl.Checked;

					if (defrostZoneCheckBox.Checked)
                        climateControlCapabilities.defrostZone = defrostZoneList;

                    climateControlCapabilities.ventilationModeAvailable = ventilationModeAvailableCb.Checked;

                    if (ventilationModeCheckBox.Checked)
                        climateControlCapabilities.ventilationMode = ventilationModeList;

					if (heatedSteeringWheelAvailableCb.Checked)
						climateControlCapabilities.heatedSteeringWheelAvailable = heatedSteeringWheelAvailable_tgl.Checked;
					if (heatedWindshieldAvailableCb.Checked)
						climateControlCapabilities.heatedWindshieldAvailable = heatedWindshieldAvailable_tgl.Checked;
					if (heatedRearWindowAvailableCb.Checked)
						climateControlCapabilities.heatedRearWindowAvailable = heatedRearWindowAvailable_tgl.Checked;
					if (heatedMirrorsAvailableCb.Checked)
						climateControlCapabilities.heatedMirrorsAvailable = heatedMirrorsAvailable_tgl.Checked;
                    if (climateEnableAvailableCb.Checked)
                        climateControlCapabilities.climateEnableAvailable = climateEnableAvailable_tgl.Checked;

                    climateControlCapabilitiesList.Add(climateControlCapabilities);
                    climateControlCapabilitiesAdapter.NotifyDataSetChanged();
					Utility.setListViewHeightBasedOnChildren(climateControlCapabilitiesListView);
                });

                climateControlCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    climateControlCapabilitiesAlertDialog.Dispose();
                });
                climateControlCapabilitiesAlertDialog.Show();
            };

            addRadioControlCapabilitiesButton.Click += delegate
			{
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder radioControlCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View radioControlCapabilitiesView = inflater.Inflate(Resource.Layout.radio_control_capabilities, null);
                radioControlCapabilitiesAlertDialog.SetView(radioControlCapabilitiesView);
                radioControlCapabilitiesAlertDialog.SetTitle("RadioControlCapabilities");

				CheckBox moduleInfoCheckBox = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)radioControlCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);
				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};

				CheckBox moduleNameCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_module_name_cb);
                EditText moduleNameEt = (EditText)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_module_name_et);
                CheckBox radioEnableAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_enable_available_cb);
				Switch radioEnableAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_enable_available_tgl);
				radioEnableAvailableCb.CheckedChange += (sender, e) => {
					radioEnableAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox radioBandAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_band_available_cb);
				Switch radioBandAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_band_available_tgl);

				radioBandAvailableCb.CheckedChange += (sender, e) => {
					radioBandAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox radioFrequencyAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_frequency_available_cb);
				Switch radioFrequencyAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_radio_frequency_available_tgl);

				radioFrequencyAvailableCb.CheckedChange += (sender, e) => {
					radioFrequencyAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox hdChannelAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_hd_channel_available_cb);
				Switch hdChannelAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_hd_channel_available_tgl);

				hdChannelAvailableCb.CheckedChange += (sender, e) => {
					hdChannelAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox rdsDataAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_rds_data_available_cb);
				Switch rdsDataAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_rds_data_available_tgl);

				rdsDataAvailableCb.CheckedChange += (sender, e) => {
					rdsDataAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox availableHDsAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_available_hds_available_cb);
				Switch availableHDsAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_available_hds_available_tgl);

				availableHDsAvailableCb.CheckedChange += (sender, e) => {
						availableHDsAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox stateAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_state_available_cb);
				Switch stateAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_state_available_tgl);

				stateAvailableCb.CheckedChange += (sender, e) => {
					stateAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox signalStrengthAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_strength_available_cb);
				Switch signalStrengthAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_strength_available_tgl);

				signalStrengthAvailableCb.CheckedChange += (sender, e) => {
					signalStrengthAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox signalChangeThresholdAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_available_cb);
				Switch signalChangeThresholdAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_available_tgl);

				signalChangeThresholdAvailableCb.CheckedChange += (sender, e) => {
					signalChangeThresholdAvailable_tgl.Enabled = e.IsChecked;
				};
				CheckBox sisDataAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_sis_data_available_cb);
				Switch sisDataAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.radio_control_data_sis_data_available_tgl);

				sisDataAvailableCb.CheckedChange += (sender, e) => {
					sisDataAvailable_tgl.Enabled = e.IsChecked;
				};
                CheckBox hdRadioEnableAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.hdRadioEnableAvailable_cb);
                Switch hdRadioEnableAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.hdRadioEnableAvailable_tgl);

                hdRadioEnableAvailableCb.CheckedChange += (sender, e) => {
                    hdRadioEnableAvailable_tgl.Enabled = e.IsChecked;
                };
                CheckBox siriusxmRadioAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.siriusxmRadioAvailable_cb);
                Switch siriusxmRadioAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.siriusxmRadioAvailable_tgl);


                siriusxmRadioAvailableCb.CheckedChange += (sender, e) => {
                    siriusxmRadioAvailable_tgl.Enabled = e.IsChecked;
                };

                CheckBox availableHdChannelsAvailableCb = (CheckBox)radioControlCapabilitiesView.FindViewById(Resource.Id.availableHdChannelsAvailable_cb);
                Switch availableHdChannelsAvailable_tgl = (Switch)radioControlCapabilitiesView.FindViewById(Resource.Id.availableHdChannelsAvailable_tgl);

                availableHdChannelsAvailableCb.CheckedChange += (sender, e) => {
                    availableHdChannelsAvailable_tgl.Enabled = e.IsChecked;
                };

				
				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);

				moduleNameCb.CheckedChange += (sen, evn) => moduleNameEt.Enabled = evn.IsChecked;

                radioControlCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    RadioControlCapabilities radioControlCapabilities = new RadioControlCapabilities();

                    if (moduleNameCb.Checked)
                        radioControlCapabilities.moduleName = moduleNameEt.Text;
					if (moduleInfoCheckBox.Checked)
					{
						radioControlCapabilities.moduleInfo = moduleInfo;
					}
					if (radioEnableAvailableCb.Checked)
						radioControlCapabilities.radioEnableAvailable = radioEnableAvailable_tgl.Checked;
					if (radioBandAvailableCb.Checked)
						radioControlCapabilities.radioBandAvailable = radioBandAvailable_tgl.Checked;
					if (radioFrequencyAvailableCb.Checked)
						radioControlCapabilities.radioFrequencyAvailable = radioFrequencyAvailable_tgl.Checked;

					if (hdChannelAvailableCb.Checked)
						radioControlCapabilities.hdChannelAvailable = hdChannelAvailable_tgl.Checked;

					if (rdsDataAvailableCb.Checked)
						radioControlCapabilities.rdsDataAvailable = rdsDataAvailable_tgl.Checked;
					if (availableHDsAvailableCb.Checked)
						radioControlCapabilities.availableHDsAvailable = availableHDsAvailable_tgl.Checked;
					if (stateAvailableCb.Checked)
						radioControlCapabilities.stateAvailable = stateAvailable_tgl.Checked;
					if (signalStrengthAvailableCb.Checked)
						radioControlCapabilities.signalStrengthAvailable = signalStrengthAvailable_tgl.Checked;

					if (signalChangeThresholdAvailableCb.Checked)
						radioControlCapabilities.signalChangeThresholdAvailable = signalChangeThresholdAvailable_tgl.Checked;
					if (sisDataAvailableCb.Checked)
						radioControlCapabilities.sisDataAvailable = sisDataAvailable_tgl.Checked;
                    if (hdRadioEnableAvailableCb.Checked)
                        radioControlCapabilities.hdRadioEnableAvailable = hdRadioEnableAvailable_tgl.Checked;
                    if (siriusxmRadioAvailableCb.Checked)
                        radioControlCapabilities.siriusxmRadioAvailable = siriusxmRadioAvailable_tgl.Checked;
                    if (availableHdChannelsAvailableCb.Checked)
                        radioControlCapabilities.availableHdChannelsAvailable = availableHdChannelsAvailable_tgl.Checked;
					
					radioControlCapabilitiesList.Add(radioControlCapabilities);
                    radioControlCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(radioControlCapabilitiesListView);
                });

                radioControlCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    radioControlCapabilitiesAlertDialog.Dispose();
                });
                radioControlCapabilitiesAlertDialog.Show();
            };

            addButtonCapabilitiesButton.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder btnCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View btnCapabilitiesView = inflater.Inflate(Resource.Layout.button_capabilities, null);
                btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
                btnCapabilitiesAlertDialog.SetTitle("ButtonCapabilities");

                TextView textViewButtonName = (TextView)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);

                Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);
                string[] btnCapabilitiesButtonName = Enum.GetNames(typeof(ButtonName));
                var btnCapabilitiesButtonNameAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, btnCapabilitiesButtonName);
                spnButtonNames.Adapter = btnCapabilitiesButtonNameAdapter;

                CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);
				Switch checkBoxShortPressAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_tgl);

				CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);
				Switch checkBoxLongPressAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_tgl);

				CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);
				Switch checkBoxUpDownAvailable_tgl = (Switch)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_tgl);
				checkBoxShortPressAvailable.CheckedChange += (sender, e) => {
					checkBoxShortPressAvailable_tgl.Enabled = e.IsChecked;
				};
				checkBoxLongPressAvailable.CheckedChange += (sender, e) => {
					checkBoxLongPressAvailable_tgl.Enabled = e.IsChecked;
				};
				checkBoxUpDownAvailable.CheckedChange += (sender, e) => {
					checkBoxUpDownAvailable_tgl.Enabled = e.IsChecked;
				};
				btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    ButtonCapabilities btn = new ButtonCapabilities();
                    btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;
					if (checkBoxShortPressAvailable.Checked)
						btn.shortPressAvailable = checkBoxShortPressAvailable_tgl.Checked;
					if (checkBoxLongPressAvailable.Checked)
						btn.longPressAvailable = checkBoxLongPressAvailable_tgl.Checked;
					if (checkBoxUpDownAvailable.Checked)
						btn.upDownAvailable = checkBoxUpDownAvailable_tgl.Checked;

                    btnCapList.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);
                });

                btnCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    btnCapabilitiesAlertDialog.Dispose();
                });
                btnCapabilitiesAlertDialog.Show();
            };

            addSeatControlCapabilitiesButton.Click += delegate
			{
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder seatCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View seatCapabilitiesView = inflater.Inflate(Resource.Layout.seat_control_cap, null);
                seatCapabilitiesAlertDialog.SetView(seatCapabilitiesView);
                seatCapabilitiesAlertDialog.SetTitle("SeatControlCapabilities");

                CheckBox moduleName_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_moduleName_chk);
                EditText moduleName_et = (EditText)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_moduleName_et);
				moduleName_chk.CheckedChange += (sender, e) =>
				{
					moduleName_et.Enabled = e.IsChecked;
				};
				CheckBox moduleInfoCheckBox = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)seatCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);
				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};

				CheckBox heatingEnabledAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_heatingEnabledAvailable_chk);
                Switch heatingEnabledAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_heatingEnabledAvailable_tgl);
				heatingEnabledAvailable_chk.CheckedChange += (sender, e) =>
				{
					heatingEnabledAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox coolingEnabledAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_coolingEnabledAvailable_chk);
                Switch coolingEnabledAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_coolingEnabledAvailable_tgl);
				coolingEnabledAvailable_chk.CheckedChange += (sender, e) => {
					coolingEnabledAvailable_tgl.Enabled = e.IsChecked; };

                CheckBox heatingLevelAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_heatingLevelAvailable_chk);
                Switch heatingLevelAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_heatingLevelAvailable_tgl);
                heatingLevelAvailable_chk.CheckedChange += (sender, e) =>
				{
					heatingLevelAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox coolingLevelAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_coolingLevelAvailable_chk);
                Switch coolingLevelAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_coolingLevelAvailable_tgl);
                coolingLevelAvailable_chk.CheckedChange += (sender, e) =>{
					coolingLevelAvailable_tgl.Enabled = e.IsChecked; };


                CheckBox horizontalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_horizontalPositionAvailable_chk);
                Switch horizontalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_horizontalPositionAvailable_tgl);
				horizontalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					horizontalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox verticalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_verticalPositionAvailable_chk);
                Switch verticalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_verticalPositionAvailable_tgl);
				verticalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					verticalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox frontVerticalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_frontVerticalPositionAvailable_chk);
                Switch frontVerticalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_frontVerticalPositionAvailable_tgl);
				frontVerticalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					frontVerticalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox backVerticalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_backVerticalPositionAvailable_chk);
                Switch backVerticalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_backVerticalPositionAvailable_tgl);
				backVerticalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					backVerticalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox backTiltAngleAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_backTiltAngleAvailable_chk);
                Switch backTiltAngleAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_backTiltAngleAvailable_tgl);
				backTiltAngleAvailable_chk.CheckedChange += (sender, e) =>
				{
					backTiltAngleAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox headSupportHorizontalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_headSupportHorizontalPositionAvailable_chk);
                Switch headSupportHorizontalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_headSupportHorizontalPositionAvailable_tgl);
				headSupportHorizontalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					headSupportHorizontalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox headSupportVerticalPositionAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_headSupportVerticalPositionAvailable_chk);
                Switch headSupportVerticalPositionAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_headSupportVerticalPositionAvailable_tgl);
				headSupportVerticalPositionAvailable_chk.CheckedChange += (sender, e) =>
				{
					headSupportVerticalPositionAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox massageEnabledAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageEnabledAvailable_chk);
                Switch massageEnabledAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageEnabledAvailable_tgl);
				massageEnabledAvailable_chk.CheckedChange += (sender, e) => {
					massageEnabledAvailable_tgl.Enabled = e.IsChecked; };

                CheckBox massageModeAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageModeAvailable_chk);
                Switch massageModeAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageModeAvailable_tgl);
				massageModeAvailable_chk.CheckedChange += (sender, e) => {
					massageModeAvailable_tgl.Enabled = e.IsChecked; };

                CheckBox massageCushionFirmnessAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageCushionFirmnessAvailable_chk);
                Switch massageCushionFirmnessAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_massageCushionFirmnessAvailable_tgl);
				massageCushionFirmnessAvailable_chk.CheckedChange += (sender, e) =>
				{
					massageCushionFirmnessAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox memoryAvailable_chk = (CheckBox)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_memoryAvailable_chk);
                Switch memoryAvailable_tgl = (Switch)seatCapabilitiesView.FindViewById(Resource.Id.seat_cap_memoryAvailable_tgl);
				memoryAvailable_chk.CheckedChange += (sender, e) =>
				{
					memoryAvailable_tgl.Enabled = e.IsChecked;
				};
				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);

				seatCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    SeatControlCapabilities seat = new SeatControlCapabilities();
                    if (moduleName_chk.Checked)
                        seat.moduleName = moduleName_et.Text;
					if (moduleInfoCheckBox.Checked)
					{
						seat.moduleInfo = moduleInfo;
					}
					if (heatingEnabledAvailable_chk.Checked)
                        seat.heatingEnabledAvailable = heatingEnabledAvailable_tgl.Checked;
                    if (coolingEnabledAvailable_chk.Checked)
                        seat.coolingEnabledAvailable = coolingEnabledAvailable_tgl.Checked;
                    if (heatingLevelAvailable_chk.Checked)
                        seat.heatingLevelAvailable = heatingLevelAvailable_tgl.Checked;
                    if (coolingLevelAvailable_chk.Checked)
                        seat.coolingLevelAvailable = coolingLevelAvailable_tgl.Checked;
                    if (horizontalPositionAvailable_chk.Checked)
                        seat.horizontalPositionAvailable = horizontalPositionAvailable_tgl.Checked;
                    if (verticalPositionAvailable_chk.Checked)
                        seat.verticalPositionAvailable = verticalPositionAvailable_tgl.Checked;
                    if (frontVerticalPositionAvailable_chk.Checked)
                        seat.frontVerticalPositionAvailable = frontVerticalPositionAvailable_tgl.Checked;
                    if (backVerticalPositionAvailable_chk.Checked)
                        seat.backVerticalPositionAvailable = backVerticalPositionAvailable_tgl.Checked;
                    if (backTiltAngleAvailable_chk.Checked)
                        seat.backTiltAngleAvailable = backTiltAngleAvailable_tgl.Checked;
                    if (headSupportHorizontalPositionAvailable_chk.Checked)
                        seat.headSupportHorizontalPositionAvailable = headSupportHorizontalPositionAvailable_tgl.Checked;
                    if (headSupportVerticalPositionAvailable_chk.Checked)
                        seat.headSupportVerticalPositionAvailable = headSupportVerticalPositionAvailable_tgl.Checked;
                    if (massageEnabledAvailable_chk.Checked)
                        seat.massageEnabledAvailable = massageEnabledAvailable_tgl.Checked;
                    if (massageModeAvailable_chk.Checked)
                        seat.massageModeAvailable = massageModeAvailable_tgl.Checked;
                    if (massageCushionFirmnessAvailable_chk.Checked)
                        seat.massageCushionFirmnessAvailable = massageCushionFirmnessAvailable_tgl.Checked;
                    if (memoryAvailable_chk.Checked)
                        seat.memoryAvailable = memoryAvailable_tgl.Checked;
                    

                    seatCapList.Add(seat);
                    seatControlCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(seatControlCapabilitiesListView);
                });

                seatCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    seatCapabilitiesAlertDialog.Dispose();
                });
                seatCapabilitiesAlertDialog.Show();
            };

            addAudioControlCapabilitiesButton.Click += delegate
            {
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder audioCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View audioCapabilitiesView = inflater.Inflate(Resource.Layout.audio_control_cap, null);
                audioCapabilitiesAlertDialog.SetView(audioCapabilitiesView);
                audioCapabilitiesAlertDialog.SetTitle("AudioControlCapabilities");

                CheckBox moduleName_chk = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_moduleName_chk);
                EditText moduleName_et = (EditText)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_moduleName_et);
                moduleName_chk.CheckedChange += (sender, e) => moduleName_et.Enabled = e.IsChecked;

				CheckBox moduleInfoCheckBox = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)audioCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);
				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};
				CheckBox sourceAvailable_chk = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_sourceAvailable_chk);
                Switch sourceAvailable_tgl = (Switch)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_sourceAvailable_tgl);
				sourceAvailable_chk.CheckedChange += (sender, e) =>
				{
					sourceAvailable_tgl.Enabled = e.IsChecked;
				};

                CheckBox volumeAvailable_chk = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_volumeAvailable_chk);
                Switch volumeAvailable_tgl = (Switch)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_volumeAvailable_tgl);
				volumeAvailable_chk.CheckedChange += (sender, e) =>
				{
					volumeAvailable_tgl.Enabled = e.IsChecked;
				};
                CheckBox equalizerAvailable_chk = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_equalizerAvailable_chk);
                Switch equalizerAvailable_tgl = (Switch)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_equalizerAvailable_tgl);
				equalizerAvailable_chk.CheckedChange += (sender, e) => {
					equalizerAvailable_tgl.Enabled = e.IsChecked; };

                CheckBox equalizerMaxChannelId_chk = (CheckBox)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_equalizerMaxChannelId_chk);
                EditText equalizerMaxChannelId_et = (EditText)audioCapabilitiesView.FindViewById(Resource.Id.audio_cap_equalizerMaxChannelId_et);
				equalizerMaxChannelId_chk.CheckedChange += (sender, e) =>
				{
					equalizerMaxChannelId_et.Enabled = e.IsChecked;
				};
				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);

				audioCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    AudioControlCapabilities audio = new AudioControlCapabilities();
					if(audio.sourceAvailable == null)

					if (moduleName_chk.Checked)
                        audio.moduleName = moduleName_et.Text;
					if (moduleInfoCheckBox.Checked)
					{
						audio.moduleInfo = moduleInfo;
					}
					if (sourceAvailable_chk.Checked)
						audio.sourceAvailable = sourceAvailable_tgl.Checked;
					
                    if (volumeAvailable_chk.Checked)
                        audio.volumeAvailable = volumeAvailable_tgl.Checked;
                    if (equalizerAvailable_chk.Checked)
                        audio.equalizerAvailable = equalizerAvailable_tgl.Checked;
                    if (equalizerMaxChannelId_chk.Checked)
                    {
                        try
                        {
                            audio.equalizerMaxChannelId = Int32.Parse(equalizerMaxChannelId_et.Text);
                        }
                        catch (Exception e) { }
                    }

                    audioCapList.Add(audio);
                    audioControlCapabilitiesAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(audioControlCapabilitiesListView);
                });

                audioCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    audioCapabilitiesAlertDialog.Dispose();
                });
                audioCapabilitiesAlertDialog.Show();
            };

			hmiSettingControlCapabilitiesButton.Click += delegate
			{
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder hmiSettingCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
				View hmisCapabilitiesView = inflater.Inflate(Resource.Layout.hmi_setting_cap, null);
				hmiSettingCapabilitiesAlertDialog.SetView(hmisCapabilitiesView);
				hmiSettingCapabilitiesAlertDialog.SetTitle("Hmi Setting ControlCapabilities");

				CheckBox moduleName_chk = (CheckBox)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_moduleName_chk);
				EditText moduleName_et = (EditText)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_moduleName_et);
				moduleName_chk.CheckedChange += (sender, e) => moduleName_et.Enabled = e.IsChecked;

				CheckBox moduleInfoCheckBox = (CheckBox)hmisCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)hmisCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);

				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};
				CheckBox distanceUnitAvailable_chk = (CheckBox)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_distanceUnitAvailable_chk);
				Switch distanceUnitAvailable_tgl = (Switch)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_distanceUnitAvailable_tgl);
				distanceUnitAvailable_chk.CheckedChange += (sender, e) =>
				{
					distanceUnitAvailable_tgl.Enabled = e.IsChecked;
				};

				CheckBox temperatureUnitAvailable_chk = (CheckBox)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_temperatureUnitAvailable_chk);
				Switch temperatureUnitAvailable_tgl = (Switch)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_temperatureUnitAvailable_tgl);
				temperatureUnitAvailable_chk.CheckedChange += (sender, e) =>
				{
					if (e.IsChecked == false)
						temperatureUnitAvailable_tgl.Checked = false;
					temperatureUnitAvailable_tgl.Enabled = e.IsChecked;
				};

				CheckBox displayModeUnitAvailable_chk = (CheckBox)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_displayModeUnitAvailable_chk);
				Switch displayModeUnitAvailable_tgl = (Switch)hmisCapabilitiesView.FindViewById(Resource.Id.hmi_setting_cap_displayModeUnitAvailable_tgl);
				displayModeUnitAvailable_chk.CheckedChange += (sender, e) =>
				{
					displayModeUnitAvailable_tgl.Enabled = e.IsChecked;


				};

				if (null != hmis)
				{
					if (null != hmis.getModuleName())
					{
						moduleName_et.Text = hmis.getModuleName();
					}
					else
					{
						moduleName_chk.Checked = false;
					}

					if (null != hmis.getDistanceUnitAvailable())
					{
						distanceUnitAvailable_tgl.Checked = (bool)hmis.getDistanceUnitAvailable();
					}
					else
					{
						distanceUnitAvailable_chk.Checked = false;
					}

					if (null != hmis.getTemperatureUnitAvailable())
					{
						temperatureUnitAvailable_tgl.Checked = (bool)hmis.getTemperatureUnitAvailable();
					}
					else
					{
						temperatureUnitAvailable_chk.Checked = false;
					}

					if (null != hmis.getDisplayModeUnitAvailable())
					{
						displayModeUnitAvailable_tgl.Checked = (bool)hmis.getDisplayModeUnitAvailable();
					}
					else
					{
						displayModeUnitAvailable_chk.Checked = false;
					}
				}
				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);

				hmiSettingCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				{
					hmis = new HMISettingsControlCapabilities();
					if (moduleName_chk.Checked)
						hmis.moduleName = moduleName_et.Text;
					if (moduleInfoCheckBox.Checked)
					{
						hmis.moduleInfo = moduleInfo;
					}
					if (distanceUnitAvailable_chk.Checked)
						hmis.distanceUnitAvailable = distanceUnitAvailable_tgl.Checked;
					if (temperatureUnitAvailable_chk.Checked)
						hmis.temperatureUnitAvailable = temperatureUnitAvailable_tgl.Checked;
					if (displayModeUnitAvailable_chk.Checked)
						hmis.displayModeUnitAvailable = displayModeUnitAvailable_tgl.Checked;
				});

				hmiSettingCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
				{
					hmiSettingCapabilitiesAlertDialog.Dispose();
				});
				hmiSettingCapabilitiesAlertDialog.Show();
			};

            lightControlCapabilitiesButton.Click += delegate
            {
				moduleInfo = null;

				Android.Support.V7.App.AlertDialog.Builder lightCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                View lightCapabilitiesView = inflater.Inflate(Resource.Layout.light_control_cap, null);
                lightCapabilitiesAlertDialog.SetView(lightCapabilitiesView);
                lightCapabilitiesAlertDialog.SetTitle("Light ControlCapabilities");

                CheckBox moduleName_chk = (CheckBox)lightCapabilitiesView.FindViewById(Resource.Id.light_cap_moduleName_chk);
                EditText moduleName_et = (EditText)lightCapabilitiesView.FindViewById(Resource.Id.light_cap_moduleName_et);
                moduleName_chk.CheckedChange += (sender, e) => moduleName_et.Enabled = e.IsChecked;

				CheckBox moduleInfoCheckBox = (CheckBox)lightCapabilitiesView.FindViewById(Resource.Id.moduleInfo_chk);
				Button moduleInfoButton = (Button)lightCapabilitiesView.FindViewById(Resource.Id.moduleInfo_btn);
				moduleInfoCheckBox.CheckedChange += (sender, e) => {
					moduleInfoButton.Enabled = e.IsChecked;
				};
				CheckBox supportedLights_chk = (CheckBox)lightCapabilitiesView.FindViewById(Resource.Id.light_cap_supportedLights_chk);
                Button supportedLights_btn = (Button)lightCapabilitiesView.FindViewById(Resource.Id.light_cap_supportedLights_btn);
                supportedLights_chk.CheckedChange += (sender, e) => supportedLights_btn.Enabled = e.IsChecked;

                ListView lightListView = (ListView)lightCapabilitiesView.FindViewById(Resource.Id.light_cap_list_view);
                List<LightCapabilities> lightCapList = new List<LightCapabilities>();
                var lightCapabilitiesAdapter = new LightCapabilitiesAdapter(this, lightCapList);
                lightListView.Adapter = lightCapabilitiesAdapter;
				moduleInfoButton.Click += (object sender, EventArgs e) => ModuleInfoDialogue(inflater);

				supportedLights_btn.Click += delegate
                {
                    Android.Support.V7.App.AlertDialog.Builder add_light_capAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
                    View addLightCapabilitiesView = inflater.Inflate(Resource.Layout.add_light_cap, null);
                    add_light_capAlert.SetView(addLightCapabilitiesView);
                    add_light_capAlert.SetTitle("Add Light Capability");

                    CheckBox lightName_chk = (CheckBox)addLightCapabilitiesView.FindViewById(Resource.Id.alc_light_name_chk);
                    Spinner lightName_spn = (Spinner)addLightCapabilitiesView.FindViewById(Resource.Id.alc_light_name_spn);
                    lightName_chk.CheckedChange += (sender, e) => lightName_spn.Enabled = e.IsChecked;

                    string[] lightNameArray = Enum.GetNames(typeof(LightName));
                    var LightNameAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, lightNameArray);
                    lightName_spn.Adapter = LightNameAdapter;

                    CheckBox densityAvailable_chk = (CheckBox)addLightCapabilitiesView.FindViewById(Resource.Id.alc_densityAvailable_chk);
                    Switch densityAvailable_tgl = (Switch)addLightCapabilitiesView.FindViewById(Resource.Id.alc_densityAvailable_tgl);
					densityAvailable_chk.CheckedChange += (sender, e) =>
					{
						densityAvailable_tgl.Enabled = e.IsChecked;
					};
                    CheckBox rgbColorSpaceAvailable_chk = (CheckBox)addLightCapabilitiesView.FindViewById(Resource.Id.alc_rgbColorSpaceAvailable_chk);
                    Switch rgbColorSpaceAvailable_tgl = (Switch)addLightCapabilitiesView.FindViewById(Resource.Id.alc_rgbColorSpaceAvailable_tgl);
                    rgbColorSpaceAvailable_chk.CheckedChange += (sender, e) =>
					{
                        rgbColorSpaceAvailable_tgl.Enabled = e.IsChecked;
					};
                    CheckBox statusAvailable_chk = (CheckBox)addLightCapabilitiesView.FindViewById(Resource.Id.alc_statusAvailable_chk);
                    Switch statusAvailable_tgl = (Switch)addLightCapabilitiesView.FindViewById(Resource.Id.alc_statusAvailable_tgl);
                    statusAvailable_chk.CheckedChange += (sender, e) =>
                    {
                        statusAvailable_tgl.Enabled = e.IsChecked;
                    };

					add_light_capAlert.SetNegativeButton("ok", (senderAlert, args) =>
                    {
						LightCapabilities lightCapability = new LightCapabilities();

						if (lightName_chk.Checked)
                            lightCapability.name = (LightName)lightName_spn.SelectedItemPosition;
						
						if (densityAvailable_chk.Checked)
                            lightCapability.densityAvailable = densityAvailable_tgl.Checked;
                        if (rgbColorSpaceAvailable_chk.Checked)
                            lightCapability.rgbColorSpaceAvailable = rgbColorSpaceAvailable_tgl.Checked;
                        if (statusAvailable_chk.Checked)
                            lightCapability.statusAvailable = statusAvailable_tgl.Checked;
                        lightCapList.Add(lightCapability);
                        Utility.setListViewHeightBasedOnChildren(lightListView);
                    });

                    add_light_capAlert.SetPositiveButton("Cancel", (senderAlert, args) =>
                    {
                        add_light_capAlert.Dispose();
                    });
                    add_light_capAlert.Show();
                };

                if (null != lightCap)
                {
                    if (null != lightCap.getModuleName())
                    {
                        moduleName_et.Text = lightCap.getModuleName();
                    }
                    else
                    {
                        moduleName_chk.Checked = false;
                    }

                    if (null != lightCap.getSupportedLights())
                    {
                        lightCapList.AddRange(lightCap.getSupportedLights());
                        lightCapabilitiesAdapter.NotifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(lightListView);
                    }
                    else
                    {
                        supportedLights_chk.Checked = false;
                    }
                }

                lightCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    lightCap = new LightControlCapabilities();
                    if (moduleName_chk.Checked)
                        lightCap.moduleName = moduleName_et.Text;
					if (moduleInfoCheckBox.Checked)
					{
						lightCap.moduleInfo = moduleInfo;
					}
					if (supportedLights_chk.Checked)
                        lightCap.supportedLights = lightCapList;
                });

                lightCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    lightCapabilitiesAlertDialog.Dispose();
                });
                lightCapabilitiesAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, evn) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton("Tx Later", (senderAlert, evn) =>
            {
                RemoteControlCapabilities remoteControlCapabilities = new RemoteControlCapabilities();

                if (remoteControlCapabilitiesCheckBox.Checked)
                {
                    if (climateControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.climateControlCapabilities = climateControlCapabilitiesList;

                    if (radioControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.radioControlCapabilities = radioControlCapabilitiesList;

                    if (buttonCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.buttonCapabilities = btnCapList;

                    if (seatControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.seatControlCapabilities = seatCapList;

                    if (audioControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.audioControlCapabilities = audioCapList;

                    if (hmiSettingControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.hmiSettingsControlCapabilities = hmis;

                    if (lightControlCapabilitiesCheckBox.Checked)
                        remoteControlCapabilities.lightControlCapabilities = lightCap;
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCb.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildRcGetCapabilitiesResponse(BuildRpc.getNextId(), rsltCode, remoteControlCapabilities);
                AppUtils.savePreferenceValueForRpc(this, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, even) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(this, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		

		public void CreateRCResponseIsReady()
        {
            Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)GetSystemService(LayoutInflaterService);
            View rpcView = inflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle("RC.IsReady");

            CheckBox checkBoxAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAvailable = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			checkBoxAvailable.CheckedChange += (s, e) => switchAvailable.Enabled = e.IsChecked;

			CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            var adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            HmiApiLib.Controllers.RC.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.IsReady();
            tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.IsReady>(this, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.IsReady);
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)BuildDefaults.buildDefaultMessage(type, 0);
            }
			if (tmpObj != null)
			{
				if (tmpObj.getAvailable() == null)
				{
					checkBoxAvailable.Checked = false;
					switchAvailable.Enabled = false;
				}
				else
				{
					checkBoxAvailable.Checked = true;
					switchAvailable.Checked = (bool)tmpObj.getAvailable();
				}
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}
			resultCodeCb.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

            rpcAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            checkBoxAvailable.Text = ("Available");
            rpcAlertDialog.SetNegativeButton("TX Later", (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (resultCodeCb.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }
                RpcResponse rpcMessage = BuildRpc.buildIsReadyResponse(BuildRpc.getNextId(), InterfaceType.RC, checkBoxAvailable.Checked, selectedResultCode);
                AppUtils.savePreferenceValueForRpc(this, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton("Reset", (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        public void sendRcOnRemoteControlSettings()
        {
            RequestNotifyMessage rpcResponse = BuildRpc.buildRcOnRemoteControlSettings(appSetting.isRCEnabled(), null);
            AppUtils.savePreferenceValueForRpc(this, rpcResponse.getMethod(), rpcResponse);
            AppInstanceManager.Instance.sendRpc(rpcResponse);
        }
    }
}