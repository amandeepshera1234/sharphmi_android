﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class AudioControlCapabilitiesAdapter : BaseAdapter<AudioControlCapabilities>
    {
        List<AudioControlCapabilities> audioControlCapabilities;
        Activity context;

        public AudioControlCapabilitiesAdapter(Activity act, List<AudioControlCapabilities> list) : base()
        {
            audioControlCapabilities = list;
            context = act;
        }

        public override AudioControlCapabilities this[int position] => audioControlCapabilities[position];

        public override int Count => audioControlCapabilities.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(
                Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = audioControlCapabilities[position].getModuleName();

            return view;
        }
    }
}
