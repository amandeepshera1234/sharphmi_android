﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
	public class ButtonCapabilitiesAdapter : BaseAdapter<ButtonCapabilities>
	{
		List<ButtonCapabilities> buttonCapabilities;
		Activity context;
		public ViewStates isVisible;
		ListView listView;

		public ButtonCapabilitiesAdapter(Activity act, List<ButtonCapabilities> list,ViewStates Visibility, ListView listView) : base()
		{
			buttonCapabilities = list;
			context = act;
			isVisible = Visibility;
			this.listView = listView;

		}

		public override ButtonCapabilities this[int position] => buttonCapabilities[position];

		public override int Count => buttonCapabilities.Count;

		public override long GetItemId(int position)
		{
            return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ButtonCapabilities btnCapabilities = this[position];

			var view = context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);
			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			image_delete.Visibility = isVisible;
			image_delete.Click += (sender, e) =>
			{
				buttonCapabilities.Remove(btnCapabilities);
				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);

			};
			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = buttonCapabilities[position].getName().ToString();

			return view;
		}
	}
}
