﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
	class DisplayCapabilityAdapter : BaseAdapter<DisplayCapability>
	{
		List<DisplayCapability> dispCapList;
		Activity context;
		private String Text = "DisplayCap ";
		ListView listView;
		public DisplayCapabilityAdapter(Activity act, List<DisplayCapability> list,ListView listView) : base()
		{
			dispCapList = list;
			context = act;
			this.listView = listView;
		}

		public override DisplayCapability this[int position] => dispCapList[position];

		public override int Count => dispCapList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			DisplayCapability displayCapability = this[position];
			 String item = (String)parent.GetChildAt(position);
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);
			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			//image_delete.Visibility = isVisible;

			image_delete.Click += (sender, e) =>
			{
				dispCapList.Remove(displayCapability);

				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);


			};
			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = dispCapList[position].displayName;
			
			return view;
		}
	}
}