﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class EqualizerSettingsAdapter : BaseAdapter<EqualizerSettings>
    {
        Activity context;
        List<EqualizerSettings> EqualizerSettingsList;

        public EqualizerSettingsAdapter(Activity activity, List<EqualizerSettings> climateControlCapabilitiesList)
        {
            this.context = activity;
            this.EqualizerSettingsList = climateControlCapabilitiesList;
        }

        public override EqualizerSettings this[int position] => EqualizerSettingsList[position];

        public override int Count => EqualizerSettingsList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            String str = "";
            if (EqualizerSettingsList[position].getChannelId() != null)
            {
                str = str + EqualizerSettingsList[position].getChannelId().ToString() + " - ";
            }
            if (EqualizerSettingsList[position].getChannelName() != null)
            {
                str = str + EqualizerSettingsList[position].getChannelName();
            }
            text.Text = str;
            return view;
        }
    }
}
