﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    public class FileTypeAdapter : BaseAdapter<FileType>
	{
        List<FileType> fileTypeList;
		Activity context;

		public FileTypeAdapter(Activity act, List<FileType> list) : base()
		{
			fileTypeList = list;
			context = act;
		}

		public override FileType this[int position] => fileTypeList[position];

		public override int Count => fileTypeList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = fileTypeList[position].ToString();

			return view;
		}
	}
}
