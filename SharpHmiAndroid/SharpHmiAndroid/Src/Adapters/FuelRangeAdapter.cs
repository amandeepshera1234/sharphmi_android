﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
	public class FuelRangeAdapter : BaseAdapter<FuelRange>
	{
		private List<FuelRange> fuelRangeList;
		private Activity context;

		public FuelRangeAdapter(Activity act, List<FuelRange> list) : base()
		{
			fuelRangeList = list;
			context = act;
		}

		public override FuelRange this[int position] => fuelRangeList[position];

		public override int Count =>
			
			fuelRangeList.Count;
	
        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = fuelRangeList[position].type.ToString();

            return view;
        }
    }
}