﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    public class ImageFieldAdapter : BaseAdapter<ImageField>
	{
        List<ImageField> imageFieldList;
		Activity context;
		ListView listView;

		public ViewStates isVisible;

		public ImageFieldAdapter(Activity act, List<ImageField> list, ViewStates Visibility, ListView listView) : base()
		{
			imageFieldList = list;
			context = act;
			isVisible = Visibility;
			this.listView = listView;

		}

		public override ImageField this[int position] => imageFieldList[position];

		public override int Count => imageFieldList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ImageField imageField = this[position];

			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);
			//var image_delete = view.FindViewById<ImageButton>(Resource.Id.delete);
			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			image_delete.Visibility = isVisible;
			image_delete.Click += (sender, e) =>
			{
				imageFieldList.Remove(imageField);
				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);

			};
			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = imageFieldList[position].getName().ToString();

			return view;
		}
	}
}
