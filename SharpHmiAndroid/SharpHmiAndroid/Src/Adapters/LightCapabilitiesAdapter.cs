﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class LightCapabilitiesAdapter : BaseAdapter<LightCapabilities>
    {
        List<LightCapabilities> lightCapList;
        Activity context;

        public LightCapabilitiesAdapter(Activity act, List<LightCapabilities> list) : base()
        {
            lightCapList = list;
            context = act;
        }

        public override LightCapabilities this[int position] => lightCapList[position];

        public override int Count => lightCapList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(
                Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = lightCapList[position].getName().ToString();

            return view;
        }
    }
}
