﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class LightStateAdapter : BaseAdapter<LightState>
    {
        Activity context;
        List<LightState> LightStateList;

        public LightStateAdapter(Activity activity, List<LightState> LightStateList) : base()
        {
            this.context = activity;
            this.LightStateList = LightStateList;
        }

        public override LightState this[int position] => LightStateList[position];

        public override int Count => LightStateList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            String str = "";
            if (LightStateList[position].getId() != null)
            {
                str = str + LightStateList[position].getId().ToString() + " - ";
            }
            if (LightStateList[position].getStatus() != null)
            {
                str = str + LightStateList[position].getStatus();
            }
            text.Text = str;
            return view;
        }
    }
}