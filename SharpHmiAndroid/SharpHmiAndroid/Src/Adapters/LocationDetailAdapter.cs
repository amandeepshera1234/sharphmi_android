﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    public class LocationDetailAdapter : BaseAdapter<LocationDetails>
	{
		List<LocationDetails> locationList;
		Activity context;

		public LocationDetailAdapter(Activity act, List<LocationDetails> list) : base()
		{
			locationList = list;
			context = act;
		}

		public override LocationDetails this[int position] => locationList[position];

		public override int Count => locationList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = locationList[position].getLocationName().ToString();

			return view;
		}
	}
}
