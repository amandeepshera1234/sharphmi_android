﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class MassageCushionAdapter : BaseAdapter<MassageCushionFirmness>
    {
        Activity context;
        List<MassageCushionFirmness> MassageCushionFirmnessList;

        public MassageCushionAdapter(Activity activity, List<MassageCushionFirmness> climateControlCapabilitiesList)
        {
            this.context = activity;
            this.MassageCushionFirmnessList = climateControlCapabilitiesList;
        }

        public override MassageCushionFirmness this[int position] => MassageCushionFirmnessList[position];

        public override int Count => MassageCushionFirmnessList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            String str = "";
            if (MassageCushionFirmnessList[position].getMassageCushion() != null)
            {
                str = str + MassageCushionFirmnessList[position].getMassageCushion().ToString() + " - ";
            }
            if (MassageCushionFirmnessList[position].getFirmness() != null)
            {
                str = str + MassageCushionFirmnessList[position].getFirmness().ToString();
            }
            text.Text = str;
            return view;
        }
    }
}
