﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class MassageModeDataAdapter : BaseAdapter<MassageModeData>
    {
        Activity context;
        List<MassageModeData> MassageModeDataList;

        public MassageModeDataAdapter(Activity activity, List<MassageModeData> climateControlCapabilitiesList)
        {
            this.context = activity;
            this.MassageModeDataList = climateControlCapabilitiesList;
        }

        public override MassageModeData this[int position] => MassageModeDataList[position];

        public override int Count => MassageModeDataList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            String str = "";
            if (MassageModeDataList[position].getMassageMode() != null)
            {
                str = str + MassageModeDataList[position].getMassageMode().ToString() + " - ";
            }
            if (MassageModeDataList[position].getMassageZone() != null)
            {
                str = str + MassageModeDataList[position].getMassageZone().ToString();
            }
            text.Text = str;
            return view;
        }
    }
}
