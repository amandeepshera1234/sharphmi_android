﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    public class PrerecordedSpeechAdapter : BaseAdapter<PrerecordedSpeech>
    {
        List<PrerecordedSpeech> preRecordedSpeechList;
        Activity context;

        public PrerecordedSpeechAdapter(Activity cntxt, List<PrerecordedSpeech> list)
        {
            context = cntxt;
            preRecordedSpeechList = list;
        }

        public override PrerecordedSpeech this[int position] => preRecordedSpeechList[position];

        public override int Count => preRecordedSpeechList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			var view = convertView ?? context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = preRecordedSpeechList[position].ToString();
			return view;
        }
    }
}
