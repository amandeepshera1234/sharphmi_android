﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
    public class SeatControlCapabilitiesAdapter : BaseAdapter<SeatControlCapabilities>
    {
        List<SeatControlCapabilities> seatControlCapabilities;
        Activity context;

        public SeatControlCapabilitiesAdapter(Activity act, List<SeatControlCapabilities> list) : base()
        {
            seatControlCapabilities = list;
            context = act;
        }

        public override SeatControlCapabilities this[int position] => seatControlCapabilities[position];

        public override int Count => seatControlCapabilities.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(
                Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = seatControlCapabilities[position].getModuleName();

            return view;
        }
    }
}
