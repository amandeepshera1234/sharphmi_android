﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
	class SeatLocationAdapter : BaseAdapter<SeatLocation>
	{
		List<SeatLocation> seatLocationList;
		Activity context;
		public ViewStates isVisible;

		public SeatLocationAdapter(Activity act, List<SeatLocation> list, ViewStates Visibility) : base()
		{
			seatLocationList = list;
			context = act;
			isVisible = Visibility;

		}

		public override SeatLocation this[int position] => seatLocationList[position];

		public override int Count => seatLocationList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			SeatLocation seatLocation = this[position];

			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			image_delete.Visibility = isVisible;
			image_delete.Click += (sender, e) =>
			{
				seatLocationList.Remove(seatLocation);
				this.NotifyDataSetChanged();

			};
			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			if(seatLocationList[position].getGrid()!=null)
			text.Text = seatLocationList[position].getGrid().col.ToString();

			return view;
		}
	}
}
