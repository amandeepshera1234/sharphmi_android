﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{ 
	public class SoftButtonCapabilitiesAdapter : BaseAdapter<SoftButtonCapabilities>
	{
        List<SoftButtonCapabilities> softButtonCapabilities;
		Activity context;
        private String ButtonText = "SoftButtonCap ";
		public ViewStates isVisible;
		ListView listView;

		public SoftButtonCapabilitiesAdapter(Activity act, List<SoftButtonCapabilities> list, ViewStates Visibility,ListView listView) : base()
		{
			softButtonCapabilities = list;
			context = act;
			isVisible = Visibility;
			this.listView = listView;

		}

		public override SoftButtonCapabilities this[int position] => softButtonCapabilities[position];

		public override int Count => softButtonCapabilities.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			SoftButtonCapabilities softBtnCapabilities = this[position];

			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);
			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);
		
			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);
			
			relativeLayout.AddView(image_delete, param);
			image_delete.Visibility = isVisible;

			image_delete.Click += (sender, e) =>
			{

				softButtonCapabilities.Remove(softBtnCapabilities);
				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);

			};
			

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);

			
			text.Text = ButtonText + (position + 1);

			return view;
		}

	}
}
