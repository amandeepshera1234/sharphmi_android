﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    public class TextFieldAdapter : BaseAdapter<TextField>
	{
        List<TextField> textFieldList;
		Activity context;
		ListView listView;

		public ViewStates isVisible;

		public TextFieldAdapter(Activity act, List<TextField> list, ViewStates Visibility, ListView listView) : base()
		{
			textFieldList = list;
			context = act;
			isVisible = Visibility;
			this.listView = listView;

		}

		public override TextField this[int position] => textFieldList[position];

		public override int Count => textFieldList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			TextField textField = this[position];
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);
			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			image_delete.Visibility = isVisible;
			image_delete.Click += (sender, e) =>
			{
				textFieldList.Remove(textField);
				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);

			};
			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
			text.Text = textFieldList[position].getName().ToString();

			return view;
		}
	}
}