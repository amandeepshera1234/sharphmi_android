﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid.Src.Adapters
{
    public class VideoStreamingCodecAdapter:BaseAdapter<VideoStreamingCodec>
    {
        List<VideoStreamingCodec> videoStreamingCodecList;
        Activity context;

        public VideoStreamingCodecAdapter(Activity cntxt, List<VideoStreamingCodec> list)
        {
            videoStreamingCodecList = list;
                context = cntxt;
        }

        public override VideoStreamingCodec this[int position] => videoStreamingCodecList[position];

        public override int Count => videoStreamingCodecList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = videoStreamingCodecList[position].ToString();
            return view;
        }
    }
}
