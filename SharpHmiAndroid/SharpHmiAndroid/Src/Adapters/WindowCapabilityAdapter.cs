﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid.Src.Adapters
{
	class WindowCapabilityAdapter : BaseAdapter<WindowCapability>
	{
		List<WindowCapability> windowCapList;
		Activity context;
		ListView listView;

		public WindowCapabilityAdapter(Activity act, List<WindowCapability> list, ListView listView) : base()
		{
			windowCapList = list;
			context = act;
			this.listView = listView;

		}

		public override WindowCapability this[int position] => windowCapList[position];

		public override int Count => windowCapList.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			WindowCapability WindowCapability = this[position];

			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);

			var param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,
			ViewGroup.LayoutParams.WrapContent);

			RelativeLayout relativeLayout = view.FindViewById<RelativeLayout>(Resource.Id.rl); ;
			ImageButton image_delete = new ImageButton(context);

			image_delete.SetBackgroundResource(Android.Resource.Drawable.IcDelete);
			param.AddRule(LayoutRules.AlignParentRight);

			relativeLayout.AddView(image_delete, param);
			//image_delete.Visibility = isVisible;

			image_delete.Click += (sender, e) =>
			{
				windowCapList.Remove(WindowCapability);
				this.NotifyDataSetChanged();
				Utility.setListViewHeightBasedOnChildren(listView);

			};
			if (windowCapList[position].windowID!=null)
			text.Text = windowCapList[position].windowID.ToString();
			return view;
		}
	}
}