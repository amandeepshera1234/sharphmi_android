﻿using System;
using Android.App;

namespace SharpHmiAndroid
{
    [Application]
	public class SharpHmiApplication : Application
	{
		private static SharpHmiApplication mInstance;

        public SharpHmiApplication(IntPtr handle, global::Android.Runtime.JniHandleOwnership transer)
           : base(handle, transer)
        {
        }

        public override void OnCreate()
		{
            base.OnCreate();
			mInstance = this;
		}

        public static SharpHmiApplication getInstance(){
            return mInstance;
        }
	}
}
