﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using Newtonsoft.Json;

namespace SharpHmiAndroid
{
	public class ConsoleFragment : Fragment
	{
        private string CategoryAll = "All";
        private string CategoryBasicCommunication = "Basic Communication";
        private string CategoryButtons = "Buttons";
        private string CategoryNavigation = "Navigation";
        private string CategoryRC = "RC";
        private string CategorySDL = "SDL";
        private string CategoryTTS = "TTS";
        private string CategoryUI = "UI";
        private string CategoryVehicleInfo = "Vehicle Info";
        private string CategoryVR = "VR";

        private string TX_LATER = "Tx Later";
        private string TX_NOW = "Tx Now";
        private string RESET = "Reset";
        private string OK = "Ok";
        private string CANCEL = "Cancel";

        // Basic Communication Outgoing Response
        private string BCResponseActivateApp = "BC.ActivateApp";
		private string BCResponseAllowDeviceToConnect = "BC.AllowDeviceToConnect";
        private string BCResponseDecryptCertificate = "BC.DecryptCertificate";
		private string BCResponseDialNumber = "BC.DialNumber";
		private string BCResponseGetSystemInfo = "BC.GetSystemInfo";
		private string BCResponseMixingAudioSupported = "BC.MixingAudioSupported";
		private string BCResponsePolicyUpdate = "BC.PolicyUpdate";
		private string BCResponseSystemRequest = "BC.SystemRequest";
		private string BCResponseUpdateAppList = "BC.UpdateAppList";
		private string BCResponseUpdateDeviceList = "BC.UpdateDeviceList";
        private string BCResponseGetSystemTime = "BC.GetSystemTime";

		// Basic Communication Outgoing Notification
		private string BCNotificationOnAppActivated = "BC.OnAppActivated";
		private string BCNotificationOnAppDeactivated = "BC.OnAppDeactivated";
		private string BCNotificationOnAwakeSDL = "BC.OnAwakeSDL";
		private string BCNotificationOnDeactivateHMI = "BC.OnDeactivateHMI";
		private string BCNotificationOnDeviceChosen = "BC.OnDeviceChosen";
		private string BCNotificationOnEmergencyEvent = "BC.OnEmergencyEvent";
		private string BCNotificationOnEventChanged = "BC.OnEventChanged";
		private string BCNotificationOnExitAllApplications = "BC.OnExitAllApplications";
		private string BCNotificationOnExitApplication = "BC.OnExitApplication";
		private string BCNotificationOnFindApplications = "BC.OnFindApplications";
		private string BCNotificationOnIgnitionCycleOver = "BC.OnIgnitionCycleOver";
		private string BCNotificationOnPhoneCall = "BC.OnPhoneCall";
		private string BCNotificationOnReady = "BC.OnReady";
		private string BCNotificationOnStartDeviceDiscovery = "BC.OnStartDeviceDiscovery";
		private string BCNotificationOnSystemInfoChanged = "BC.OnSystemInfoChanged";
		private string BCNotificationOnSystemRequest = "BC.OnSystemRequest";
		private string BCNotificationOnUpdateDeviceList = "BC.OnUpdateDeviceList";
        private string BCNotificationOnSystemTimeReady = "BC.OnSystemTimeReady";

		// Button Outgoing Response
		private string ButtonsResponseGetCapabilities = "Buttons.GetCapabilities";
        private string ButtonsResponseButtonPress = "Buttons.ButtonPress";

		// Button Outgoing Notifications
		private string ButtonsNotificationOnButtonEvent = "Buttons.OnButtonEvent";
		private string ButtonsNotificationOnButtonPress = "Buttons.OnButtonPress";

		// Navigation Outgoing Response
		private string NavigationResponseAlertManeuver = "Navigation.AlertManeuver";
		private string NavigationResponseGetWayPoints = "Navigation.GetWayPoints";
		private string NavigationResponseIsReady = "Navigation.IsReady";
		private string NavigationResponseSendLocation = "Navigation.SendLocation";
		private string NavigationResponseShowConstantTBT = "Navigation.ShowConstantTBT";
		private string NavigationResponseStartAudioStream = "Navigation.StartAudioStream";
		private string NavigationResponseStartStream = "Navigation.StartStream";
		private string NavigationResponseStopAudioStream = "Navigation.StopAudioStream";
		private string NavigationResponseStopStream = "Navigation.StopStream";
		private string NavigationResponseSubscribeWayPoints = "Navigation.SubscribeWayPoints";
		private string NavigationResponseUnsubscribeWayPoints = "Navigation.UnsubscribeWayPoints";
		private string NavigationResponseUpdateTurnList = "Navigation.UpdateTurnList";
        private string NavigationResponseSetVideoConfig = "Navigation.SetVideoConfig";

		// Navigation Outgoing Notifications
		private string NavigationNotificationOnTBTClientState = "Navigation.OnTBTClientState";

        // RC Outgoing Response
        private string RCResponseGetCapabilities = "RC.GetCapabilities";
        private string RCResponseGetInteriorVehicleData = "RC.GetInteriorVehicleData";
        private string RCResponseGetInteriorVehicleDataConsent = "RC.GetInteriorVehicleDataConsent";
		private string RCResponseReleaseInteriorVehicleDataConsent = "RC.ReleaseInteriorVehicleDataConsent";

		private string RCResponseIsReady = "RC.IsReady";
        private string RCResponseSetInteriorVehicleData = "RC.SetInteriorVehicleData";

		// RC Outgoing Notifications
		private string RCNotificationOnInteriorVehicleData = "RC.OnInteriorVehicleData";
        private string RCNotificationOnRemoteControlSettings = "RC.OnRemoteControlSettings";

		// SDL Outgoing Request
		private string SDLRequestActivateApp = "SDL.ActivateApp";
		private string SDLRequestGetListOfPermissions = "SDL.GetListOfPermissions";
		private string SDLRequestGetStatusUpdate = "SDL.GetStatusUpdate";
		private string SDLRequestGetURLS = "SDL.GetURLS";
		private string SDLRequestGetUserFriendlyMessage = "SDL.GetUserFriendlyMessage";
		private string SDLRequestUpdateSDL = "SDL.UpdateSDL";

		// SDL Outgoing Notifications
		private string SDLNotificationOnAllowSDLFunctionality = "SDL.OnAllowSDLFunctionality";
		private string SDLNotificationOnAppPermissionConsent = "SDL.OnAppPermissionConsent";
		private string SDLNotificationOnPolicyUpdate = "SDL.OnPolicyUpdate";
		private string SDLNotificationOnReceivedPolicyUpdate = "SDL.OnReceivedPolicyUpdate";

		// TTS Outgoing Response
		private string TTSResponseChangeRegistration = "TTS.ChangeRegistration";
		private string TTSResponseGetCapabilities = "TTS.GetCapabilities";
		private string TTSResponseGetLanguage = "TTS.GetLanguage";
		private string TTSResponseGetSupportedLanguages = "TTS.GetSupportedLanguages";
		private string TTSResponseIsReady = "TTS.IsReady";
		private string TTSResponseSetGlobalProperties = "TTS.SetGlobalProperties";
		private string TTSResponseSpeak = "TTS.Speak";
		private string TTSResponseStopSpeaking = "TTS.StopSpeaking";

		// TTS Outgoing Notifications
		private string TTSNotificationOnLanguageChange = "TTS.OnLanguageChange";
		private string TTSNotificationOnResetTimeout = "TTS.OnResetTimeout";
		private string TTSNotificationStarted = "TTS.Started";
		private string TTSNotificationStopped = "TTS.Stopped";

		// UI Outgoing Response
		private string UIResponseAddCommand = "UI.AddCommand";
		private string UIResponseCreateWindow = "UI.CreateWindow";

		private string UIResponseAddSubMenu = "UI.AddSubMenu";
		private string UIResponseAlert = "UI.Alert";
		private string UIResponseChangeRegistration = "UI.ChangeRegistration";
		private string UIResponseClosePopUp = "UI.ClosePopUp";
		private string UIResponseDeleteCommand = "UI.DeleteCommand";
		private string UIResponseDeleteWindow = "UI.DeleteWindow";

		private string UIResponseDeleteSubMenu = "UI.DeleteSubMenu";
		private string UIResponseEndAudioPassThru = "UI.EndAudioPassThru";
		private string UIResponseGetCapabilities = "UI.GetCapabilities";
		private string UIResponseGetLanguage = "UI.GetLanguage";
		private string UIResponseGetSupportedLanguages = "UI.GetSupportedLanguages";
		private string UIResponseIsReady = "UI.IsReady";
		private string UIResponsePerformAudioPassThru = "UI.PerformAudioPassThru";
		private string UIResponsePerformInteraction = "UI.PerformInteraction";
		private string UIResponseScrollableMessage = "UI.ScrollableMessage";
		private string UIResponseSetAppIcon = "UI.SetAppIcon";
		private string UIResponseSetDisplayLayout = "UI.SetDisplayLayout";
		private string UIResponseSetGlobalProperties = "UI.SetGlobalProperties";
		private string UIResponseSetMediaClockTimer = "UI.SetMediaClockTimer";
		private string UIResponseShow = "UI.Show";
		private string UIResponseShowCustomForm = "UI.ShowCustomForm";
		private string UIResponseSlider = "UI.Slider";
        private string UIResponseShowAppMenu = "UI.ShowAppMenu";
        private string UIResponseCloseApplication = "UI.CloseApplication";

        // UI Outgoing Notifications
        private string UINotificationOnCommand = "UI.OnCommand";
		private string UINotificationOnDriverDistraction = "UI.OnDriverDistraction";
		private string UINotificationOnKeyboardInput = "UI.OnKeyboardInput";
		private string UINotificationOnLanguageChange = "UI.OnLanguageChange";
		//private string UINotificationOnRecordStart = "UI.OnRecordStart";
		private string UINotificationOnResetTimeout = "UI.OnResetTimeout";
		private string UINotificationOnSystemContext = "UI.OnSystemContext";
		private string UINotificationOnTouchEvent = "UI.OnTouchEvent";

		// VehicleInfo Outgoing Response
		private string VIResponseDiagnosticMessage = "VI.DiagnosticMessage";
		private string VIResponseGetDTCs = "VI.GetDTCs";
		private string VIResponseGetVehicleData = "VI.GetVehicleData";
		private string VIResponseGetVehicleType = "VI.GetVehicleType";
		private string VIResponseIsReady = "VI.IsReady";
		private string VIResponseReadDID = "VI.ReadDID";
		private string VIResponseSubscribeVehicleData = "VI.SubscribeVehicleData";
		private string VIResponseUnsubscribeVehicleData = "VI.UnsubscribeVehicleData";

		// VehicleInfo Outgoing Notifications
		private string VINotificationOnVehicleData = "VI.OnVehicleData";

		// VR Outgoing Response
		private string VRResponseAddCommand = "VR.AddCommand";
		private string VRResponseChangeRegistration = "VR.ChangeRegistration";
		private string VRResponseDeleteCommand = "VR.DeleteCommand";
		private string VRResponseGetCapabilities = "VR.GetCapabilities";
		private string VRResponseGetLanguage = "VR.GetLanguage";
		private string VRResponseGetSupportedLanguages = "VR.GetSupportedLanguages";
		private string VRResponseIsReady = "VR.IsReady";
		private string VRResponsePerformInteraction = "VR.PerformInteraction";

		// VR Outgoing Notifications
		private string VRNotificationOnCommand = "VR.OnCommand";
		private string VRNotificationOnLanguageChange = "VR.OnLanguageChange";
		private string VRNotificationStarted = "VR.Started";
		private string VRNotificationStopped = "VR.Stopped";



		private ListView _listview = null;
		public MessageAdapter _msgAdapter = null;
		private List<LogMessage> _logMessages = new List<LogMessage>();

		//int appID;
		////public static readonly String sClickedAppID = "APP_ID";

		LayoutInflater layoutInflater;


		string[] resultCode = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
		string[] vehicleDataType = Enum.GetNames(typeof(VehicleDataType));
        string[] vehicleDataResultCode = Enum.GetNames(typeof(VehicleDataResultCode));
		string[] languages = Enum.GetNames(typeof(Language));
		string[] transportType = Enum.GetNames(typeof(TransportType));
		string[] eventTypes = Enum.GetNames(typeof(EventTypes));
		string[] fileType = Enum.GetNames(typeof(FileType));
		string[] requestType = Enum.GetNames(typeof(RequestType));
		string[] appsCloseReason = Enum.GetNames(typeof(ApplicationsCloseReason));
		string[] appsExitReason = Enum.GetNames(typeof(ApplicationExitReason));
		string[] buttonNames = Enum.GetNames(typeof(ButtonName));
		string[] buttonEventMode = Enum.GetNames(typeof(ButtonEventMode));
		string[] buttonPressMode = Enum.GetNames(typeof(ButtonPressMode));
		string[] imageType = Enum.GetNames(typeof(ImageType));
		string[] driverDistractionState = Enum.GetNames(typeof(DriverDistractionState));
		string[] keyBoardEvent = Enum.GetNames(typeof(KeyboardEvent));
		string[] vrCapabilities = Enum.GetNames(typeof(VrCapabilities));
		string[] speechCapabilities = Enum.GetNames(typeof(SpeechCapabilities));
        string[] prerecordedSpeech = Enum.GetNames(typeof(PrerecordedSpeech));
        string[] tbtStateArray = Enum.GetNames(typeof(TBTState));
		String[] PowerModeQualificationStatusArray = Enum.GetNames(typeof(PowerModeQualificationStatus));
		String[] CarModeStatusArray = Enum.GetNames(typeof(CarModeStatus));
		String[] PowerModeStatusArray = Enum.GetNames(typeof(PowerModeStatus));
		String[] EmergencyEventTypeArray = Enum.GetNames(typeof(EmergencyEventType));
		String[] FuelCutoffStatusArray = Enum.GetNames(typeof(FuelCutoffStatus));
		string[] vehicleDataNotificationStatusArray = Enum.GetNames(typeof(VehicleDataNotificationStatus));
		string[] ECallConfirmationStatusArray = Enum.GetNames(typeof(ECallConfirmationStatus));
        string[] AmbientLightStatusArray = Enum.GetNames(typeof(AmbientLightStatus));
		String[] DeviceLevelStatusArray = Enum.GetNames(typeof(DeviceLevelStatus));
		String[] PrimaryAudioSourceArray = Enum.GetNames(typeof(PrimaryAudioSource));
		String[] IgnitionStableStatusArray = Enum.GetNames(typeof(IgnitionStableStatus));
		String[] IgnitionStatusArray = Enum.GetNames(typeof(IgnitionStatus));
		String[] WarningLightStatusArray = Enum.GetNames(typeof(WarningLightStatus));
		String[] SingleTireStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
		String[] CompassDirectionArray = Enum.GetNames(typeof(CompassDirection));
		String[] DimensionArray = Enum.GetNames(typeof(Dimension));
		String[] PRNDLArray = Enum.GetNames(typeof(PRNDL));
		String[] WiperStatusArray = Enum.GetNames(typeof(WiperStatus));
		String[] VehicleDataStatusArray = Enum.GetNames(typeof(VehicleDataStatus));
        String[] ComponentVolumeStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] VehicleDataEventStatusArray = Enum.GetNames(typeof(VehicleDataEventStatus));
        String[] RCAccessModeArray = Enum.GetNames(typeof(RCAccessMode));
        String[] RCModuleTypeArray = Enum.GetNames(typeof(ModuleType));
        String[] RCRadioBandArray = Enum.GetNames(typeof(RadioBand));
        String[] RCRadioStateArray = Enum.GetNames(typeof(RadioState));
        String[] RCTempUnitArray= Enum.GetNames(typeof(TemperatureUnit));
        String[] RCDefrostZoneArray= Enum.GetNames(typeof(DefrostZone));
        String[] RCVentilationModeArray= Enum.GetNames(typeof(VentilationMode));
        String[] VIElectronicParkBrakeStatusArray = Enum.GetNames(typeof(ElectronicParkBrakeStatus));
        String[] TurnSignalArray = Enum.GetNames(typeof(TurnSignal));

		string[] systemContext = Enum.GetNames(typeof(SystemContext));

		RadioControlData radioControlData = null;
		ClimateControlData climateControlData = null;
        SeatControlData seatControlData = null;
        AudioControlData audioControlData = null;
        LightControlData lightControlData = null;
        HMISettingsControlData hmiSettingControlData = null;
        SisData sisData = null;
        StationIDNumber stationIDNumber = null;
        GPSData gpsData = null;

		public ConsoleFragment()
		{
			
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			if (AppInstanceManager.Instance.getMsgAdapter() == null)
			{
				_msgAdapter = new MessageAdapter(this.Activity, _logMessages);
				AppInstanceManager.Instance.setMsgAdapter(_msgAdapter);
			}
			else
			{
				_msgAdapter = AppInstanceManager.Instance.getMsgAdapter();
				_msgAdapter.updateActivity(this.Activity);
			}

			if (SdlService.instance == null)
			{
				var intent = new Intent((MainActivity)this.Activity, typeof(SdlService));
				((MainActivity)this.Activity).StartService(intent);
			}
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.Inflate(Resource.Layout.console_fragment, container,
											 false);
			layoutInflater = inflater;
			// appID = Arguments.GetIntArray(sClickedAppID);

			_listview = (ListView)rootView.FindViewById(Resource.Id.messageList);
			_listview.Clickable = true;

			_listview.Adapter = _msgAdapter;
			_listview.TranscriptMode = TranscriptMode.AlwaysScroll;

			_listview.ItemClick += listView_ItemClick;

			if (_listview.Adapter.Count > 10)
			{
				_listview.StackFromBottom = true;
			}

            Button sendRPCButton = (Button)rootView.FindViewById(Resource.Id.send_rpc);
            sendRPCButton.Click += (object sender, EventArgs e) => showRPCListDialog();

			return rootView;
		}

		private void showDialogWithBack(string sTitle, string sBody, Boolean isRpcResendAllowed, AlertDialog.Builder builder, View jsonLayout)
		{
			AlertDialog.Builder dialogNew = new AlertDialog.Builder(this.Activity);
			dialogNew.SetMessage(sBody);
			dialogNew.SetTitle("Show Getter Methods");
			dialogNew.SetPositiveButton("Back", (senderAlert, args) =>
				{

					if (jsonLayout != null)
					{
						ViewGroup parent = (ViewGroup)jsonLayout.Parent;
						if (parent != null)
						{
							parent.RemoveView(jsonLayout);
						}
					}
					AlertDialog alertDlg = builder.Create();
					alertDlg.Show();

					Button resendRpcAllowed = alertDlg.GetButton((int)DialogButtonType.Neutral);
					if (isRpcResendAllowed)
					{
						resendRpcAllowed.Enabled = true;
					}
					else
					{
						resendRpcAllowed.Enabled = false;
					}

				});
			AlertDialog ad = dialogNew.Create();
			ad.Show();
		}

		void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			Object listObj = _msgAdapter[e.Position];

			if (listObj is RpcLogMessage)
			{
				LayoutInflater requestJSON = (LayoutInflater)this.Activity.GetSystemService(Context.LayoutInflaterService);
				View jsonLayout = requestJSON.Inflate(Resource.Layout.consolelogpreview, null);
				EditText jsonText = (EditText)jsonLayout.FindViewById(Resource.Id.consoleLogPreview_jsonContent);
                CheckBox chkBoxEditJSON = (CheckBox)jsonLayout.FindViewById(Resource.Id.consoleLogPreview_jsonContent_editChkBox);

				RpcMessage message = ((RpcLogMessage)listObj).getMessage();
				AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);

				int corrId = -1;

                jsonText.Focusable = chkBoxEditJSON.Checked;

                chkBoxEditJSON.Visibility = ViewStates.Gone;

				if (message is RpcRequest)
				{
					corrId = ((RpcRequest)message).getId();
				}
				else if (message is RpcResponse)
				{
                    if (null != ((RpcResponse)message).getId())
					    corrId = (int)((RpcResponse)message).getId();
				}

                if(message.getRpcMessageFlow() == RpcMessageFlow.OUTGOING)
                {
                    chkBoxEditJSON.Visibility = ViewStates.Visible;
                }

                string rawJSON = Utils.getSerializedRpcMessage(message);

                if (rawJSON == null) return;

				string finalJSON = rawJSON;

				jsonText.Text = finalJSON;

				chkBoxEditJSON.CheckedChange += (s, args) =>
				{
                    if (!args.IsChecked)
                    {
                        jsonText.Text = finalJSON;
                        jsonText.Focusable = false;
                    }
                    else 
                    {
                        jsonText.FocusableInTouchMode = true;
                    }
				};

				builder.SetTitle("Raw JSON" + (corrId != -1 ? " (Corr ID " + corrId + ")" : ""));

				builder.SetView(jsonLayout);

				builder.SetPositiveButton("Getters", (senderAlert, args) =>
				{
					string sInfo = RpcMessageGetterInfo.viewDetails(message, false, 0);
					Boolean isRpcResendAllowed = false;

					if (message.rpcMessageFlow == HmiApiLib.Common.Enums.RpcMessageFlow.OUTGOING)
					{
						isRpcResendAllowed = true;
					}

					showDialogWithBack("GetterInfo", sInfo, isRpcResendAllowed, builder, jsonLayout);
				});

				builder.SetNeutralButton("Resend", (senderAlert, args) =>
				{
                    RpcMessage updMsg = null;
                    updMsg = Utils.getDeserializedRpcMessage(jsonText.Text);

                    if(updMsg != null) 
                    {
						AppInstanceManager.Instance.sendRpc((RpcMessage)updMsg);                        
                    }
				});

				builder.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					builder.Dispose();
				});

				AlertDialog ad = builder.Create();
				ad.Show();

				Button resendRpc = ad.GetButton((int)DialogButtonType.Neutral);
				if (message.rpcMessageFlow == HmiApiLib.Common.Enums.RpcMessageFlow.OUTGOING)
				{
					resendRpc.Enabled = true;
				}
				else
				{
					resendRpc.Enabled = false;
				}

			}

			else if (listObj is StringLogMessage)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);
				string sMessageText = ((StringLogMessage)listObj).getData();
				if (sMessageText == "")
				{
					sMessageText = ((StringLogMessage)listObj).getMessage();
				}
				builder.SetMessage(sMessageText);
				builder.SetPositiveButton("OK", (senderAlert, args) =>
				{
					builder.Dispose();
				});
				AlertDialog ad = builder.Create();
				ad.Show();
			}
		}

		public void showRPCListDialog()
		{
            string[] categoryListArray = {CategoryAll, CategoryBasicCommunication, CategoryButtons, 
                CategoryNavigation, CategoryRC, CategorySDL, CategoryTTS, CategoryUI, CategoryVehicleInfo, CategoryVR};
            
			string[] BasicCommunicationArray = {BCResponseActivateApp, BCResponseAllowDeviceToConnect, BCResponseDecryptCertificate, BCResponseDialNumber,
				BCResponseGetSystemInfo, BCResponseMixingAudioSupported, BCResponsePolicyUpdate, BCResponseSystemRequest,
                BCResponseUpdateAppList, BCResponseUpdateDeviceList, BCResponseGetSystemTime, BCNotificationOnAppActivated, BCNotificationOnAppDeactivated,
				BCNotificationOnAwakeSDL, BCNotificationOnDeactivateHMI, BCNotificationOnDeviceChosen,
				BCNotificationOnEmergencyEvent, BCNotificationOnEventChanged, BCNotificationOnExitAllApplications,
				BCNotificationOnExitApplication, BCNotificationOnFindApplications, BCNotificationOnIgnitionCycleOver,
				BCNotificationOnPhoneCall, BCNotificationOnReady, BCNotificationOnStartDeviceDiscovery,
                BCNotificationOnSystemInfoChanged, BCNotificationOnSystemRequest, BCNotificationOnUpdateDeviceList, BCNotificationOnSystemTimeReady};
            
            string[] ButtonsArray = {ButtonsResponseGetCapabilities, ButtonsResponseButtonPress, ButtonsNotificationOnButtonEvent, ButtonsNotificationOnButtonPress};

			string[] NavigationArray = {NavigationResponseAlertManeuver, NavigationResponseGetWayPoints, NavigationResponseIsReady,
				NavigationResponseSendLocation, NavigationResponseShowConstantTBT, NavigationResponseStartAudioStream,
				NavigationResponseStartStream, NavigationResponseStopAudioStream, NavigationResponseStopStream,
                NavigationResponseSubscribeWayPoints, NavigationResponseUnsubscribeWayPoints, NavigationResponseUpdateTurnList, NavigationResponseSetVideoConfig,
                NavigationNotificationOnTBTClientState};

            string[] RCArray = {RCResponseGetCapabilities, RCResponseGetInteriorVehicleData, RCResponseGetInteriorVehicleDataConsent,RCResponseReleaseInteriorVehicleDataConsent, RCResponseIsReady,
                RCResponseSetInteriorVehicleData, RCNotificationOnInteriorVehicleData, RCNotificationOnRemoteControlSettings};

			string[] SDLArray = {SDLRequestActivateApp, SDLRequestGetListOfPermissions,
				SDLRequestGetStatusUpdate, SDLRequestGetURLS, SDLRequestGetUserFriendlyMessage, SDLRequestUpdateSDL,
				SDLNotificationOnAllowSDLFunctionality, SDLNotificationOnAppPermissionConsent, SDLNotificationOnPolicyUpdate,
				SDLNotificationOnReceivedPolicyUpdate};

			string[] TTSArray = {TTSResponseChangeRegistration, TTSResponseGetCapabilities, TTSResponseGetLanguage, 
                TTSResponseGetSupportedLanguages, TTSResponseIsReady, TTSResponseSetGlobalProperties,
				TTSResponseSpeak, TTSResponseStopSpeaking, TTSNotificationOnLanguageChange, TTSNotificationOnResetTimeout,
				TTSNotificationStarted, TTSNotificationStopped};

			string[] UIArray = {UIResponseAddCommand, UIResponseCreateWindow, UIResponseAddSubMenu, UIResponseAlert, UIResponseChangeRegistration, 
                UIResponseClosePopUp, UIResponseDeleteCommand,UIResponseDeleteWindow, UIResponseDeleteSubMenu, UIResponseEndAudioPassThru, 
                UIResponseGetCapabilities, UIResponseGetLanguage, UIResponseGetSupportedLanguages,
				UIResponseIsReady, UIResponsePerformAudioPassThru, UIResponsePerformInteraction, UIResponseScrollableMessage,
				UIResponseSetAppIcon, UIResponseSetDisplayLayout, UIResponseSetGlobalProperties, UIResponseSetMediaClockTimer,
				UIResponseShow, UIResponseShowCustomForm, UIResponseSlider, UINotificationOnCommand, UINotificationOnDriverDistraction,
				UINotificationOnKeyboardInput, UINotificationOnLanguageChange, UINotificationOnResetTimeout,
				UINotificationOnSystemContext, UINotificationOnTouchEvent, UIResponseShowAppMenu, UIResponseCloseApplication};

			string[] VehicleInfoArray = {VIResponseDiagnosticMessage, VIResponseGetDTCs,
				VIResponseGetVehicleData, VIResponseGetVehicleType, VIResponseIsReady, VIResponseReadDID,
				VIResponseSubscribeVehicleData, VIResponseUnsubscribeVehicleData, VINotificationOnVehicleData};

			string[] VRArray = {VRResponseAddCommand, VRResponseChangeRegistration, VRResponseDeleteCommand, 
                VRResponseGetCapabilities, VRResponseGetLanguage, VRResponseGetSupportedLanguages, VRResponseIsReady, 
                VRResponsePerformInteraction, VRNotificationOnCommand, VRNotificationOnLanguageChange, 
                VRNotificationStarted, VRNotificationStopped};

            String[] allRpcListArray = { BCResponseActivateApp, BCResponseAllowDeviceToConnect, BCResponseDecryptCertificate, BCResponseDialNumber,
				BCResponseGetSystemInfo, BCResponseMixingAudioSupported, BCResponsePolicyUpdate, BCResponseSystemRequest,
                BCResponseUpdateAppList, BCResponseUpdateDeviceList, BCResponseGetSystemTime, BCNotificationOnAppActivated, BCNotificationOnAppDeactivated,
				BCNotificationOnAwakeSDL, BCNotificationOnDeactivateHMI, BCNotificationOnDeviceChosen,
				BCNotificationOnEmergencyEvent, BCNotificationOnEventChanged, BCNotificationOnExitAllApplications,
				BCNotificationOnExitApplication, BCNotificationOnFindApplications, BCNotificationOnIgnitionCycleOver,
				BCNotificationOnPhoneCall, BCNotificationOnReady, BCNotificationOnStartDeviceDiscovery,
                BCNotificationOnSystemInfoChanged, BCNotificationOnSystemRequest, BCNotificationOnUpdateDeviceList, BCNotificationOnSystemTimeReady,
                ButtonsResponseGetCapabilities, ButtonsResponseButtonPress,ButtonsNotificationOnButtonEvent, ButtonsNotificationOnButtonPress,
				NavigationResponseAlertManeuver, NavigationResponseGetWayPoints, NavigationResponseIsReady,
				NavigationResponseSendLocation, NavigationResponseShowConstantTBT, NavigationResponseStartAudioStream,
				NavigationResponseStartStream, NavigationResponseStopAudioStream, NavigationResponseStopStream,
                NavigationResponseSubscribeWayPoints, NavigationResponseUnsubscribeWayPoints, NavigationResponseUpdateTurnList, NavigationResponseSetVideoConfig,
                NavigationNotificationOnTBTClientState, RCResponseGetCapabilities, RCResponseGetInteriorVehicleData, 
                RCResponseGetInteriorVehicleDataConsent,RCResponseReleaseInteriorVehicleDataConsent,RCResponseIsReady, RCResponseSetInteriorVehicleData, 
                RCNotificationOnInteriorVehicleData, RCNotificationOnRemoteControlSettings, SDLRequestActivateApp, SDLRequestGetListOfPermissions,
				SDLRequestGetStatusUpdate, SDLRequestGetURLS, SDLRequestGetUserFriendlyMessage, SDLRequestUpdateSDL,
				SDLNotificationOnAllowSDLFunctionality, SDLNotificationOnAppPermissionConsent, SDLNotificationOnPolicyUpdate,
				SDLNotificationOnReceivedPolicyUpdate, TTSResponseChangeRegistration, TTSResponseGetCapabilities,
				TTSResponseGetLanguage, TTSResponseGetSupportedLanguages, TTSResponseIsReady, TTSResponseSetGlobalProperties,
				TTSResponseSpeak, TTSResponseStopSpeaking, TTSNotificationOnLanguageChange, TTSNotificationOnResetTimeout,
				TTSNotificationStarted, TTSNotificationStopped, UIResponseAddCommand,UIResponseCreateWindow, UIResponseAddSubMenu, UIResponseAlert,
				UIResponseChangeRegistration, UIResponseClosePopUp, UIResponseDeleteCommand,UIResponseDeleteWindow, UIResponseDeleteSubMenu,
				UIResponseEndAudioPassThru, UIResponseGetCapabilities, UIResponseGetLanguage, UIResponseGetSupportedLanguages,
				UIResponseIsReady, UIResponsePerformAudioPassThru, UIResponsePerformInteraction, UIResponseScrollableMessage,
				UIResponseSetAppIcon, UIResponseSetDisplayLayout, UIResponseSetGlobalProperties, UIResponseSetMediaClockTimer,
				UIResponseShow, UIResponseShowCustomForm, UIResponseSlider, UINotificationOnCommand, UINotificationOnDriverDistraction,
				UINotificationOnKeyboardInput, UINotificationOnLanguageChange, UINotificationOnResetTimeout,
				UINotificationOnSystemContext, UINotificationOnTouchEvent, UIResponseShowAppMenu, UIResponseCloseApplication, 
                VIResponseDiagnosticMessage, VIResponseGetDTCs,
				VIResponseGetVehicleData, VIResponseGetVehicleType, VIResponseIsReady, VIResponseReadDID,
				VIResponseSubscribeVehicleData, VIResponseUnsubscribeVehicleData, VINotificationOnVehicleData, VRResponseAddCommand,
				VRResponseChangeRegistration, VRResponseDeleteCommand, VRResponseGetCapabilities,
				VRResponseGetLanguage, VRResponseGetSupportedLanguages, VRResponseIsReady, VRResponsePerformInteraction,
                VRNotificationOnCommand, VRNotificationOnLanguageChange, VRNotificationStarted, VRNotificationStopped};

			AlertDialog.Builder rpcListAlertDialog = new AlertDialog.Builder(Activity);
			View layout = (View)layoutInflater.Inflate(Resource.Layout.rpclistLayout, null);
			rpcListAlertDialog.SetView(layout);
			rpcListAlertDialog.SetTitle("Pick an RPC");

			rpcListAlertDialog.SetNegativeButton("Done", (senderAlert, args) =>
			{
				rpcListAlertDialog.Dispose();
			});

            ArrayAdapter<String> selectionSpinnerAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleListItem1, categoryListArray);
            Spinner rpcSelectionSpinner = layout.FindViewById<Spinner>(Resource.Id.rpc_list_selection_spinner);
            rpcSelectionSpinner.Adapter = selectionSpinnerAdapter;

            String[] selectedRPC = allRpcListArray;

			ListView rpcListView = (ListView)layout.FindViewById(Resource.Id.rpc_list_view);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleListItem1, selectedRPC);
			rpcListView.Adapter = adapter;

			rpcSelectionSpinner.ItemSelected += (s,e) =>
			{
				switch (e.Position)
				{
					case 0:
						selectedRPC = allRpcListArray;
						break;
					case 1:
						selectedRPC = BasicCommunicationArray;
						break;
					case 2:
						selectedRPC = ButtonsArray;
						break;
					case 3:
						selectedRPC = NavigationArray;
						break;
					case 4:
                        selectedRPC = RCArray;
                        break;
					case 5:
						selectedRPC = SDLArray;
						break;
					case 6:
						selectedRPC = TTSArray;
						break;
					case 7:
						selectedRPC = UIArray;
						break;
					case 8:
						selectedRPC = VehicleInfoArray;
						break;
					case 9:
						selectedRPC = VRArray;
						break;
				}
                Activity.RunOnUiThread(() => 
                {
					adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleListItem1, selectedRPC);
					rpcListView.Adapter = adapter;
					adapter.NotifyDataSetChanged();
                });
			};

			rpcListView.ItemClick += (object sender, Android.Widget.AdapterView.ItemClickEventArgs e) =>
			 {
				 string clickedItem = rpcListView.GetItemAtPosition(e.Position).ToString();


				 if (clickedItem.Equals(BCResponseActivateApp))
				 {
					 CreateBCResponseActivateApp();
				 }
				 if (clickedItem.Equals(BCResponseAllowDeviceToConnect))
				 {
					 CreateBCResponseAllowDeviceToConnect();
				 }
                 if (clickedItem.Equals(BCResponseDecryptCertificate))
                 {
                     CreateBCResponseDecryptCertificate();
                 }
                 else if (clickedItem.Equals(BCResponseDialNumber))
                 {
                     CreateBCResponseDialNumber();
                 }
                 else if (clickedItem.Equals(BCResponseGetSystemInfo))
                 {
                     ((MainActivity)Activity).CreateBCResponseGetSystemInfo();
                 }
                 else if (clickedItem.Equals(BCResponseMixingAudioSupported))
                 {
                     ((MainActivity)Activity).CreateBCResponseMixingAudioSupported();
                 }
                 else if (clickedItem.Equals(BCResponsePolicyUpdate))
                 {
                     CreateBCResponsePolicyUpdate();
                 }
                 else if (clickedItem.Equals(BCResponseSystemRequest))
                 {
                     CreateBCResponseSystemRequest();
                 }
                 else if (clickedItem.Equals(BCResponseUpdateAppList))
                 {
                     CreateBCResponseUpdateAppList();
                 }
                 else if (clickedItem.Equals(BCResponseUpdateDeviceList))
                 {
                     CreateBCResponseUpdateDeviceList();
                }
                 else if (clickedItem.Equals(BCResponseGetSystemTime))
                 {
                     CreateBCResponseGetSystemTime();
                 }
                 else if (clickedItem.Equals(BCNotificationOnAppActivated))
                 {
                     CreateBCNotificationOnAppActivated();
                 }
                 else if (clickedItem.Equals(BCNotificationOnAppDeactivated))
                 {
                     CreateBCNotificationOnAppDeactivated();
                 }
                 else if (clickedItem.Equals(BCNotificationOnAwakeSDL))
                 {
                     CreateBCNotificationOnAwakeSDL();
                 }
                 else if (clickedItem.Equals(BCNotificationOnDeactivateHMI))
                 {
                     CreateBCNotificationOnDeactivateHMI();
                 }
                 else if (clickedItem.Equals(BCNotificationOnDeviceChosen))
                 {
                     CreateBCNotificationOnDeviceChosen();
                 }
                 else if (clickedItem.Equals(BCNotificationOnEmergencyEvent))
                 {
                     CreateBCNotificationOnEmergencyEvent();
                 }
                 else if (clickedItem.Equals(BCNotificationOnEventChanged))
                 {
                     CreateBCNotificationOnEventChanged();
                 }
                 else if (clickedItem.Equals(BCNotificationOnExitAllApplications))
                 {
                     CreateBCNotificationOnExitAllApplications();
                 }
                 else if (clickedItem.Equals(BCNotificationOnExitApplication))
                 {
                     CreateBCNotificationOnExitApplication();
                 }
                 else if (clickedItem.Equals(BCNotificationOnFindApplications))
                 {
                     CreateBCNotificationOnFindApplications();
                 }
                 else if (clickedItem.Equals(BCNotificationOnIgnitionCycleOver))
                 {
                     CreateBCNotificationOnIgnitionCycleOver();
                 }
                 else if (clickedItem.Equals(BCNotificationOnPhoneCall))
                 {
                     CreateBCNotificationOnPhoneCall();
                 }
                 else if (clickedItem.Equals(BCNotificationOnReady))
                 {
                     CreateBCNotificationOnReady();
                 }
                 else if (clickedItem.Equals(BCNotificationOnStartDeviceDiscovery))
                 {
                     CreateBCNotificationOnStartDeviceDiscovery();
                 }
                 else if (clickedItem.Equals(BCNotificationOnSystemInfoChanged))
                 {
                     CreateBCNotificationOnSystemInfoChanged();
                 }
                 else if (clickedItem.Equals(BCNotificationOnSystemRequest))
                 {
                     CreateBCNotificationOnSystemRequest();
                 }
                 else if (clickedItem.Equals(BCNotificationOnUpdateDeviceList))
                 {
                     CreateBCNotificationOnUpdateDeviceList();
                 }
                else if (clickedItem.Equals(BCNotificationOnSystemTimeReady))
                 {
                    CreateBCNotificationOnSystemTimeReady();
                 }
				 else if (clickedItem.Equals(ButtonsResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateButtonsGetCapabilities();
				 }
                else if (clickedItem.Equals(ButtonsResponseButtonPress))
				 {
                    CreateButtonsResponseButtonPress();
				 }
				 else if (clickedItem.Equals(ButtonsNotificationOnButtonEvent))
				 {
					 CreateButtonsNotificationOnButtonEvent();
				 }
				 else if (clickedItem.Equals(ButtonsNotificationOnButtonPress))
				 {
					 CreateButtonsNotificationOnButtonPress();
				 }
				 else if (clickedItem.Equals(NavigationResponseAlertManeuver))
				 {
					 CreateNavigationResponseAlertManeuver();
				 }
				 else if (clickedItem.Equals(NavigationResponseGetWayPoints))
				 {
					 CreateNavigationResponseGetWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateNavigationResponseIsReady();
				 }
				 else if (clickedItem.Equals(NavigationResponseSendLocation))
				 {
					 CreateNavigationResponseSendLocation();
				 }
				 else if (clickedItem.Equals(NavigationResponseShowConstantTBT))
				 {
					 CreateNavigationResponseShowConstantTBT();
				 }
				 else if (clickedItem.Equals(NavigationResponseStartAudioStream))
				 {
					 CreateNavigationResponseStartAudioStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStartStream))
				 {
					 CreateNavigationResponseStartStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStopAudioStream))
				 {
					 CreateNavigationResponseStopAudioStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStopStream))
				 {
					 CreateNavigationResponseStopStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseSubscribeWayPoints))
				 {
					 CreateNavigationResponseSubscribeWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseUnsubscribeWayPoints))
				 {
					 CreateNavigationResponseUnsubscribeWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseUpdateTurnList))
				 {
					 CreateNavigationResponseUpdateTurnList();
				 }
                 else if (clickedItem.Equals(NavigationResponseSetVideoConfig))
                 {
                     CreateNavigationResponseSetVideoConfig();
                 }
				 else if (clickedItem.Equals(NavigationNotificationOnTBTClientState))
				 {
					 CreateNavigationNotificationOnTBTClientState();
				 }
				 else if (clickedItem.Equals(RCResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateRCResponseGetCapabilities();
				 }
                else if (clickedItem.Equals(RCResponseGetInteriorVehicleData))
				 {
					 CreateRCResponseGetInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCResponseGetInteriorVehicleDataConsent))
				 {
					 CreateRCResponseGetInteriorVehicleDataConsent();
				 }
				 else if (clickedItem.Equals(RCResponseReleaseInteriorVehicleDataConsent))
				 {
					 CreateRCResponseReleaseInteriorVehicleDataConsent();
				 }
				 else if (clickedItem.Equals(RCResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateRCResponseIsReady();
				 }
                else if (clickedItem.Equals(RCResponseSetInteriorVehicleData))
				 {
					 CreateRCResponseSetInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCNotificationOnInteriorVehicleData))
				 {
					 CreateRCNotificationOnInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCNotificationOnRemoteControlSettings))
				 {
					 CreateRCNotificationOnRemoteControlSettings();
				 }
				 else if (clickedItem.Equals(SDLRequestActivateApp))
				 {
					 CreateSDLRequestActivateApp();
				 }
				 else if (clickedItem.Equals(SDLRequestGetListOfPermissions))
				 {
					 CreateSDLRequestGetListOfPermissions();
				 }
				 else if (clickedItem.Equals(SDLRequestGetStatusUpdate))
				 {
					 CreateSDLRequestGetStatusUpdate();
				 }
				 else if (clickedItem.Equals(SDLRequestGetURLS))
				 {
					 CreateSDLRequestGetURLS();
				 }
				 else if (clickedItem.Equals(SDLRequestGetUserFriendlyMessage))
				 {
					 CreateSDLRequestGetUserFriendlyMessage();
				 }
				 else if (clickedItem.Equals(SDLRequestUpdateSDL))
				 {
					 CreateSDLRequestUpdateSDL();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnAllowSDLFunctionality))
				 {
					 CreateSDLNotificationOnAllowSDLFunctionality();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnAppPermissionConsent))
				 {
					 CreateSDLNotificationOnAppPermissionConsent();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnPolicyUpdate))
				 {
					 CreateSDLNotificationOnPolicyUpdate();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnReceivedPolicyUpdate))
				 {
					 CreateSDLNotificationOnReceivedPolicyUpdate();
				 }
				 else if (clickedItem.Equals(TTSResponseChangeRegistration))
				 {
					 CreateTTSResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(TTSResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(TTSResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(TTSResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(TTSResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateTTSResponseIsReady();
				 }
				 else if (clickedItem.Equals(TTSResponseSetGlobalProperties))
				 {
					 CreateTTSResponseSetGlobalProperties();
				 }
				 else if (clickedItem.Equals(TTSResponseSpeak))
				 {
					 CreateTTSResponseSpeak();
				 }
				 else if (clickedItem.Equals(TTSResponseStopSpeaking))
				 {
					 CreateTTSResponseStopSpeaking();
				 }
				 else if (clickedItem.Equals(TTSNotificationOnLanguageChange))
				 {
					 CreateTTSNotificationOnLanguageChange();
				 }
				 else if (clickedItem.Equals(TTSNotificationOnResetTimeout))
				 {
					 CreateTTSNotificationOnResetTimeout();
				 }
				 else if (clickedItem.Equals(TTSNotificationStarted))
				 {
					 CreateTTSNotificationStarted();
				 }
				 else if (clickedItem.Equals(TTSNotificationStopped))
				 {
					 CreateTTSNotificationStopped();
				 }
				 else if (clickedItem.Equals(UIResponseAddCommand))
				 {
					 CreateUIResponseAddCommand();
				 }
				 else if (clickedItem.Equals(UIResponseCreateWindow))
				 {
					 CreateUIResponseCreateWindow();
				 }
				 else if (clickedItem.Equals(UIResponseDeleteWindow))
				 {
					 CreateUIResponseDeleteWindow();
				 }
				 else if (clickedItem.Equals(UIResponseAddSubMenu))
				 {
					 CreateUIResponseAddSubMenu();
				 }
                 else if (clickedItem.Equals(UIResponseShowAppMenu))
                 {
                     CreateUIResponseShowAppMenu();
                 }
                 else if (clickedItem.Equals(UIResponseCloseApplication))
                 {
                     CreateUIResponseCloseApplication();
                 }
                 else if (clickedItem.Equals(UIResponseAlert))
				 {
					 CreateUIResponseAlert();
				 }
				 else if (clickedItem.Equals(UIResponseChangeRegistration))
				 {
					 CreateUIResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(UIResponseClosePopUp))
				 {
					 CreateUIResponseClosePopUp();
				 }
				 else if (clickedItem.Equals(UIResponseDeleteCommand))
				 {
					 CreateUIResponseDeleteCommand();
				 }
				 else if (clickedItem.Equals(UIResponseDeleteSubMenu))
				 {
					 CreateUIResponseDeleteSubMenu();
				 }
				 else if (clickedItem.Equals(UIResponseEndAudioPassThru))
				 {
					 CreateUIResponseEndAudioPassThru();
				 }
				 else if (clickedItem.Equals(UIResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(UIResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(UIResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(UIResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateUIResponseIsReady();
				 }
				 else if (clickedItem.Equals(UIResponsePerformAudioPassThru))
				 {
					 CreateUIResponsePerformAudioPassThru();
				 }
				 else if (clickedItem.Equals(UIResponsePerformInteraction))
				 {
					 CreateUIResponsePerformInteraction();
				 }
				 else if (clickedItem.Equals(UIResponseScrollableMessage))
				 {
					 CreateUIResponseScrollableMessage();
				 }
				 else if (clickedItem.Equals(UIResponseSetAppIcon))
				 {
					 CreateUIResponseSetAppIcon();
				 }
				 else if (clickedItem.Equals(UIResponseSetDisplayLayout))
				 {
					 CreateUIResponseSetDisplayLayout();
				 }
				 else if (clickedItem.Equals(UIResponseSetGlobalProperties))
				 {
					 CreateUIResponseSetGlobalProperties();
				 }
				 else if (clickedItem.Equals(UIResponseSetMediaClockTimer))
				 {
					 CreateUIResponseSetMediaClockTimer();
				 }
				 else if (clickedItem.Equals(UIResponseShow))
				 {
					 CreateUIResponseShow();
				 }
				 else if (clickedItem.Equals(UIResponseShowCustomForm))
				 {
					 CreateUIResponseShowCustomForm();
				 }
				 else if (clickedItem.Equals(UIResponseSlider))
				 {
					 CreateUIResponseSlider();
				 }
				 else if (clickedItem.Equals(UINotificationOnCommand))
				 {
					 CreateUINotificationOnCommand();
				 }
				 else if (clickedItem.Equals(UINotificationOnDriverDistraction))
				 {
					 CreateUINotificationOnDriverDistraction();
				 }
				 else if (clickedItem.Equals(UINotificationOnKeyboardInput))
				 {
					 CreateUINotificationOnKeyboardInput();
				 }
				 else if (clickedItem.Equals(UINotificationOnLanguageChange))
				 {
					 CreateUINotificationOnLanguageChange();
				 }
				 /*else if (clickedItem.Equals(UINotificationOnRecordStart))
				 {
					// CreateUINotificationOnRecordStart();
				 }*/
				 else if (clickedItem.Equals(UINotificationOnResetTimeout))
				 {
					 CreateUINotificationOnResetTimeout();
				 }
				 else if (clickedItem.Equals(UINotificationOnSystemContext))
				 {
					 CreateUINotificationOnSystemContext();
				 }
				 else if (clickedItem.Equals(UINotificationOnTouchEvent))
				 {
					 CreateUINotificationOnTouchEvent();
				 }
				 else if (clickedItem.Equals(VIResponseDiagnosticMessage))
				 {
					 CreateVIResponseDiagnosticMessage();
				 }
				 else if (clickedItem.Equals(VIResponseGetDTCs))
				 {
					 CreateVIResponseGetDTCs();
				 }
				 else if (clickedItem.Equals(VIResponseGetVehicleData))
				 {
					 ((MainActivity)Activity).CreateVIResponseGetVehicleData();
				 }
				 else if (clickedItem.Equals(VIResponseGetVehicleType))
				 {
					 ((MainActivity)Activity).CreateVIResponseGetVehicleType();
				 }
				 else if (clickedItem.Equals(VIResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateVIResponseIsReady();
				 }
				 else if (clickedItem.Equals(VIResponseReadDID))
				 {
					 CreateVIResponseReadDID();
				 }
				 else if (clickedItem.Equals(VIResponseSubscribeVehicleData))
				 {
					 CreateVIResponseSubscribeVehicleData();
				 }
				 else if (clickedItem.Equals(VIResponseUnsubscribeVehicleData))
				 {
					 CreateVIResponseUnSubscribeVehicleData();
				 }
                 else if (clickedItem.Equals(VINotificationOnVehicleData))
                {
                     CreateVINotificationOnVehicleData();
                }
				 else if (clickedItem.Equals(VRResponseAddCommand))
				 {
					 CreateVRResponseAddCommand();
				 }
				 else if (clickedItem.Equals(VRResponseChangeRegistration))
				 {
					 CreateVRResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(VRResponseDeleteCommand))
				 {
					 CreateVRResponseDeleteCommand();
				 }
				 else if (clickedItem.Equals(VRResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(VRResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(VRResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(VRResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateVRResponseIsReady();
				 }
				 else if (clickedItem.Equals(VRResponsePerformInteraction))
				 {
					 CreateVRResponsePerformInteraction();
				 }
				 else if (clickedItem.Equals(VRNotificationOnCommand))
				 {
					 CreateVRNotificationOnCommand();
				 }
				 else if (clickedItem.Equals(VRNotificationOnLanguageChange))
				 {
					 CreateVRNotificationOnLanguageChange();
				 }
				 else if (clickedItem.Equals(VRNotificationStarted))
				 {
					 CreateVRNotificationStarted();
				 }
				 else if (clickedItem.Equals(VRNotificationStopped))
				 {
					 CreateVRNotificationStopped();
				 }
			 };

			rpcListAlertDialog.Show();
		}


        private void CreateRCNotificationOnRemoteControlSettings()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(RCNotificationOnRemoteControlSettings);

            CheckBox allowedCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
            Switch switchAllow = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
            CheckBox rcAccessModeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner rcAccessModeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            rcAccessModeCheckBox.CheckedChange += (s, e) => rcAccessModeSpinner.Enabled = e.IsChecked;
            allowedCheckBox.CheckedChange += (s, e) => switchAllow.Enabled = e.IsChecked;

            var adapter1 = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCAccessModeArray);
            rcAccessModeSpinner.Adapter = adapter1;

            HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings tmpObj = new HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings();
            tmpObj = (HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings>(Activity, tmpObj.getMethod());

            if (tmpObj != null)
            {
                if (tmpObj.getAllowed() == null)
                {
                    allowedCheckBox.Checked = false;
                    switchAllow.Enabled = false;
                }
                else
                {
                    allowedCheckBox.Checked = true;
                    switchAllow.Checked = (bool)tmpObj.getAllowed();
                }
                if (tmpObj.getAccessMode() != null)
                    rcAccessModeSpinner.SetSelection((int)tmpObj.getAccessMode());
                else
                    rcAccessModeCheckBox.Checked = false;

            }
            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            allowedCheckBox.Text = "Allowed";

            rcAccessModeCheckBox.Text = "RCAccessMode";

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                RCAccessMode? rcAccessMode = null;
                if (rcAccessModeCheckBox.Checked)
                    rcAccessMode = (RCAccessMode)rcAccessModeSpinner.SelectedItemPosition;

                bool? toggleState = null;
                if (allowedCheckBox.Checked)
                {
                    toggleState = switchAllow.Checked;
                }
                RequestNotifyMessage rpcResponse = BuildRpc.buildRcOnRemoteControlSettings(toggleState, rcAccessMode);
                AppUtils.savePreferenceValueForRpc(adapter1.Context, rpcResponse.getMethod(), rpcResponse);
                AppInstanceManager.Instance.sendRpc(rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateRCNotificationOnInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCNotificationOnInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_spn);

			CheckBox moduleIdCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_id_cb);
			EditText moduleIdEt = (EditText)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_id_edittext);

			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_btn);

            CheckBox addSeatControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_seat_control_data_chk);
            Button addSeatControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_seat_control_data_btn);

            CheckBox audioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_audio_control_data_chk);
            Button audioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_audio_control_data_btn);

            CheckBox lightControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_light_control_data_chk);
            Button lightControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_light_control_data_btn);

            CheckBox hmiSettingControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_hmi_setting_control_data_chk);
            Button hmiSettingControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_hmi_setting_control_data_btn);

            moduleDataCb.CheckedChange += (s, e) =>
            {
                moduleTypeCb.Enabled = e.IsChecked;
                moduleTypeSpn.Enabled = e.IsChecked && moduleTypeCb.Checked;
                addRadioControlDataCb.Enabled = e.IsChecked;
                addRadioControlDataButton.Enabled = e.IsChecked && addRadioControlDataCb.Checked;
                addClimateControlDataCb.Enabled = e.IsChecked;
                addClimateControlDataButton.Enabled = e.IsChecked && addClimateControlDataCb.Checked;
                addSeatControlDataCb.Enabled = e.IsChecked;
                addSeatControlDataButton.Enabled = e.IsChecked && addSeatControlDataCb.Checked;
                audioControlDataCb.Enabled = e.IsChecked;
                audioControlDataButton.Enabled = e.IsChecked && audioControlDataCb.Checked;
                lightControlDataCb.Enabled = e.IsChecked;
                lightControlDataButton.Enabled = e.IsChecked && lightControlDataCb.Checked;
                hmiSettingControlDataCb.Enabled = e.IsChecked;
                hmiSettingControlDataButton.Enabled = e.IsChecked && hmiSettingControlDataCb.Checked;
				moduleIdCb.Enabled = e.IsChecked;
				moduleIdEt.Enabled = e.IsChecked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;
            addSeatControlDataCb.CheckedChange += (s, e) => addSeatControlDataButton.Enabled = e.IsChecked;
            audioControlDataCb.CheckedChange += (s, e) => audioControlDataButton.Enabled = e.IsChecked;
            lightControlDataCb.CheckedChange += (s, e) => lightControlDataButton.Enabled = e.IsChecked;
            hmiSettingControlDataCb.CheckedChange += (s, e) => hmiSettingControlDataButton.Enabled = e.IsChecked;
			moduleIdCb.CheckedChange += (s, e) => moduleIdEt.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);

			radioControlData = null;
			climateControlData = null;
            seatControlData = null;
            audioControlData = null;
            lightControlData = null;
            hmiSettingControlData = null;

			HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					if (tmpObj.getModuleData().getModuleType() != null)
						moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
					else
						moduleTypeCb.Checked = false;

					if (tmpObj.getModuleData().getModuleId() != null)
						moduleIdEt.Text = tmpObj.getModuleData().getModuleId().ToString();
					radioControlData = tmpObj.getModuleData().getRadioControlData();
                    climateControlData = tmpObj.getModuleData().getClimateControlData();
                    seatControlData = tmpObj.getModuleData().getSeatControlData();
                    audioControlData = tmpObj.getModuleData().getAudioControlData();
                    lightControlData = tmpObj.getModuleData().getLightControlData();
                    hmiSettingControlData = tmpObj.getModuleData().getHmiSettingsControlData();
				}
			}

			if (radioControlData == null)
			{
				addRadioControlDataCb.Checked = false;
				addRadioControlDataButton.Enabled = false;
			}
			if (climateControlData == null)
			{
				addClimateControlDataCb.Checked = false;
				addClimateControlDataButton.Enabled = false;
			}
            if (seatControlData == null)
            {
                addSeatControlDataCb.Checked = false;
                addSeatControlDataButton.Enabled = false;
            }

            if (audioControlData == null)
            {
                audioControlDataCb.Checked = false;
                audioControlDataButton.Enabled = false;
            }

            if (lightControlData == null)
            {
                lightControlDataCb.Checked = false;
                lightControlDataButton.Enabled = false;
            }

            if (hmiSettingControlData == null)
            {
                hmiSettingControlDataCb.Checked = false;
                hmiSettingControlDataButton.Enabled = false;
            }

            addRadioControlDataButton.Click += (object sender, EventArgs e) => RadioControlDataAlertDialogue();
            addClimateControlDataButton.Click += (object sender, EventArgs e) => ClimateControlDataAlertDialogue();
            addSeatControlDataButton.Click += (object sender, EventArgs e) => SeatControlDataAlertDialogue();
            audioControlDataButton.Click += (object sender, EventArgs e) => AudioControlDataAlertDialogue();
            lightControlDataButton.Click += (object sender, EventArgs e) => LightControlDataAlertDialogue();
            hmiSettingControlDataButton.Click += (object sender, EventArgs e) => HmiSettingControlDataAlertDialogue();

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, evn) =>
			{
                ModuleData moduleData = null;
				if (moduleDataCb.Checked)
				{
                    moduleData = new ModuleData();
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;

					if (moduleIdCb.Checked)
						moduleData.moduleId = moduleIdEt.Text.ToString();

					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;

                    if (addSeatControlDataCb.Checked)
                        moduleData.seatControlData = seatControlData;

                    if (audioControlDataCb.Checked)
                        moduleData.audioControlData = audioControlData;

                    if (lightControlDataCb.Checked)
                        moduleData.lightControlData = lightControlData;

                    if (hmiSettingControlDataCb.Checked)
                        moduleData.hmiSettingsControlData = hmiSettingControlData;
				}

				RequestNotifyMessage rpcResponse = BuildRpc.buildRcOnInteriorVehicleData(moduleData);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
				AppInstanceManager.Instance.sendRpc(rpcResponse);

			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

        public void SisDataAlertDialogue()
		{
			AlertDialog.Builder sisDataAlertDialogue = new AlertDialog.Builder(Activity);
            View sisDataView = layoutInflater.Inflate(Resource.Layout.SisData, null);
			sisDataAlertDialogue.SetView(sisDataView);
			sisDataAlertDialogue.SetTitle("SisData");

            if (sisData == null)
			{
                sisData = BuildDefaults.buildSisData();
			}

            CheckBox stationShortNameCb = (CheckBox)sisDataView.FindViewById(Resource.Id.sis_data_station_short_name_cb);
			EditText stationShortNameEt = (EditText)sisDataView.FindViewById(Resource.Id.sis_data_station_short_name_et);

			stationShortNameCb.CheckedChange += (s, e) => stationShortNameEt.Enabled = e.IsChecked;

			CheckBox stationLongNameCb = (CheckBox)sisDataView.FindViewById(Resource.Id.sis_data_station_long_name_cb);
			EditText stationLongNameEt = (EditText)sisDataView.FindViewById(Resource.Id.sis_data_station_long_name_et);

			stationLongNameCb.CheckedChange += (s, e) => stationLongNameEt.Enabled = e.IsChecked;

            CheckBox stationMessageCb = (CheckBox)sisDataView.FindViewById(Resource.Id.sis_data_station_message_cb);
            EditText stationMessageEt = (EditText)sisDataView.FindViewById(Resource.Id.sis_data_station_message_et);

			stationMessageCb.CheckedChange += (s, e) => stationMessageEt.Enabled = e.IsChecked;

            stationIDNumber = sisData.getStationIDNumber();
			CheckBox stationIDNumberCb = (CheckBox)sisDataView.FindViewById(Resource.Id.sis_data_station_id_number_cb);
			Button stationIDNumberButton = (Button)sisDataView.FindViewById(Resource.Id.sis_data_station_id_number_btn);

			stationIDNumberCb.CheckedChange += (s, e) => stationIDNumberButton.Enabled = e.IsChecked;
            stationIDNumberButton.Click += (object sender, EventArgs e) => StationIDNumberAlertDialogue(stationIDNumber);

            gpsData = sisData.getStationLocation();
            CheckBox stationLocationCb = (CheckBox)sisDataView.FindViewById(Resource.Id.sis_data_station_location_cb);
			Button stationLocationButton = (Button)sisDataView.FindViewById(Resource.Id.sis_data_station_location_btn);

			stationLocationCb.CheckedChange += (s, e) => stationLocationButton.Enabled = e.IsChecked;
            stationLocationButton.Click += (object sender, EventArgs e) => GPSDataAlertDialogue(gpsData);

            stationShortNameEt.Text = sisData.getStationShortName();
            stationLongNameEt.Text = sisData.getStationLongName();
            stationMessageEt.Text = sisData.getStationMessage();

			sisDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
			{
				sisDataAlertDialogue.Dispose();
			});

			sisDataAlertDialogue.SetPositiveButton("OK", (senderAlert, evn) =>
			{
                sisData = new SisData();

                if (stationShortNameCb.Checked)
                    sisData.stationShortName = stationShortNameEt.Text;

				if (stationLongNameCb.Checked)
                    sisData.stationLongName = stationLongNameEt.Text;

				if (stationMessageCb.Checked)
                    sisData.stationMessage = stationMessageEt.Text;

                if (stationIDNumberCb.Checked)
                    sisData.stationIDNumber = stationIDNumber;

				if (stationLocationCb.Checked)
                    sisData.stationLocation = gpsData;
			});

			sisDataAlertDialogue.Show();
		}

		public void StationIDNumberAlertDialogue(StationIDNumber initStationIdNumber)
		{
			AlertDialog.Builder stationIDNumberAlertDialogue = new AlertDialog.Builder(Activity);
            View stationIDNumberView = layoutInflater.Inflate(Resource.Layout.station_id_number, null);
			stationIDNumberAlertDialogue.SetView(stationIDNumberView);
			stationIDNumberAlertDialogue.SetTitle("StationIDNumber");

			if (initStationIdNumber == null)
			{
                initStationIdNumber = BuildDefaults.buildStationIDNumber();
			}

            CheckBox countryCodeCb = (CheckBox)stationIDNumberView.FindViewById(Resource.Id.station_id_number_country_code_cb);
            EditText countryCodeEt = (EditText)stationIDNumberView.FindViewById(Resource.Id.station_id_number_country_code_et);

			countryCodeCb.CheckedChange += (s, e) => countryCodeEt.Enabled = e.IsChecked;

            CheckBox fccFacilityIdCb = (CheckBox)stationIDNumberView.FindViewById(Resource.Id.station_id_number_fcc_facility_Id_cb);
			EditText fccFacilityIdEt = (EditText)stationIDNumberView.FindViewById(Resource.Id.station_id_number_fcc_facility_Id_et);

			fccFacilityIdCb.CheckedChange += (s, e) => fccFacilityIdEt.Enabled = e.IsChecked;

            if(initStationIdNumber.getCountryCode() != null){
                countryCodeCb.Checked = true;
				countryCodeEt.Text = initStationIdNumber.getCountryCode().ToString();
            }

            if(initStationIdNumber.getFccFacilityId() != null){
                fccFacilityIdCb.Checked = true;
				fccFacilityIdEt.Text = initStationIdNumber.getFccFacilityId().ToString();                
            }

			stationIDNumberAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
			{
				stationIDNumberAlertDialogue.Dispose();
			});

			stationIDNumberAlertDialogue.SetPositiveButton("OK", (senderAlert, evn) =>
			{
                stationIDNumber = initStationIdNumber;

                if (countryCodeCb.Checked && countryCodeEt.Text != null && countryCodeEt.Text.Length > 0) {
					stationIDNumber.countryCode = Java.Lang.Integer.ParseInt(countryCodeEt.Text);   
                } else {
                    stationIDNumber.countryCode = null;
                }

                if (fccFacilityIdCb.Checked && fccFacilityIdEt.Text != null && fccFacilityIdEt.Text.Length > 0){
					stationIDNumber.fccFacilityId = Java.Lang.Integer.ParseInt(fccFacilityIdEt.Text);                    
                } else {
                    stationIDNumber.fccFacilityId = null;
                }
			});

			stationIDNumberAlertDialogue.Show();
		}

        public void GPSDataAlertDialogue(GPSData initGpsData)
		{
			AlertDialog.Builder gpsDataAlertDialogue = new AlertDialog.Builder(Activity);
            View gpsDataView = layoutInflater.Inflate(Resource.Layout.gps_location, null);
            gpsDataAlertDialogue.SetView(gpsDataView);
            gpsDataAlertDialogue.SetTitle("GPSData");

            if (initGpsData == null)
			{
                initGpsData = BuildDefaults.buildDefaultGPSData();
			}

            CheckBox longitudeDegreesCb = (CheckBox)gpsDataView.FindViewById(Resource.Id.gps_location_longitude_degrees_checkbox);
            EditText longitudeDegreesEt = (EditText)gpsDataView.FindViewById(Resource.Id.gps_location_longitude_degrees_edittext);

			longitudeDegreesCb.CheckedChange += (s, e) => longitudeDegreesEt.Enabled = e.IsChecked;

            CheckBox latitudeDegreesCb = (CheckBox)gpsDataView.FindViewById(Resource.Id.gps_location_latitude_degrees_checkbox);
            EditText latitudeDegreesEt = (EditText)gpsDataView.FindViewById(Resource.Id.gps_location_latitude_degrees_edittext);

			latitudeDegreesCb.CheckedChange += (s, e) => latitudeDegreesEt.Enabled = e.IsChecked;

            CheckBox altitudeMetersCb = (CheckBox)gpsDataView.FindViewById(Resource.Id.gps_location_altitude_meters_checkbox);
            EditText altitudeMetersEt = (EditText)gpsDataView.FindViewById(Resource.Id.gps_location_altitude_meters_edittext);

			altitudeMetersCb.CheckedChange += (s, e) => altitudeMetersEt.Enabled = e.IsChecked;

            if(initGpsData.getLongitudeDegrees() != null){
                longitudeDegreesCb.Checked = true;
                longitudeDegreesEt.Text = initGpsData.getLongitudeDegrees().ToString();
            }

            if(initGpsData.getLatitudeDegrees() != null){
                latitudeDegreesCb.Checked = true;
                latitudeDegreesEt.Text = initGpsData.getLatitudeDegrees().ToString();
            }

            if(initGpsData.getAltitude() != null){
                altitudeMetersCb.Checked = true;
                altitudeMetersEt.Text = initGpsData.getAltitude().ToString();
            }

            gpsDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
			{
                gpsDataAlertDialogue.Dispose();
			});

            gpsDataAlertDialogue.SetPositiveButton("OK", (senderAlert, evn) =>
			{
                gpsData = initGpsData;

                if (longitudeDegreesCb.Checked && longitudeDegreesEt.Text != null && longitudeDegreesEt.Text.Length > 0){
                    gpsData.longitudeDegrees = Java.Lang.Float.ParseFloat(longitudeDegreesEt.Text);                    
                } else {
                    gpsData.longitudeDegrees = null;
                }

                if (latitudeDegreesCb.Checked && latitudeDegreesEt.Text != null && latitudeDegreesEt.Text.Length > 0){
                    gpsData.latitudeDegrees = Java.Lang.Float.ParseFloat(latitudeDegreesEt.Text);
                } else {
                    gpsData.latitudeDegrees = null;
                }
					
                if (altitudeMetersCb.Checked && altitudeMetersEt.Text != null && altitudeMetersEt.Text.Length > 0){
                    gpsData.altitude = Java.Lang.Float.ParseFloat(altitudeMetersEt.Text);
                } else {
                    gpsData.altitude = null;
				}
					
			});

            gpsDataAlertDialogue.Show();
		}

		public void RadioControlDataAlertDialogue()
		{
			AlertDialog.Builder radioControlDataAlertDialogue = new AlertDialog.Builder(Activity);
			View radioControlDataView = layoutInflater.Inflate(Resource.Layout.radio_control_data, null);
			radioControlDataAlertDialogue.SetView(radioControlDataView);
			radioControlDataAlertDialogue.SetTitle("RadioControlData");

			CheckBox frequencyIntegerCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_cb);
			EditText frequencyIntegerEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_et);

			CheckBox frequencyFractionCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_cb);
			EditText frequencyFractionEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_et);

			CheckBox radioBandCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_cb);
			Spinner radioBandSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_spn);


			CheckBox rdsDataCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rds_data_cb);

			CheckBox rdsDataPsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ps_cb);
			EditText rdsDataPsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ps_et);

			CheckBox rdsDataRtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rt_cb);
			EditText rdsDataRtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_rt_et);

			CheckBox rdsDataCtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ct_cb);
			EditText rdsDataCtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ct_et);

			CheckBox rdsDataPiCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pi_cb);
			EditText rdsDataPiEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pi_et);

			CheckBox rdsDataPtyCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pty_cb);
			EditText rdsDataPtyEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pty_et);

			CheckBox rdsDataTpCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_tp_cb);

			CheckBox rdsDataTaCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ta_cb);

			CheckBox rdsDataRegCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_reg_cb);
			EditText rdsDataRegEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_reg_et);

			CheckBox availableHDsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_cb);
			EditText availableHDsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_et);

			CheckBox hdChannelCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_cb);
			EditText hdChannelEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_et);

			CheckBox signalStrengthCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_cb);
			EditText signalStrengthEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_et);

			CheckBox signalChangeThresholdCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_cb);
			EditText signalChangeThresholdEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_et);

			CheckBox radioEnableCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_enable_cb);
			Switch radioEnable_tgl = (Switch)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_enable_tgl);

            CheckBox hdRadioEnableCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.hdRadioEnable_cb);
            Switch hdRadioEnable_tgl = (Switch)radioControlDataView.FindViewById(Resource.Id.hdRadioEnable_tgl);

			CheckBox radioStateCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_cb);
			Spinner radioStateSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_spn);

            CheckBox availableHdChannelsCheckbox = (CheckBox)radioControlDataView.FindViewById(Resource.Id.availableHdChannels_cb);
            EditText availableHdChannelsEditText = (EditText)radioControlDataView.FindViewById(Resource.Id.availableHdChannels_edittext);

            availableHdChannelsCheckbox.CheckedChange += (send, e1) => availableHdChannelsEditText.Enabled = e1.IsChecked;

            CheckBox sisDataCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_sis_data_cb);
			Button sisDataButton = (Button)radioControlDataView.FindViewById(Resource.Id.radio_control_data_sis_data_btn);
			radioEnableCb.CheckedChange += (s, e) =>
			{
				radioEnable_tgl.Enabled = e.IsChecked;
			};
            hdRadioEnableCb.CheckedChange += (s, e) =>
            {
                hdRadioEnable_tgl.Enabled = e.IsChecked;
            };
			radioBandSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioBandArray);
			radioStateSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioStateArray);

            frequencyIntegerCb.CheckedChange += (s, e) => frequencyIntegerEt.Enabled = e.IsChecked;
            frequencyFractionCb.CheckedChange += (s, e) => frequencyFractionEt.Enabled = e.IsChecked;
            radioBandCb.CheckedChange += (s, e) => radioBandSpn.Enabled = e.IsChecked;

            rdsDataCb.CheckedChange += (s, e) =>
            {
                rdsDataPsCb.Enabled = e.IsChecked;
                rdsDataRtCb.Enabled = e.IsChecked;
                rdsDataCtCb.Enabled = e.IsChecked;
                rdsDataPiCb.Enabled = e.IsChecked;
                rdsDataPtyCb.Enabled = e.IsChecked;
                rdsDataTpCb.Enabled = e.IsChecked;
                rdsDataTaCb.Enabled = e.IsChecked;
                rdsDataRegCb.Enabled = e.IsChecked;

                rdsDataPsEt.Enabled = e.IsChecked;
                rdsDataRtEt.Enabled = e.IsChecked;
                rdsDataCtEt.Enabled = e.IsChecked;
                rdsDataPiEt.Enabled = e.IsChecked;
                rdsDataPtyEt.Enabled = e.IsChecked;
                rdsDataRegEt.Enabled = e.IsChecked;
            };
            rdsDataPsCb.CheckedChange += (s, e) => rdsDataPsEt.Enabled = e.IsChecked;
            rdsDataRtCb.CheckedChange += (s, e) => rdsDataRtEt.Enabled = e.IsChecked;
            rdsDataCtCb.CheckedChange += (s, e) => rdsDataCtEt.Enabled = e.IsChecked;
            rdsDataPiCb.CheckedChange += (s, e) => rdsDataPiEt.Enabled = e.IsChecked;
            rdsDataPtyCb.CheckedChange += (s, e) => rdsDataPtyEt.Enabled = e.IsChecked;
            rdsDataRegCb.CheckedChange += (s, e) => rdsDataRegEt.Enabled = e.IsChecked;

            availableHDsCb.CheckedChange += (s, e) => availableHDsEt.Enabled = e.IsChecked;
            hdChannelCb.CheckedChange += (s, e) => hdChannelEt.Enabled = e.IsChecked;
            signalStrengthCb.CheckedChange += (s, e) => signalStrengthEt.Enabled = e.IsChecked;
            signalChangeThresholdCb.CheckedChange += (s, e) => signalChangeThresholdEt.Enabled = e.IsChecked;
            radioStateCb.CheckedChange += (s, e) => radioStateSpn.Enabled = e.IsChecked;
            sisDataCb.CheckedChange += (s, e) => sisDataButton.Enabled = e.IsChecked;
            sisDataButton.Click += (object sender, EventArgs e) => SisDataAlertDialogue();

            if (null == radioControlData)
            {
                radioControlData = BuildDefaults.buildRadioControlData();
            }
            if (null != radioControlData)
            {
                if (null != radioControlData.getFrequencyInteger())
                {
                    frequencyIntegerEt.Text = radioControlData.getFrequencyInteger().ToString();
                }
                else
                {
                    frequencyIntegerCb.Checked = false;
                }

                if (null != radioControlData.getFrequencyFraction())
                {
                    frequencyFractionEt.Text = radioControlData.getFrequencyFraction().ToString();
                }
                else
                {
                    frequencyFractionCb.Checked = false;
                }

                if (null != radioControlData.getBand())
                {
                    radioBandSpn.SetSelection((int)radioControlData.getBand());
                }
                else
                {
                    radioBandCb.Checked = false;
                }

                if (radioControlData.getRdsData() != null)
                {
                    if (null != radioControlData.getRdsData().PS)
                    {
                        rdsDataPsEt.Text = radioControlData.getRdsData().PS;
                    }
                    else
                    {
                        rdsDataPsCb.Checked = false;
                    }

                    if (null != radioControlData.getRdsData().RT)
                    {
                        rdsDataRtEt.Text = radioControlData.getRdsData().RT;
                    }
                    else
                    {
                        rdsDataRtCb.Checked = false;
                    }

                    if (null != radioControlData.getRdsData().CT)
                    {
                        rdsDataCtEt.Text = radioControlData.getRdsData().CT;
                    }
                    else
                    {
                        rdsDataCtCb.Checked = false;
                    }

                    if (null != radioControlData.getRdsData().PI)
                        rdsDataPiEt.Text = radioControlData.getRdsData().PI;
                    else
                        rdsDataPiCb.Checked = false;

                    if (null != radioControlData.getRdsData().PTY)
                        rdsDataPtyEt.Text = radioControlData.getRdsData().PTY.ToString();
                    else
                        rdsDataPtyCb.Checked = false;

                    if (null != radioControlData.getRdsData().TP)
                        rdsDataTpCb.Checked = (bool)radioControlData.getRdsData().TP;
                    else
                        rdsDataTpCb.Checked = false;

                    if (null != radioControlData.getRdsData().TA)
                        rdsDataTaCb.Checked = (bool)radioControlData.getRdsData().TA;
                    else
                        rdsDataTaCb.Checked = false;

                    if (null != radioControlData.getRdsData().REG)
                        rdsDataRegEt.Text = radioControlData.getRdsData().REG;
                    else
                        rdsDataRegCb.Checked = false;
                }
                if (null != radioControlData.getAvailableHDs())
                {
                    availableHDsEt.Text = radioControlData.getAvailableHDs().ToString();
                }
                else
                {
                    availableHDsCb.Checked = false;
                }
                    
                if (null != radioControlData.getHdChannel())
                {
                    hdChannelEt.Text = radioControlData.getHdChannel().ToString();
                }
                else
                {
                    hdChannelCb.Checked = false;
                }

                if(null != radioControlData.getAvailableHdChannels())
                {
                    foreach (int val in radioControlData.getAvailableHdChannels())
                    {
                        availableHdChannelsEditText.Text += val + ",";
                    }
                } else {
                    availableHdChannelsCheckbox.Checked = false;
                }

                if (null != radioControlData.getSignalStrength())
                    signalStrengthEt.Text = radioControlData.getSignalStrength().ToString();
                else
                    signalStrengthCb.Checked = false;

                if (null != radioControlData.getSignalChangeThreshold())
                    signalChangeThresholdEt.Text = radioControlData.getSignalChangeThreshold().ToString();
                else
                    signalChangeThresholdCb.Checked = false;

                if (null != radioControlData.getRadioEnable())
                {
                    radioEnableCb.Checked = true;
                    radioEnable_tgl.Checked = (bool)radioControlData.getRadioEnable();
                }
                else
                {
                    radioEnableCb.Checked = false;
                }

                if (null != radioControlData.getHdRadioEnable())
                {
                    hdRadioEnableCb.Checked = true;
                    hdRadioEnable_tgl.Checked = (bool)radioControlData.getHdRadioEnable();
                }
                else
                {
                    hdRadioEnableCb.Checked = false;
                }

                if (null != radioControlData.getState())
                    radioStateSpn.SetSelection((int)radioControlData.getState());
                else
                    radioStateCb.Checked = false;
                
                sisData = radioControlData.getSisData();
            }

            radioControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
            {
                radioControlDataAlertDialogue.Dispose();
            });

			radioControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
			{
				radioControlData = new RadioControlData();
				if (frequencyIntegerCb.Checked && frequencyIntegerEt.Text != null && frequencyIntegerEt.Text.Length > 0)
					radioControlData.frequencyInteger = Java.Lang.Integer.ParseInt(frequencyIntegerEt.Text);
				else
					radioControlData.frequencyInteger = 0;

				if (frequencyFractionCb.Checked && frequencyFractionEt.Text != null && frequencyFractionEt.Text.Length > 0)
					radioControlData.frequencyFraction = Java.Lang.Integer.ParseInt(frequencyFractionEt.Text);
				else
					radioControlData.frequencyFraction = 0;

				if (radioBandCb.Checked)
					radioControlData.band = (RadioBand)radioBandSpn.SelectedItemPosition;
				else
					radioControlData.band = RadioBand.AM;


				radioControlData.rdsData = null;
				if (rdsDataCb.Checked)
				{
					radioControlData.rdsData = new RdsData();

					if (rdsDataPsCb.Checked)
						radioControlData.rdsData.PS = rdsDataPsEt.Text;

					if (rdsDataRtCb.Checked)
						radioControlData.rdsData.RT = rdsDataRtEt.Text;

					if (rdsDataCtCb.Checked)
						radioControlData.rdsData.CT = rdsDataCtEt.Text;

					if (rdsDataPiCb.Checked)
						radioControlData.rdsData.PI = rdsDataPiEt.Text;

					if (rdsDataPtyCb.Checked && rdsDataPtyEt.Text != null && rdsDataPtyEt.Text.Length > 0)
						radioControlData.rdsData.PTY = Java.Lang.Integer.ParseInt(rdsDataPtyEt.Text);


					if (rdsDataRegCb.Checked)
						radioControlData.rdsData.REG = rdsDataRegEt.Text;

					radioControlData.rdsData.TP = rdsDataTpCb.Checked;
					radioControlData.rdsData.TA = rdsDataTaCb.Checked;
				}

				if (availableHDsCb.Checked && availableHDsEt.Text != null && availableHDsEt.Text.Length > 0)
					radioControlData.availableHDs = Java.Lang.Integer.ParseInt(availableHDsEt.Text);
				else
					radioControlData.availableHDs = 0;

				if (hdChannelCb.Checked && hdChannelEt.Text != null && hdChannelEt.Text.Length > 0)
					radioControlData.hdChannel = Java.Lang.Integer.ParseInt(hdChannelEt.Text);
				else
					radioControlData.hdChannel = 0;

				if (signalStrengthCb.Checked && signalStrengthEt.Text != null && signalStrengthEt.Text.Length > 0)
					radioControlData.signalStrength = Java.Lang.Integer.ParseInt(signalStrengthEt.Text);
				else
					radioControlData.signalStrength = 0;

				if (signalChangeThresholdCb.Checked && signalChangeThresholdEt.Text != null && signalChangeThresholdEt.Text.Length > 0)
					radioControlData.signalChangeThreshold = Java.Lang.Integer.ParseInt(signalChangeThresholdEt.Text);
				else
					radioControlData.signalChangeThreshold = 0;

                if (radioEnableCb.Checked)
                    radioControlData.radioEnable = radioEnable_tgl.Checked;
                else
                    radioControlData.radioEnable = null;

                if (hdRadioEnableCb.Checked)
                    radioControlData.hdRadioEnable = hdRadioEnable_tgl.Checked;
                else
                    radioControlData.hdRadioEnable = null;
                
				if (radioStateCb.Checked)
					radioControlData.state = (RadioState)radioStateSpn.SelectedItemPosition;
				else
					radioControlData.state = RadioState.ACQUIRING;

				if (sisDataCb.Checked)
				{
					radioControlData.sisData = sisData;
				}

                if (availableHdChannelsCheckbox.Checked)
                {
                    List<int> availableHdChannelsList = new List<int>();
                    string[] t = availableHdChannelsEditText.Text.Split(',');
                    foreach (string availableHdChannels in t)
                    {
                        try
                        {
                            availableHdChannelsList.Add(Int32.Parse(availableHdChannels));
                        }
                        catch (Exception e3)
                        {

                        }
                    }
                    radioControlData.availableHdChannels = availableHdChannelsList;
                }
            });

			radioControlDataAlertDialogue.Show();
		}

		public void ClimateControlDataAlertDialogue()
		{
			AlertDialog.Builder climateControlDataAlertDialogue = new AlertDialog.Builder(Activity);
			View climateControlDataView = layoutInflater.Inflate(Resource.Layout.climate_control_data, null);
			climateControlDataAlertDialogue.SetView(climateControlDataView);
			climateControlDataAlertDialogue.SetTitle("ClimateControlData");

			CheckBox fanSpeedCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_cb);
			EditText fanSpeedEt = (EditText)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_et);

			CheckBox currentTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_current_temp_cb);

			CheckBox currentTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_cb);
			Spinner currentTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_spn);

			CheckBox currentTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_value_cb);
			EditText currentTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.current_temp_value_et);


			CheckBox desiredTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_desired_temp_cb);

			CheckBox desiredTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_cb);
			Spinner desiredTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_spn);

			CheckBox desiredTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_cb);
			EditText desiredTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_et);


			CheckBox acEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_enable_cb);
			Switch acEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_enable_tgl);
			acEnableCb.CheckedChange += (sender, e) => {
				acEnable_tgl.Enabled = e.IsChecked;
			};

			CheckBox circulatedAirEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_circulated_air_enable_cb);
			Switch circulatedAirEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_circulated_air_enable_tgl);
			circulatedAirEnableCb.CheckedChange += (sender, e) => {
				circulatedAirEnable_tgl.Enabled = e.IsChecked;
			};

			CheckBox autoModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_auto_mode_enable_cb);
			Switch autoModeEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_auto_mode_enable_tgl);
			autoModeEnableCb.CheckedChange += (sender, e) => {
				autoModeEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox defrostZoneEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_cb);
			
			Spinner defrostZoneEnableSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_spn);

			CheckBox dualModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_dual_mode_enable_cb);
			Switch dualModeEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_dual_mode_enable_tgl);
			dualModeEnableCb.CheckedChange += (sender, e) => {
				dualModeEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox acMaxEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_max_enable_cb);
			Switch acMaxEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_max_enable_tgl);
			acMaxEnableCb.CheckedChange += (sender, e) => {
				acMaxEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox heatedSteeringWheelEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_steering_wheel_enable_cb);
			Switch heatedSteeringWheelEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_steering_wheel_enable_tgl);
			heatedSteeringWheelEnableCb.CheckedChange += (sender, e) => {
				heatedSteeringWheelEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox heatedWindshieldEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_windshield_enable_cb);
			Switch heatedWindshieldEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_windshield_enable_tgl);
			heatedWindshieldEnableCb.CheckedChange += (sender, e) => {
				heatedWindshieldEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox heatedRearWindowEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_rear_window_enable_cb);
			Switch heatedRearWindowEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_rear_window_enable_tgl);
			heatedRearWindowEnableCb.CheckedChange += (sender, e) => {
				heatedRearWindowEnable_tgl.Enabled = e.IsChecked;
			};
			CheckBox heatedMirrorsEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_mirrors_enable_cb);
			Switch heatedMirrorsEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_heated_mirrors_enable_tgl);
			heatedMirrorsEnableCb.CheckedChange += (sender, e) => {
				heatedMirrorsEnable_tgl.Enabled = e.IsChecked;
			};

            CheckBox climateEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_climate_enable_cb);
            Switch climateEnable_tgl = (Switch)climateControlDataView.FindViewById(Resource.Id.climate_control_data_climate_enable_tgl);
            climateEnableCb.CheckedChange += (sender, e) => {
                climateEnable_tgl.Enabled = e.IsChecked;
            };

            CheckBox dataVentilationModeCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_cb);
			
			Spinner dataVentilationModeSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_spn);

			currentTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
			desiredTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
			defrostZoneEnableSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCDefrostZoneArray);
			dataVentilationModeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCVentilationModeArray);

			fanSpeedCb.CheckedChange += (s, e) => fanSpeedEt.Enabled = e.IsChecked;

			currentTempCb.CheckedChange += (s, e) =>
			{
				currentTempUnitCb.Enabled = e.IsChecked;
				currentTempValueCb.Enabled = e.IsChecked;
			};

			currentTempUnitCb.CheckedChange += (s, e) => currentTempUnitSpn.Enabled = e.IsChecked;
			currentTempValueCb.CheckedChange += (s, e) => currentTempValueEt.Enabled = e.IsChecked;

			desiredTempCb.CheckedChange += (s, e) =>
			{
				desiredTempUnitCb.Enabled = e.IsChecked;
				desiredTempValueCb.Enabled = e.IsChecked;
			};

			desiredTempUnitCb.CheckedChange += (s, e) => desiredTempUnitSpn.Enabled = e.IsChecked;
			desiredTempValueCb.CheckedChange += (s, e) => desiredTempValueEt.Enabled = e.IsChecked;

			defrostZoneEnableCb.CheckedChange += (s, e) => defrostZoneEnableSpn.Enabled = e.IsChecked;
			dataVentilationModeCb.CheckedChange += (s, e) => dataVentilationModeSpn.Enabled = e.IsChecked;

            if (null == climateControlData)
                climateControlData = BuildDefaults.buildClimateControlData();

            if (null != climateControlData)
            {
                if (null != climateControlData.getFanSpeed())
                    fanSpeedEt.Text = climateControlData.getFanSpeed().ToString();
                else
                    fanSpeedCb.Checked = false;

                if (climateControlData.getCurrentTemperature() != null)
                {
                    if (null != climateControlData.getCurrentTemperature().getUnit())
                        currentTempUnitSpn.SetSelection((int)climateControlData.getCurrentTemperature().getUnit());
                    else
                        currentTempUnitCb.Checked = false;

                    if (null != climateControlData.getCurrentTemperature().getValue())
                        currentTempValueEt.Text = climateControlData.getCurrentTemperature().getValue().ToString();
                    else
                        currentTempValueCb.Checked = false;
                }
                else
                {
                    currentTempCb.Checked = false;
                }

                if (climateControlData.getDesiredTemperature() != null)
                {
                    if (null != climateControlData.getDesiredTemperature().getUnit())
                        desiredTempUnitSpn.SetSelection((int)climateControlData.getDesiredTemperature().getUnit());
                    else
                        desiredTempUnitCb.Checked = false;

                    if (null != climateControlData.getDesiredTemperature().getValue())
                        desiredTempValueEt.Text = climateControlData.getDesiredTemperature().getValue().ToString();
                    else
                        desiredTempValueCb.Checked = false;
                }
                else
                {
                    desiredTempCb.Checked = false;
                }

                if (null != climateControlData.getAcEnable())
                {
                    acEnable_tgl.Checked = (bool)climateControlData.getAcEnable();
                }
                else
                {
                    acEnableCb.Checked = false;
                }


                if (null != climateControlData.getCirculateAirEnable())
                {
                    circulatedAirEnable_tgl.Checked = (bool)climateControlData.getCirculateAirEnable();
                }
                else
                {
                    circulatedAirEnableCb.Checked = false;
                }


                if (null != climateControlData.getAutoModeEnable())
                {
                    autoModeEnable_tgl.Checked = (bool)climateControlData.getAutoModeEnable();
                }
                else
                {
                    autoModeEnableCb.Checked = false;
                }


                if (null != climateControlData.getDefrostZone())
                    defrostZoneEnableSpn.SetSelection((int)climateControlData.getDefrostZone());

                if (null != climateControlData.getDualModeEnable())
                {
                    dualModeEnable_tgl.Checked = (bool)climateControlData.getDualModeEnable();
                }
                else
                {
                    dualModeEnableCb.Checked = false;
                }


                if (null != climateControlData.getAcMaxEnable())
                {
                    acMaxEnable_tgl.Checked = (bool)climateControlData.getAcMaxEnable();
                }
                else
                {
                    acMaxEnableCb.Checked = false;
                }


                if (null != climateControlData.getVentilationMode())
                    dataVentilationModeSpn.SetSelection((int)climateControlData.getVentilationMode());
                else
                    dataVentilationModeCb.Checked = false;

                if (null != climateControlData.getHeatedSteeringWheelEnable())
                {
                    heatedSteeringWheelEnable_tgl.Checked = (bool)climateControlData.getHeatedSteeringWheelEnable();
                }
                else
                {
                    heatedSteeringWheelEnableCb.Checked = false;

                }


                if (null != climateControlData.getHeatedWindshieldEnable())
                {
                    heatedWindshieldEnable_tgl.Checked = (bool)climateControlData.getHeatedWindshieldEnable();
                }
                else
                {
                    heatedWindshieldEnableCb.Checked = false;
                }


                if (null != climateControlData.getHeatedRearWindowEnable())
                {
                    heatedRearWindowEnable_tgl.Checked = (bool)climateControlData.getHeatedRearWindowEnable();
                }
                else
                {
                    heatedRearWindowEnableCb.Checked = false;
                }


                if (null != climateControlData.getHeatedMirrorsEnable())
                {
                    heatedMirrorsEnable_tgl.Checked = (bool)climateControlData.getHeatedMirrorsEnable();
                }
                else
                {
                    heatedMirrorsEnableCb.Checked = false;
                }


                if (null != climateControlData.getClimateEnable())
                {
                    climateEnable_tgl.Checked = (bool)climateControlData.getClimateEnable();
                }
                else
                {
                    climateEnableCb.Checked = false;
                }

            }
			climateControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
			{
				climateControlDataAlertDialogue.Dispose();
			});

			climateControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
			{
				climateControlData = new ClimateControlData();
				
					if (fanSpeedCb.Checked && fanSpeedEt.Text != null && fanSpeedEt.Text.Length > 0)
						climateControlData.fanSpeed = Java.Lang.Integer.ParseInt(fanSpeedEt.Text);

					climateControlData.currentTemperature = null;
					if (currentTempCb.Checked)
					{
						climateControlData.currentTemperature = new Temperature();
						if (currentTempUnitCb.Checked)
							climateControlData.currentTemperature.unit = (TemperatureUnit)currentTempUnitSpn.SelectedItemPosition;

						if (currentTempValueCb.Checked && currentTempValueEt.Text != null && currentTempValueEt.Text.Length > 0)
							climateControlData.currentTemperature.value = Java.Lang.Float.ParseFloat(currentTempValueEt.Text);
					}

					climateControlData.desiredTemperature = null;
					if (desiredTempCb.Checked)
					{
						climateControlData.desiredTemperature = new Temperature();
						if (desiredTempUnitCb.Checked)
							climateControlData.desiredTemperature.unit = (TemperatureUnit)desiredTempUnitSpn.SelectedItemPosition;

						if (desiredTempValueCb.Checked && desiredTempValueEt.Text != null && desiredTempValueEt.Text.Length > 0)
							climateControlData.desiredTemperature.value = Java.Lang.Float.ParseFloat(desiredTempValueEt.Text);
					}
					if (acEnableCb.Checked)
					{
						climateControlData.acEnable = acEnable_tgl.Checked;
					}
					if (circulatedAirEnableCb.Checked)
					{
						climateControlData.circulateAirEnable = circulatedAirEnable_tgl.Checked;
					}
					if (autoModeEnableCb.Checked)
					{
						climateControlData.autoModeEnable = autoModeEnable_tgl.Checked;
					}
					if (dualModeEnableCb.Checked)
					{
						climateControlData.dualModeEnable = dualModeEnable_tgl.Checked;
					}
					if (acMaxEnableCb.Checked)
					{
						climateControlData.acMaxEnable = acMaxEnable_tgl.Checked;
					}
					if (heatedSteeringWheelEnableCb.Checked)
					{
						climateControlData.heatedSteeringWheelEnable = heatedSteeringWheelEnable_tgl.Checked;
					}
					if (heatedWindshieldEnableCb.Checked)
					{
						climateControlData.heatedWindshieldEnable = heatedWindshieldEnable_tgl.Checked;
					}
					if (heatedRearWindowEnableCb.Checked)
					{
						climateControlData.heatedRearWindowEnable = heatedRearWindowEnable_tgl.Checked;
					}
					if (heatedMirrorsEnableCb.Checked)
					{
						climateControlData.heatedMirrorsEnable = heatedMirrorsEnable_tgl.Checked;
					}

                    if (climateEnableCb.Checked)
                    {
                        climateControlData.climateEnable = climateEnable_tgl.Checked;
                    }

					if (defrostZoneEnableCb.Checked)
					{
						climateControlData.defrostZone = (DefrostZone)defrostZoneEnableSpn.SelectedItemPosition;
					}

					if (dataVentilationModeCb.Checked)
					{
						climateControlData.ventilationMode = (VentilationMode)dataVentilationModeSpn.SelectedItemPosition;
					}
				
			});

			climateControlDataAlertDialogue.Show();
		}

        public void SeatControlDataAlertDialogue()
        {
            AlertDialog.Builder seatControlDataAlertDialogue = new AlertDialog.Builder(Activity);
            View seatControlDataView = layoutInflater.Inflate(Resource.Layout.seat_control_data, null);
            seatControlDataAlertDialogue.SetView(seatControlDataView);
            seatControlDataAlertDialogue.SetTitle("SeatControlData");

            List<MassageModeData> massageModeList = null;
            List<MassageCushionFirmness> massageCushionList = null;
            if (null != seatControlData)
            {
                massageModeList = seatControlData.getMassageMode();
                massageCushionList = seatControlData.getMassageCushionFirmness();
            }
            if (null == massageModeList)
            {
                massageModeList = new List<MassageModeData>();
            }
            if (null == massageCushionList)
            {
                massageCushionList = new List<MassageCushionFirmness>();
            }

            CheckBox id_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_id_chk);
            Spinner id_spn = (Spinner)seatControlDataView.FindViewById(Resource.Id.seat_control_id_spn);

            String[] SupportedSeatArray = Enum.GetNames(typeof(SupportedSeat));
            id_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, SupportedSeatArray);

            id_chk.CheckedChange += (s, e) => id_spn.Enabled = e.IsChecked;

            CheckBox heating_enabled_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_heating_enabled_chk);
            Switch heating_enabled_tgl = (Switch)seatControlDataView.FindViewById(Resource.Id.seat_control_heating_enabled_tgl);
			heating_enabled_chk.CheckedChange += (sender, e) => {
				heating_enabled_tgl.Enabled = e.IsChecked;
			};

            CheckBox cooling_enabled_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_cooling_enabled_chk);
            Switch cooling_enabled_tgl = (Switch)seatControlDataView.FindViewById(Resource.Id.seat_control_cooling_enabled_tgl);
			cooling_enabled_chk.CheckedChange += (sender, e) =>
			{
				cooling_enabled_tgl.Enabled = e.IsChecked;
			};

            CheckBox heating_level_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_heating_level_chk);
            EditText heating_level_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_heating_level_et);
            heating_level_chk.CheckedChange += (sender, e) => heating_level_et.Enabled = e.IsChecked;

            CheckBox cooling_level_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_cooling_level_chk);
            EditText cooling_level_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_cooling_level_et);
            cooling_level_chk.CheckedChange += (sender, e) => cooling_level_et.Enabled = e.IsChecked;

            CheckBox horizontal_position_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_horizontal_position_chk);
            EditText horizontal_position_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_horizontal_position_et);
            horizontal_position_chk.CheckedChange += (sender, e) => horizontal_position_et.Enabled = e.IsChecked;

            CheckBox vertical_position_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_vertical_position_chk);
            EditText vertical_position_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_vertical_position_et);
            vertical_position_chk.CheckedChange += (sender, e) => vertical_position_et.Enabled = e.IsChecked;

            CheckBox front_vertical_position_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_front_vertical_position_chk);
            EditText front_vertical_position_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_front_vertical_position_et);
            front_vertical_position_chk.CheckedChange += (sender, e) => front_vertical_position_et.Enabled = e.IsChecked;

            CheckBox back_vertical_position_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_back_vertical_position_chk);
            EditText back_vertical_position_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_back_vertical_position_et);
            back_vertical_position_chk.CheckedChange += (sender, e) => back_vertical_position_et.Enabled = e.IsChecked;

            CheckBox back_tilt_angle_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_back_tilt_angle_chk);
            EditText back_tilt_angle_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_back_tilt_angle_et);
            back_tilt_angle_chk.CheckedChange += (sender, e) => back_tilt_angle_et.Enabled = e.IsChecked;

            CheckBox head_support_horizontal_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_head_support_horizontal_chk);
            EditText head_support_horizontal_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_head_support_horizontal_et);
            head_support_horizontal_chk.CheckedChange += (sender, e) => head_support_horizontal_et.Enabled = e.IsChecked;

            CheckBox head_support_vertical_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_head_support_vertical_chk);
            EditText head_support_vertical_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_head_support_vertical_et);
            head_support_vertical_chk.CheckedChange += (sender, e) => head_support_vertical_et.Enabled = e.IsChecked;

            CheckBox massage_enabled_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_enabled_chk);
            Switch massage_enabled_tgl = (Switch)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_enabled_tgl);
			massage_enabled_chk.CheckedChange += (sender, e) => massage_enabled_tgl.Enabled = e.IsChecked;

            CheckBox massage_zone_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_zone_chk);
            Spinner massage_zone_spn = (Spinner)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_zone_spn);
            massage_zone_chk.CheckedChange += (sender, e) => massage_zone_spn.Enabled = e.IsChecked;
            String[] MassageZoneArray = Enum.GetNames(typeof(MassageZone));
            massage_zone_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageZoneArray);

            CheckBox massage_mode_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_mode_chk);
            Spinner massage_mode_spn = (Spinner)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_mode_spn);
            massage_mode_chk.CheckedChange += (sender, e) => massage_mode_spn.Enabled = e.IsChecked;
            String[] MassageModeArray = Enum.GetNames(typeof(MassageMode));
            massage_mode_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageModeArray);

            CheckBox massage_mode_data_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_mode_data_chk);
            Button massage_mode_btn = (Button)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_mode_btn);
            massage_mode_data_chk.CheckedChange += (sender, e) => 
            {
                massage_mode_btn.Enabled = e.IsChecked;
                massage_zone_chk.Enabled = e.IsChecked;
                massage_zone_spn.Enabled = e.IsChecked && massage_zone_chk.Checked;
                massage_mode_chk.Enabled = e.IsChecked;
                massage_mode_spn.Enabled = e.IsChecked && massage_mode_chk.Checked;
            };

            massage_mode_btn.Click += delegate 
            {
                AlertDialog.Builder massageModeDataAlertDialogue = new AlertDialog.Builder(Activity);
                View massageModeDataView = layoutInflater.Inflate(Resource.Layout.add_more_massage_data, null);
                massageModeDataAlertDialogue.SetView(massageModeDataView);
                massageModeDataAlertDialogue.SetTitle("Massage Mode Data");

                CheckBox massageZoneCheck = (CheckBox)massageModeDataView.FindViewById(Resource.Id.message_zone_check);
                Spinner massageZoneSpinner = (Spinner)massageModeDataView.FindViewById(Resource.Id.message_zone_spinner);
                massageZoneSpinner.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageZoneArray);
                CheckBox massageModeCheck = (CheckBox)massageModeDataView.FindViewById(Resource.Id.massage_mode_check);
                Spinner massageModeSpinner = (Spinner)massageModeDataView.FindViewById(Resource.Id.massage_mode_spinner);
                massageModeSpinner.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageModeArray);
                ListView massageModeListView = (ListView)massageModeDataView.FindViewById(Resource.Id.massage_mode_list);

                var massageModeAdapter = new Src.Adapters.MassageModeDataAdapter(Activity, massageModeList);
                massageModeListView.Adapter = massageModeAdapter;

                massageZoneCheck.CheckedChange += (s1, e1) => massageZoneSpinner.Enabled = e1.IsChecked;
                massageModeCheck.CheckedChange += (s1, e1) => massageModeSpinner.Enabled = e1.IsChecked;

                Button addButton = (Button)massageModeDataView.FindViewById(Resource.Id.massage_mode_button);
                addButton.Click += (s1, e1) => 
                {
                    if (massageZoneCheck.Checked || massageModeCheck.Checked)
                    {
                        MassageModeData data = new MassageModeData();
                        if (massageZoneCheck.Checked)
                        {
                            data.massageZone = (MassageZone)massageZoneSpinner.SelectedItemPosition;
                        }
                        if (massageModeCheck.Checked)
                        {
                            data.massageMode = (MassageMode)massageModeSpinner.SelectedItemPosition;
                        }
                        massageModeList.Add(data);
                        massageModeAdapter.NotifyDataSetChanged();
                    }
                };

                massageModeDataAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
                {
					massageModeDataAlertDialogue.Dispose();
				});

                massageModeDataAlertDialogue.Show();
            };

            CheckBox massage_cushion_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_chk);
            Spinner massage_cushion_spn = (Spinner)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_spn);
            massage_cushion_chk.CheckedChange += (sender, e) => massage_cushion_spn.Enabled = e.IsChecked;
            String[] MassageCushionArray = Enum.GetNames(typeof(MassageCushion));
            massage_cushion_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageCushionArray);

            CheckBox massage_cushion_firmness_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_firmness_chk);
            EditText massage_cushion_firmness_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_firmness_et);
            massage_cushion_firmness_chk.CheckedChange += (sender, e) => massage_cushion_firmness_et.Enabled = e.IsChecked;

            CheckBox cushion_firm_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_firm_chk);
            Button massage_cushion_btn = (Button)seatControlDataView.FindViewById(Resource.Id.seat_control_massage_cushion_btn);
            cushion_firm_chk.CheckedChange += (sender, e) =>
            {
                massage_cushion_btn.Enabled = e.IsChecked;
                massage_cushion_chk.Enabled = e.IsChecked;
                massage_cushion_spn.Enabled = e.IsChecked && massage_cushion_chk.Checked;
                massage_cushion_firmness_chk.Enabled = e.IsChecked;
                massage_cushion_firmness_et.Enabled = e.IsChecked && massage_cushion_firmness_chk.Checked;
            };

            massage_cushion_btn.Click += delegate
            {
                AlertDialog.Builder massageCushionAlertDialogue = new AlertDialog.Builder(Activity);
                View massageCushionView = layoutInflater.Inflate(Resource.Layout.add_more_cushion_firmness, null);
                massageCushionAlertDialogue.SetView(massageCushionView);
                massageCushionAlertDialogue.SetTitle("Massage Cushion Firmness");

                CheckBox massageCushionCheck = (CheckBox)massageCushionView.FindViewById(Resource.Id.massage_cushion_check);
                Spinner massageCushionSpinner = (Spinner)massageCushionView.FindViewById(Resource.Id.massage_cushion_spinner);
                massageCushionSpinner.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, MassageCushionArray);
                CheckBox firmnessCheck = (CheckBox)massageCushionView.FindViewById(Resource.Id.firmness_check);
                EditText firmness_et = (EditText)massageCushionView.FindViewById(Resource.Id.firmness_et);
                ListView massageCushionListView = (ListView)massageCushionView.FindViewById(Resource.Id.massage_cushion_list);

                var massageCushionAdapter = new Src.Adapters.MassageCushionAdapter(Activity, massageCushionList);
                massageCushionListView.Adapter = massageCushionAdapter;

                massageCushionCheck.CheckedChange += (s1, e1) => massageCushionSpinner.Enabled = e1.IsChecked;
                firmnessCheck.CheckedChange += (s1, e1) => firmness_et.Enabled = e1.IsChecked;

                Button addButton = (Button)massageCushionView.FindViewById(Resource.Id.massage_cushion_button);
                addButton.Click += (s1, e1) =>
                {
                    if (massageCushionCheck.Checked || firmnessCheck.Checked)
                    {
                        MassageCushionFirmness data = new MassageCushionFirmness();
                        if (massageCushionCheck.Checked)
                        {
                            data.cushion = (MassageCushion)massageCushionSpinner.SelectedItemPosition;
                        }
                        if (firmnessCheck.Checked)
                        {
                            try
                            {
                                data.firmness = Int32.Parse(firmness_et.Text);
                            }
                            catch (Exception e)
                            {}
                        }
                        massageCushionList.Add(data);
                        massageCushionAdapter.NotifyDataSetChanged();
                    }
                };

                massageCushionAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
                {
					massageCushionAlertDialogue.Dispose();
				});

                massageCushionAlertDialogue.Show();
            };

            CheckBox memory_id_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_id_chk);
            EditText memory_id_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_id_et);
            memory_id_chk.CheckedChange += (sender, e) => memory_id_et.Enabled = e.IsChecked;

            CheckBox memory_label_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_label_chk);
            EditText memory_label_et = (EditText)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_label_et);
            memory_label_chk.CheckedChange += (sender, e) => memory_label_et.Enabled = e.IsChecked;

            CheckBox memory_action_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_action_chk);
            Spinner memory_action_spn = (Spinner)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_action_spn);
            memory_action_chk.CheckedChange += (sender, e) => memory_action_spn.Enabled = e.IsChecked;
            String[] SeatMemoryActionTypeArray = Enum.GetNames(typeof(SeatMemoryActionType));
            memory_action_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, SeatMemoryActionTypeArray);

            CheckBox memory_chk = (CheckBox)seatControlDataView.FindViewById(Resource.Id.seat_control_memory_chk);
            memory_chk.CheckedChange += (sender, e) => 
            {
                memory_id_chk.Enabled = e.IsChecked;
                memory_id_et.Enabled = e.IsChecked && memory_id_chk.Checked;
                memory_label_chk.Enabled = e.IsChecked;
                memory_label_et.Enabled = e.IsChecked && memory_label_chk.Checked;
                memory_action_chk.Enabled = e.IsChecked;
                memory_action_spn.Enabled = e.IsChecked && memory_action_chk.Checked;
            };

            if (null != seatControlData)
            {
                if (null != seatControlData.getId())
                {
                    id_spn.SetSelection((int)seatControlData.getId());
                }
                else
                {
                    id_chk.Checked = false;
                    id_spn.Enabled = false;
                }
                if (null != seatControlData.getHeatingEnabled())
                {
                    heating_enabled_tgl.Checked = (bool)seatControlData.getHeatingEnabled();
                }
                else
                {
                    heating_enabled_chk.Checked = false;
                    heating_enabled_tgl.Enabled = false;
                }
                if (null != seatControlData.getCoolingEnabled())
                {
                    cooling_enabled_tgl.Checked = (bool)seatControlData.getCoolingEnabled();
                }
                else
                {
                    cooling_enabled_chk.Checked = false;
                    cooling_enabled_tgl.Enabled = false;
                }
                if (null != seatControlData.getHeatingLevel())
                {
                    heating_level_et.Text = seatControlData.getHeatingLevel().ToString();
                }
                else
                {
                    heating_level_chk.Checked = false;
                    heating_level_et.Enabled = false;
                }
                if (null != seatControlData.getCoolingLevel())
                {
                    cooling_level_et.Text = seatControlData.getCoolingLevel().ToString();
                }
                else
                {
                    cooling_level_chk.Checked = false;
                    cooling_level_et.Enabled = false;
                }
                if (null != seatControlData.getHorizontalPosition())
                {
                    horizontal_position_et.Text = seatControlData.getHorizontalPosition().ToString();
                }
                else
                {
                    horizontal_position_chk.Checked = false;
                    horizontal_position_et.Enabled = false;
                }
                if (null != seatControlData.getVerticalPosition())
                {
                    vertical_position_et.Text = seatControlData.getVerticalPosition().ToString();
                }
                else
                {
                    vertical_position_chk.Checked = false;
                    vertical_position_et.Enabled = false;
                }
                if (null != seatControlData.getFrontVerticalPosition())
                {
                    front_vertical_position_et.Text = seatControlData.getFrontVerticalPosition().ToString();
                }
                else
                {
                    front_vertical_position_chk.Checked = false;
                    front_vertical_position_et.Enabled = false;
                }
                if (null != seatControlData.getBackVerticalPosition())
                {
                    back_vertical_position_et.Text = seatControlData.getBackVerticalPosition().ToString();
                }
                else
                {
                    back_vertical_position_chk.Checked = false;
                    back_vertical_position_et.Enabled = false;
                }
                if (null != seatControlData.getBackTiltAngle())
                {
                    back_tilt_angle_et.Text = seatControlData.getBackTiltAngle().ToString();
                }
                else
                {
                    back_tilt_angle_chk.Checked = false;
                    back_tilt_angle_et.Enabled = false;
                }
                if (null != seatControlData.getHeadSupportHorizontalPosition())
                {
                    head_support_horizontal_et.Text = seatControlData.getHeadSupportHorizontalPosition().ToString();
                }
                else
                {
                    head_support_horizontal_chk.Checked = false;
                    head_support_horizontal_et.Enabled = false;
                }
                if (null != seatControlData.getHeadSupportVerticalPosition())
                {
                    head_support_vertical_et.Text = seatControlData.getHeadSupportVerticalPosition().ToString();
                }
                else
                {
                    head_support_vertical_chk.Checked = false;
                    head_support_vertical_et.Enabled = false;
                }
                if (null != seatControlData.getMassageEnabled())
                {
                    massage_enabled_tgl.Checked = (bool)seatControlData.getMassageEnabled();
                }
                else
                {
                    massage_enabled_chk.Checked = false;
                    massage_enabled_tgl.Enabled = false;
                }
                int massageModeListCount = massageModeList.Count;
                if (massageModeListCount > 0)
                {
                    MassageModeData data = massageModeList[massageModeListCount - 1];
                    if (null != data.getMassageZone())
                    {
                        massage_zone_spn.SetSelection((int)data.getMassageZone());
                    }
                    else
                    {
                        massage_zone_spn.Enabled = false;
                        massage_zone_chk.Checked = false;
                    }
                    if (null != data.getMassageMode())
                    {
                        massage_mode_spn.SetSelection((int)data.getMassageMode());
                    }
                    else
                    {
                        massage_mode_spn.Enabled = false;
                        massage_mode_chk.Checked = false;
                    }
                    massageModeList.RemoveAt(massageModeListCount - 1);
                }
                else
                {
                    massage_mode_data_chk.Checked = false;
                    massage_zone_chk.Enabled = false;
                    massage_zone_spn.Enabled = false;
                    massage_mode_chk.Enabled = false;
                    massage_mode_spn.Enabled = false;
                    massage_mode_btn.Enabled = false;
                }
                int massageCushionListCount = massageCushionList.Count;
                if (massageCushionListCount > 0)
                {
                    MassageCushionFirmness data = massageCushionList[massageCushionListCount - 1];
                    if (null != data.getMassageCushion())
                    {
                        massage_cushion_spn.SetSelection((int)data.getMassageCushion());
                    }
                    else
                    {
                        massage_cushion_spn.Enabled = false;
                        massage_cushion_chk.Checked = false;
                    }
                    if (null != data.getFirmness())
                    {
                        massage_cushion_firmness_et.Text = data.getFirmness().ToString();
                    }
                    else
                    {
                        massage_cushion_firmness_et.Enabled = false;
                        massage_cushion_firmness_chk.Checked = false;
                    }
                    massageCushionList.RemoveAt(massageCushionListCount - 1);
                }
                else
                {
                    cushion_firm_chk.Checked = false;
                    massage_cushion_chk.Enabled = false;
                    massage_cushion_spn.Enabled = false;
                    massage_cushion_firmness_chk.Enabled = false;
                    massage_cushion_firmness_et.Enabled = false;
                    massage_cushion_btn.Enabled = false;
                }
                if (null != seatControlData.getMemory())
                {
                    if (null != seatControlData.getMemory().id)
                    {
                        memory_id_et.Text = seatControlData.getMemory().id.ToString();
                    }
                    else
                    {
                        memory_id_chk.Checked = false;
                        memory_id_et.Enabled = false;
                    }
                    if (null != seatControlData.getMemory().label)
                    {
                        memory_label_et.Text = seatControlData.getMemory().label;
                    }
                    else
                    {
                        memory_label_chk.Checked = false;
                        memory_label_et.Enabled = false;
                    }
                    if (null != seatControlData.getMemory().action)
                    {
                        memory_action_spn.SetSelection((int)seatControlData.getMemory().action);
                    }
                    else
                    {
                        memory_action_chk.Checked = false;
                        memory_action_spn.Enabled = false;
                    }
                }
                else
                {
                    memory_chk.Checked = false;
                    memory_id_chk.Enabled = false;
                    memory_id_et.Enabled = false;
                    memory_label_chk.Enabled = false;
                    memory_label_et.Enabled = false;
                    memory_action_chk.Enabled = false;
                    memory_action_spn.Enabled = false;
                }
            }

            seatControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
            {
                seatControlDataAlertDialogue.Dispose();
            });

            seatControlDataAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
            {
                seatControlData = new SeatControlData();
                if (id_chk.Checked)
                {
                    seatControlData.id = (SupportedSeat)id_spn.SelectedItemPosition;
                }
                if (heating_enabled_chk.Checked)
                {
                    seatControlData.heatingEnabled = heating_enabled_tgl.Checked;
                }
                if (cooling_enabled_chk.Checked)
                {
                    seatControlData.coolingEnabled = cooling_enabled_tgl.Checked;
                }
                if (heating_level_chk.Checked)
                {
                    try
                    {
                        seatControlData.heatingLevel = Int32.Parse(heating_level_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (cooling_level_chk.Checked)
                {
                    try
                    {
                        seatControlData.coolingLevel = Int32.Parse(cooling_level_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (horizontal_position_chk.Checked)
                {
                    try
                    {
                        seatControlData.horizontalPosition = Int32.Parse(horizontal_position_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (vertical_position_chk.Checked)
                {
                    try
                    {
                        seatControlData.verticalPosition = Int32.Parse(vertical_position_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (front_vertical_position_chk.Checked)
                {
                    try
                    {
                        seatControlData.frontVerticalPosition = Int32.Parse(front_vertical_position_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (back_vertical_position_chk.Checked)
                {
                    try
                    {
                        seatControlData.backVerticalPosition = Int32.Parse(back_vertical_position_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (back_tilt_angle_chk.Checked)
                {
                    try
                    {
                        seatControlData.backTiltAngle = Int32.Parse(back_tilt_angle_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (head_support_horizontal_chk.Checked)
                {
                    try
                    {
                        seatControlData.headSupportHorizontalPosition = Int32.Parse(head_support_horizontal_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (head_support_vertical_chk.Checked)
                {
                    try
                    {
                        seatControlData.headSupportVerticalPosition = Int32.Parse(head_support_vertical_et.Text.ToString());
                    }
                    catch (Exception e)
                    {

                    }
                }
                if (massage_enabled_chk.Checked)
                {
                    seatControlData.massageEnabled = massage_enabled_tgl.Checked;
                }
                if (massage_mode_data_chk.Checked)
                {
                    MassageModeData data = new MassageModeData();
                    if (massage_zone_chk.Checked)
                        data.massageZone = (MassageZone)massage_zone_spn.SelectedItemPosition;
                    if (massage_mode_chk.Checked)
                        data.massageMode = (MassageMode)massage_mode_spn.SelectedItemPosition;
                    massageModeList.Add(data);
                    seatControlData.massageMode = massageModeList;
                }
                if (cushion_firm_chk.Checked)
                {
                    MassageCushionFirmness data = new MassageCushionFirmness();
                    if (massage_cushion_chk.Checked)
                        data.cushion = (MassageCushion)massage_cushion_spn.SelectedItemPosition;
                    if (massage_cushion_firmness_chk.Checked)
                    {
                        try
                        {
                            data.firmness = Int32.Parse(massage_cushion_firmness_et.Text.ToString());
                        }
                        catch (Exception e)
                        {

                        }
                    }                        
                    massageCushionList.Add(data);
                    seatControlData.massageCushionFirmness = massageCushionList;
                }
                if (memory_chk.Checked)
                {
                    SeatMemoryAction memory = new SeatMemoryAction();
                    if (memory_id_chk.Checked)
                    {
                        try
                        {
                            memory.id = Int32.Parse(memory_id_et.Text.ToString());
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    if (memory_label_chk.Checked)
                    {
                        memory.label = memory_label_et.Text.ToString();
                    }
                    if (memory_action_chk.Checked)
                    {
                        memory.action = (SeatMemoryActionType)memory_action_spn.SelectedItemPosition;
                    }
                    seatControlData.memory = memory;
                }
            });

            seatControlDataAlertDialogue.Show();
        }

        public void AudioControlDataAlertDialogue()
        {
            AlertDialog.Builder audioControlDataAlertDialogue = new AlertDialog.Builder(Activity);
            View audioControlDataView = layoutInflater.Inflate(Resource.Layout.audio_control_data, null);
            audioControlDataAlertDialogue.SetView(audioControlDataView);
            audioControlDataAlertDialogue.SetTitle("AudioControlData");

            List<EqualizerSettings> EqualizerSettingsList = null;
            if (null != audioControlData)
            {
                EqualizerSettingsList = audioControlData.getEqualizerSettings();
            }
            if (null == EqualizerSettingsList)
            {
                EqualizerSettingsList = new List<EqualizerSettings>();
            }

            CheckBox source_cb = (CheckBox)audioControlDataView.FindViewById(Resource.Id.primary_audio_source_check);
            Spinner source_spn = (Spinner)audioControlDataView.FindViewById(Resource.Id.primary_audio_source_spn);
            source_cb.CheckedChange += (sender, e) => source_spn.Enabled = e.IsChecked;
            source_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PrimaryAudioSourceArray);

            CheckBox context_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.keep_context_check);
            Switch context_tgl = (Switch)audioControlDataView.FindViewById(Resource.Id.keep_context_tgl);
			context_check.CheckedChange += (sender, e) => context_tgl.Enabled = e.IsChecked;

            CheckBox volume_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.volume_check);
            EditText volume_et = (EditText)audioControlDataView.FindViewById(Resource.Id.volume_et);
            volume_check.CheckedChange += (sender, e) => volume_et.Enabled = e.IsChecked;

            CheckBox equalizer_setting_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_check);
            Button add_more_equalizer = (Button)audioControlDataView.FindViewById(Resource.Id.add_more_equalizer);

            CheckBox channel_id_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_id_check);
            EditText channel_id_et = (EditText)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_id_et);
            channel_id_check.CheckedChange += (sender, e) => channel_id_et.Enabled = e.IsChecked;

            CheckBox name_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_name_check);
            EditText name_et = (EditText)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_name_et);
            name_check.CheckedChange += (sender, e) => name_et.Enabled = e.IsChecked;

            CheckBox channel_setting_check = (CheckBox)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_setting_check);
            EditText channel_setting_et = (EditText)audioControlDataView.FindViewById(Resource.Id.equalizer_setting_channel_setting_et);
            channel_setting_check.CheckedChange += (sender, e) => channel_setting_et.Enabled = e.IsChecked;

            equalizer_setting_check.CheckedChange += (sender, e) => 
            {
                channel_id_check.Checked = e.IsChecked;
                channel_id_et.Enabled = e.IsChecked && channel_id_check.Checked;
                name_check.Checked = e.IsChecked;
                name_et.Enabled = e.IsChecked && name_check.Checked;
                channel_setting_check.Checked = e.IsChecked;
                channel_setting_et.Enabled = e.IsChecked && channel_setting_check.Checked;
                add_more_equalizer.Enabled = e.IsChecked;
            };

            add_more_equalizer.Click += delegate
            {
                AlertDialog.Builder equalizerAlertDialogue = new AlertDialog.Builder(Activity);
                View equalizerView = layoutInflater.Inflate(Resource.Layout.add_more_equalizer, null);
                equalizerAlertDialogue.SetView(equalizerView);
                equalizerAlertDialogue.SetTitle("Equalizer Settings");

                CheckBox eChannel_id_check = (CheckBox)equalizerView.FindViewById(Resource.Id.eq_setting_channel_id_check);
                EditText eChannel_id_et = (EditText)equalizerView.FindViewById(Resource.Id.eq_setting_channel_id_et);
                eChannel_id_check.CheckedChange += (sender, e) => eChannel_id_et.Enabled = e.IsChecked;

                CheckBox eName_check = (CheckBox)equalizerView.FindViewById(Resource.Id.eq_setting_channel_name_check);
                EditText eName_et = (EditText)equalizerView.FindViewById(Resource.Id.eq_setting_channel_name_et);
                eName_check.CheckedChange += (sender, e) => eName_et.Enabled = e.IsChecked;

                CheckBox eChannel_setting_check = (CheckBox)equalizerView.FindViewById(Resource.Id.eq_setting_channel_setting_check);
                EditText eChannel_setting_et = (EditText)equalizerView.FindViewById(Resource.Id.eq_setting_channel_setting_et);
                eChannel_setting_check.CheckedChange += (sender, e) => eChannel_setting_et.Enabled = e.IsChecked;

                ListView equalizerListView = (ListView)equalizerView.FindViewById(Resource.Id.eq_setting_list);

                var equalizerAdapter = new Src.Adapters.EqualizerSettingsAdapter(Activity, EqualizerSettingsList);
                equalizerListView.Adapter = equalizerAdapter;

                Button addButton = (Button)equalizerView.FindViewById(Resource.Id.add_equalizer_setting);
                addButton.Click += (s1, e1) =>
                {
                    if (eChannel_id_check.Checked || eName_check.Checked || eChannel_setting_check.Checked)
                    {
                        EqualizerSettings data = new EqualizerSettings();
                        if (eChannel_id_check.Checked)
                        {
                            try
                            {
                                data.channelId = Int32.Parse(eChannel_id_et.Text);
                            }
                            catch (Exception e) {}
                        }
                        if (eName_check.Checked)
                        {
                            data.channelName = eName_et.Text;
                        }
                        if (eChannel_setting_check.Checked)
                        {
                            try
                            {
                                data.channelSetting = Int32.Parse(eChannel_setting_et.Text);
                            }
                            catch (Exception e) { }
                        }
                        EqualizerSettingsList.Add(data);
                        equalizerAdapter.NotifyDataSetChanged();
                    }
                };


                equalizerAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
                {
                    equalizerAlertDialogue.Dispose();
                });

                equalizerAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
                {

                });

                equalizerAlertDialogue.Show();
            };

            if (null != audioControlData)
            {
                if (null != audioControlData.getSource())
                {
                    source_spn.SetSelection((int) audioControlData.getSource());
                }
                else
                {
                    source_cb.Checked = false;
                    source_spn.Enabled = false;
                }
                if (null != audioControlData.getKeepContext())
                {
                    context_tgl.Checked = (bool)audioControlData.getKeepContext();
                }
                else
                {
                    context_check.Checked = false;
                    context_tgl.Enabled = false;
                }
                if (null != audioControlData.getVolume())
                {
                    volume_et.Text = audioControlData.getVolume().ToString();
                }
                else
                {
                    volume_check.Checked = false;
                    volume_et.Enabled = false;
                }
                int EqualizerSettingsListCount = EqualizerSettingsList.Count;
                if (EqualizerSettingsListCount > 0)
                {
                    EqualizerSettings eq = EqualizerSettingsList[EqualizerSettingsListCount - 1];
                    if (null != eq)
                    {
                        if (null != eq.getChannelId())
                        {
                            channel_id_et.Text = eq.getChannelId().ToString();
                        }
                        else
                        {
                            channel_id_check.Checked = false;
                            channel_id_et.Enabled = false;
                        }
                        if (null != eq.getChannelName())
                        {
                            name_et.Text = eq.getChannelName();
                        }
                        else
                        {
                            name_check.Checked = false;
                            name_et.Enabled = false;
                        }
                        if (null != eq.getChannelSetting())
                        {
                            channel_setting_et.Text = eq.getChannelSetting().ToString();
                        }
                        else
                        {
                            channel_setting_check.Enabled = false;
                            channel_setting_et.Enabled = false;
                        }
                    }
                    else
                    {
                        channel_setting_check.Checked = false;
                        channel_id_check.Enabled = false;
                        channel_id_et.Enabled = false;
                        name_check.Enabled = false;
                        name_et.Enabled = false;
                        channel_setting_check.Enabled = false;
                        channel_setting_et.Enabled = false;
                        add_more_equalizer.Enabled = false;
                    }
                    EqualizerSettingsList.RemoveAt(EqualizerSettingsListCount - 1);
                }
                else
                {
                    channel_setting_check.Checked = false;
                    channel_id_check.Enabled = false;
                    channel_id_et.Enabled = false;
                    name_check.Enabled = false;
                    name_et.Enabled = false;
                    channel_setting_check.Enabled = false;
                    channel_setting_et.Enabled = false;
                    add_more_equalizer.Enabled = false;
                }
            }

            audioControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
            {
                audioControlDataAlertDialogue.Dispose();
            });

            audioControlDataAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
            {
                audioControlData = new AudioControlData();
                if (source_cb.Checked)
                {
                    audioControlData.source = (PrimaryAudioSource)source_spn.SelectedItemPosition;
                }
                if (context_check.Checked)
                {
                    audioControlData.keepContext = context_tgl.Checked;
                }
                if (volume_check.Checked)
                {
                    try
                    {
                        audioControlData.volume = Int32.Parse(volume_et.Text);
                    }
                    catch (Exception e) {}
                }
                if (equalizer_setting_check.Checked)
                {
                    EqualizerSettings eq = new EqualizerSettings();
                    if (channel_id_check.Checked)
                    {
                        try
                        {
                            eq.channelId = Int32.Parse(channel_id_et.Text);
                        }
                        catch (Exception e) { }
                    }
                    if (name_check.Checked)
                    {
                        eq.channelName = name_et.Text;
                    }
                    if (channel_setting_check.Checked)
                    {
                        try
                        {
                            eq.channelSetting = Int32.Parse(channel_setting_et.Text);
                        }
                        catch (Exception e) { }
                    }
                    EqualizerSettingsList.Add(eq);
                    audioControlData.equalizerSettings = EqualizerSettingsList;
                }
            });

            audioControlDataAlertDialogue.Show();
        }

        public void LightControlDataAlertDialogue()
        {
            AlertDialog.Builder lightControlDataAlertDialogue = new AlertDialog.Builder(Activity);
            View lightControlDataView = layoutInflater.Inflate(Resource.Layout.light_control_data, null);
            lightControlDataAlertDialogue.SetView(lightControlDataView);
            lightControlDataAlertDialogue.SetTitle("LightControlData");

            List<LightState> LightStateList = null;
            if (null != lightControlData)
            {
                LightStateList = lightControlData.getLightState();
            }
            if (null == LightStateList)
            {
                LightStateList = new List<LightState>();
            }

            CheckBox id_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_id_check);
            Spinner id_spn = (Spinner)lightControlDataView.FindViewById(Resource.Id.light_state_id_spn);
            id_check.CheckedChange += (sender, e) => id_spn.Enabled = e.IsChecked;
            String[] LightNameArray = Enum.GetNames(typeof(LightName));
            id_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, LightNameArray);

            CheckBox status_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_status_check);
            Spinner status_spn = (Spinner)lightControlDataView.FindViewById(Resource.Id.light_state_status_spn);
            status_check.CheckedChange += (sender, e) => status_spn.Enabled = e.IsChecked;
            String[] LightStatusArray = Enum.GetNames(typeof(LightStatus));
            status_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, LightStatusArray);

            CheckBox density_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_density_check);
            EditText density_et = (EditText)lightControlDataView.FindViewById(Resource.Id.light_state_density_et);
            density_check.CheckedChange += (sender, e) => density_et.Enabled = e.IsChecked;

            CheckBox rgb_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_check);

            CheckBox red_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_red_check);
            EditText red_et = (EditText)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_red_et);
            red_check.CheckedChange += (sender, e) => red_et.Enabled = e.IsChecked;

            CheckBox green_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_green_check);
            EditText green_et = (EditText)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_green_et);
            green_check.CheckedChange += (sender, e) => green_et.Enabled = e.IsChecked;

            CheckBox blue_check = (CheckBox)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_blue_check);
            EditText blue_et = (EditText)lightControlDataView.FindViewById(Resource.Id.light_state_rgb_blue_et);
            blue_check.CheckedChange += (sender, e) => blue_et.Enabled = e.IsChecked;

            ListView listView = (ListView)lightControlDataView.FindViewById(Resource.Id.light_status_list_view);
            Button addBtn = (Button)lightControlDataView.FindViewById(Resource.Id.add_more_light_status);

            rgb_check.CheckedChange += (sender, e) => 
            {
                red_check.Enabled = e.IsChecked;
                red_et.Enabled = e.IsChecked && red_check.Checked;
                green_check.Enabled = e.IsChecked;
                green_et.Enabled = e.IsChecked && green_check.Checked;
                blue_check.Enabled = e.IsChecked;
                blue_et.Enabled = e.IsChecked && blue_check.Checked;
            };

            var lsAdapter = new Src.Adapters.LightStateAdapter(Activity, LightStateList);
            listView.Adapter = lsAdapter;

            addBtn.Click += (sender, e) => 
            {
                if (id_check.Checked || status_check.Checked || density_check.Checked || rgb_check.Checked)
                {
                    LightState state = new LightState();
                    if (id_check.Checked)
                        state.id = (LightName)HmiApiLib.Utils.getEnumValue(typeof(LightName), (String)id_spn.SelectedItem);
                    if (status_check.Checked)
                        state.status = (LightStatus)status_spn.SelectedItemPosition;
                    if (density_check.Checked)
                    {
                        try
                        {
                            state.density = Int32.Parse(density_et.Text);
                        }
                        catch (Exception e1) { }
                    }
                    if (rgb_check.Checked)
                    {
                        RGBColor color = new RGBColor();
                        if (red_check.Checked)
                        {
                            try
                            {
                                color.red = Int32.Parse(red_et.Text);
                            }
                            catch (Exception e1) { }
                        }
                        if (green_check.Checked)
                        {
                            try
                            {
                                color.green = Int32.Parse(green_et.Text);
                            }
                            catch (Exception e1) { }
                        }
                        if (blue_check.Checked)
                        {
                            try
                            {
                                color.blue = Int32.Parse(blue_et.Text);
                            }
                            catch (Exception e1) { }
                        }
                        state.color = color;
                    }
                    LightStateList.Add(state);
                    lsAdapter.NotifyDataSetChanged();
                }
            };

            if (null != lightControlData)
            {
                int count = LightStateList.Count;
                if (count > 0)
                {
                    LightState data = LightStateList[count - 1];
                    if (null != data.getId())
                    {
                        String[] arr = Enum.GetNames(typeof(LightName));
                        for (int i = 0; i < arr.Length; i++)
                        {
                            if (arr[i].Equals(data.getId().ToString()))
                            {
                                id_spn.SetSelection(i);
                                break;
                            }
                        }
                    }
                    else
                    {
                        id_spn.Enabled = false;
                        id_check.Checked = false;
                    }
                    if (null != data.getStatus())
                    {
                        status_spn.SetSelection((int)data.getStatus());
                    }
                    else
                    {
                        status_check.Checked = false;
                        status_spn.Enabled = false;
                    }
                    if (null != data.getDensity())
                    {
                        density_et.Text = data.getDensity().ToString();
                    }
                    else
                    {
                        density_check.Checked = false;
                        density_et.Enabled = false;
                    }
                    RGBColor col = data.getRGBColor();
                    if (null != col)
                    {
                        if (null != col.getRed())
                        {
                            red_et.Text = col.getRed().ToString();
                        }
                        else
                        {
                            red_check.Checked = false;
                            red_et.Enabled = false;
                        }
                        if (null != col.getGreen())
                        {
                            green_et.Text = col.getGreen().ToString();
                        }
                        else
                        {
                            green_check.Checked = false;
                            green_et.Enabled = false;
                        }
                        if (null != col.getBlue())
                        {
                            blue_et.Text = col.getBlue().ToString();
                        }
                        else
                        {
                            blue_check.Checked = false;
                            blue_et.Enabled = false;
                        }
                    }
                    else
                    {
                        red_check.Enabled = false;
                        red_et.Enabled = false;
                        blue_check.Enabled = false;
                        blue_et.Enabled = false;
                        green_check.Enabled = false;
                        green_et.Enabled = false;
                        addBtn.Enabled = false;
                    }
                    LightStateList.RemoveAt(count - 1);
                }
            }

            lightControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
            {
                lightControlDataAlertDialogue.Dispose();
            });

            lightControlDataAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
            {
                lightControlData = new LightControlData();
                if (id_check.Checked || status_check.Checked || density_check.Checked || rgb_check.Checked)
                {
                    LightState state = new LightState();
                    if (id_check.Checked)
                        state.id = (LightName)HmiApiLib.Utils.getEnumValue(typeof(LightName) , (String)id_spn.SelectedItem);
                    if (status_check.Checked)
                        state.status = (LightStatus)status_spn.SelectedItemPosition;
                    if (density_check.Checked)
                    {
                        try
                        {
                            state.density = Int32.Parse(density_et.Text);
                        }
                        catch (Exception e) {}
                    }
                    if (rgb_check.Checked)
                    {
                        RGBColor color = new RGBColor();
                        if (red_check.Checked)
                        {
                            try
                            {
                                color.red = Int32.Parse(red_et.Text);
                            }
                            catch (Exception e) { }
                        }
                        if (green_check.Checked)
                        {
                            try
                            {
                                color.green = Int32.Parse(green_et.Text);
                            }
                            catch (Exception e) { }
                        }
                        if (blue_check.Checked)
                        {
                            try
                            {
                                color.blue = Int32.Parse(blue_et.Text);
                            }
                            catch (Exception e) { }
                        }
                        state.color = color;
                    }
                    LightStateList.Add(state);
                }
                lightControlData.lightState = LightStateList;
            });

            lightControlDataAlertDialogue.Show();
        }

        public void HmiSettingControlDataAlertDialogue()
        {
            AlertDialog.Builder hmiSettingControlDataAlertDialogue = new AlertDialog.Builder(Activity);
            View hmiSettingControlDataView = layoutInflater.Inflate(Resource.Layout.hmi_setting_control_data, null);
            hmiSettingControlDataAlertDialogue.SetView(hmiSettingControlDataView);
            hmiSettingControlDataAlertDialogue.SetTitle("HMI Setting Control Data");

            CheckBox display_mode_check = (CheckBox)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_display_mode_check);
            Spinner display_mode_spn = (Spinner)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_display_mode_spn);
            display_mode_check.CheckedChange += (sender, e) => display_mode_spn.Enabled = e.IsChecked;
            String[] DisplayModeArray = Enum.GetNames(typeof(DisplayMode));
            display_mode_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DisplayModeArray);

            CheckBox temperature_unit_check = (CheckBox)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_temperature_unit_check);
            Spinner temperature_unit_spn = (Spinner)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_temperature_unit_spn);
            temperature_unit_check.CheckedChange += (sender, e) => temperature_unit_spn.Enabled = e.IsChecked;
            String[] TemperatureUnitArray = Enum.GetNames(typeof(TemperatureUnit));
            temperature_unit_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, TemperatureUnitArray);

            CheckBox distance_unit_check = (CheckBox)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_distance_unit_check);
            Spinner distance_unit_spn = (Spinner)hmiSettingControlDataView.FindViewById(Resource.Id.hmi_setting_distance_unit_spn);
            distance_unit_check.CheckedChange += (sender, e) => distance_unit_spn.Enabled = e.IsChecked;
            String[] DistanceUnitArray = Enum.GetNames(typeof(MassageMode));
            distance_unit_spn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DistanceUnitArray);

            if (null != hmiSettingControlData)
            {
                if (null != hmiSettingControlData.getDisplayMode())
                {
                    display_mode_spn.SetSelection((int)hmiSettingControlData.getDisplayMode());
                }
                else
                {
                    display_mode_check.Checked = false;
                }
                if (null != hmiSettingControlData.getTemperatureUnit())
                {
                    temperature_unit_spn.SetSelection((int)hmiSettingControlData.getTemperatureUnit());
                }
                else
                {
                    temperature_unit_check.Checked = false;
                }
                if (null != hmiSettingControlData.getDistanceUnit())
                {
                    distance_unit_spn.SetSelection((int)hmiSettingControlData.getDistanceUnit());
                }
                else
                {
                    distance_unit_check.Checked = false;
                }
            }

            hmiSettingControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
            {
                hmiSettingControlDataAlertDialogue.Dispose();
            });

            hmiSettingControlDataAlertDialogue.SetPositiveButton(OK, (senderAlert, evn) =>
            {
                hmiSettingControlData = new HMISettingsControlData();
                if (display_mode_check.Checked)
                    hmiSettingControlData.displayMode = (DisplayMode)display_mode_spn.SelectedItemPosition;
                if (temperature_unit_check.Checked)
                    hmiSettingControlData.temperatureUnit = (TemperatureUnit)temperature_unit_spn.SelectedItemPosition;
                if (distance_unit_check.Checked)
                    hmiSettingControlData.distanceUnit = (DistanceUnit)distance_unit_spn.SelectedItemPosition;
            });

            hmiSettingControlDataAlertDialogue.Show();
        }

		private void CreateRCResponseSetInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.set_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCResponseSetInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_spn);

			CheckBox moduleIdCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_id_cb);
			EditText moduleIdEt = (EditText)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_id_edittext);

			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_btn);

            CheckBox addSeatControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_seat_control_data_chk);
            Button addSeatControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_seat_control_data_btn);

            CheckBox audioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_audio_control_data_chk);
            Button audioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_audio_control_data_btn);

            CheckBox lightControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_light_control_data_chk);
            Button lightControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_light_control_data_btn);

            CheckBox hmiSettingControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_hmi_setting_control_data_chk);
            Button hmiSettingControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_hmi_setting_control_data_btn);

			CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.set_interior_vehicle_data_result_code_cb);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.set_interior_vehicle_data_result_code_spn);

			moduleDataCb.CheckedChange += (s, e) =>
			{
				moduleTypeCb.Enabled = e.IsChecked;
                moduleTypeSpn.Enabled = e.IsChecked && moduleTypeCb.Checked;
				moduleIdCb.Enabled = e.IsChecked;
				moduleIdEt.Enabled = e.IsChecked;
				addRadioControlDataCb.Enabled = e.IsChecked;
                addRadioControlDataButton.Enabled = e.IsChecked && addRadioControlDataCb.Checked;
				addClimateControlDataCb.Enabled = e.IsChecked;
                addClimateControlDataButton.Enabled = e.IsChecked && addClimateControlDataCb.Checked;
                addSeatControlDataCb.Enabled = e.IsChecked;
                addSeatControlDataButton.Enabled = e.IsChecked && addSeatControlDataCb.Checked;
                audioControlDataCb.Enabled = e.IsChecked;
                audioControlDataButton.Enabled = e.IsChecked && audioControlDataCb.Checked;
                lightControlDataCb.Enabled = e.IsChecked;
                lightControlDataButton.Enabled = e.IsChecked && lightControlDataCb.Checked;
                hmiSettingControlDataCb.Enabled = e.IsChecked;
                hmiSettingControlDataButton.Enabled = e.IsChecked && hmiSettingControlDataCb.Checked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;
            addSeatControlDataCb.CheckedChange += (s, e) => addSeatControlDataButton.Enabled = e.IsChecked;
            audioControlDataCb.CheckedChange += (s, e) => audioControlDataButton.Enabled = e.IsChecked;
            lightControlDataCb.CheckedChange += (s, e) => lightControlDataButton.Enabled = e.IsChecked;
            hmiSettingControlDataCb.CheckedChange += (s, e) => hmiSettingControlDataButton.Enabled = e.IsChecked;
			resultCodeCb.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;
			moduleIdCb.CheckedChange += (s, e) => moduleIdEt.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);
			resultCodeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);

            radioControlData = null;
			climateControlData = null;
            seatControlData = null;
            audioControlData = null;
            lightControlData = null;
            hmiSettingControlData = null;

			HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData);
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					if (tmpObj.getModuleData().getModuleType() != null)
						moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
					else
						moduleTypeCb.Checked = false;
					if (tmpObj.getModuleData().getModuleId() != null)
						moduleIdEt.Text = tmpObj.getModuleData().getModuleId().ToString();
					else
					{
						moduleIdCb.Checked = false;
						moduleIdEt.Enabled = false;
					}

					radioControlData = tmpObj.getModuleData().getRadioControlData();
					climateControlData = tmpObj.getModuleData().getClimateControlData();
					seatControlData = tmpObj.getModuleData().getSeatControlData();
					audioControlData = tmpObj.getModuleData().getAudioControlData();
					lightControlData = tmpObj.getModuleData().getLightControlData();
					hmiSettingControlData = tmpObj.getModuleData().getHmiSettingsControlData();
				}
				else {
					moduleTypeSpn.Enabled = false;
					moduleTypeCb.Checked = false;
					moduleIdCb.Checked = false;
					moduleIdEt.Enabled = false;

				}
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
			}

			if (radioControlData == null)
            {
                addRadioControlDataCb.Checked = false;
                addRadioControlDataButton.Enabled = false;
            }

			if (climateControlData == null)
            {
                addClimateControlDataCb.Checked = false;
                addClimateControlDataButton.Enabled = false;
            }

            if (seatControlData == null)
            {
                addSeatControlDataCb.Checked = false;
                addSeatControlDataButton.Enabled = false;
            }

            if (audioControlData == null)
            {
                audioControlDataCb.Checked = false;
                audioControlDataButton.Enabled = false;
            }

            if (lightControlData == null)
            {
                lightControlDataCb.Checked = false;
                lightControlDataButton.Enabled = false;
            }

            if (hmiSettingControlData == null)
            {
                hmiSettingControlDataCb.Checked = false;
                hmiSettingControlDataButton.Enabled = false;
            }

            addRadioControlDataButton.Click += (object sender, EventArgs e) => RadioControlDataAlertDialogue();
            addClimateControlDataButton.Click += (object sender, EventArgs e) => ClimateControlDataAlertDialogue();
            addSeatControlDataButton.Click += (object sender, EventArgs e) => SeatControlDataAlertDialogue();
            audioControlDataButton.Click += (object sender, EventArgs e) => AudioControlDataAlertDialogue();
            lightControlDataButton.Click += (object sender, EventArgs e) => LightControlDataAlertDialogue();
            hmiSettingControlDataButton.Click += (object sender, EventArgs e) => HmiSettingControlDataAlertDialogue();

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, evn) =>
			{
                ModuleData moduleData = null;
				if (moduleDataCb.Checked)
				{
                    moduleData = new ModuleData();
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;

					if (moduleIdCb.Checked)
						moduleData.moduleId = moduleIdEt.Text.ToString();

					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;

                    if (addSeatControlDataCb.Checked)
                        moduleData.seatControlData = seatControlData;

                    if (audioControlDataCb.Checked)
                        moduleData.audioControlData = audioControlData;

                    if (lightControlDataCb.Checked)
                        moduleData.lightControlData = lightControlData;

                    if (hmiSettingControlDataCb.Checked)
                        moduleData.hmiSettingsControlData = hmiSettingControlData;
				}

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCb.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildRcSetInteriorVehicleDataResponse(BuildRpc.getNextId(), rsltCode, moduleData);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}
		private void CreateUIResponseCreateWindow()
		{
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.create_delete_window, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseCreateWindow);

			CheckBox checkBoxSuccess = (CheckBox)rpcView.FindViewById(Resource.Id.success_cb);
			Switch switchSuccess = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			checkBoxSuccess.CheckedChange += (s, e) => switchSuccess.Enabled = e.IsChecked;

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_code);
			CheckBox checkBoxInfo = (CheckBox)rpcView.FindViewById(Resource.Id.info_cb);
			EditText editTextInfo = (EditText)rpcView.FindViewById(Resource.Id.info_et);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow>(Activity, tmpObj.getMethod());
			if (tmpObj == null)
			{
				Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow);
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow)BuildDefaults.buildDefaultMessage(type, 0);
			}

			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				

				if (tmpObj.getSuccess() == null)
				{
					checkBoxSuccess.Checked = false;
					switchSuccess.Enabled = false;
				}
				else
				{
					checkBoxSuccess.Checked = true;
					switchSuccess.Checked = (bool)tmpObj.getSuccess();
				}

				if (tmpObj.getInfo() == null )
				{
					checkBoxInfo.Checked = false;
					editTextInfo.Enabled = false;
				}
				else
				{
					checkBoxInfo.Checked = true;
					editTextInfo.Text = tmpObj.getInfo();
				}
			}

			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxInfo.CheckedChange += (sender, e) => editTextInfo.Enabled = e.IsChecked;
			checkBoxSuccess.CheckedChange += (sender, e) => switchSuccess.Enabled = e.IsChecked;


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			checkBoxSuccess.Text = ("Success");
			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				bool? toggleState = null;
				
				if (checkBoxSuccess.Checked)
				{
					toggleState = switchSuccess.Checked;

				}
				String textInfo = null;
				if (checkBoxInfo.Checked)
				{
					textInfo = editTextInfo.Text;

				}

				RpcResponse rpcMessage = BuildRpc.buildUICreateWindowResponse(BuildRpc.getNextId(), selectedResultCode, toggleState, textInfo);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}
		private void CreateUIResponseDeleteWindow()
		{
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.create_delete_window, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseDeleteWindow);

			CheckBox checkBoxSuccess = (CheckBox)rpcView.FindViewById(Resource.Id.success_cb);
			Switch switchSuccess = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			checkBoxSuccess.CheckedChange += (s, e) => switchSuccess.Enabled = e.IsChecked;

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_code);
			CheckBox checkBoxInfo = (CheckBox)rpcView.FindViewById(Resource.Id.info_cb);
			EditText editTextInfo = (EditText)rpcView.FindViewById(Resource.Id.info_et);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow>(Activity, tmpObj.getMethod());
			if (tmpObj == null)
			{
				Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow);
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow)BuildDefaults.buildDefaultMessage(type, 0);
			}

			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());


				if (tmpObj.getSuccess() == null)
				{
					checkBoxSuccess.Checked = false;
					switchSuccess.Enabled = false;
				}
				else
				{
					checkBoxSuccess.Checked = true;
					switchSuccess.Checked = (bool)tmpObj.getSuccess();
				}

				if (tmpObj.getInfo() == null )
				{
					checkBoxInfo.Checked = false;
					editTextInfo.Enabled = false;
				}
				else
				{
					checkBoxInfo.Checked = true;
					editTextInfo.Text = tmpObj.getInfo();
				}
			}

			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxInfo.CheckedChange += (sender, e) => editTextInfo.Enabled = e.IsChecked;
			checkBoxSuccess.CheckedChange += (sender, e) => switchSuccess.Enabled = e.IsChecked;

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			checkBoxSuccess.Text = ("Success");
			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				bool? toggleState = null;

				if (checkBoxSuccess.Checked)
				{
					toggleState = switchSuccess.Checked;

				}
				String textInfo = null;
				if (checkBoxInfo.Checked)
				{
					textInfo = editTextInfo.Text;

				}

				RpcResponse rpcMessage = BuildRpc.buildUIDeleteWindowResponse(BuildRpc.getNextId(), selectedResultCode, toggleState, textInfo);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateRCResponseGetInteriorVehicleDataConsent()
		{
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.get_interior_vehicle_data_consent, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(RCResponseGetInteriorVehicleDataConsent);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAllow = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			checkBoxAllow.CheckedChange += (s, e) => switchAllow.Enabled = e.IsChecked;

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);
			CheckBox checkBoxInfo = (CheckBox)rpcView.FindViewById(Resource.Id.info_cb);
			EditText editTextInfo = (EditText)rpcView.FindViewById(Resource.Id.info_et);


			CheckBox checkBoxSuccess = (CheckBox)rpcView.FindViewById(Resource.Id.success);

			Switch switchSuccess = (Switch)rpcView.FindViewById(Resource.Id.toggle_button_success);
			checkBoxSuccess.CheckedChange += (s, e) => switchSuccess.Enabled = e.IsChecked;
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent);
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)BuildDefaults.buildDefaultMessage(type, 0);
            }
           
			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getAllowed() == null)
				{
					checkBoxAllow.Checked = false;
					switchAllow.Enabled = false;
				}
				else
				{
					checkBoxAllow.Checked = true;
					switchAllow.Checked = (bool)tmpObj.getAllowed();
				}

				if (tmpObj.getSuccess() == null)
				{
					checkBoxSuccess.Checked = false;
					switchSuccess.Enabled = false;
				}
				else
				{
					checkBoxSuccess.Checked = true;
					switchSuccess.Checked = (bool)tmpObj.getSuccess();
				}

				if (tmpObj.getInfo() == null)
				{
					checkBoxInfo.Checked = false;
					editTextInfo.Enabled = false;
				}
				else
				{
					checkBoxInfo.Checked = true;
					editTextInfo.Text = tmpObj.getInfo();
				}
			}

			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxInfo.CheckedChange += (sender, e) => editTextInfo.Enabled = e.IsChecked;

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			checkBoxAllow.Text = ("Allowed");
			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				bool? toggleState = null;
				if (checkBoxAllow.Checked)
				{
					toggleState = switchAllow.Checked;

				}
				bool? toggleStateSuccess = null;
				if (checkBoxSuccess.Checked)
				{
					toggleStateSuccess = switchSuccess.Checked;

				}
				String textInfo = null;
				if (checkBoxInfo.Checked)
				{
					textInfo = editTextInfo.Text;

				}

				RpcResponse rpcMessage = BuildRpc.buildRcGetInteriorVehicleDataConsentResponse(BuildRpc.getNextId(), selectedResultCode, toggleState, toggleStateSuccess, textInfo);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}
		private void CreateRCResponseReleaseInteriorVehicleDataConsent()
		{
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.releaseinteriorvehicledatamodule, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(RCResponseReleaseInteriorVehicleDataConsent);

			CheckBox checkBoxInfo= (CheckBox)rpcView.FindViewById(Resource.Id.release_interior_vehicle_data_consent_info_cb);
			EditText editTextInfo = (EditText)rpcView.FindViewById(Resource.Id.release_interior_vehicle_data_consent_info_edittext);


			CheckBox checkBoxSuccess = (CheckBox)rpcView.FindViewById(Resource.Id.success);

			Switch switchSuccess = (Switch)rpcView.FindViewById(Resource.Id.toggle_button_success);
			checkBoxSuccess.CheckedChange += (s, e) => switchSuccess.Enabled = e.IsChecked;

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent>(Activity, tmpObj.getMethod());
			if (tmpObj == null)
			{
				Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent);
				tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent)BuildDefaults.buildDefaultMessage(type, 0);
			}

			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getSuccess() == null)
				{
					checkBoxSuccess.Checked = false;
					switchSuccess.Enabled = false;
				}
				else
				{
					checkBoxSuccess.Checked = true;
					switchSuccess.Checked = (bool)tmpObj.getSuccess();
				}

				if (tmpObj.getInfo() == null)
				{
					checkBoxInfo.Checked = false;
					editTextInfo.Enabled = false;
				}
				else
				{
					checkBoxInfo.Checked = true;
					editTextInfo.Text = tmpObj.getInfo();
				}
			}

			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxInfo.CheckedChange += (sender, e) => editTextInfo.Enabled = e.IsChecked;

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			//checkBoxSuccess.Text = ("Success");
			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				bool? toggleState = null;
				if (checkBoxSuccess.Checked)
				{
					toggleState = switchSuccess.Checked;

				}
				String textInfo=null;
				if (checkBoxInfo.Checked)
				{
					textInfo = editTextInfo.Text;

				}
				RpcResponse rpcMessage = BuildRpc.buildRcReleaseInteriorVehicleDataConsentResponse(BuildRpc.getNextId(), selectedResultCode, toggleState, textInfo);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateRCResponseGetInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.get_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCResponseGetInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_type_spn);

			CheckBox moduleIdCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_id_cb);
			EditText moduleIdEt = (EditText)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_id_edittext);


			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_climate_control_data_btn);

            CheckBox addSeatControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_seat_control_data_chk);
            Button addSeatControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_seat_control_data_btn);

            CheckBox addAudioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_audio_control_data_chk);
            Button addAudioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_audio_control_data_btn);

            CheckBox addLightControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_light_control_data_chk);
            Button addLightControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_light_control_data_btn);

            CheckBox addHmiSettingControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_hmi_setting_control_data_chk);
            Button addHmiSettingControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_hmi_setting_control_data_btn);

			CheckBox isSubscribedCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_is_subscribed_cb);
			Switch isSubscribed_tgl = (Switch)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_is_subscribed_tgl);

			CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_result_code_cb);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_result_code_spn);

			moduleDataCb.CheckedChange += (s, e) =>
			{
				moduleTypeCb.Enabled = e.IsChecked;
				addRadioControlDataCb.Enabled = e.IsChecked;
				addClimateControlDataCb.Enabled = e.IsChecked;
                addSeatControlDataCb.Enabled = e.IsChecked;
                addAudioControlDataCb.Enabled = e.IsChecked;
                addLightControlDataCb.Enabled = e.IsChecked;
                addHmiSettingControlDataCb.Enabled = e.IsChecked;
				moduleIdCb.Enabled = e.IsChecked;
				moduleIdEt.Enabled = e.IsChecked;
				addRadioControlDataButton.Enabled = e.IsChecked;
                addClimateControlDataButton.Enabled = e.IsChecked;
                addSeatControlDataButton.Enabled = e.IsChecked;
                addAudioControlDataButton.Enabled = e.IsChecked;
                addLightControlDataButton.Enabled = e.IsChecked;
                addHmiSettingControlDataButton.Enabled = e.IsChecked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;
            addSeatControlDataCb.CheckedChange += (s, e) => addSeatControlDataButton.Enabled = e.IsChecked;
            addAudioControlDataCb.CheckedChange += (s, e) => addAudioControlDataButton.Enabled = e.IsChecked;
            addLightControlDataCb.CheckedChange += (s, e) => addLightControlDataButton.Enabled = e.IsChecked;
            addHmiSettingControlDataCb.CheckedChange += (s, e) => addHmiSettingControlDataButton.Enabled = e.IsChecked;
			resultCodeCb.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;
			isSubscribedCb.CheckedChange += (s, e) => isSubscribed_tgl.Enabled = e.IsChecked;
			moduleIdCb.CheckedChange += (s, e) => moduleIdEt.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);
			resultCodeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);

			radioControlData = null;
			climateControlData = null;
            seatControlData = null;
            audioControlData = null;
            lightControlData = null;
            hmiSettingControlData = null;

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(Activity, tmpObj.getMethod());

            if (tmpObj == null) //lets get the default values
            {
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData);
                tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					if (tmpObj.getModuleData().getModuleType() != null)
						moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
					else
						moduleTypeCb.Checked = false;

					if (tmpObj.getModuleData().getModuleId() != null)
						moduleIdEt.Text = tmpObj.getModuleData().getModuleId().ToString();
					else
						moduleIdCb.Checked = false;

					radioControlData = tmpObj.getModuleData().getRadioControlData();
					climateControlData = tmpObj.getModuleData().getClimateControlData();
                    seatControlData = tmpObj.getModuleData().getSeatControlData();
                    audioControlData = tmpObj.getModuleData().getAudioControlData();
                    lightControlData = tmpObj.getModuleData().getLightControlData();
                    hmiSettingControlData = tmpObj.getModuleData().getHmiSettingsControlData();
				}

				if (tmpObj.getIsSubscribed() == null)

				{
					isSubscribedCb.Checked = false;
					isSubscribed_tgl.Enabled = false;
				}
				else
				{
					isSubscribedCb.Checked = true;
					isSubscribed_tgl.Checked = (bool)tmpObj.getIsSubscribed();
				}
				

				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
			}

			if (radioControlData == null)
            {
				addRadioControlDataCb.Checked = false;
                addRadioControlDataButton.Enabled = false;
            }
			
			if (climateControlData == null)
            {
                addClimateControlDataCb.Checked = false;
                addClimateControlDataButton.Enabled = false;
            }

            if (seatControlData == null)
            {
                addSeatControlDataCb.Checked = false;
                addSeatControlDataButton.Enabled = false;
            }

            if (audioControlData == null)
            {
                addAudioControlDataCb.Checked = false;
                addAudioControlDataButton.Enabled = false;
            }

            if (lightControlData == null)
            {
                addLightControlDataCb.Checked = false;
                addLightControlDataButton.Enabled = false;
            }

            if (hmiSettingControlData == null)
            {
                addHmiSettingControlDataCb.Checked = false;
                addHmiSettingControlDataButton.Enabled = false;
            }

            addRadioControlDataButton.Click += (object sender, EventArgs e) => RadioControlDataAlertDialogue();
            addClimateControlDataButton.Click += (object sender, EventArgs e) => ClimateControlDataAlertDialogue();
            addSeatControlDataButton.Click += (object sender, EventArgs e) => SeatControlDataAlertDialogue();
            addAudioControlDataButton.Click += (object sender, EventArgs e) => AudioControlDataAlertDialogue();
            addLightControlDataButton.Click += (object sender, EventArgs e) => LightControlDataAlertDialogue();
            addHmiSettingControlDataButton.Click += (object sender, EventArgs e) => HmiSettingControlDataAlertDialogue();

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, evn) =>
			{
                ModuleData moduleData = new ModuleData();
				if (moduleDataCb.Checked)
				{
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;
					if (moduleIdCb.Checked)
						moduleData.moduleId= moduleIdEt.Text.ToString();


					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;

                    if (addSeatControlDataCb.Checked)
                        moduleData.seatControlData = seatControlData;

                    if (addAudioControlDataCb.Checked)
                        moduleData.audioControlData = audioControlData;

                    if (addLightControlDataCb.Checked)
                        moduleData.lightControlData = lightControlData;

                    if (addHmiSettingControlDataCb.Checked)
                        moduleData.hmiSettingsControlData = hmiSettingControlData;
				}

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCb.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
				}
				bool? toggleState = null;
				if (isSubscribedCb.Checked)
				{
					toggleState = isSubscribed_tgl.Checked;

				}
				RpcResponse rpcResponse = BuildRpc.buildRcGetInteriorVehicleDataResponse(BuildRpc.getNextId(), rsltCode, moduleData, toggleState);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationStopped()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			rpcAlertDialog.SetTitle(VRNotificationStopped);

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
			    AppInstanceManager.Instance.sendRpc(BuildRpc.buildVRStoppedNotification());
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{

			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationStarted()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			rpcAlertDialog.SetTitle(VRNotificationStarted);

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
			    AppInstanceManager.Instance.sendRpc(BuildRpc.buildVRStartedNotification());
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{

			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationOnLanguageChange()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRNotificationOnLanguageChange);

			CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			languageCheckBox.Text = "Language";
			Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;
            string[] language = Enum.GetNames(typeof(Language));
			var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
			spnLanguage.Adapter = languageAdapter;

            HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange();
            tmpObj = (HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getLanguage() != null)
                {
                    languageCheckBox.Checked = true;
                    spnLanguage.SetSelection((int)tmpObj.getLanguage());
                }
                else
                {
                    languageCheckBox.Checked = false;
                    spnLanguage.Enabled = false;
                }
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                Language? lang = null;
                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                RequestNotifyMessage rpcMessage = BuildRpc.buildVROnLanguageChange(lang);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
			   if (tmpObj != null)
			   {
			       AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
			   }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationOnCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRNotificationOnCommand);

            CheckBox cmdIdCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
            Spinner cmdIdSpinner = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);

			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);

            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            List<int?> cmdIDList = new List<int?>();

            List<int> appIds = new List<int>();
            foreach (AppItem item in AppInstanceManager.appList)
            {
                appIds.Add(item.getAppID());
            }

            int[] appIDArray = appIds.ToArray();

            for (int i = 0; i < AppInstanceManager.commandIdList.Count; i++)
            {
                List<int?> commandIdList = AppInstanceManager.commandIdList[appIDArray[i]];
                cmdIDList.AddRange(commandIdList);
            }

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var cmdIdAdapter = new ArrayAdapter<int?>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, cmdIDList);
			cmdIdSpinner.Adapter = cmdIdAdapter;


            cmdIdCheck.CheckedChange += (sender, e) => cmdIdSpinner.Enabled = e.IsChecked;
            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };			

            HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand tmpObj = new HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand();
            tmpObj = (HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getAppId() != null)
                {
                    appIDCheckBox.Checked = true;
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
                else
                    appIDCheckBox.Checked = false;

                if (tmpObj.getCmdID() != null)
                {
                    cmdIdCheck.Checked = true;
                    cmdIdSpinner.SetSelection((int)tmpObj.getCmdID());
                }
                else 
                    cmdIdSpinner.Enabled = false;
            }
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = appIds[registerdAppIdSpn.SelectedItemPosition];

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 int? commandId = null;

                 if (cmdIdCheck.Checked)
                     commandId = (int)cmdIdSpinner.SelectedItem;

                 RequestNotifyMessage rpcMessage = BuildRpc.buildVROnCommand(commandId, selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponsePerformInteraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.vr_perform_interaction_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRResponsePerformInteraction);

			CheckBox checkBoxChoiceID = (CheckBox)rpcView.FindViewById(Resource.Id.vr_choice_id_chk);
			EditText editTextChoiceID = (EditText)rpcView.FindViewById(Resource.Id.vr_choice_id_et);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.vr_result_code_chk);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.vr_perform_interaction_result_code_spn);

            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = resultCodeAdapter;

			checkBoxChoiceID.CheckedChange += (sender, e) => editTextChoiceID.Enabled = e.IsChecked;
			resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());

                if(tmpObj.getChoiceID() != null)
                {
                    checkBoxChoiceID.Checked = true;
                    editTextChoiceID.Text = tmpObj.getChoiceID().ToString();
                }
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
				 rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				int? choiceId = null;

				if (checkBoxChoiceID.Checked)
                    if(editTextChoiceID.Text != null && editTextChoiceID.Text.Length > 0)
				        choiceId = Java.Lang.Integer.ParseInt(editTextChoiceID.Text);

				RpcResponse rpcResponse = BuildRpc.buildVrPerformInteractionResponse(BuildRpc.getNextId(), choiceId, rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseDeleteCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseDeleteCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
                resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = null;
			   rpcResponse = BuildRpc.buildVrDeleteCommandResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseChangeRegistration()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseChangeRegistration);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
                resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = BuildRpc.buildVrChangeRegistrationResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseAddCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseAddCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

			HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand);
                tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
                resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
            });

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = null;
			   rpcResponse = BuildRpc.buildVrAddCommandResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseAddCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseAddCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (sender, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcMessage rpcMessage = BuildRpc.buildUiAddCommandResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseAddSubMenu()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseAddSubMenu);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcMessage rpcMessage = BuildRpc.buildUiAddSubMenuResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

        private void CreateUIResponseShowAppMenu()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseShowAppMenu);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcMessage rpcMessage = BuildRpc.buildUiShowAppMenuResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateUIResponseCloseApplication()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseCloseApplication);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcMessage rpcMessage = BuildRpc.buildUiCloseApplicationResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateUIResponseAlert()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseAlert);

			CheckBox tryAgainTimeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);
			EditText tryAgainTimeEditText = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);

			tryAgainTimeEditText.Text = "0";

			var appExitReasonAdapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = appExitReasonAdapter;

            tryAgainTimeCheckBox.CheckedChange += (s, e) => tryAgainTimeEditText.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.Alert tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Alert();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Alert>(Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Alert);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getTryAgainTime() != null)
					tryAgainTimeEditText.Text = tmpObj.getTryAgainTime().ToString();
				else
					tryAgainTimeCheckBox.Checked = false;
			}

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                int? tryAgainTime = null;

                if (tryAgainTimeCheckBox.Checked && tryAgainTimeEditText.Text != null && tryAgainTimeEditText.Text.Length > 0)
                    tryAgainTime =Java.Lang.Integer.ParseInt(tryAgainTimeEditText.Text);

                if (resultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiAlertResponse(BuildRpc.getNextId(), rsltCode, tryAgainTime);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
			rpcAlertDialog.Show();
		}

        private void CreateUIResponseChangeRegistration()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(UIResponseChangeRegistration);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
           {
               HmiApiLib.Common.Enums.Result? resltCode = null;
               if (resultCodeCheckbox.Checked)
               {
                   resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
               }

               RpcMessage rpcMessage = BuildRpc.buildUiChangeRegistrationResponse(BuildRpc.getNextId(), resltCode);
               AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
           });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateUIResponseClosePopUp()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseClosePopUp);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiClosePopUpResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseDeleteCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseDeleteCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiDeleteCommandResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseDeleteSubMenu()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseDeleteSubMenu);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) =>spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiDeleteSubMenuResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseEndAudioPassThru()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseEndAudioPassThru);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiEndAudioPassThruResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponsePerformAudioPassThru()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponsePerformAudioPassThru);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiPerformAudioPassThruResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponsePerformInteraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.perform_interaction_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponsePerformInteraction);

            CheckBox choiceIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.choice_id_cb);
			EditText choiceIDEditText = (EditText)rpcView.FindViewById(Resource.Id.choice_id_et);

			CheckBox manualTextEntryCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.manual_text_entry_cb);
			EditText manualTextEntryEditText = (EditText)rpcView.FindViewById(Resource.Id.manual_text_entry_et);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.perform_interaction_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = resultCodeAdapter;

            choiceIDCheckBox.CheckedChange += (s, e) => choiceIDEditText.Enabled = e.IsChecked;
            manualTextEntryCheckBox.CheckedChange += (s, e) => manualTextEntryEditText.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getChoiceID() != null)
					choiceIDEditText.Text = tmpObj.getChoiceID().ToString();
				else
					choiceIDCheckBox.Checked = false;

				if (tmpObj.getManualTextEntry() != null)
					manualTextEntryEditText.Text = tmpObj.getManualTextEntry();
				else
					manualTextEntryCheckBox.Checked = false;

			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                int? choiceID = null;
                String manualTextEntry = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (choiceIDCheckBox.Checked && manualTextEntryEditText.Text != null && choiceIDEditText.Text.Length > 0)
                    choiceID = Java.Lang.Integer.ParseInt(choiceIDEditText.Text);

                if (manualTextEntryCheckBox.Checked)
                    manualTextEntry = manualTextEntryEditText.Text;

                if (resultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
                
                RpcResponse rpcMessage = BuildRpc.buildUiPerformInteractionResponse(BuildRpc.getNextId(), choiceID, manualTextEntry, rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseScrollableMessage()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseScrollableMessage);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcMessage rpcMessage = null;
				rpcMessage = BuildRpc.buildUiScrollableMessageResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetAppIcon()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetAppIcon);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetAppIconResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetDisplayLayout()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.set_display_layout, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseSetDisplayLayout);

			DisplayCapabilities dspCap = null;
            CheckBox displayCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_add_display_capabilities_chk);
			Button addDisplayCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_display_capabilities_btn);

            displayCapabilitiesCheckBox.CheckedChange += (sen, evn) => addDisplayCapabilitiesButton.Enabled = evn.IsChecked;
			

			HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)BuildDefaults.buildDefaultMessage(type, 0);
            }
			if (tmpObj != null)
			{
				dspCap = tmpObj.getDisplayCapabilities();
				 if(tmpObj.getDisplayCapabilities()==null)
				 displayCapabilitiesCheckBox.Checked = false;
			}
			

			if (dspCap == null)
			{
				dspCap = new DisplayCapabilities();
			}
            addDisplayCapabilitiesButton.Click += delegate
         {
             Android.Support.V7.App.AlertDialog.Builder displayCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
             View displayCapabilitiesView = layoutInflater.Inflate(Resource.Layout.display_capabilities, null);
             displayCapabilitiesAlertDialog.SetView(displayCapabilitiesView);
             displayCapabilitiesAlertDialog.SetTitle("Display Capabilities");

             CheckBox checkBoxDisplayType = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_type_checkbox);
             Spinner spnButtonDisplayType = (Spinner)displayCapabilitiesView.FindViewById(Resource.Id.display_type_spinner);

             string[] displayType = Enum.GetNames(typeof(DisplayType));
             var displayTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, displayType);
             spnButtonDisplayType.Adapter = displayTypeAdapter;
			 if (dspCap.getDisplayType() != null)
				 spnButtonDisplayType.SetSelection((int)dspCap.getDisplayType());
			 else
				 checkBoxDisplayType.Checked = false;


			 List<TextField> textFieldsList = new List<TextField>();
             ListView textFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_text_fields_listview);
             Button textFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_btn);
             CheckBox addTextFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_chk);
			 if (dspCap.getTextFields() != null)
			 {
				 textFieldsList.AddRange(dspCap.getTextFields());
			 }
			 else
				 addTextFieldsChk.Checked = false;

			 var textFieldAdapter = new TextFieldAdapter(Activity, textFieldsList,ViewStates.Invisible, textFieldsListView);
             textFieldsListView.Adapter = textFieldAdapter;
             Utility.setListViewHeightBasedOnChildren(textFieldsListView);

             List<ImageField> imageFieldsList = new List<ImageField>();
             ListView imageFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_fields_listview);
             Button imageFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_btn);
             CheckBox imageFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_chk);
			 if (dspCap.getImageFields() != null)
			 {
				 imageFieldsList.AddRange(dspCap.getImageFields());
			 }
			 else
				 imageFieldsChk.Checked = false;
				var imageFieldAdapter = new ImageFieldAdapter(Activity, imageFieldsList, ViewStates.Invisible, imageFieldsListView);
             imageFieldsListView.Adapter = imageFieldAdapter;
             Utility.setListViewHeightBasedOnChildren(imageFieldsListView);

             List<MediaClockFormat> mediaClockFormatsList = new List<MediaClockFormat>();
             ListView mediaClockFormatsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_media_clock_formats_listview);
             Button mediaClockFormatsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_btn);
             CheckBox mediaClockFormatsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_chk);
			 if (dspCap.getMediaClockFormats() != null)
			 {
				 mediaClockFormatsList.AddRange(dspCap.getMediaClockFormats());
			 }
			 else
				 mediaClockFormatsChk.Checked = false;
			 var mediaClockFormatAdapter = new MediaClockFormatAdapter(Activity, mediaClockFormatsList);
             mediaClockFormatsListView.Adapter = mediaClockFormatAdapter;
             Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);

             List<ImageType> imageTypeList = new List<ImageType>();
             ListView imageTypeListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_types_listview);
             CheckBox imageTypeCheckbox = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_chk);
             Button addImageTypeButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_btn);
			 if (dspCap.getImageCapabilities() != null)
			 {
				 imageTypeList.AddRange(dspCap.getImageCapabilities());
			 }
			 else
				 imageTypeCheckbox.Checked = false;
			 var imageTypeAdapter = new ImageTypeAdapter(Activity, imageTypeList);
             imageTypeListView.Adapter = imageTypeAdapter;
             Utility.setListViewHeightBasedOnChildren(imageTypeListView);

             CheckBox screenParamsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_chk);
             Button screenParamsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_btn);

             CheckBox checkBoxGraphicSupported = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_graphic_supported_checkbox);
			 if (null != dspCap.getGraphicSupported())
				 checkBoxGraphicSupported.Checked = (bool)dspCap.getGraphicSupported();
			 else
				 checkBoxGraphicSupported.Checked = false;
			 CheckBox checkBoxTemplatesAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_checkbox);
             EditText editTextTemplatesAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_edittext);
			 if (dspCap.getTemplatesAvailable() != null)
			 {
				 String availableTemplates = null;
				 for (int i = 0; i < dspCap.getTemplatesAvailable().Count; i++)
				 {
					 if (i == 0)
					 {
						 availableTemplates = dspCap.getTemplatesAvailable()[i];
					 }
					 else
					 {
						 availableTemplates += availableTemplates + "," + dspCap.getTemplatesAvailable()[i];
					 }
				 }
				 editTextTemplatesAvailable.Text = availableTemplates;
			 }
			 else
				 checkBoxTemplatesAvailable.Checked = false;


				 checkBoxTemplatesAvailable.CheckedChange += (s, e) => editTextTemplatesAvailable.Enabled = e.IsChecked;
             screenParamsChk.CheckedChange += (s, e) => screenParamsButton.Enabled = e.IsChecked;
             checkBoxDisplayType.CheckedChange += (s, e) => spnButtonDisplayType.Enabled = e.IsChecked;
             addTextFieldsChk.CheckedChange += (s, e) =>
             {
                 textFieldsButton.Enabled = e.IsChecked;
                 textFieldsListView.Enabled = e.IsChecked;
             };
             imageFieldsChk.CheckedChange += (s, e) =>
             {
                 imageFieldsButton.Enabled = e.IsChecked;
                 imageFieldsListView.Enabled = e.IsChecked;
             };
             mediaClockFormatsChk.CheckedChange += (sender, e) =>
             {
                 mediaClockFormatsButton.Enabled = e.IsChecked;
                 mediaClockFormatsListView.Enabled = e.IsChecked;
             };

			 imageTypeCheckbox.CheckedChange += (sender, e) =>
			  {
				  addImageTypeButton.Enabled = e.IsChecked;
				  imageTypeListView.Enabled = e.IsChecked;
			  };


             textFieldsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder textFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View textFieldsView = layoutInflater.Inflate(Resource.Layout.text_field, null);
                 textFieldsAlertDialog.SetView(textFieldsView);
                 textFieldsAlertDialog.SetTitle("TextField");

                 CheckBox checkBoxName = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_name_checkbox);
                 Spinner spnName = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_name_spinner);

                 string[] textFieldNames = Enum.GetNames(typeof(TextFieldName));
                 var textFieldNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, textFieldNames);
                 spnName.Adapter = textFieldNamesAdapter;

                 CheckBox checkBoxCharacterSet = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_checkbox);
                 Spinner spnCharacterSet = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_spinner);

                 string[] characterSet = Enum.GetNames(typeof(CharacterSet));
                 var characterSetAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, characterSet);
                 spnCharacterSet.Adapter = characterSetAdapter;

                 CheckBox checkBoxWidth = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_width_checkbox);
                 EditText editTextWidth = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_width_edittext);

                 CheckBox checkBoxRow = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_row_checkbox);
                 EditText editTextRow = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_row_edittext);

                 checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                 checkBoxCharacterSet.CheckedChange += (s, e) => spnCharacterSet.Enabled = e.IsChecked;
                 checkBoxWidth.CheckedChange += (s, e) => editTextWidth.Enabled = e.IsChecked;
                 checkBoxRow.CheckedChange += (s, e) => editTextRow.Enabled = e.IsChecked;

                 textFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     TextField txtField = new TextField();

                     if (checkBoxName.Checked)
                     {
                         txtField.name = (TextFieldName)spnName.SelectedItemPosition;
                     }
                     if (checkBoxCharacterSet.Checked)
                     {
                         txtField.characterSet = (CharacterSet)spnCharacterSet.SelectedItemPosition;
                     }
                     if (checkBoxWidth.Checked)
                     {
                         if (editTextWidth.Text.Equals(""))
                             txtField.width = 0;
                         else
                             txtField.width = Java.Lang.Integer.ParseInt(editTextWidth.Text);
                     }
                     if (checkBoxRow.Checked)
                     {
                         if (editTextRow.Text.Equals(""))
                             txtField.rows = 0;
                         else
                             txtField.rows = Java.Lang.Integer.ParseInt(editTextRow.Text);
                     }
                     textFieldsList.Add(txtField);
                     textFieldAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(textFieldsListView);
                 });

                 textFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     textFieldsAlertDialog.Dispose();
                 });
                 textFieldsAlertDialog.Show();
             };

             imageFieldsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder imageFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View imageFieldsView = layoutInflater.Inflate(Resource.Layout.image_field, null);
                 imageFieldsAlertDialog.SetView(imageFieldsView);
                 imageFieldsAlertDialog.SetTitle("ImageField");

                 CheckBox checkBoxName = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_name_checkbox);
                 Spinner spnName = (Spinner)imageFieldsView.FindViewById(Resource.Id.image_field_name_spinner);

                 string[] imageFieldNames = Enum.GetNames(typeof(ImageFieldName));
                 var imageFieldNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageFieldNames);
                 spnName.Adapter = imageFieldNamesAdapter;

                 List<FileType> fileTypeList = new List<FileType>();
                 ListView fileTypeListView = (ListView)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_listview);
                 var fileTypeAdapter = new FileTypeAdapter(Activity, fileTypeList);
                 fileTypeListView.Adapter = fileTypeAdapter;

                 string[] fileTypes = Enum.GetNames(typeof(FileType));
                 bool[] fileTypeBoolArray = new bool[fileTypes.Length];

                 CheckBox fileTypeChk = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_chk);
                 Button fileTypeButton = (Button)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_btn);

                 checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                 fileTypeChk.CheckedChange += (sender, e) => fileTypeButton.Enabled = e.IsChecked;

                 fileTypeButton.Click += (sender, e1) =>
                 {
                     Android.Support.V7.App.AlertDialog.Builder fileTypeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                     fileTypeAlertDialog.SetTitle("FileType");

                     fileTypeAlertDialog.SetMultiChoiceItems(fileTypes, fileTypeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => fileTypeBoolArray[e.Which] = e.IsChecked);

                     fileTypeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                     {
                         fileTypeAlertDialog.Dispose();
                     });

                     fileTypeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                     {
                         for (int i = 0; i < fileTypes.Length; i++)
                         {
                             if (fileTypeBoolArray[i])
                             {
                                 fileTypeList.Add(((FileType)typeof(FileType).GetEnumValues().GetValue(i)));
                             }
                         }
                         fileTypeAdapter.NotifyDataSetChanged();
                         Utility.setListViewHeightBasedOnChildren(fileTypeListView);
                     });
                     fileTypeAlertDialog.Show();
                 };

                 CheckBox checkBoxImageResolution = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_resolution_checkbox);

                 CheckBox checkBoxResolutionWidth = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_checkbox);
                 EditText editTextResolutionWidth = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_edit_text);

                 CheckBox checkBoxResolutionHeight = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_checkbox);
                 EditText editTextResolutionHeight = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_edit_text);

                 checkBoxImageResolution.CheckedChange += (sender, e) =>
                 {
                     checkBoxResolutionWidth.Enabled = e.IsChecked;
                     editTextResolutionWidth.Enabled = e.IsChecked;
                     checkBoxResolutionHeight.Enabled = e.IsChecked;
                     editTextResolutionHeight.Enabled = e.IsChecked;
                 };
                 checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                 checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                 imageFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     ImageField imgField = new ImageField();

                     if (checkBoxName.Checked)
                     {
                         imgField.name = (ImageFieldName)spnName.SelectedItemPosition;
                     }
                     if (fileTypeChk.Checked)
                     {
                         imgField.imageTypeSupported = fileTypeList;
                     }

                     ImageResolution resolution = null;
                     if (checkBoxImageResolution.Checked)
                     {
                         resolution = new ImageResolution();

                         if (checkBoxResolutionWidth.Checked)
                         {
                             if (editTextResolutionWidth.Text.Equals(""))
                                 resolution.resolutionWidth = 0;
                             else
                                 resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                         }
                         if (checkBoxResolutionHeight.Checked)
                         {
                             if (editTextResolutionHeight.Text.Equals(""))
                                 resolution.resolutionHeight = 0;
                             else
                                 resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                         }
                     }
                     imgField.imageResolution = resolution;

                     imageFieldsList.Add(imgField);
                     imageFieldAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(imageFieldsListView);
                 });

                 imageFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     imageFieldsAlertDialog.Dispose();
                 });
                 imageFieldsAlertDialog.Show();
             };

             string[] mediaClockFormats = Enum.GetNames(typeof(MediaClockFormat));
             bool[] mediaClockFormatsBoolArray = new bool[mediaClockFormats.Length];

             mediaClockFormatsButton.Click += (sender, e1) =>
             {
                 Android.Support.V7.App.AlertDialog.Builder mediaClockFormatsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                 mediaClockFormatsAlertDialog.SetTitle("MediaClockFormats");

                 mediaClockFormatsAlertDialog.SetMultiChoiceItems(mediaClockFormats, mediaClockFormatsBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => mediaClockFormatsBoolArray[e.Which] = e.IsChecked);

                 mediaClockFormatsAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                 {
                     mediaClockFormatsAlertDialog.Dispose();
                 });

                 mediaClockFormatsAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                 {
                     for (int i = 0; i < mediaClockFormats.Length; i++)
                     {
                         if (mediaClockFormatsBoolArray[i])
                         {
                             mediaClockFormatsList.Add(((MediaClockFormat)typeof(MediaClockFormat).GetEnumValues().GetValue(i)));
                         }
                     }
                     mediaClockFormatAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);
                 });

                 mediaClockFormatsAlertDialog.Show();
             };

             string[] imageCapabilities = Enum.GetNames(typeof(ImageType));
             bool[] imageCapabilitiesBoolArray = new bool[imageCapabilities.Length];

             addImageTypeButton.Click += (sender, e1) =>
             {
                 Android.Support.V7.App.AlertDialog.Builder imageCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                 imageCapabilitiesAlertDialog.SetTitle("ImageCapabilities");

                 imageCapabilitiesAlertDialog.SetMultiChoiceItems(imageCapabilities, imageCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => imageCapabilitiesBoolArray[e.Which] = e.IsChecked);

                 imageCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                 {
                     imageCapabilitiesAlertDialog.Dispose();
                 });

                 imageCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                 {
                     for (int i = 0; i < imageCapabilities.Length; i++)
                     {
                         if (imageCapabilitiesBoolArray[i])
                         {
                             imageTypeList.Add(((ImageType)typeof(ImageType).GetEnumValues().GetValue(i)));
                         }
                     }
                     imageTypeAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(imageTypeListView);
                 });

                 imageCapabilitiesAlertDialog.Show();
             };

             // fetch here from preferences
             ScreenParams scrnParam = new ScreenParams();
             screenParamsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder screenParamsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View screenParamsView = layoutInflater.Inflate(Resource.Layout.screen_param, null);
                 screenParamsAlertDialog.SetView(screenParamsView);
                 screenParamsAlertDialog.SetTitle("ScreenParams");

                 CheckBox checkBoxImageResolution = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_image_resolution_checkbox);
                 CheckBox checkBoxResolutionWidth = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_checkbox);
                 EditText editTextResolutionWidth = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_edit_text);

                 CheckBox checkBoxResolutionHeight = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_checkbox);
                 EditText editTextResolutionHeight = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_edit_text);

                 CheckBox checkTouchEventCapabilities = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_touch_event_capability_checkbox);
                 CheckBox checkBoxpressAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_press_available_checkbox);
                 CheckBox checkBoxMultiTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_multi_touch_available_checkbox);
                 CheckBox checkBoxDoubleTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_double_press_available_checkbox);

                 checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                 checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                 checkBoxImageResolution.CheckedChange += (sender, e) =>
                 {
                     checkBoxResolutionWidth.Enabled = e.IsChecked;
                     editTextResolutionWidth.Enabled = e.IsChecked;
                     checkBoxResolutionHeight.Enabled = e.IsChecked;
                     editTextResolutionHeight.Enabled = e.IsChecked;
                 };

                 checkTouchEventCapabilities.CheckedChange += (sender, e) =>
                 {
                     checkBoxpressAvailable.Enabled = e.IsChecked;
                     checkBoxMultiTouchAvailable.Enabled = e.IsChecked;
                     checkBoxDoubleTouchAvailable.Enabled = e.IsChecked;
                 };

                 if (scrnParam.getResolution() != null)
                 {
                     checkBoxImageResolution.Checked = true;
                     editTextResolutionWidth.Text = scrnParam.getResolution().getResolutionWidth().ToString();
                     editTextResolutionHeight.Text = scrnParam.getResolution().getResolutionHeight().ToString();
                 }
                 if (scrnParam.getTouchEventAvailable() != null)
                 {
                     checkTouchEventCapabilities.Checked = true;
                     if (null != scrnParam.getTouchEventAvailable().getPressAvailable())
                         checkBoxpressAvailable.Checked = (bool)scrnParam.getTouchEventAvailable().getPressAvailable();
                     if (null != scrnParam.getTouchEventAvailable().getMultiTouchAvailable())
                         checkBoxMultiTouchAvailable.Checked = (bool)scrnParam.getTouchEventAvailable().getMultiTouchAvailable();
                     if (null != scrnParam.getTouchEventAvailable().getDoublePressAvailable())
                         checkBoxDoubleTouchAvailable.Checked = (bool)scrnParam.getTouchEventAvailable().getDoublePressAvailable();
                 }

                 screenParamsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     ImageResolution imgResolution = null;
                     if (checkBoxImageResolution.Checked)
                     {
                         imgResolution = new ImageResolution();
                         if (checkBoxResolutionWidth.Checked)
                         {
                             if (editTextResolutionWidth.Text.Equals(""))
                                 imgResolution.resolutionWidth = 0;
                             else
                                 imgResolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                         }
                         if (checkBoxResolutionHeight.Checked)
                         {
                             if (editTextResolutionHeight.Text.Equals(""))
                                 imgResolution.resolutionHeight = 0;
                             else
                                 imgResolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                         }
                     }
                     scrnParam.resolution = imgResolution;

                     TouchEventCapabilities touchEventCapabilities = null;
                     if (checkTouchEventCapabilities.Checked)
                     {
                         touchEventCapabilities = new TouchEventCapabilities();

                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                     }
                     scrnParam.touchEventAvailable = touchEventCapabilities;
                 });

                 screenParamsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     screenParamsAlertDialog.Dispose();
                 });
                 screenParamsAlertDialog.Show();
             };

             CheckBox checkBoxNumCustomPresetsAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_checkbox);
             EditText editTextNumCustomPresetsAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_edittext);

             checkBoxNumCustomPresetsAvailable.CheckedChange += (s, e) => editTextNumCustomPresetsAvailable.Enabled = e.IsChecked;

             editTextNumCustomPresetsAvailable.Text = dspCap.getNumCustomPresetsAvailable().ToString();
			 if (dspCap.getNumCustomPresetsAvailable() == null)
				 checkBoxNumCustomPresetsAvailable.Checked = false;

			 displayCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
             {
                 if (checkBoxDisplayType.Checked)
                 {
                     dspCap.displayType = (DisplayType)spnButtonDisplayType.SelectedItemPosition;
                 }
                 if (addTextFieldsChk.Checked)
                 {
                     dspCap.textFields = textFieldsList;
                 }
                 if (imageFieldsChk.Checked)
                 {
                     dspCap.imageFields = imageFieldsList;
                 }
                 if (mediaClockFormatsChk.Checked)
                 {
                     dspCap.mediaClockFormats = mediaClockFormatsList;
                 }
                 if (imageTypeCheckbox.Checked)
                 {
                     dspCap.imageCapabilities = imageTypeList;
                 }
                 dspCap.graphicSupported = checkBoxGraphicSupported.Checked;
                 if (checkBoxTemplatesAvailable.Checked)
                 {
                     List<String> templatesAvailable = new List<string>();
                     templatesAvailable.AddRange(editTextTemplatesAvailable.Text.Split(','));
                     dspCap.templatesAvailable = templatesAvailable;
                 }
                 if (screenParamsChk.Checked)
                 {
                     dspCap.screenParams = scrnParam;
                 }
                 if (checkBoxNumCustomPresetsAvailable.Checked)
                 {
                     if (editTextNumCustomPresetsAvailable.Text.Equals(""))
                         dspCap.numCustomPresetsAvailable = 0;
                     else
                         dspCap.numCustomPresetsAvailable = Java.Lang.Integer.ParseInt(editTextNumCustomPresetsAvailable.Text);
                 }
             });

             displayCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
             {
                 displayCapabilitiesAlertDialog.Dispose();
             });
             displayCapabilitiesAlertDialog.Show();
         };



            CheckBox buttonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_add_button_capabilities_chk);
			Button addButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_button_capabilities_btn);
            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.set_display_capabilities_button_capabilities_listview);
            List<ButtonCapabilities> btnCapList = new List<ButtonCapabilities>();
			buttonCapabilitiesCheckBox.CheckedChange += (sen, evn) => addButtonCapabilitiesButton.Enabled = evn.IsChecked;

			if (tmpObj != null )
			{
				if (tmpObj.getButtonCapabilities() != null)
					btnCapList.AddRange(tmpObj.getButtonCapabilities());
				else
					buttonCapabilitiesCheckBox.Checked = false;
			}
			var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(Activity, btnCapList, ViewStates.Invisible, buttonCapabilitiesListView);
			buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;
			Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);


			addButtonCapabilitiesButton.Click += delegate
			{
				AlertDialog.Builder btnCapabilitiesAlertDialog = new AlertDialog.Builder(Activity);
				View btnCapabilitiesView = (View)layoutInflater.Inflate(Resource.Layout.button_capabilities, null);
				btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
				btnCapabilitiesAlertDialog.SetTitle("ButtonCapabilities");

				TextView textViewButtonName = (TextView)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);

				Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);
				string[] btnCapabilitiesButtonName = Enum.GetNames(typeof(HmiApiLib.ButtonName));
				var btnCapabilitiesButtonNameAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, btnCapabilitiesButtonName);
				spnButtonNames.Adapter = btnCapabilitiesButtonNameAdapter;


				CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);

				CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);

				CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);


				btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
			  {

				  ButtonCapabilities btn = new ButtonCapabilities();
				  btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;

				  if (checkBoxShortPressAvailable.Checked)
					  btn.shortPressAvailable = true;
				  else
					  btn.shortPressAvailable = false;

				  if (checkBoxLongPressAvailable.Checked)
					  btn.longPressAvailable = true;
				  else
					  btn.longPressAvailable = false;

				  if (checkBoxUpDownAvailable.Checked)
					  btn.upDownAvailable = true;
				  else
					  btn.upDownAvailable = false;

                    btnCapList.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();

			  });

				btnCapabilitiesAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			 {
				 btnCapabilitiesAlertDialog.Dispose();
			 });
				btnCapabilitiesAlertDialog.Show();

				
			};

			

            CheckBox softButtonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_soft_button_capabilities_chk);
			Button addSoftButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_soft_button_capabilities_btn);
            List<SoftButtonCapabilities> btnSoftButtonCapList = new List<SoftButtonCapabilities>();
			ListView softButtonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.set_display_capabilities_soft_button_capabilities_listview);
			softButtonCapabilitiesCheckBox.CheckedChange += (sen, evn) => addSoftButtonCapabilitiesButton.Enabled = evn.IsChecked;

			if (tmpObj != null )
			{
				if (tmpObj.getSoftButtonCapabilities() != null)
					btnSoftButtonCapList.AddRange(tmpObj.getSoftButtonCapabilities());
				else
					softButtonCapabilitiesCheckBox.Checked = false;
			}
			var softButtonCapabilitiesAdapter = new SoftButtonCapabilitiesAdapter(Activity, btnSoftButtonCapList,ViewStates.Invisible, softButtonCapabilitiesListView);
			softButtonCapabilitiesListView.Adapter = softButtonCapabilitiesAdapter;
			Utility.setListViewHeightBasedOnChildren(softButtonCapabilitiesListView);



			addSoftButtonCapabilitiesButton.Click += delegate
			 {
				 AlertDialog.Builder SoftButtonCapabilitiesAlertDialog = new AlertDialog.Builder(Activity);
				 View SoftButtonCapabilitiesView = (View)layoutInflater.Inflate(Resource.Layout.soft_button_capabilities, null);
				 SoftButtonCapabilitiesAlertDialog.SetView(SoftButtonCapabilitiesView);
				 SoftButtonCapabilitiesAlertDialog.SetTitle("SoftButtonCapabilities");

				 CheckBox checkBoxShortPressAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_short_press_available_checkbox);

				 CheckBox checkBoxLongPressAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_long_press_available_checkbox);

				 CheckBox checkBoxUpDownAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_up_down_available_checkbox);

				 CheckBox checkBoxImageSupported = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_image_supported_checkbox);
				 CheckBox checkBoxTextSupported = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_text_supported_checkbox);


				 SoftButtonCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				 {

					 SoftButtonCapabilities btn = new SoftButtonCapabilities();

					 if (checkBoxShortPressAvailable.Checked)
						 btn.shortPressAvailable = true;
					 else
						 btn.shortPressAvailable = false;

					 if (checkBoxLongPressAvailable.Checked)
						 btn.longPressAvailable = true;
					 else
						 btn.longPressAvailable = false;

					 if (checkBoxUpDownAvailable.Checked)
						 btn.upDownAvailable = true;
					 else
						 btn.upDownAvailable = false;

					 if (checkBoxImageSupported.Checked)
						 btn.imageSupported = true;
					 else
						 btn.imageSupported = false;

					 if (checkBoxTextSupported.Checked)
						 btn.textSupported = true;
					 else
						 btn.textSupported = false;

					 btnSoftButtonCapList.Add(btn);
                     softButtonCapabilitiesAdapter.NotifyDataSetChanged();

				 });

				 SoftButtonCapabilitiesAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			  {
				  SoftButtonCapabilitiesAlertDialog.Dispose();
			  });
				 SoftButtonCapabilitiesAlertDialog.Show();

			 };




			CheckBox checkBoxOnScreenPresetsAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.set_display_capabilities_preset_bank_capabilities_cb);

			PresetBankCapabilities prstCap = new PresetBankCapabilities();
			


			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_result_code_chk);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.set_display_capabilities_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;


			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getPresetBankCapabilities() != null)
				{
					if (tmpObj.getPresetBankCapabilities().onScreenPresetsAvailable != null)
						checkBoxOnScreenPresetsAvailable.Checked = (bool)tmpObj.getPresetBankCapabilities().onScreenPresetsAvailable;
					else
						checkBoxOnScreenPresetsAvailable.Checked = false;
				}
			}

            resultCodeCheckBox.CheckedChange += (sen, evn) => spnResultCode.Enabled = evn.IsChecked;


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			 {
				 rpcAlertDialog.Dispose();
			 });

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
		    {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                prstCap.onScreenPresetsAvailable = checkBoxOnScreenPresetsAvailable.Checked;

				if (resultCodeCheckBox.Checked)
				rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (!displayCapabilitiesCheckBox.Checked)
                    dspCap = null;

                if (!buttonCapabilitiesCheckBox.Checked)
                    btnCapList = null;

				if (!softButtonCapabilitiesCheckBox.Checked)
				    btnSoftButtonCapList = null;
				if (!checkBoxOnScreenPresetsAvailable.Checked)
				    prstCap = null;
            

                RpcMessage rpcMessage = null;
                rpcMessage = BuildRpc.buildUiSetDisplayLayoutResponse(BuildRpc.getNextId(), rsltCode, dspCap, btnCapList, btnSoftButtonCapList, prstCap);
                AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetGlobalProperties()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetGlobalProperties);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetGlobalPropertiesResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		void CreateUIResponseSetMediaClockTimer()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetMediaClockTimer);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetMediaClockTimerResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseShow()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseShow);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.Show tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Show();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Show>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Show);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiShowResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}


		private void CreateUIResponseShowCustomForm()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseShowCustomForm);

			CheckBox infoCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);			
			EditText infoEditText = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);			

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);

            infoCheckBox.Text = "Info";
            infoEditText.InputType = Android.Text.InputTypes.ClassText;
            infoEditText.Text = "DEFAULT";
			resultCodeCheckBox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = adapter;

            infoCheckBox.CheckedChange += (s, e) => infoEditText.Enabled = e.IsChecked;
			resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm>(Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getInfo() != null)
					infoEditText.Text = tmpObj.getInfo();
				else
					infoCheckBox.Checked = false;
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				String info = null;

				if (infoCheckBox.Checked && infoEditText.Text != null && infoEditText.Text.Length > 0)
					info = infoEditText.Text;

				if (resultCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiShowCustomFormResponse(BuildRpc.getNextId(), rsltCode, info);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSlider()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.slider_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseSlider);

			CheckBox slider_position_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_position_checkbox);
			EditText slider_position_edittext = (EditText)rpcView.FindViewById(Resource.Id.slider_position_et);
            slider_position_edittext.Text = "1";

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_result_code_checkbox);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.slider_result_code_spinner);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = resultCodeAdapter;

			slider_position_checkbox.CheckedChange += (s, e) => slider_position_edittext.Enabled = e.IsChecked;
			resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Slider);
                tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getSliderPosition() != null)
					slider_position_edittext.Text = tmpObj.getSliderPosition().ToString();
				else
					slider_position_checkbox.Checked = false;
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				int? sliderPosition = null;

				if (slider_position_checkbox.Checked && slider_position_edittext.Text != null && slider_position_edittext.Text.Length > 0)
					sliderPosition = Java.Lang.Integer.ParseInt(slider_position_edittext.Text);

				if (resultCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiSliderResponse(BuildRpc.getNextId(), rsltCode, sliderPosition);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.Show();
		}

        private void CreateUINotificationOnCommand()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnCommand);

            CheckBox cmdIdCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
            Spinner spnCmdId = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);

            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);

            cmdIdCheck.CheckedChange += (s, e) => spnCmdId.Enabled = e.IsChecked;

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };


            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            List<int?> cmdIDList = new List<int?>();

            List<int> appIds = new List<int>();
            foreach (AppItem item in AppInstanceManager.appList)
            {
                appIds.Add(item.getAppID());
            }

            int[] appIDArray = appIds.ToArray();

            for (int i = 0; i < AppInstanceManager.commandIdList.Count; i++)
            {
                List<int?> commandIdList = AppInstanceManager.commandIdList[appIDArray[i]];
                cmdIDList.AddRange(commandIdList);
            }

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            var cmdIdAdapter = new ArrayAdapter<int?>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, cmdIDList);
            spnCmdId.Adapter = cmdIdAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (tmpObj.getAppId() != null)
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				else
					appIDCheckBox.Checked = false;

				if (tmpObj.getCmdID() != null)
					spnCmdId.SetSelection((int)tmpObj.getCmdID());
				else
					cmdIdCheck.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null, cmdID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = appIds[registerdAppIdSpn.SelectedItemPosition];

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 if (cmdIdCheck.Checked)
                     cmdID = (int)spnCmdId.SelectedItem;

                 RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnCommand(cmdID, selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateUINotificationOnDriverDistraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnDriverDistraction);
			


			CheckBox driverDistractionStateCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner driverDistractionStateSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
            driverDistractionStateCheckbox.Text = "Driver Distraction State";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, driverDistractionState);
			driverDistractionStateSpinner.Adapter = adapter;

            driverDistractionStateCheckbox.CheckedChange += (s, e) => driverDistractionStateSpinner.Enabled = e.IsChecked;
			

			HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null )
			{
				if (tmpObj.getDriverDistractionState() != null)
					driverDistractionStateSpinner.SetSelection((int)tmpObj.getDriverDistractionState());
				else
					driverDistractionStateCheckbox.Checked = false;

				
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});					
			
			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                DriverDistractionState? driverDistractionsState = null;
				if (driverDistractionStateCheckbox.Checked)
				{
                    driverDistractionsState = (DriverDistractionState)driverDistractionStateSpinner.SelectedItemPosition;
				}
				bool? toggle_dismissalEnabled = null;
				
				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnDriverDistraction(driverDistractionsState);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnKeyboardInput()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.slider_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnKeyboardInput);

			CheckBox dataCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_position_checkbox);
			EditText dataEditText = (EditText)rpcView.FindViewById(Resource.Id.slider_position_et);

			CheckBox keyboardEventCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_result_code_checkbox);
			Spinner keyboardEventSpinner = (Spinner)rpcView.FindViewById(Resource.Id.slider_result_code_spinner);

            dataCheckbox.Text = "Data";
            dataEditText.Text = "Data";
            dataEditText.InputType = Android.Text.InputTypes.ClassText;
            			
			var keyBoardEventAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, keyBoardEvent);
			keyboardEventSpinner.Adapter = keyBoardEventAdapter;

            dataCheckbox.CheckedChange += (s, e) => dataEditText.Enabled = e.IsChecked;
            keyboardEventCheckBox.CheckedChange += (s, e) => keyboardEventSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput>(Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getKeyboardEvent() != null)
                    keyboardEventSpinner.SetSelection((int)tmpObj.getKeyboardEvent());

				if (tmpObj.getData() != null)
					dataEditText.Text = tmpObj.getData();
				else
					dataCheckbox.Checked = false;

			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{

				KeyboardEvent? keyBoardEvent = null;
				String data = "";

				if (dataCheckbox.Checked && dataEditText.Text != null && dataEditText.Text.Length > 0)
					data = dataEditText.Text;

				if (keyboardEventCheckBox.Checked)
					keyBoardEvent = (KeyboardEvent)keyboardEventSpinner.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnKeyboardInput(keyBoardEvent, data);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnLanguageChange()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnLanguageChange);

			CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			languageCheckBox.Text = "Language";
			Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;

			string[] language = Enum.GetNames(typeof(Language));
			var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
			spnLanguage.Adapter = languageAdapter;

			HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getLanguage() != null)
					spnLanguage.SetSelection((int)tmpObj.getLanguage());
				else
					languageCheckBox.Checked = false;
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				Language? lang = null;
				if (languageCheckBox.Checked)
				{
					lang = (Language)spnLanguage.SelectedItemPosition;
				}

				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnLanguageChange(lang);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
            rpcAlertDialog.Show();
		}

		/*private void CreateUINotificationOnRecordStart()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnRecordStart);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner appIDSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            appIDCheckBox.Text = "App ID";

            appIDCheckBox.CheckedChange += (sender, e) => appIDSpinner.Enabled = e.IsChecked;

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			appIDSpinner.Adapter = appIDAdapter;
			
			HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getAppId() != null)
					appIDSpinner.SetSelection((int)tmpObj.getAppId());
				else
					appIDCheckBox.Checked = false;
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				int? appId = null;
				if (appIDCheckBox.Checked)
				{
					appId = appIDSpinner.SelectedItemPosition;
				}

                RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnRecordStart(appId);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}*/

        private void CreateUINotificationOnResetTimeout()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_reset_timeout, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnResetTimeout);

            CheckBox CheckBoxMethodName = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_method_name_cb);
            EditText editTextMethodName = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_method_name_et);
            editTextMethodName.Text = "Method Name";

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_app_id_checkbox);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.tts_notification_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.tts_notification_app_id_manual_radioGroup);

            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_registered_apps);

            CheckBoxMethodName.CheckedChange += (sender, e) => editTextMethodName.Enabled = e.IsChecked;
            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (tmpObj.getAppId() != null)
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				else
					appIDCheckBox.Checked = false;


				if (tmpObj.getMethodName() != null)
					editTextMethodName.Text = tmpObj.getMethodName();
				else
					CheckBoxMethodName.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int selectedAppID = 0;
                 String methodName = null;

                 if (CheckBoxMethodName.Checked && editTextMethodName.Text != null && editTextMethodName.Text.Length > 0)
                     methodName = editTextMethodName.Text;

                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnResetTimeout(selectedAppID, methodName);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }


        private void CreateUINotificationOnSystemContext()
        {
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnSystemContext);

			CheckBox systemContextCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
			Spinner spnSystemContext = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);
			systemContextCheck.Text = "System Context";

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);
			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
			registerdAppIdSpn.Enabled = false;
			CheckBox windowIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.window_id_check);
			EditText windowIdEditText = (EditText)rpcView.FindViewById(Resource.Id.window_id_et);
			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);

			systemContextCheck.CheckedChange += (sender, e) => spnSystemContext.Enabled = e.IsChecked;
			windowIDCheckBox.CheckedChange += (s, e) =>
			{
				windowIdEditText.Enabled = e.IsChecked;


			};
			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};


			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var systemContextAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, systemContext);
			spnSystemContext.Adapter = systemContextAdapter;

			HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getAppId() != null)
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				else
					appIDCheckBox.Checked = false;
				if (tmpObj.getSystemContext() != null)
					spnSystemContext.SetSelection((int)tmpObj.getSystemContext());
				else
					systemContextCheck.Checked = false;

				if (tmpObj.getWindowId() != null)
					windowIdEditText.Text = tmpObj.getWindowId().ToString();
				else
					windowIDCheckBox.Checked = false;

			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
				int? windowID = null;
				SystemContext? systemContext = null;

				if (systemContextCheck.Checked)
					systemContext = (SystemContext)spnSystemContext.SelectedItemPosition;

				if (appIDCheckBox.Checked)
				{
					if (name.Equals("Registered Apps"))

						if (registerdAppIdSpn.Adapter.Count == 0)
						{
							Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
							return;
						}
						else
							selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

					else if (name.Equals("Manual"))
						if (manualAppIdEditText.Text.Equals(""))
							selectedAppID = 0;
						else
							selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				}
				if (windowIDCheckBox.Checked && windowIdEditText.Text!="")
				{
					windowID = Java.Lang.Integer.ParseInt(windowIdEditText.Text);

				}
				RequestNotifyMessage rpcMessage = BuildRpc.buildUiOnSystemContext(systemContext, selectedAppID, windowID);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnTouchEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_touch_event_notification, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnTouchEvent);

			CheckBox touchTypeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
			Spinner spnTouchType = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            CheckBox touchEventCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

			ListView listViewTouchEvent = (ListView)rpcView.FindViewById(Resource.Id.touch_event_listview);

			touchTypeCheck.CheckedChange += (sender, e) => spnTouchType.Enabled = e.IsChecked;
			touchEventCheck.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;

			string[] touchType = Enum.GetNames(typeof(TouchType));
			var touchTypeAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, touchType);
			spnTouchType.Adapter = touchTypeAdapter;

            List<TouchEvent> touchEventsList = new List<TouchEvent>();
            var touchEventAdapter = new TouchEventAdapter(Activity, touchEventsList);
			listViewTouchEvent.Adapter = touchEventAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent>(Activity, tmpObj.getMethod());
             
            if(tmpObj != null)
            {
				if (tmpObj.getTouchType() != null)
					spnTouchType.SetSelection((int)tmpObj.getTouchType());
				else
					touchTypeCheck.Checked = false;


				if (tmpObj.getTouchEvent() != null)
				{
					touchEventsList.AddRange(tmpObj.getTouchEvent());
					touchEventAdapter.NotifyDataSetChanged();
				}
				else
					touchEventCheck.Checked = false;

			}

			createTouchEvent.Click += (sender, e) =>
			{
				List<TouchCoord> touchCoordList = new List<TouchCoord>();
				AlertDialog.Builder touchEventAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
				View touchEventView = layoutInflater.Inflate(Resource.Layout.touch_event, null);
				touchEventAlertDialog.SetView(touchEventView);
				touchEventAlertDialog.SetTitle("Touch Event");

				CheckBox touchEventIdCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_id_checkbox);
				EditText touchEventIdEditText = (EditText)touchEventView.FindViewById(Resource.Id.touch_event_id_edit_text);

				CheckBox touchEventTsCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_ts_checkbox);
				EditText touchEventTsEditText = (EditText)touchEventView.FindViewById(Resource.Id.touch_event_ts_edittext);

				CheckBox touchEventCordCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_cord_checkbox);				
				Button createTouchCordButton = (Button)touchEventView.FindViewById(Resource.Id.create_touch_cord_button);

                ListView touchCordListView = (ListView)touchEventView.FindViewById(Resource.Id.touch_cord_list_view);

				var touchCoordAdapter = new TouchCoordAdapter(Activity, touchCoordList);
				touchCordListView.Adapter = touchCoordAdapter;
                Utility.setListViewHeightBasedOnChildren(touchCordListView);

				touchEventIdCheckbox.CheckedChange += (send, e1) => touchEventIdEditText.Enabled = e1.IsChecked;
				touchEventTsCheckbox.CheckedChange += (send, e1) => touchEventTsEditText.Enabled = e1.IsChecked;
				touchEventCordCheckbox.CheckedChange += (send, e1) => createTouchCordButton.Enabled = e1.IsChecked;

				createTouchCordButton.Click += (sender1, e1) =>
				{
					AlertDialog.Builder touchCoordAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
					View touchCoordView = layoutInflater.Inflate(Resource.Layout.touch_cord, null);
					touchCoordAlertDialog.SetView(touchCoordView);
					touchCoordAlertDialog.SetTitle("Touch Coord");

					CheckBox xCheckBox = (CheckBox)touchCoordView.FindViewById(Resource.Id.touch_cord_x_checkbox);
					EditText xEditText = (EditText)touchCoordView.FindViewById(Resource.Id.touch_cord_x_edittext);

					CheckBox yCheckBox = (CheckBox)touchCoordView.FindViewById(Resource.Id.touch_cord_y_checkbox);
					EditText yEditText = (EditText)touchCoordView.FindViewById(Resource.Id.touch_cord_y_edittext);

					xCheckBox.CheckedChange += (send, e2) => xEditText.Enabled = e2.IsChecked;
					yCheckBox.CheckedChange += (send, e2) => yEditText.Enabled = e2.IsChecked;

					touchCoordAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
					{
						touchCoordAlertDialog.Dispose();
					});

					touchCoordAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
					{
						TouchCoord coord = new TouchCoord();
						try
						{
                            if(xCheckBox.Checked)
							    coord.x = Int32.Parse(xEditText.Text);

                            if(yCheckBox.Checked)
							    coord.y = Int32.Parse(yEditText.Text);
						}
						catch (Exception e11)
						{

						}
						touchCoordList.Add(coord);
						touchCoordAdapter.NotifyDataSetChanged();
					});

					touchCoordAlertDialog.Show();
				};


				touchEventAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					touchEventAlertDialog.Dispose();
				});

				touchEventAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
				{
					TouchEvent touchEvent = new TouchEvent();
					try
					{
                        if(touchEventIdCheckbox.Checked)
						    touchEvent.id = Int32.Parse(touchEventIdEditText.Text);
					}
					catch (Exception e2)
					{
						touchEvent.id = 0;
					}

                    if (touchEventTsCheckbox.Checked)
                    {
                        List<int> tsList = new List<int>();
                        string[] t = touchEventTsEditText.Text.Split(',');
                        foreach (string ts in t)
                        {
                            try
                            {
                                tsList.Add(Int32.Parse(ts));
                            }
                            catch (Exception e3)
                            {

                            }
                        }
                        touchEvent.ts = tsList;
                    }

					touchEvent.c = touchCoordList;

					touchEventsList.Add(touchEvent);
					touchEventAdapter.NotifyDataSetChanged();
				});

				touchEventAlertDialog.Show();
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
                HmiApiLib.Common.Enums.TouchType? tchType = null;

				 if (touchTypeCheck.Checked)
                    tchType = (HmiApiLib.Common.Enums.TouchType)spnTouchType.SelectedItemPosition;

				 if (!touchEventCheck.Checked)
					 touchEventsList = null;

				 RequestNotifyMessage rpcMessage = null;
				 rpcMessage = BuildRpc.buildUiOnTouchEvent(tchType, touchEventsList);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);

             });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

        private void CreateTTSResponseChangeRegistration()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(TTSResponseChangeRegistration);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) =>
           {
               resultCodeSpn.Enabled = e.IsChecked;
           };

            HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                RpcResponse rpcMessage = BuildRpc.buildTTSChangeRegistrationResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateTTSResponseSetGlobalProperties()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(TTSResponseSetGlobalProperties);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) =>
            {
                spnResultCode.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildTTSSetGlobalPropertiesResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateTTSResponseSpeak()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(TTSResponseSpeak);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			rsltCodeCheckBox.CheckedChange += (s, e) =>
		    {
                spnResultCode.Enabled = e.IsChecked;
		    };

			HmiApiLib.Controllers.TTS.OutgoingResponses.Speak tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.Speak();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.Speak>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.Speak);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildTtsSpeakResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateTTSResponseStopSpeaking()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(TTSResponseStopSpeaking);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			rsltCodeCheckBox.CheckedChange += (s, e) =>
		    {
                spnResultCode.Enabled = e.IsChecked;
		    };

			HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking);
                tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildTtsStopSpeakingResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

        private void CreateTTSNotificationOnLanguageChange()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox onLanuageChangeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner onLanguageChangeSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            onLanguageChangeSpn.Adapter = adapter;

            onLanuageChangeCheckBox.CheckedChange += (s, e) =>
           {
               onLanguageChangeSpn.Enabled = e.IsChecked;
           };

            HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange();
            tmpObj = (HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
            if (tmpObj != null )
            {
				if(null != tmpObj.getLanguage())
                onLanguageChangeSpn.SetSelection((int)tmpObj.getLanguage());
				else
				onLanuageChangeCheckBox.Checked = false;

			}

            onLanuageChangeCheckBox.Text = "Language";

            rpcAlertDialog.SetTitle(TTSNotificationOnLanguageChange);
            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                Language? languages = null;
                if (onLanuageChangeCheckBox.Checked)
                {
                    languages = (Language)onLanguageChangeSpn.SelectedItemPosition;
                }

                RequestNotifyMessage rpcResponse = BuildRpc.buildTtsOnLanguageChange(languages);
                AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
                AppInstanceManager.Instance.sendRpc(rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        private void CreateTTSNotificationOnResetTimeout()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_reset_timeout, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_app_id_checkbox);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.tts_notification_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.tts_notification_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_registered_apps);

            CheckBox checkBoxMethodName = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_method_name_cb);
            EditText editTextMethodName = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_method_name_et);
            editTextMethodName.Text = "Method Name";

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            checkBoxMethodName.CheckedChange += (s, e) =>
            {
                editTextMethodName.Enabled = e.IsChecked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            rpcAlertDialog.SetTitle(TTSNotificationOnResetTimeout);

            HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout tmpObj = new HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout();
            tmpObj = (HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (null != tmpObj.getAppId())
				{
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				}
				else
					appIDCheckBox.Checked = false;

				editTextMethodName.Text = tmpObj.getMethodName();
				if (tmpObj.getMethodName() == null)
					checkBoxMethodName.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 String methodName = null;
                 if (checkBoxMethodName.Checked)
                 {
                     methodName = editTextMethodName.Text;
                 }
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsOnResetTimeout(selectedAppID, methodName);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        void CreateTTSNotificationStarted()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(TTSNotificationStarted);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStartedNotification();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        void CreateTTSNotificationStopped()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(TTSNotificationStopped);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStoppedNotification();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }


        private void CreateSDLRequestActivateApp()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (null != tmpObj.getAppId())
				{
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				}
				else
					appIDCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(SDLRequestActivateApp);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                         {
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();
                             AppInstanceManager.lastFullApp = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition];
                         }
                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }               

                 RpcRequest rpcMessage = BuildRpc.buildSdlActivateAppRequest(BuildRpc.getNextId(), selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetListOfPermissions()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);
            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions>(Activity, tmpObj.getMethod());
            if (tmpObj != null )
            {
				if (tmpObj.getAppId() != null)
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				else
					appIDCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(SDLRequestGetListOfPermissions);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 RpcRequest rpcMessage = BuildRpc.buildSDLGetListOfPermissionsRequest(BuildRpc.getNextId(), selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetStatusUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLRequestGetStatusUpdate);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
				 RpcRequest rpcMessage = BuildRpc.buildSDLGetStatusUpdateRequest(BuildRpc.getNextId());
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetURLS()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.sdl_get_urls, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLRequestGetURLS);

            CheckBox serviceCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_get_url_check);
            EditText editTextService = (EditText)rpcView.FindViewById(Resource.Id.sdl_get_urls_et);

            serviceCheckBox.CheckedChange += (s, e) => editTextService.Enabled = e.IsChecked;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (tmpObj.getService() != null)
					editTextService.Text = tmpObj.getService().ToString();
				else
					serviceCheckBox.Checked = false;
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 int? service = null;
                if (serviceCheckBox.Checked && editTextService.Text != null && editTextService.Text.Length > 0)
                 {
                     try
                     {
                         service = Java.Lang.Integer.ParseInt(editTextService.Text);
                     } catch (Exception e) { }
                 }
                 RpcRequest rpcMessage = BuildRpc.buildSDLGetURLSRequest(BuildRpc.getNextId(), service);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetUserFriendlyMessage()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLRequestGetUserFriendlyMessage);

            CheckBox checkBoxMessageCode = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);
            checkBoxMessageCode.Text = "MessageCode";
            EditText editTextMessageCode = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);
            editTextMessageCode.InputType = Android.Text.InputTypes.ClassText;
            editTextMessageCode.Text = "Message 1, Message 2";
            rpcView.FindViewById(Resource.Id.alert_text_hint).Visibility = ViewStates.Visible;

            CheckBox checkBoxlanguage = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
            checkBoxlanguage.Text = "Language";

            Spinner spnlanguage = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);
            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnlanguage.Adapter = languageAdapter;

            checkBoxMessageCode.CheckedChange += (s, e) => editTextMessageCode.Enabled = e.IsChecked;
            checkBoxlanguage.CheckedChange += (s, e) => spnlanguage.Enabled = e.IsChecked;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				if (tmpObj.getMessageCodes() != null)
				{
					List<string> messageCodeList = tmpObj.getMessageCodes();

					string[] messageDataRsltArray = messageCodeList.ToArray();

					for (int i = 0; i < messageCodeList.Count; i++)
					{
						if (i == messageCodeList.Count - 1)
						{
							editTextMessageCode.Append(messageDataRsltArray[i]);
							break;
						}
						editTextMessageCode.Append(messageDataRsltArray[i] + ",");
					}
				}
				else
					checkBoxMessageCode.Checked = false;


				if (tmpObj.getLanguage() != null)
					spnlanguage.SetSelection((int)tmpObj.getLanguage());
				else
					checkBoxlanguage.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                List<String> messageCodes = null;
                Language? selectedLanguage = null;
                if (checkBoxMessageCode.Checked)
                {
                    messageCodes = new List<string>();
                    if (editTextMessageCode.Text != null && editTextMessageCode.Text.Length > 0)
                    {
                        messageCodes.AddRange(editTextMessageCode.Text.Split(','));
                    }
                }
                if (checkBoxlanguage.Checked)
                {
                    selectedLanguage = (HmiApiLib.Common.Enums.Language)spnlanguage.SelectedItemPosition;
                }
                RpcRequest rpcMessage = BuildRpc.buildSDLGetUserFriendlyMessageRequest(BuildRpc.getNextId(), messageCodes, selectedLanguage);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestUpdateSDL()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLRequestUpdateSDL);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RpcRequest rpcMessage = BuildRpc.buildSDLUpdateSDLRequest(BuildRpc.getNextId());
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnAllowSDLFunctionality()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_allow_sdl_functionality, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLNotificationOnAllowSDLFunctionality);

            CheckBox checkBoxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_info_chk);
            CheckBox checkBoxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_name_check);
            EditText editTextDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name_sdl_et);

            CheckBox checkBoxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_id_sdl_check);
            EditText editTextDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id_sdl_et);

            CheckBox checkBoxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_transport_type_sdl_check);
            Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type_sdl_spn);

            CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_is_sdl_allowed_check);
            Switch tglIsSDLAllowd = (Switch)rpcView.FindViewById(Resource.Id.on_allow_sdl_is_sdl_allowed_tgl);

            CheckBox checkBoxAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_allowed_check);
            Switch tglAllowed = (Switch)rpcView.FindViewById(Resource.Id.on_allow_sdl_allowed_tgl);

            CheckBox checkBoxConsentedSource = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_consent_source_check);
            Spinner spnConsentSource = (Spinner)rpcView.FindViewById(Resource.Id.consent_source_spn);

            string[] transportType = Enum.GetNames(typeof(HmiApiLib.Common.Enums.TransportType));
            var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
            spnTransportType.Adapter = transportTypeAdapter;

            string[] consentSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ConsentSource));
            var consentSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, consentSource);
            spnConsentSource.Adapter = consentSourceAdapter;

            checkBoxDeviceName.CheckedChange += (s, e) => editTextDeviceName.Enabled = e.IsChecked;
            checkBoxDeviceId.CheckedChange += (s, e) => editTextDeviceId.Enabled = e.IsChecked;
            checkBoxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;
            checkBoxIsSDLAllowed.CheckedChange += (s, e) => tglIsSDLAllowd.Enabled = e.IsChecked;
            checkBoxConsentedSource.CheckedChange += (s, e) => spnConsentSource.Enabled = e.IsChecked;
            checkBoxAllowed.CheckedChange += (s, e) => tglAllowed.Enabled = e.IsChecked;

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality>(Activity, tmpObj.getMethod());
			checkBoxDeviceInfo.CheckedChange += (sender, e) =>
			{
				checkBoxDeviceName.Enabled = e.IsChecked;
				editTextDeviceName.Enabled = e.IsChecked && checkBoxDeviceName.Checked;
				checkBoxDeviceId.Enabled = e.IsChecked;
				editTextDeviceId.Enabled = e.IsChecked && checkBoxDeviceId.Checked;
				checkBoxTransportType.Enabled = e.IsChecked;
				spnTransportType.Enabled = e.IsChecked && checkBoxTransportType.Checked;
				checkBoxIsSDLAllowed.Enabled = e.IsChecked;
                tglIsSDLAllowd.Enabled = e.IsChecked && checkBoxIsSDLAllowed.Checked;
            };

			if (tmpObj != null)
            {
				if (tmpObj.getDevice() != null)
				{
					if (tmpObj.getDevice().getName() != null)
						editTextDeviceName.Text = tmpObj.getDevice().getName();
					else
						checkBoxDeviceName.Checked = false;

					if (tmpObj.getDevice().getId() != null)
						editTextDeviceId.Text = tmpObj.getDevice().getId();
					else
						checkBoxDeviceId.Checked = false;

					if (tmpObj.getDevice().getTransportType() != null)
						spnTransportType.SetSelection((int)tmpObj.getDevice().getTransportType());
					else
						checkBoxTransportType.Checked = false;

					if (null != tmpObj.getDevice().getIsSDLAllowed())
                        tglAllowed.Checked = (bool)tmpObj.getDevice().getIsSDLAllowed();
					else
						checkBoxIsSDLAllowed.Checked = false;
				}
				else
					checkBoxDeviceInfo.Checked = false;

				if (tmpObj.getAllowed() != null)
                    tglAllowed.Checked = (bool)tmpObj.getAllowed();
				else
					checkBoxAllowed.Checked = false;

				if (tmpObj.getSource() != null)
					spnConsentSource.SetSelection((int)tmpObj.getSource());
				else
					checkBoxConsentedSource.Checked = false;
			}
          
            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                DeviceInfo devInfo = null;
                if (checkBoxDeviceInfo.Checked)
                {
                    devInfo = new DeviceInfo();
                    if (checkBoxDeviceName.Checked)
                        devInfo.name = editTextDeviceName.Text;
                    if (checkBoxDeviceId.Checked)
                        devInfo.id = editTextDeviceId.Text;
                    if (checkBoxTransportType.Checked)
                        devInfo.transportType = (TransportType)spnTransportType.SelectedItemPosition;
                    if (checkBoxIsSDLAllowed.Checked)
                        devInfo.isSDLAllowed = tglIsSDLAllowd.Checked;
                }
                ConsentSource? source = null;
                if (checkBoxConsentedSource.Checked)
                    source = (ConsentSource)spnConsentSource.SelectedItemPosition;

                bool? allowed = null;
                if (checkBoxAllowed.Checked)
                    allowed = tglAllowed.Checked;

                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnAllowSDLFunctionality(devInfo, allowed, source);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnAppPermissionConsent()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_permission_consent, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLNotificationOnAppPermissionConsent);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_check);
            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_spn);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_app_permisssion_consent_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_app_permisssion_consent_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_app_permisssion_consent_app_id_registered_apps);

            CheckBox checkBoxConsentSource = (CheckBox)rpcView.FindViewById(Resource.Id.consented_source__sdl_check);
            Spinner spnConsentSource = (Spinner)rpcView.FindViewById(Resource.Id.consented_source_sdl_spn);

            checkBoxConsentSource.CheckedChange += (s, e) =>
            {
                spnConsentSource.Enabled = e.IsChecked;
            };

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            string[] consentSource = Enum.GetNames(typeof(ConsentSource));
            var consentSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, consentSource);
            spnConsentSource.Adapter = consentSourceAdapter;

            CheckBox addPermissionCheck = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_app_permission_consent_add_permission_chk);
            ListView ListViewConsentedFunctions = (ListView)rpcView.FindViewById(Resource.Id.consented_functions_sdl_lv);
            List<PermissionItem> consentedFunctions = new List<PermissionItem>();
            var permissionItemAdapter = new PermissionItemAdapter(Activity, consentedFunctions);
            ListViewConsentedFunctions.Adapter = permissionItemAdapter;

            Button consentFunctionsButton = (Button)rpcView.FindViewById(Resource.Id.consented_functions_sdl_btn);

            addPermissionCheck.CheckedChange += (sender, e) =>
            {
                ListViewConsentedFunctions.Enabled = e.IsChecked;
                consentFunctionsButton.Enabled = e.IsChecked;
            };

            consentFunctionsButton.Click += delegate
            {
                AlertDialog.Builder consentFunctionsAlertDialog = new AlertDialog.Builder(Activity);
                View consentFunctionsView = layoutInflater.Inflate(Resource.Layout.permissison_item, null);
                consentFunctionsAlertDialog.SetView(consentFunctionsView);
                consentFunctionsAlertDialog.SetTitle("Add Permission Item");

                CheckBox checkBoxName = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_name_check);
                EditText editTextName = (EditText)consentFunctionsView.FindViewById(Resource.Id.permission_item_name_et);

                CheckBox checkBoxID = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_id_check);
                EditText editTextID = (EditText)consentFunctionsView.FindViewById(Resource.Id.permission_item_id_et);

                CheckBox checkBoxAllowed = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_allowed_cb);
                Switch tglAllowed = (Switch)consentFunctionsView.FindViewById(Resource.Id.permission_item_allowed_tgl);

                checkBoxName.CheckedChange += (s, e) => editTextName.Enabled = e.IsChecked;
                checkBoxID.CheckedChange += (s, e) => editTextID.Enabled = e.IsChecked;
                checkBoxAllowed.CheckedChange += (s, e) => tglAllowed.Enabled = e.IsChecked;

                consentFunctionsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
               {
                   PermissionItem item = new PermissionItem();
                   if (checkBoxName.Checked)
                   {
                       item.name = editTextName.Text;
                   }
                   if (checkBoxID.Checked)
                   {
                       if (editTextID.Text != null && editTextID.Text.Length > 0)
                       {
                           try
                           {
                               item.id = Java.Lang.Integer.ParseInt(editTextID.Text);
                           }
                           catch (Exception e) { }
                       }
                   }

                   if (checkBoxAllowed.Checked)
                       item.allowed = tglAllowed.Checked;

                   consentedFunctions.Add(item);
                   permissionItemAdapter.NotifyDataSetChanged();
               });

                consentFunctionsAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
               {
                   consentFunctionsAlertDialog.Dispose();
               });
                consentFunctionsAlertDialog.Show();
            };

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent>(Activity, tmpObj.getMethod());
            if (null != tmpObj)
            {
				if (tmpObj.getAppId() != null)
				{
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				}
				else
					appIDCheckBox.Checked = false;

				if (tmpObj.getConsentedFunctions() != null)
				{
					consentedFunctions.AddRange(tmpObj.getConsentedFunctions());
					permissionItemAdapter.NotifyDataSetChanged();
				}
				else
					addPermissionCheck.Checked = false;
				if (tmpObj.getSource() != null)
					spnConsentSource.SetSelection((int)tmpObj.getSource());
				else
					checkBoxConsentSource.Checked = false;
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                string name = manualRegisterdSelectedOptionRadioButton.Text;

                int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                }
                List<PermissionItem> list = null;
                if (addPermissionCheck.Checked)
                {
                    list = consentedFunctions;
                }
                ConsentSource? selectedSource = null;
                if (checkBoxConsentSource.Checked)
                {
                    selectedSource = (ConsentSource)spnConsentSource.SelectedItemPosition;
                }
                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnAppPermissionConsent(selectedAppID, list, selectedSource);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
           {
               if (tmpObj != null)
               {
                   AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
               }
           });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnPolicyUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLNotificationOnPolicyUpdate);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
				RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnPolicyUpdate();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        void CreateSDLNotificationOnReceivedPolicyUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = (View)layoutInflater.Inflate(Resource.Layout.on_received_policy_update, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox policyFileCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.policy_file_check);
            EditText policyFileEditText = (EditText)rpcView.FindViewById(Resource.Id.policy_file_et);
            policyFileEditText.Text = "Policy File";

            rpcAlertDialog.SetTitle(SDLNotificationOnReceivedPolicyUpdate);

            policyFileCheckBox.CheckedChange += (s, e) =>
            {
               policyFileEditText.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate>(Activity, tmpObj.getMethod());
            if (null != tmpObj)
            {
                policyFileEditText.Text = tmpObj.getPolicyfile();
				if (tmpObj.getPolicyfile() == null)
					policyFileCheckBox.Checked = false;
			}
			


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                String file = null;
                if (policyFileCheckBox.Checked)
                {
                    file = policyFileEditText.Text;
                }
                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnReceivedPolicyUpdate(file);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		void CreateNavigationResponseAlertManeuver()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(NavigationResponseAlertManeuver);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rsltCode.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			rsltCode.Text = "Result Code";

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? resultCode = null;
                if (rsltCode.Checked)
                {
                    resultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }
				RpcResponse rpcResponse = BuildRpc.buildNavAlertManeuverResponse(BuildRpc.getNextId(), resultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseGetWayPoints()
		{
			AlertDialog.Builder getSystemInfoRpcAlertDialog = new AlertDialog.Builder(Activity);
			View getSystemInfoRpcView = layoutInflater.Inflate(Resource.Layout.get_way_points, null);
			getSystemInfoRpcAlertDialog.SetView(getSystemInfoRpcView);
			getSystemInfoRpcAlertDialog.SetTitle(NavigationResponseGetWayPoints);

            CheckBox resultCodeChk = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_result_code_tv);
			Spinner spnResultCode = (Spinner)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

			ListView locationListview = (ListView)getSystemInfoRpcView.FindViewById(Resource.Id.location_listview);

			List<LocationDetails> wayPoints = new List<LocationDetails>();
            var adapter = new LocationDetailAdapter(Activity, wayPoints);
            locationListview.Adapter = adapter;

			Button addLocationDetailsButton = (Button)getSystemInfoRpcView.FindViewById(Resource.Id.add_location_details);
            CheckBox addLocationDetailsChk = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_location_chk);

            addLocationDetailsChk.CheckedChange += (sender, e) => 
            {
                addLocationDetailsButton.Enabled = e.IsChecked;
                locationListview.Enabled = e.IsChecked;
            };
			resultCodeChk.CheckedChange += (sender, e) =>
			{
				spnResultCode.Enabled = e.IsChecked;
			};

            HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints>(Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (null != tmpObj.getWayPoints())
				{
					wayPoints.AddRange(tmpObj.getWayPoints());
					adapter.NotifyDataSetChanged();
				}
				else
					addLocationDetailsChk.Checked = false;

			}

			addLocationDetailsButton.Click += delegate
			{
				AlertDialog.Builder locationDetailsAlertDialog = new AlertDialog.Builder(Activity);
				View locationDetailsView = layoutInflater.Inflate(Resource.Layout.location_details, null);
				locationDetailsAlertDialog.SetView(locationDetailsView);
				locationDetailsAlertDialog.SetTitle("Add Location Details");

                CheckBox chkCoordinate = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_coordinate);
                CheckBox chkLatitudeDegree = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_latitude_degree_tv);
				EditText editTextLatitudeDegree = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_latitude_degree_et);

				CheckBox chkLongitudeDegree = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_longitude_degree_tv);
				EditText editTextLongitudeDegree = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_longitude_degree_et);

				CheckBox chkLocationName = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_name_tv);
				EditText editTextLocationName = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_name_et);

				CheckBox chkAddress = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_tv);
				EditText editTextAddressLine1 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line1_et);
				EditText editTextAddressLine2 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line2_et);
				EditText editTextAddressLine3 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line3_et);
                EditText editTextAddressLine4 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line4_et);

				CheckBox chkLocationDescription = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_description_tv);
				EditText editTextLocationDescription = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_description_et);

				CheckBox chkPhoneNumber = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_phone_number_tv);
				EditText editTextPhoneNumber = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_phone_number_et);

                CheckBox chkLocationImage = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_image);
				CheckBox chkImageValue = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_value_tv);
				EditText editTextImageValue = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_value_et);

				CheckBox chkImageType = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_type_tv);
				Spinner spnImageType = (Spinner)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_type_spn);
				var imageTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageType);
				spnImageType.Adapter = imageTypeAdapter;

                CheckBox chkOASISAddress = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_oasis_address_chk);
                LinearLayout layoutOASISAddress = (LinearLayout)locationDetailsView.FindViewById(Resource.Id.get_way_points_oasis_address_layout);

				CheckBox chkCountryName = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_name_tv);
				EditText editTextCountryName = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_name_et);

				CheckBox chkCountryCode = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_code_tv);
				EditText editTextCountryCode = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_code_et);

				CheckBox chkPostalCode = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_postal_code_tv);
				EditText editTextPostalCode = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_postal_code_et);

				CheckBox chkAdministrativeArea = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_administrative_area_tv);
				EditText editTextAdministrativeArea = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_administrative_area_et);

				CheckBox chkSubAdministrativeArea = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_administrative_area_tv);
				EditText editTextSubAdministrativeArea = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_administrative_area_et);

				CheckBox chkLocality = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_locality_tv);
				EditText editTextLocality = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_locality_et);

				CheckBox chkSubLocality = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_locality_tv);
				EditText editTextSubLocality = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_locality_et);

				CheckBox chkThoruoghFare = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_thorough_fare_tv);
				EditText editTextThoruoghFare = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_thorough_fare_et);

				CheckBox chkSubThoruoghFare = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_thorough_fare_tv);
				EditText editTextSubThoruoghFare = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_thorough_fare_et);

                chkCoordinate.CheckedChange += (sender, e) => 
                {
                    chkLatitudeDegree.Enabled = e.IsChecked;
                    editTextLatitudeDegree.Enabled = (e.IsChecked && chkLatitudeDegree.Checked);
                    chkLongitudeDegree.Enabled = e.IsChecked;
                    editTextLongitudeDegree.Enabled = (e.IsChecked && chkLongitudeDegree.Checked);
                };

                chkLatitudeDegree.CheckedChange += (sender, e) => 
                {
                    editTextLatitudeDegree.Enabled = e.IsChecked;
                };

				chkLongitudeDegree.CheckedChange += (sender, e) =>
				{
					editTextLongitudeDegree.Enabled = e.IsChecked;
				};

				chkLocationName.CheckedChange += (sender, e) =>
				{
					editTextLocationName.Enabled = e.IsChecked;
				};

				chkAddress.CheckedChange += (sender, e) =>
				{
					editTextAddressLine1.Enabled = e.IsChecked;
                    editTextAddressLine2.Enabled = e.IsChecked;
                    editTextAddressLine3.Enabled = e.IsChecked;
                    editTextAddressLine4.Enabled = e.IsChecked;
				};

				chkLocationDescription.CheckedChange += (sender, e) =>
				{
					editTextLocationDescription.Enabled = e.IsChecked;
				};

				chkPhoneNumber.CheckedChange += (sender, e) =>
				{
					editTextPhoneNumber.Enabled = e.IsChecked;
				};

				chkLocationImage.CheckedChange += (sender, e) =>
				{
                    chkImageValue.Enabled = e.IsChecked;
					editTextImageValue.Enabled = (e.IsChecked && chkImageValue.Checked);
                    chkImageType.Enabled = e.IsChecked;
                    spnImageType.Enabled = (e.IsChecked && chkImageType.Checked);
				};

				chkImageValue.CheckedChange += (sender, e) =>
				{
					editTextImageValue.Enabled = e.IsChecked;
				};

				chkImageType.CheckedChange += (sender, e) =>
				{
					spnImageType.Enabled = e.IsChecked;
				};

				chkOASISAddress.CheckedChange += (sender, e) =>
				{
                    chkCountryName.Enabled = e.IsChecked;
                    chkCountryCode.Enabled = e.IsChecked;
                    chkPostalCode.Enabled = e.IsChecked;
                    chkAdministrativeArea.Enabled = e.IsChecked;
                    chkSubAdministrativeArea.Enabled = e.IsChecked;
                    chkLocality.Enabled = e.IsChecked;
                    chkSubLocality.Enabled = e.IsChecked;
                    chkThoruoghFare.Enabled = e.IsChecked;
                    chkSubThoruoghFare.Enabled = e.IsChecked;
                    editTextCountryName.Enabled = (e.IsChecked && chkCountryName.Checked);
                    editTextCountryCode.Enabled = (e.IsChecked && chkCountryCode.Checked);
                    editTextPostalCode.Enabled = (e.IsChecked && chkPostalCode.Checked);
                    editTextAdministrativeArea.Enabled = (e.IsChecked && chkAdministrativeArea.Checked);
                    editTextSubAdministrativeArea.Enabled = (e.IsChecked && chkSubAdministrativeArea.Checked);
                    editTextLocality.Enabled = (e.IsChecked && chkLocality.Checked);
                    editTextSubLocality.Enabled = (e.IsChecked && chkSubLocality.Checked);
                    editTextThoruoghFare.Enabled = (e.IsChecked && chkThoruoghFare.Checked);
                    editTextSubThoruoghFare.Enabled = (e.IsChecked && chkSubThoruoghFare.Checked);
				};

				chkCountryName.CheckedChange += (sender, e) =>
				{
					editTextCountryName.Enabled = e.IsChecked;
				};

				chkCountryCode.CheckedChange += (sender, e) =>
				{
					editTextCountryCode.Enabled = e.IsChecked;
				};

				chkPostalCode.CheckedChange += (sender, e) =>
				{
					editTextPostalCode.Enabled = e.IsChecked;
				};

				chkAdministrativeArea.CheckedChange += (sender, e) =>
				{
					editTextAdministrativeArea.Enabled = e.IsChecked;
				};

				chkSubAdministrativeArea.CheckedChange += (sender, e) =>
				{
					editTextSubAdministrativeArea.Enabled = e.IsChecked;
				};

				chkLocality.CheckedChange += (sender, e) =>
				{
					editTextLocality.Enabled = e.IsChecked;
				};

				chkSubLocality.CheckedChange += (sender, e) =>
				{
					editTextSubLocality.Enabled = e.IsChecked;
				};

				chkThoruoghFare.CheckedChange += (sender, e) =>
				{
					editTextThoruoghFare.Enabled = e.IsChecked;
				};

				chkSubThoruoghFare.CheckedChange += (sender, e) =>
				{
					editTextSubThoruoghFare.Enabled = e.IsChecked;
				};

				locationDetailsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
			   {
				   Coordinate coordinate = new Coordinate();

                    if (chkLatitudeDegree.Checked) 
                    {
                        if (editTextLatitudeDegree.Text != null && editTextLatitudeDegree.Text.Length > 0)
					        coordinate.latitudeDegrees = Java.Lang.Float.ParseFloat(editTextLatitudeDegree.Text);
                    }
                    if (chkLongitudeDegree.Checked)
                    {
					   if (editTextLongitudeDegree.Text != null && editTextLongitudeDegree.Text.Length > 0)
						   coordinate.longitudeDegrees = Java.Lang.Float.ParseFloat(editTextLongitudeDegree.Text);
                    }

				   List<string> addressLines = new List<string>();
                    if (chkAddress.Checked) 
                    {
					   addressLines.Add(editTextAddressLine1.Text);
					   addressLines.Add(editTextAddressLine2.Text);
					   addressLines.Add(editTextAddressLine3.Text);
                        addressLines.Add(editTextAddressLine4.Text);
                    }

				   Image locationImage = new Image();
                    if (chkImageType.Checked)
                    {
                        locationImage.imageType = (ImageType)spnResultCode.SelectedItemPosition;
                    }
                    if (chkImageValue.Checked)
                    {
                        locationImage.value = editTextImageValue.Text;
                    }

				   OASISAddress searchAddress = new OASISAddress();
                    if (chkCountryName.Checked)
                    {
                        searchAddress.countryName = editTextCountryName.Text;
                    }
                    if (chkCountryCode.Checked)
                    {
                        searchAddress.countryCode = editTextCountryCode.Text;
                    }
                    if (chkPostalCode.Checked)
                    {
                        searchAddress.postalCode = editTextPostalCode.Text;
                    }
                    if (chkAdministrativeArea.Checked)
                    {
                        searchAddress.administrativeArea = editTextAdministrativeArea.Text;
                    }
                    if (chkSubAdministrativeArea.Checked)
                    {
                        searchAddress.subAdministrativeArea = editTextSubAdministrativeArea.Text;
                    }
                    if (chkLocality.Checked)
				   {
                        searchAddress.locality = editTextLocality.Text;
				   }
                    if (chkSubLocality.Checked)
				   {
                        searchAddress.subLocality = editTextSubLocality.Text;
				   }
                    if (chkThoruoghFare.Checked)
				   {
                        searchAddress.thoroughfare = editTextThoruoghFare.Text;
				   }
                    if (chkSubThoruoghFare.Checked)
				   {
                        searchAddress.subThoroughfare = editTextSubThoruoghFare.Text;
				   }
				   
				   LocationDetails lctnDetails = new LocationDetails();
                    if (chkCoordinate.Checked)
                    {
                        lctnDetails.coordinate = coordinate;
                    }
                    if (chkLocationName.Checked)
                    {
                        lctnDetails.locationName = editTextLocationName.Text;
                    }
                    if (chkAddress.Checked)
                    {
                        lctnDetails.addressLines = addressLines;
                    }
                    if (chkLocationDescription.Checked)
                    {
                        lctnDetails.locationDescription = editTextLocationDescription.Text;
                    }
                    if (chkPhoneNumber.Checked)
                    {
                        lctnDetails.phoneNumber = editTextPhoneNumber.Text;
                    }
                    if (chkLocationImage.Checked)
                    {
                        lctnDetails.locationImage = locationImage;
                    }
                    if (chkOASISAddress.Checked)
                    {
                        lctnDetails.searchAddress = searchAddress;
                    }
				   
				   wayPoints.Add(lctnDetails);
                    adapter.NotifyDataSetChanged();
			   });

                locationDetailsAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			   {
				   locationDetailsAlertDialog.Dispose();
			   });

				locationDetailsAlertDialog.Show();

			};

            getSystemInfoRpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				getSystemInfoRpcAlertDialog.Dispose();
			});

            getSystemInfoRpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeChk.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				List<LocationDetails> finalWayPoints = null;
				if (addLocationDetailsChk.Checked)
				{
					finalWayPoints = wayPoints;
				}
				RpcMessage rpcMessage = BuildRpc.buildNavGetWayPointsResponse(BuildRpc.getNextId(), resltCode, finalWayPoints);
				AppUtils.savePreferenceValueForRpc(Context, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});

            getSystemInfoRpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
		   {
                AppUtils.removeSavedPreferenceValueForRpc(Context, tmpObj.getMethod());
		   });

			getSystemInfoRpcAlertDialog.Show();
		}

		private void CreateNavigationResponseSendLocation()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseSendLocation);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rsltCode.CheckedChange += (sender, e) => 
            {
                spnGeneric.Enabled = e.IsChecked;
            };

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCode.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }
				rpcResponse = BuildRpc.buildNavSendLocationResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseShowConstantTBT()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseShowConstantTBT);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rsltCode.CheckedChange += (sender, e) => 
            {
                spnGeneric.Enabled = e.IsChecked;
            };

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
                rpcResponse = BuildRpc.buildNavShowConstantTBTResponse(BuildRpc.getNextId(), selectedResultCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
   			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStartAudioStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStartAudioStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStartAudioStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStartStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStartStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStartStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStopAudioStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStopAudioStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStopAudioStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStopStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStopStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStopStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseSubscribeWayPoints()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseSubscribeWayPoints);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavSubscribeWayPointsResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseUnsubscribeWayPoints()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseUnsubscribeWayPoints);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				RpcResponse rpcResponse = BuildRpc.buildNavUnsubscribeWayPointsResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseUpdateTurnList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseUpdateTurnList);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavUpdateTurnListResponse(BuildRpc.getNextId(), (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationNotificationOnTBTClientState()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationNotificationOnTBTClientState);

			CheckBox tbtStateCheck = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, tbtStateArray);
			spnGeneric.Adapter = adapter;

            tbtStateCheck.Text = "TBT State";
            tbtStateCheck.CheckedChange += (sender, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState tmpObj = new HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
                if (tmpObj.getState() != null)
                    spnGeneric.SetSelection((int)tmpObj.getState());
                else
                    tbtStateCheck.Checked = false;
            }
            
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                TBTState? state = null;
                if (tbtStateCheck.Checked)
                {
                    state = (TBTState)spnGeneric.SelectedItemPosition;
                }
				RequestNotifyMessage rpcMessage = BuildRpc.buildNavigationOnTBTClientState(state);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateButtonsNotificationOnButtonPress()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.on_button, null);
			rpcAlertDialog.SetView(rpcView);

            CheckBox buttonNameChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_button_name_tv);
			Spinner spnButtonName = (Spinner)rpcView.FindViewById(Resource.Id.on_button_button_name_spn);

			CheckBox buttonModeChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_mode_tv);
			Spinner spnButtonMode = (Spinner)rpcView.FindViewById(Resource.Id.on_button_mode_spn);

			CheckBox customButtonIdChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_custom_button_id_tv);
			EditText editFieldCustomButton = (EditText)rpcView.FindViewById(Resource.Id.on_button_custom_button_spn);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.on_button_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_button_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.on_button_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_registered_apps);

			buttonNameChk.CheckedChange += (sender, e) => spnButtonName.Enabled = e.IsChecked;
			buttonModeChk.CheckedChange += (sender, e) => spnButtonMode.Enabled = e.IsChecked;
			customButtonIdChk.CheckedChange += (sender, e) => editFieldCustomButton.Enabled = e.IsChecked;

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			rpcAlertDialog.SetTitle(ButtonsNotificationOnButtonPress);
			buttonModeChk.Text = "Button Press Mode";

			var buttonNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
			spnButtonName.Adapter = buttonNamesAdapter;

			var buttonPressModeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonPressMode);
			spnButtonMode.Adapter = buttonPressModeAdapter;

			HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress();
			tmpObj = (HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getName() != null)
				{
					spnButtonName.SetSelection((int)tmpObj.getName());
				}
				else
					buttonNameChk.Checked = false;
				if (null != tmpObj.getMode())
				{
					spnButtonMode.SetSelection((int)tmpObj.getMode());
				}
				else
					buttonModeChk.Checked = false;
				if (null != tmpObj.getCustomButtonID())
				{
					editFieldCustomButton.Text = tmpObj.getCustomButtonID().ToString();
				}
				else
					customButtonIdChk.Checked = false;
				if (null != tmpObj.getAppId())
				{
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				}
				else
					appIDCheckBox.Checked = false;
			}

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				}
				ButtonName? buttonName = null;
				if (buttonNameChk.Checked)
				{
					buttonName = (ButtonName)spnButtonName.SelectedItemPosition;
				}
				ButtonPressMode? mode = null;
				if (buttonModeChk.Checked)
				{
					mode = (ButtonPressMode)spnButtonMode.SelectedItemPosition;
				}
				int? customButtonID = null;
				if (customButtonIdChk.Checked)
				{
					if (editFieldCustomButton.Text != null && editFieldCustomButton.Text.Length > 0)
					{
						customButtonID = Java.Lang.Integer.ParseInt(editFieldCustomButton.Text);
					}
				}
				RequestNotifyMessage rpcMessage = BuildRpc.buildButtonsOnButtonPress(buttonName, mode, customButtonID, selectedAppID);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateButtonsNotificationOnButtonEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_button, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(ButtonsNotificationOnButtonEvent);

            CheckBox buttonNameChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_button_name_tv);
			Spinner spnButtonName = (Spinner)rpcView.FindViewById(Resource.Id.on_button_button_name_spn);

			CheckBox eventModeChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_mode_tv);
			Spinner spnButtonMode = (Spinner)rpcView.FindViewById(Resource.Id.on_button_mode_spn);

            CheckBox customButtonIdChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_custom_button_id_tv);
			EditText editFieldCustomButton = (EditText)rpcView.FindViewById(Resource.Id.on_button_custom_button_spn);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.on_button_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_button_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.on_button_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_registered_apps);

            buttonNameChk.CheckedChange += (sender, e) => spnButtonName.Enabled = e.IsChecked;
			eventModeChk.CheckedChange += (sender, e) => spnButtonMode.Enabled = e.IsChecked;
			customButtonIdChk.CheckedChange += (sender, e) => editFieldCustomButton.Enabled = e.IsChecked;
			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var buttonEventModeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonEventMode);
			spnButtonMode.Adapter = buttonEventModeAdapter;

			var buttonNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
			spnButtonName.Adapter = buttonNamesAdapter;

            HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent tmpObj = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent();
			tmpObj = (HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getName() != null)
				{
					spnButtonName.SetSelection((int)tmpObj.getName());
				}
				else
					buttonNameChk.Checked = false;

				if (null != tmpObj.getMode())
                {
                    spnButtonMode.SetSelection((int)tmpObj.getMode());
                }
				else
					eventModeChk.Checked = false;

				if (null != tmpObj.getCustomButtonID())
				{
					editFieldCustomButton.Text = tmpObj.getCustomButtonID().ToString();
				}
				else
					customButtonIdChk.Checked = false;

				if (null != tmpObj.getAppId())
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
				else
					appIDCheckBox.Checked = false;

			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				}
                ButtonName? buttonName = null;
                if (buttonNameChk.Checked)
                {
                    buttonName = (ButtonName)spnButtonName.SelectedItemPosition;
                }
                ButtonEventMode? mode = null;
                if (eventModeChk.Checked)
                {
                    mode = (ButtonEventMode)spnButtonMode.SelectedItemPosition;
                }
                int? customButtonID = null;
                if (customButtonIdChk.Checked)
                {
                    if (editFieldCustomButton.Text != null && editFieldCustomButton.Text.Length > 0)
                    {
                        customButtonID = Java.Lang.Integer.ParseInt(editFieldCustomButton.Text);
                    }
				}
                RequestNotifyMessage rpcMessage = BuildRpc.buildButtonsOnButtonEvent(buttonName, mode, customButtonID, selectedAppID);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

        void CreateBCNotificationOnSystemInfoChanged()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox languageCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            spnGeneric.Adapter = adapter;

            languageCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
				{
				if (tmpObj.getLanguage() != null)
                spnGeneric.SetSelection((int)tmpObj.getLanguage());
				else
				languageCheckbox.Checked = false;
			}
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            languageCheckbox.Text = "Language";
			
			rpcAlertDialog.SetTitle(BCNotificationOnSystemInfoChanged);
            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Language? language = null;
                if (languageCheckbox.Checked)
                {
                    language = (Language)spnGeneric.SelectedItemPosition;
                }

                RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnSystemInfoChanged(language);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                 }
             });
            rpcAlertDialog.Show();
        }

		void CreateBCNotificationOnUpdateDeviceList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnUpdateDeviceList);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnUpdateDeviceList();
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				
			});
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnSystemRequest()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View getSystemRequestRpcView = layoutInflater.Inflate(Resource.Layout.on_system_request, null);

			rpcAlertDialog.SetView(getSystemRequestRpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnSystemRequest);

            CheckBox checkBoxRequestType = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_request_type_check);
			Spinner spnRequestType = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.request_type_spn);

            CheckBox checkBoxRequestSubType = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.request_subtype_check);
            EditText editTextRequestSubType = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.request_subtype_et);

			CheckBox checkBoxURL = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_url_check);
			EditText editTextURL = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.url_et);

			CheckBox checkBoxFileType = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_file_type_check);
			Spinner spnFileType = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.file_type_spn);

			CheckBox checkBoxOffset = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_offset_check);
			EditText editTextOffset = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.offset_et);

			CheckBox checkBoxLength = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_length_check);
			EditText editTextLength = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.length_et);

			CheckBox checkBoxTimeOut = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_timeout_check);
			EditText editTextTimeOut = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.time_out_et);

			CheckBox checkBoxFileName = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_filename_check);
			EditText editTextFileName = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.file_name_et);

			CheckBox appIDCheckBox = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_check);

			EditText manualAppIdEditText = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = getSystemRequestRpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_system_request_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(Resource.Id.bc_on_system_request_app_id_manual);
			RadioButton registerdAppIdRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(Resource.Id.bc_on_system_request_app_id_registered_apps);

            checkBoxRequestType.CheckedChange += (s, e) => spnRequestType.Enabled = e.IsChecked;
            checkBoxRequestSubType.CheckedChange += (s, e) => editTextRequestSubType.Enabled = e.IsChecked;
            checkBoxURL.CheckedChange += (s, e) => editTextURL.Enabled = e.IsChecked;
            checkBoxFileType.CheckedChange += (s, e) => spnFileType.Enabled = e.IsChecked;
            checkBoxOffset.CheckedChange += (s, e) => editTextOffset.Enabled = e.IsChecked;
            checkBoxLength.CheckedChange += (s, e) => editTextLength.Enabled = e.IsChecked;
            checkBoxTimeOut.CheckedChange += (s, e) => editTextTimeOut.Enabled = e.IsChecked;
            checkBoxFileName.CheckedChange += (s, e) => editTextFileName.Enabled = e.IsChecked;

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var requsetTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, requestType);
			spnRequestType.Adapter = requsetTypeAdapter;

			var fileTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, fileType);
			spnFileType.Adapter = fileTypeAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getRequestType() != null)
				{
					spnRequestType.SetSelection((int)tmpObj.getRequestType());
				}
				else
                    checkBoxRequestType.Checked = false;

                if (tmpObj.getRequestSubType() != null)
                    editTextRequestSubType.Text = tmpObj.getRequestSubType();
                else
                    checkBoxRequestSubType.Checked = false;

				if (tmpObj.getUrl() != null)
				    editTextURL.Text = tmpObj.getUrl();		
				else
					checkBoxURL.Checked = false;
				if (tmpObj.getFileType() != null)
					spnFileType.SetSelection((int)tmpObj.getFileType());
				else
					checkBoxFileType.Checked = false;

                if (tmpObj.getOffset() != null)
				    editTextOffset.Text = tmpObj.getOffset().ToString();
				else
					checkBoxOffset.Checked = false;

				if (tmpObj.getLength() != null)
				    editTextLength.Text = tmpObj.getLength().ToString();
				else
					checkBoxLength.Checked = false;

				if (tmpObj.getTimeout() != null)
				    editTextTimeOut.Text = tmpObj.getTimeout().ToString();
				else
					checkBoxTimeOut.Checked = false;

				if (tmpObj.getFileName() != null)
				    editTextFileName.Text = tmpObj.getFileName();
				else
					checkBoxFileName.Checked = false;

				if (tmpObj.getAppId() != null)
				    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				else
					appIDCheckBox.Checked = false;
			}

			
			
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 String url = null;
                 if (checkBoxURL.Checked)
                     if (editTextURL.Text != null && editTextURL.Text.Length > 0)
                         url = editTextURL.Text;

                String fileName = null;
                if (checkBoxFileName.Checked)
                     if (editTextFileName.Text != null && editTextFileName.Text.Length > 0)
                    fileName = editTextFileName.Text;

                String requestSubtype = null;
                 if (checkBoxRequestSubType.Checked)
                     if (editTextRequestSubType.Text != null && editTextRequestSubType.Text.Length > 0)
                    requestSubtype = editTextRequestSubType.Text;
                
                 RequestType? selectedRequestType = null;
                 if (checkBoxRequestType.Checked)
                 {
                     selectedRequestType = (RequestType)spnRequestType.SelectedItemPosition;
                 }

                 FileType? selectedFileType = null;
                 if (checkBoxFileType.Checked)
                 {
                     selectedFileType = (FileType)spnFileType.SelectedItemPosition;
                 }

                 int? offset = null, length = null, timeOut = null;
                 if (checkBoxOffset.Checked)
                     if (editTextOffset.Text != null && editTextOffset.Text.Length > 0)
                         offset = Java.Lang.Integer.ParseInt(editTextOffset.Text);
                 if (checkBoxLength.Checked)
                     if (editTextLength.Text != null && editTextLength.Text.Length > 0)
                         length = Java.Lang.Integer.ParseInt(editTextLength.Text);
                 if (checkBoxTimeOut.Checked)
                     if (editTextTimeOut.Text != null && editTextTimeOut.Text.Length > 0)
                         timeOut = Java.Lang.Integer.ParseInt(editTextTimeOut.Text);

                 RequestNotifyMessage rpcMessage = null;
                rpcMessage = BuildRpc.buildBasicCommunicationOnSystemRequestNotification(selectedRequestType, url, selectedFileType, offset, length, timeOut, fileName, selectedAppID, requestSubtype);

                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnStartDeviceDiscovery()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnStartDeviceDiscovery);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnStartDeviceDiscovery();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnReady()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnReady);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnReady();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnPhoneCall()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox checkBoxIsActive = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);
			rpcAlertDialog.SetTitle(BCNotificationOnPhoneCall);
			checkBoxIsActive.Text = "Active";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{				
                checkBoxIsActive.Checked = (bool)tmpObj.getActive();
				if (tmpObj.getActive() == null)
				{
					checkBoxIsActive.Checked = false;
				}
			}
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});		

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnPhoneCallNotification(checkBoxIsActive.Checked);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnIgnitionCycleOver()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnIgnitionCycleOver);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnIgnitionCycleOver();
				AppUtils.savePreferenceValueForRpc(Activity, ((RequestNotifyMessage)rpcMessage).getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}
        private void CreateBCNotificationOnSystemTimeReady()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnSystemTimeReady);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemTimeReady tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemTimeReady();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemTimeReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemTimeReady>(Activity, tmpObj.getMethod());

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnSystemTimeReady();
                AppUtils.savePreferenceValueForRpc(Activity, ((RequestNotifyMessage)rpcMessage).getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }
		private void CreateBCNotificationOnFindApplications()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_device_chosen, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnFindApplications);

            CheckBox checkboxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.device_info_chk);
			CheckBox checkboxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.device_name_cb);
			EditText editFieldDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name);

			CheckBox checkboxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.device_id_cb);
			EditText editFieldDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id);

			CheckBox checkboxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.transport_type_cb);
			Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type);

			CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.is_sdl_allowed);

			checkboxDeviceInfo.CheckedChange += (sender, e) =>
			{
				checkboxDeviceName.Enabled = e.IsChecked;
				editFieldDeviceName.Enabled = e.IsChecked;
				checkboxDeviceId.Enabled = e.IsChecked;
				editFieldDeviceId.Enabled = e.IsChecked;
				checkboxTransportType.Enabled = e.IsChecked;
				spnTransportType.Enabled = e.IsChecked;
				checkBoxIsSDLAllowed.Enabled = e.IsChecked;
			};
            checkboxDeviceName.CheckedChange += (s, e) => editFieldDeviceName.Enabled = e.IsChecked;
            checkboxDeviceId.CheckedChange += (s, e) => editFieldDeviceId.Enabled = e.IsChecked;
            checkboxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;

			var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
			spnTransportType.Adapter = transportTypeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications>(Activity, tmpObj.getMethod());
			if (tmpObj != null && tmpObj.getDeviceInfo()!=null)
			{
				

				
				if (tmpObj.getDeviceInfo().getName() == null)
				{
					checkboxDeviceName.Checked = false;
				}
				else
					editFieldDeviceName.Text = tmpObj.getDeviceInfo().getName();
				if (tmpObj.getDeviceInfo().getId() == null)
				{
					checkboxDeviceId.Checked = false;
				}
				else
					editFieldDeviceId.Text = tmpObj.getDeviceInfo().getId();
				if (tmpObj.getDeviceInfo().getTransportType() == null)
				{
					checkboxTransportType.Checked = false;
				}
				else
					spnTransportType.SetSelection((int)tmpObj.getDeviceInfo().getTransportType());
				if (tmpObj.getDeviceInfo().getIsSDLAllowed() == null)
				{
					checkBoxIsSDLAllowed.Checked = false;
				}
				else
					checkBoxIsSDLAllowed.Checked = (bool)tmpObj.getDeviceInfo().getIsSDLAllowed();

			}
			else
				checkboxDeviceInfo.Checked = false;
			

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 DeviceInfo devInfo = new DeviceInfo();

				 if (checkboxDeviceName.Checked)
					 devInfo.name = editFieldDeviceName.Text;

				 if (checkboxDeviceId.Checked)
					 devInfo.id = editFieldDeviceId.Text;

				 if (checkboxTransportType.Checked)
					 devInfo.transportType = (TransportType)spnTransportType.SelectedItemPosition;

				 devInfo.isSDLAllowed = checkBoxIsSDLAllowed.Checked;

				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnFindApplicationsNotification(devInfo);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });


            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnExitApplication()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_exit_application, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_exit_application_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_exit_application_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_exit_application_app_id_registered_apps);

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

            CheckBox textViewAppExitReason = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_exit_reason_cb);

			Spinner spnAppExitReason = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_exit_reason);
			var appExitReasonAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, appsExitReason);
			spnAppExitReason.Adapter = appExitReasonAdapter;

            textViewAppExitReason.CheckedChange += (s, e) => spnAppExitReason.Enabled = e.IsChecked;

			rpcAlertDialog.SetTitle("OnExitApplication");

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getApplicationExitReason() != null)
                {
                    spnAppExitReason.SetSelection((int)tmpObj.getApplicationExitReason());
                }
				else
					textViewAppExitReason.Checked = false;
				if (tmpObj.getAppId() != null)
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
				else
					appIDCheckBox.Checked = false;
				
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{

				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                }
				ApplicationExitReason? applicationExitReason = null;
				if (textViewAppExitReason.Checked)
					applicationExitReason = (ApplicationExitReason)spnAppExitReason.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnExitApplication(applicationExitReason, selectedAppID);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			
			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnExitAllApplications()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appsCloseReasonCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner appsCloseReasonSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, appsCloseReason);
			appsCloseReasonSpn.Adapter = adapter;

            appsCloseReasonCheckBox.CheckedChange += (s, e) => appsCloseReasonSpn.Enabled = e.IsChecked;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null && tmpObj.getApplicationsCloseReason() != null)
			{
				 appsCloseReasonSpn.SetSelection((int)tmpObj.getApplicationsCloseReason());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			appsCloseReasonCheckBox.Text = "App Close Reason";

			rpcAlertDialog.SetTitle(BCNotificationOnExitAllApplications);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{				
				ApplicationsCloseReason? applicationCloseReason = null;
				if (appsCloseReasonCheckBox.Checked)
					applicationCloseReason = (ApplicationsCloseReason)appsCloseReasonSpn.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnExitAllApplications(applicationCloseReason);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                 }
			 });
			if (tmpObj != null && tmpObj.getApplicationsCloseReason() == null)
			{
				appsCloseReasonCheckBox.Checked = false;
			}
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnEventChanged()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(BCNotificationOnEventChanged);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAllow = (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox eventTypesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnEventType = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            eventTypesCheckBox.CheckedChange += (s, e) => spnEventType.Enabled = e.IsChecked;

			checkBoxAllow.Text = ("IsActive");
			checkBoxAllow.CheckedChange += (s, e) =>
			{
				switchAllow.Enabled = e.IsChecked;
			};

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, eventTypes);
			spnEventType.Adapter = adapter;

			eventTypesCheckBox.Text = "Event Types";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged>(adapter.Context, tmpObj.getMethod());
			
			if (tmpObj != null)
			{

				spnEventType.SetSelection((int)tmpObj.getEventName());
				if (tmpObj.getEventName() == null)
				{
					eventTypesCheckBox.Checked = false;
				}
				if (tmpObj.getActive() == null)
				{
					checkBoxAllow.Checked = false;
					switchAllow.Enabled = false;
				}
				else
				{
					switchAllow.Checked = (bool)tmpObj.getActive();
				}

			}
			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
		   {
			   HmiApiLib.Common.Enums.EventTypes? eventTypes = null;
			   if (eventTypesCheckBox.Checked)
				   eventTypes = (HmiApiLib.Common.Enums.EventTypes)spnEventType.SelectedItemPosition;

			   bool? toggleState = null;
			   if (checkBoxAllow.Checked)
			   {
				   toggleState = switchAllow.Checked;

			   }
			   RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnEventChangedNotification(eventTypes, toggleState);


			   AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			   AppInstanceManager.Instance.sendRpc(rpcMessage);

		   });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				 }
			 });
			
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			 {
				 rpcAlertDialog.Dispose();
			 });
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnEmergencyEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(BCNotificationOnEmergencyEvent);

			CheckBox checkBoxEnabled = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);
            checkBoxEnabled.Text = "Enabled";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{				
                checkBoxEnabled.Checked = (bool)tmpObj.getEnabled();
				if (tmpObj.getEnabled() == null)
				{
					checkBoxEnabled.Checked = false;
				}
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnEmergencyEventNotification(checkBoxEnabled.Checked);
				 AppUtils.savePreferenceValueForRpc(Activity, ((RequestNotifyMessage)rpcMessage).getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });
			
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnDeviceChosen()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_device_chosen, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnDeviceChosen);

            CheckBox checkboxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.device_info_chk);
            CheckBox checkboxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.device_name_cb);
			EditText editFieldDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name);

			CheckBox checkboxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.device_id_cb);
			EditText editFieldDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id);

			CheckBox checkboxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.transport_type_cb);
			Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type);

			CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.is_sdl_allowed);
            Switch toggleIsSDLAllowed = (Switch)rpcView.FindViewById(Resource.Id.is_sdl_allowed_tgl);

            checkboxDeviceInfo.CheckedChange += (sender, e) => 
            {
                checkboxDeviceName.Enabled = e.IsChecked;
                editFieldDeviceName.Enabled = e.IsChecked && checkboxDeviceName.Checked;
                checkboxDeviceId.Enabled = e.IsChecked;
                editFieldDeviceId.Enabled = e.IsChecked && checkboxDeviceId.Checked;
                checkboxTransportType.Enabled = e.IsChecked;
                spnTransportType.Enabled = e.IsChecked && checkboxTransportType.Checked;
                checkBoxIsSDLAllowed.Enabled = e.IsChecked;
                toggleIsSDLAllowed.Enabled = e.IsChecked && checkBoxIsSDLAllowed.Checked;
            };
            checkboxDeviceName.CheckedChange += (s, e) => editFieldDeviceName.Enabled = e.IsChecked;
            checkboxDeviceId.CheckedChange += (s, e) => editFieldDeviceId.Enabled = e.IsChecked;
            checkboxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;
            checkBoxIsSDLAllowed.CheckedChange += (s, e) => toggleIsSDLAllowed.Enabled = e.IsChecked;

            var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
			spnTransportType.Adapter = transportTypeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (null != tmpObj.getDeviceInfo())
                {
                    if (tmpObj.getDeviceInfo().getName() == null)
                        checkboxDeviceName.Checked = false;
                    else
                        editFieldDeviceName.Text = tmpObj.getDeviceInfo().getName();

                    if (null == tmpObj.getDeviceInfo().getId())
                        checkboxDeviceId.Checked = false;
                    else
                        editFieldDeviceId.Text = tmpObj.getDeviceInfo().getId();

                    if (tmpObj.getDeviceInfo().getTransportType() != null)
                        spnTransportType.SetSelection((int)tmpObj.getDeviceInfo().getTransportType());
                    else
                        checkboxTransportType.Checked = false;

                    if (null != tmpObj.getDeviceInfo().getIsSDLAllowed())
                        toggleIsSDLAllowed.Checked = (bool)tmpObj.getDeviceInfo().getIsSDLAllowed();
                    else
                        checkBoxIsSDLAllowed.Checked = false;
                }
                else
                    checkboxDeviceInfo.Checked = false;
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                DeviceInfo devInfo = null;
                if (checkboxDeviceInfo.Checked)
                {
                    devInfo = new DeviceInfo();

					 if (checkboxDeviceName.Checked)
						 devInfo.name = editFieldDeviceName.Text;

					 if (checkboxDeviceId.Checked)
						 devInfo.id = editFieldDeviceId.Text;

					 if (checkboxTransportType.Checked)
						 devInfo.transportType = (TransportType)spnTransportType.SelectedItemPosition;

					 devInfo.isSDLAllowed = checkBoxIsSDLAllowed.Checked;
                }

				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnDeviceChosenNotification(devInfo);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnDeactivateHMI()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox checkBoxIsDeactivated = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);
            Switch IsDeactivatedToggle = (Switch)rpcView.FindViewById(Resource.Id.on_deactivate_hmi_toggle);

            checkBoxIsDeactivated.CheckedChange += (s, e) => IsDeactivatedToggle.Enabled = e.IsChecked;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
                if (null != tmpObj.getDeactivatedStatus())
                    IsDeactivatedToggle.Checked = (bool)tmpObj.getDeactivatedStatus();
                else
                    checkBoxIsDeactivated.Checked = false;
            }
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle(BCNotificationOnDeactivateHMI);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
                 bool? isDeactivated = null;
                 if (checkBoxIsDeactivated.Checked)
                     isDeactivated = IsDeactivatedToggle.Checked;
                 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnDeactivateHMINotification(isDeactivated);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

        void CreateBCNotificationOnAwakeSDL()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL>(Activity, tmpObj.getMethod());

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(BCNotificationOnAwakeSDL);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAwakeSDL();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });
            rpcAlertDialog.Show();
        }

		private void CreateBCNotificationOnAppDeactivated()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);
			CheckBox windowIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_window_id_check);
			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
			registerdAppIdSpn.Enabled = false;
			EditText windowIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_window_id_et);

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

			windowIDCheckBox.CheckedChange += (s, e) =>
			{
				windowIdEditText.Enabled = e.IsChecked;


			};
			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated(); 
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				if (tmpObj.getAppId() == null)
				{
					appIDCheckBox.Checked = false;
				}
				if (tmpObj.getWindowId() == null)
				{
					windowIDCheckBox.Checked = false;
				}
				else
					windowIdEditText.Text = tmpObj.getWindowId().ToString();

			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle(BCNotificationOnAppDeactivated);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				 string name = manualRegisterdSelectedOptionRadioButton.Text;

				 int? selectedAppID = null;
				 int? windowID = null;

				 if (appIDCheckBox.Checked)
				 {
					 if (name.Equals("Registered Apps"))
						 if (registerdAppIdSpn.Adapter.Count == 0)
						 {
							 Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
							 return;
						 }
						 else
							 selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

					 else if (name.Equals("Manual"))
						 if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
							 selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				 }
				 if (windowIDCheckBox.Checked && windowIdEditText.Text!="")
				 {
					 windowID = Java.Lang.Integer.ParseInt(windowIdEditText.Text);
				 }

				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAppDeactivated(selectedAppID, windowID);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);


			 });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnAppActivated()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);
			CheckBox windowIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_window_id_check);
			EditText windowIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_window_id_et);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);
			windowIDCheckBox.CheckedChange += (s, e) =>
			{
				windowIdEditText.Enabled = e.IsChecked;


			};
			appIDCheckBox.CheckedChange += (s, e) =>
			{
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				if (tmpObj.getAppId() == null)
				{
					appIDCheckBox.Checked = false;
				}
				if (tmpObj.getWindowId() == null)
				{
					windowIDCheckBox.Checked = false;
				}
				else
					windowIdEditText.Text = tmpObj.getWindowId().ToString();
			}
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle("OnAppActivated");

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
				 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				 string name = manualRegisterdSelectedOptionRadioButton.Text;

				 int? selectedAppID = null;
				 int? windowID = null;

				 if (appIDCheckBox.Checked)
				 {
					 if (name.Equals("Registered Apps"))
					 {
						 if (registerdAppIdSpn.Adapter.Count == 0)
						 {
							 Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
							 return;
						 }
						 else
						 {
							 selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();
						 }
					 }
					 else if (name.Equals("Manual"))
					 {
						 if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
							 selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
					 }
				 }
				 if (windowIDCheckBox.Checked && windowIdEditText.Text!="")
				 {
					 windowID = Java.Lang.Integer.ParseInt(windowIdEditText.Text);

				 }
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAppActivated(selectedAppID, windowID);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}


        private void CreateVIResponseUnSubscribeVehicleData()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.subscribe_vehicle_data_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(VIResponseUnsubscribeVehicleData);

            var resultCode_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_result_code_chk);
            var gps_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps);
            var speed_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed);
            var rpm_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm);
            var fuelLevel_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level);
            var fuelLevel_State_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state);
            var instantFuelConsumption_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption);
            var externalTemperature_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature);
            var prndl_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl);
            var turnSignal_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal);
            var tirePressure_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure);
            var odometer_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer);
            var beltStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status);
            var bodyInformation_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info);
            var deviceStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status);
            var driverBraking_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking);
            var wiperStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status);
            var headLampStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status);
            var engineTorque_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque);
            var accPedalPosition_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos);
            var steeringWheelAngle_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle);
            var eCallInfo_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info);
            var airbagStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status);
            var emergencyEvent_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event);
            var clusterModes_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes);
            var myKey_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key);
			var electronicParkBrakeStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status);
            var engineOilLife_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life);
            var fuelRange_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range);

			var gps_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_data_type_chk);
            var speed_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_data_type_chk);
            var rpm_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_date_type_chk);
            var fuelLevel_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_data_type_chk);
            var fuelLevel_State_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_chk);
            var instantFuelConsumption_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_chk);
            var externalTemperature_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_data_type_chk);
            var prndl_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_data_type_chk);
            var turnSignal_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal_data_type_chk);
            var tirePressure_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_chk);
            var odometer_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_data_type_chk);
            var beltStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_data_type_chk);
            var bodyInformation_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_data_type_chk);
            var deviceStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_data_type_chk);
            var driverBraking_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_data_type_chk);
            var wiperStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_data_type_chk);
            var headLampStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_chk);
            var engineTorque_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_data_type_chk);
            var accPedalPosition_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_chk);
            var steeringWheelAngle_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_chk);
            var eCallInfo_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_data_type_chk);
            var airbagStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_data_type_chk);
            var emergencyEvent_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_data_type_chk);
            var clusterModes_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_chk);
            var myKey_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_data_type_chk);
			var electronicParkBrakeStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_chk);
            var engineOilLife_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_chk);
            var fuelRange_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_data_type_chk);

			var gps_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_result_code_chk);
            var speed_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_result_code_chk);
            var rpm_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_result_code_chk);
            var fuelLevel_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_result_code_chk);
            var fuelLevel_State_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_chk);
            var instantFuelConsumption_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_chk);
            var externalTemperature_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_result_code_chk);
            var prndl_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_result_code_chk);
            var turnSignal_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal_result_code_chk);
            var tirePressure_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_chk);
            var odometer_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_result_code_chk);
            var beltStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_result_code_chk);
            var bodyInformation_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_result_code_chk);
            var deviceStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_result_code_chk);
            var driverBraking_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_result_code_chk);
            var wiperStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_result_code_chk);
            var headLampStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_chk);
            var engineTorque_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_result_code_chk);
            var accPedalPosition_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_chk);
            var steeringWheelAngle_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_chk);
            var eCallInfo_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_result_code_chk);
            var airbagStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_result_code_chk);
            var emergencyEvent_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_result_code_chk);
            var clusterModes_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_chk);
            var myKey_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_result_code_chk);
			var electronicParkBrakeStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_chk);
            var engineOilLife_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_chk);
            var fuelRange_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_result_code_chk);

			var gps_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_data_type_spinner);
            var speed_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_data_type_spinner);
            var rpm_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_date_type_spinner);
            var fuelLevel_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_data_type_spinner);
            var fuelLevel_State_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_spinner);
            var instantFuelConsumption_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_spinner);
            var externalTemperature_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_data_type_spinner);
            var prndl_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_data_type_spinner);
            var turnSignal_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_turnSignal_data_type_spinner);
            var tirePressure_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_spinner);
            var odometer_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_data_type_spinner);
            var beltStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_data_type_spinner);
            var bodyInformation_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_data_type_spinner);
            var deviceStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_data_type_spinner);
            var driverBraking_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_data_type_spinner);
            var wiperStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_data_type_spinner);
            var headLampStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_spinner);
            var engineTorque_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_data_type_spinner);
            var accPedalPosition_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_spinner);
            var steeringWheelAngle_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_spinner);
            var eCallInfo_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_data_type_spinner);
            var airbagStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_data_type_spinner);
            var emergencyEvent_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_data_type_spinner);
            var clusterModes_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_spinner);
            var myKey_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_data_type_spinner);
			var electronicParkBrakeStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_spinner);
            var engineOilLife_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_spinner);
            var fuelRange_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_data_type_spinner);

			var result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_result_code_spinner);
            var gps_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_result_code_spinner);
            var speed_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_result_code_spinner);
            var rpm_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_result_code_spinner);
            var fuelLevel_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_result_code_spinner);
            var fuelLevel_State_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_spinner);
            var instantFuelConsumption_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_spinner);
            var externalTemperature_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_result_code_spinner);
            var prndl_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_result_code_spinner);
            var turnSignal_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_turnSignal_result_code_spinner);
            var tirePressure_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_spinner);
            var odometer_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_result_code_spinner);
            var beltStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_result_code_spinner);
            var bodyInformation_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_result_code_spinner);
            var deviceStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_result_code_spinner);
            var driverBraking_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_result_code_spinner);
            var wiperStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_result_code_spinner);
            var headLampStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_spinner);
            var engineTorque_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_result_code_spinner);
            var accPedalPosition_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_spinner);
            var steeringWheelAngle_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_spinner);
            var eCallInfo_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_result_code_spinner);
            var airbagStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_result_code_spinner);
            var emergencyEvent_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_result_code_spinner);
            var clusterModes_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_spinner);
            var myKey_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_result_code_spinner);
			var electronicParkBrakeStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_spinner);
            var engineOilLife_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_spinner);
            var fuelRange_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_result_code_spinner);

			resultCode_chk.CheckedChange += (sender, e) => result_code_spinner.Enabled = e.IsChecked;

            gps_chk.CheckedChange += (sender, e) =>
            {
                gps_data_type_chk.Enabled = e.IsChecked;
                gps_result_code_chk.Enabled = e.IsChecked;
                gps_data_type_spinner.Enabled = e.IsChecked && gps_data_type_chk.Checked;
                gps_result_code_spinner.Enabled = e.IsChecked && gps_result_code_chk.Checked;
            };

            speed_chk.CheckedChange += (sender, e) =>
            {
                speed_data_type_chk.Enabled = e.IsChecked;
                speed_result_code_chk.Enabled = e.IsChecked;
                speed_data_type_spinner.Enabled = e.IsChecked && speed_data_type_chk.Checked;
                speed_result_code_spinner.Enabled = e.IsChecked && speed_result_code_chk.Checked;
            };

            rpm_chk.CheckedChange += (sender, e) =>
            {
                rpm_data_type_chk.Enabled = e.IsChecked;
                rpm_result_code_chk.Enabled = e.IsChecked;
                rpm_data_type_spinner.Enabled = e.IsChecked && rpm_data_type_chk.Checked;
                rpm_result_code_spinner.Enabled = e.IsChecked && rpm_result_code_chk.Checked;
            };

            fuelLevel_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_data_type_spinner.Enabled = e.IsChecked && fuelLevel_data_type_chk.Checked;
                fuelLevel_result_code_spinner.Enabled = e.IsChecked && fuelLevel_result_code_chk.Checked;
            };

            fuelLevel_State_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_State_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_State_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_State_data_type_spinner.Enabled = e.IsChecked && fuelLevel_State_data_type_chk.Checked;
                fuelLevel_State_result_code_spinner.Enabled = e.IsChecked && fuelLevel_State_result_code_chk.Checked;
            };

            instantFuelConsumption_chk.CheckedChange += (sender, e) =>
            {
                instantFuelConsumption_data_type_chk.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_chk.Enabled = e.IsChecked;
                instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked && instantFuelConsumption_data_type_chk.Checked;
                instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked && instantFuelConsumption_result_code_chk.Checked;
            };

            externalTemperature_chk.CheckedChange += (sender, e) =>
            {
                externalTemperature_data_type_chk.Enabled = e.IsChecked;
                externalTemperature_result_code_chk.Enabled = e.IsChecked;
                externalTemperature_data_type_spinner.Enabled = e.IsChecked && externalTemperature_data_type_chk.Checked;
                externalTemperature_result_code_spinner.Enabled = e.IsChecked && externalTemperature_result_code_chk.Checked;
            };

            prndl_chk.CheckedChange += (sender, e) =>
            {
                prndl_data_type_chk.Enabled = e.IsChecked;
                prndl_result_code_chk.Enabled = e.IsChecked;
                prndl_data_type_spinner.Enabled = e.IsChecked && prndl_data_type_chk.Checked;
                prndl_result_code_spinner.Enabled = e.IsChecked && prndl_result_code_chk.Checked;
            };

            turnSignal_chk.CheckedChange += (sender, e) =>
            {
                turnSignal_data_type_chk.Enabled = e.IsChecked;
                turnSignal_result_code_chk.Enabled = e.IsChecked;
                turnSignal_data_type_spinner.Enabled = e.IsChecked && turnSignal_data_type_chk.Checked;
                turnSignal_result_code_spinner.Enabled = e.IsChecked && turnSignal_result_code_chk.Checked;
            };

            tirePressure_chk.CheckedChange += (sender, e) =>
            {
                tirePressure_data_type_chk.Enabled = e.IsChecked;
                tirePressure_result_code_chk.Enabled = e.IsChecked;
                tirePressure_data_type_spinner.Enabled = e.IsChecked && tirePressure_data_type_chk.Checked;
                tirePressure_result_code_spinner.Enabled = e.IsChecked && tirePressure_result_code_chk.Checked;
            };

            odometer_chk.CheckedChange += (sender, e) =>
            {
                odometer_data_type_chk.Enabled = e.IsChecked;
                odometer_result_code_chk.Enabled = e.IsChecked;
                odometer_data_type_spinner.Enabled = e.IsChecked && odometer_data_type_chk.Checked;
                odometer_result_code_spinner.Enabled = e.IsChecked && odometer_result_code_chk.Checked;
            };

            beltStatus_chk.CheckedChange += (sender, e) =>
            {
                beltStatus_data_type_chk.Enabled = e.IsChecked;
                beltStatus_result_code_chk.Enabled = e.IsChecked;
                beltStatus_data_type_spinner.Enabled = e.IsChecked && beltStatus_data_type_chk.Checked;
                beltStatus_result_code_spinner.Enabled = e.IsChecked && beltStatus_result_code_chk.Checked;
            };

            bodyInformation_chk.CheckedChange += (sender, e) =>
            {
                bodyInformation_data_type_chk.Enabled = e.IsChecked;
                bodyInformation_result_code_chk.Enabled = e.IsChecked;
                bodyInformation_data_type_spinner.Enabled = e.IsChecked && bodyInformation_data_type_chk.Checked;
                bodyInformation_result_code_spinner.Enabled = e.IsChecked && bodyInformation_result_code_chk.Checked;
            };

            deviceStatus_chk.CheckedChange += (sender, e) =>
            {
                deviceStatus_data_type_chk.Enabled = e.IsChecked;
                deviceStatus_result_code_chk.Enabled = e.IsChecked;
                deviceStatus_data_type_spinner.Enabled = e.IsChecked && deviceStatus_data_type_chk.Checked;
                deviceStatus_result_code_spinner.Enabled = e.IsChecked && deviceStatus_result_code_chk.Checked;
            };

            driverBraking_chk.CheckedChange += (sender, e) =>
            {
                driverBraking_data_type_chk.Enabled = e.IsChecked;
                driverBraking_result_code_chk.Enabled = e.IsChecked;
                driverBraking_data_type_spinner.Enabled = e.IsChecked && driverBraking_data_type_chk.Checked;
                driverBraking_result_code_spinner.Enabled = e.IsChecked && driverBraking_result_code_chk.Checked;
            };

            wiperStatus_chk.CheckedChange += (sender, e) =>
            {
                wiperStatus_data_type_chk.Enabled = e.IsChecked;
                wiperStatus_result_code_chk.Enabled = e.IsChecked;
                wiperStatus_data_type_spinner.Enabled = e.IsChecked && wiperStatus_data_type_chk.Checked;
                wiperStatus_result_code_spinner.Enabled = e.IsChecked && wiperStatus_result_code_chk.Checked;
            };

            headLampStatus_chk.CheckedChange += (sender, e) =>
            {
                headLampStatus_data_type_chk.Enabled = e.IsChecked;
                headLampStatus_result_code_chk.Enabled = e.IsChecked;
                headLampStatus_data_type_spinner.Enabled = e.IsChecked && headLampStatus_data_type_chk.Checked;
                headLampStatus_result_code_spinner.Enabled = e.IsChecked && headLampStatus_result_code_chk.Checked;
            };

            engineTorque_chk.CheckedChange += (sender, e) =>
            {
                engineTorque_data_type_chk.Enabled = e.IsChecked;
                engineTorque_result_code_chk.Enabled = e.IsChecked;
                engineTorque_data_type_spinner.Enabled = e.IsChecked && engineTorque_data_type_chk.Checked;
                engineTorque_result_code_spinner.Enabled = e.IsChecked && engineTorque_result_code_chk.Checked;
            };

            accPedalPosition_chk.CheckedChange += (sender, e) =>
            {
                accPedalPosition_data_type_chk.Enabled = e.IsChecked;
                accPedalPosition_result_code_chk.Enabled = e.IsChecked;
                accPedalPosition_data_type_spinner.Enabled = e.IsChecked && accPedalPosition_data_type_chk.Checked;
                accPedalPosition_result_code_spinner.Enabled = e.IsChecked && accPedalPosition_result_code_chk.Checked;
            };

            steeringWheelAngle_chk.CheckedChange += (sender, e) =>
            {
                steeringWheelAngle_data_type_chk.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_chk.Enabled = e.IsChecked;
                steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked && steeringWheelAngle_data_type_chk.Checked;
                steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked && steeringWheelAngle_result_code_chk.Checked;
            };

            eCallInfo_chk.CheckedChange += (sender, e) =>
            {
                eCallInfo_data_type_chk.Enabled = e.IsChecked;
                eCallInfo_result_code_chk.Enabled = e.IsChecked;
                eCallInfo_data_type_spinner.Enabled = e.IsChecked && eCallInfo_data_type_chk.Checked;
                eCallInfo_result_code_spinner.Enabled = e.IsChecked && eCallInfo_result_code_chk.Checked;
            };

            airbagStatus_chk.CheckedChange += (sender, e) =>
            {
                airbagStatus_data_type_chk.Enabled = e.IsChecked;
                airbagStatus_result_code_chk.Enabled = e.IsChecked;
                airbagStatus_data_type_spinner.Enabled = e.IsChecked && airbagStatus_data_type_chk.Checked;
                airbagStatus_result_code_spinner.Enabled = e.IsChecked && airbagStatus_result_code_chk.Checked;
            };

            emergencyEvent_chk.CheckedChange += (sender, e) =>
            {
                emergencyEvent_data_type_chk.Enabled = e.IsChecked;
                emergencyEvent_result_code_chk.Enabled = e.IsChecked;
                emergencyEvent_data_type_spinner.Enabled = e.IsChecked && emergencyEvent_data_type_chk.Checked;
                emergencyEvent_result_code_spinner.Enabled = e.IsChecked && emergencyEvent_result_code_chk.Checked;
            };

            clusterModes_chk.CheckedChange += (sender, e) =>
            {
                clusterModes_data_type_chk.Enabled = e.IsChecked;
                clusterModes_result_code_chk.Enabled = e.IsChecked;
                clusterModes_data_type_spinner.Enabled = e.IsChecked && clusterModes_data_type_chk.Checked;
                clusterModes_result_code_spinner.Enabled = e.IsChecked && clusterModes_result_code_chk.Checked;
            };

            myKey_chk.CheckedChange += (sender, e) =>
            {
                myKey_data_type_chk.Enabled = e.IsChecked;
                myKey_result_code_chk.Enabled = e.IsChecked;
                myKey_data_type_spinner.Enabled = e.IsChecked && myKey_data_type_chk.Checked;
                myKey_result_code_spinner.Enabled = e.IsChecked && myKey_result_code_chk.Checked;
            };

            electronicParkBrakeStatus_chk.CheckedChange += (sender, e) =>
            {
                electronicParkBrakeStatus_data_type_chk.Enabled = e.IsChecked;
                electronicParkBrakeStatus_result_code_chk.Enabled = e.IsChecked;
                electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked && electronicParkBrakeStatus_data_type_chk.Checked;
                electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked && electronicParkBrakeStatus_result_code_chk.Checked;
            };

            engineOilLife_chk.CheckedChange += (sender, e) =>
            {
                engineOilLife_data_type_chk.Enabled = e.IsChecked;
                engineOilLife_result_code_chk.Enabled = e.IsChecked;
                engineOilLife_data_type_spinner.Enabled = e.IsChecked && engineOilLife_data_type_chk.Checked;
                engineOilLife_result_code_spinner.Enabled = e.IsChecked && engineOilLife_result_code_chk.Checked;
            };

            fuelRange_chk.CheckedChange += (sender, e) =>
            {
                fuelRange_data_type_chk.Enabled = e.IsChecked;
                fuelRange_result_code_chk.Enabled = e.IsChecked;
                fuelRange_data_type_spinner.Enabled = e.IsChecked && fuelRange_data_type_chk.Checked;
                fuelRange_result_code_spinner.Enabled = e.IsChecked && fuelRange_result_code_chk.Checked;
            };

			gps_data_type_chk.CheckedChange += (sender, e) => gps_data_type_spinner.Enabled = e.IsChecked;
            speed_data_type_chk.CheckedChange += (sender, e) => speed_data_type_spinner.Enabled = e.IsChecked;
            rpm_data_type_chk.CheckedChange += (sender, e) => rpm_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_data_type_chk.CheckedChange += (sender, e) => fuelLevel_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_State_data_type_chk.CheckedChange += (sender, e) => fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_data_type_chk.CheckedChange += (sender, e) => instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
            externalTemperature_data_type_chk.CheckedChange += (sender, e) => externalTemperature_data_type_spinner.Enabled = e.IsChecked;
            prndl_data_type_chk.CheckedChange += (sender, e) => prndl_data_type_spinner.Enabled = e.IsChecked;
            turnSignal_data_type_chk.CheckedChange += (sender, e) => turnSignal_data_type_spinner.Enabled = e.IsChecked;
            tirePressure_data_type_chk.CheckedChange += (sender, e) => tirePressure_data_type_spinner.Enabled = e.IsChecked;
            odometer_data_type_chk.CheckedChange += (sender, e) => odometer_data_type_spinner.Enabled = e.IsChecked;
            beltStatus_data_type_chk.CheckedChange += (sender, e) => beltStatus_data_type_spinner.Enabled = e.IsChecked;
            bodyInformation_data_type_chk.CheckedChange += (sender, e) => bodyInformation_data_type_spinner.Enabled = e.IsChecked;
            deviceStatus_data_type_chk.CheckedChange += (sender, e) => deviceStatus_data_type_spinner.Enabled = e.IsChecked;
            driverBraking_data_type_chk.CheckedChange += (sender, e) => driverBraking_data_type_spinner.Enabled = e.IsChecked;
            wiperStatus_data_type_chk.CheckedChange += (sender, e) => wiperStatus_data_type_spinner.Enabled = e.IsChecked;
            headLampStatus_data_type_chk.CheckedChange += (sender, e) => headLampStatus_data_type_spinner.Enabled = e.IsChecked;
            engineTorque_data_type_chk.CheckedChange += (sender, e) => engineTorque_data_type_spinner.Enabled = e.IsChecked;
            accPedalPosition_data_type_chk.CheckedChange += (sender, e) => accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_data_type_chk.CheckedChange += (sender, e) => steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
            eCallInfo_data_type_chk.CheckedChange += (sender, e) => eCallInfo_data_type_spinner.Enabled = e.IsChecked;
            airbagStatus_data_type_chk.CheckedChange += (sender, e) => airbagStatus_data_type_spinner.Enabled = e.IsChecked;
            emergencyEvent_data_type_chk.CheckedChange += (sender, e) => emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
            clusterModes_data_type_chk.CheckedChange += (sender, e) => clusterModes_data_type_spinner.Enabled = e.IsChecked;
            myKey_data_type_chk.CheckedChange += (sender, e) => myKey_data_type_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_data_type_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
            engineOilLife_data_type_chk.CheckedChange += (sender, e) => engineOilLife_data_type_spinner.Enabled = e.IsChecked;
            fuelRange_data_type_chk.CheckedChange += (sender, e) => fuelRange_data_type_spinner.Enabled = e.IsChecked;

			gps_result_code_chk.CheckedChange += (sender, e) => gps_result_code_spinner.Enabled = e.IsChecked;
            speed_result_code_chk.CheckedChange += (sender, e) => speed_result_code_spinner.Enabled = e.IsChecked;
            rpm_result_code_chk.CheckedChange += (sender, e) => rpm_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_result_code_chk.CheckedChange += (sender, e) => fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_State_result_code_chk.CheckedChange += (sender, e) => fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_result_code_chk.CheckedChange += (sender, e) => instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            externalTemperature_result_code_chk.CheckedChange += (sender, e) => externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            prndl_result_code_chk.CheckedChange += (sender, e) => prndl_result_code_spinner.Enabled = e.IsChecked;
            turnSignal_result_code_chk.CheckedChange += (sender, e) => turnSignal_result_code_spinner.Enabled = e.IsChecked;
            tirePressure_result_code_chk.CheckedChange += (sender, e) => tirePressure_result_code_spinner.Enabled = e.IsChecked;
            odometer_result_code_chk.CheckedChange += (sender, e) => odometer_result_code_spinner.Enabled = e.IsChecked;
            beltStatus_result_code_chk.CheckedChange += (sender, e) => beltStatus_result_code_spinner.Enabled = e.IsChecked;
            bodyInformation_result_code_chk.CheckedChange += (sender, e) => bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            deviceStatus_result_code_chk.CheckedChange += (sender, e) => deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            driverBraking_result_code_chk.CheckedChange += (sender, e) => driverBraking_result_code_spinner.Enabled = e.IsChecked;
            wiperStatus_result_code_chk.CheckedChange += (sender, e) => wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            headLampStatus_result_code_chk.CheckedChange += (sender, e) => headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            engineTorque_result_code_chk.CheckedChange += (sender, e) => engineTorque_result_code_spinner.Enabled = e.IsChecked;
            accPedalPosition_result_code_chk.CheckedChange += (sender, e) => accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_result_code_chk.CheckedChange += (sender, e) => steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            eCallInfo_result_code_chk.CheckedChange += (sender, e) => eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            airbagStatus_result_code_chk.CheckedChange += (sender, e) => airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            emergencyEvent_result_code_chk.CheckedChange += (sender, e) => emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            clusterModes_result_code_chk.CheckedChange += (sender, e) => clusterModes_result_code_spinner.Enabled = e.IsChecked;
            myKey_result_code_chk.CheckedChange += (sender, e) => myKey_result_code_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_result_code_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
            engineOilLife_result_code_chk.CheckedChange += (sender, e) => engineOilLife_result_code_spinner.Enabled = e.IsChecked;
            fuelRange_result_code_chk.CheckedChange += (sender, e) => fuelRange_result_code_spinner.Enabled = e.IsChecked;

			var vehicleDataTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataType);
            gps_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            speed_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            rpm_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelLevel_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelLevel_State_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            instantFuelConsumption_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            externalTemperature_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            prndl_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            turnSignal_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            tirePressure_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            odometer_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            beltStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            bodyInformation_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            deviceStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            driverBraking_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            wiperStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            headLampStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineTorque_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            accPedalPosition_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            steeringWheelAngle_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            eCallInfo_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            airbagStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            emergencyEvent_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            clusterModes_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			myKey_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			electronicParkBrakeStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineOilLife_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelRange_data_type_spinner.Adapter = vehicleDataTypeAdapter;

            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCode);
            result_code_spinner.Adapter = resultCodeAdapter;
            gps_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            speed_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            rpm_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelLevel_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelLevel_State_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            instantFuelConsumption_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            externalTemperature_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            prndl_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            turnSignal_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            tirePressure_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            odometer_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            beltStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            bodyInformation_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            deviceStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            driverBraking_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            wiperStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            headLampStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineTorque_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            accPedalPosition_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            steeringWheelAngle_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            eCallInfo_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            airbagStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            emergencyEvent_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            clusterModes_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			myKey_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			electronicParkBrakeStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineOilLife_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelRange_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }
			if (tmpObj != null)
			{
				result_code_spinner.SetSelection((int)tmpObj.getResultCode());
                if (null != tmpObj.getGps())
                {
                    if (null != tmpObj.getGps().getDataType())
                        gps_data_type_spinner.SetSelection((int)tmpObj.getGps().getDataType());
                    else
                        gps_data_type_chk.Checked = false;

                    if (null != tmpObj.getGps().getResultCode())
                        gps_result_code_spinner.SetSelection((int)tmpObj.getGps().getResultCode());
                    else
                        gps_result_code_chk.Checked = false;
                }
                else
                    gps_chk.Checked = false;

                if (null != tmpObj.getSpeed())
                {
                    if (null != tmpObj.getSpeed().getDataType())
                        speed_data_type_spinner.SetSelection((int)tmpObj.getSpeed().getDataType());
                    else
                        speed_data_type_chk.Checked = false;

                    if (null != tmpObj.getSpeed().getResultCode())
                        speed_result_code_spinner.SetSelection((int)tmpObj.getSpeed().getResultCode());
                    else
                        speed_result_code_chk.Checked = false;
                }
                else
                    speed_chk.Checked = false;

                if (null != tmpObj.getRpm())
                {
                    if (null != tmpObj.getRpm().getDataType())
                        rpm_data_type_spinner.SetSelection((int)tmpObj.getRpm().getDataType());
                    else
                        rpm_data_type_chk.Checked = false;

                    if (null != tmpObj.getRpm().getResultCode())
                        rpm_result_code_spinner.SetSelection((int)tmpObj.getRpm().getResultCode());
                    else
                        rpm_result_code_chk.Checked = false;
                }
                else
                    rpm_chk.Checked = false;

                if (null != tmpObj.getFuelLevel())
                {
                    if (null != tmpObj.getFuelLevel().getDataType())
                        fuelLevel_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel().getDataType());
                    else
                        fuelLevel_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelLevel().getResultCode())
                        fuelLevel_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel().getResultCode());
                    else
                        fuelLevel_result_code_chk.Checked = false;
                }
                else
                    fuelLevel_chk.Checked = false;

                if (null != tmpObj.getFuelLevel_State())
                {
                    if (null != tmpObj.getFuelLevel_State().getDataType())
                        fuelLevel_State_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getDataType());
                    else
                        fuelLevel_State_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelLevel_State().getResultCode())
                        fuelLevel_State_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());
                    else
                        fuelLevel_State_result_code_chk.Checked = false;
                }
                else
                    fuelLevel_State_chk.Checked = false;

                if (null != tmpObj.getInstantFuelConsumption())
                {
                    if (null != tmpObj.getInstantFuelConsumption().getDataType())
                        instantFuelConsumption_data_type_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getDataType());
                    else
                        instantFuelConsumption_data_type_chk.Checked = false;

                    if (null != tmpObj.getInstantFuelConsumption().getResultCode())
                        instantFuelConsumption_result_code_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getResultCode());
                    else
                        instantFuelConsumption_result_code_chk.Checked = false;
                }
                else
                    instantFuelConsumption_chk.Checked = false;

                if (null != tmpObj.getExternalTemperature())
                {
                    if (null != tmpObj.getExternalTemperature().getDataType())
                        externalTemperature_data_type_spinner.SetSelection((int)tmpObj.getExternalTemperature().getDataType());
                    else
                        externalTemperature_data_type_chk.Checked = false;

                    if (null != tmpObj.getExternalTemperature().getResultCode())
                        externalTemperature_result_code_spinner.SetSelection((int)tmpObj.getExternalTemperature().getResultCode());
                    else
                        externalTemperature_result_code_chk.Checked = false;
                }
                else
                    externalTemperature_chk.Checked = false;

                if (null != tmpObj.getPrndl())
                {
                    if (null != tmpObj.getPrndl().getDataType())
                        prndl_data_type_spinner.SetSelection((int)tmpObj.getPrndl().getDataType());
                    else
                        prndl_data_type_chk.Checked = false;

                    if (null != tmpObj.getPrndl().getResultCode())
                        prndl_result_code_spinner.SetSelection((int)tmpObj.getPrndl().getResultCode());
                    else
                        prndl_result_code_chk.Checked = false;
                }
                else
                    prndl_chk.Checked = false;

                if (null != tmpObj.getTurnSignal())
                {
                    if (null != tmpObj.getTurnSignal().getDataType())
                        turnSignal_data_type_spinner.SetSelection((int)tmpObj.getTurnSignal().getDataType());
                    else
                        turnSignal_data_type_chk.Checked = false;

                    if (null != tmpObj.getTurnSignal().getResultCode())
                        turnSignal_result_code_spinner.SetSelection((int)tmpObj.getTurnSignal().getResultCode());
                    else
                        turnSignal_result_code_chk.Checked = false;
                }
                else
                    turnSignal_chk.Checked = false;

                if (null != tmpObj.getTirePressure())
                {
                    if (null != tmpObj.getTirePressure().getDataType())
                        tirePressure_data_type_spinner.SetSelection((int)tmpObj.getTirePressure().getDataType());
                    else
                        tirePressure_data_type_chk.Checked = false;

                    if (null != tmpObj.getTirePressure().getResultCode())
                        tirePressure_result_code_spinner.SetSelection((int)tmpObj.getTirePressure().getResultCode());
                    else
                        tirePressure_result_code_chk.Checked = false;
                }
                else
                    tirePressure_chk.Checked = false;

                if (null != tmpObj.getOdometer())
                {
                    if (null != tmpObj.getOdometer().getDataType())
                        odometer_data_type_spinner.SetSelection((int)tmpObj.getOdometer().getDataType());
                    else
                        odometer_data_type_chk.Checked = false;

                    if (null != tmpObj.getOdometer().getResultCode())
                        odometer_result_code_spinner.SetSelection((int)tmpObj.getOdometer().getResultCode());
                    else
                        odometer_result_code_chk.Checked = false;
                }
                else
                    odometer_chk.Checked = false;

                if (null != tmpObj.getBeltStatus())
                {
                    if (null != tmpObj.getBeltStatus().getDataType())
                        beltStatus_data_type_spinner.SetSelection((int)tmpObj.getBeltStatus().getDataType());
                    else
                        beltStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getBeltStatus().getResultCode())
                        beltStatus_result_code_spinner.SetSelection((int)tmpObj.getBeltStatus().getResultCode());
                    else
                        beltStatus_result_code_chk.Checked = false;
                }
                else
                    beltStatus_chk.Checked = false;

                if (null != tmpObj.getBodyInformation())
                {
                    if (null != tmpObj.getBodyInformation().getDataType())
                        bodyInformation_data_type_spinner.SetSelection((int)tmpObj.getBodyInformation().getDataType());
                    else
                        bodyInformation_data_type_chk.Checked = false;

                    if (null != tmpObj.getBodyInformation().getResultCode())
                        bodyInformation_result_code_spinner.SetSelection((int)tmpObj.getBodyInformation().getResultCode());
                    else
                        bodyInformation_result_code_chk.Checked = false;
                }
                else
                    bodyInformation_chk.Checked = false;

                if (null != tmpObj.getDeviceStatus())
                {
                    if (null != tmpObj.getDeviceStatus().getDataType())
                        deviceStatus_data_type_spinner.SetSelection((int)tmpObj.getDeviceStatus().getDataType());
                    else
                        deviceStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getDeviceStatus().getResultCode())
                        deviceStatus_result_code_spinner.SetSelection((int)tmpObj.getDeviceStatus().getResultCode());
                    else
                        deviceStatus_result_code_chk.Checked = false;
                }
                else
                    deviceStatus_chk.Checked = false;

                if (null != tmpObj.getdriverBraking())
                {
                    if (null != tmpObj.getdriverBraking().getDataType())
                        driverBraking_data_type_spinner.SetSelection((int)tmpObj.getdriverBraking().getDataType());
                    else
                        driverBraking_data_type_chk.Checked = false;

                    if (null != tmpObj.getdriverBraking().getResultCode())
                        driverBraking_result_code_spinner.SetSelection((int)tmpObj.getdriverBraking().getResultCode());
                    else
                        driverBraking_result_code_chk.Checked = false;
                }
                else
                    driverBraking_chk.Checked = false;

                if (null != tmpObj.getWiperStatus())
                {
                    if (null != tmpObj.getWiperStatus().getDataType())
                        wiperStatus_data_type_spinner.SetSelection((int)tmpObj.getWiperStatus().getDataType());
                    else
                        wiperStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getWiperStatus().getResultCode())
                        wiperStatus_result_code_spinner.SetSelection((int)tmpObj.getWiperStatus().getResultCode());
                    else
                        wiperStatus_result_code_chk.Checked = false;
                }
                else
                    wiperStatus_chk.Checked = false;

                if (null != tmpObj.getHeadLampStatus())
                {
                    if (null != tmpObj.getHeadLampStatus().getDataType())
                        headLampStatus_data_type_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getDataType());
                    else
                        headLampStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getHeadLampStatus().getResultCode())
                        headLampStatus_result_code_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getResultCode());
                    else
                        headLampStatus_result_code_chk.Checked = false;
                }
                else
                    headLampStatus_chk.Checked = false;

                if (null != tmpObj.getEngineTorque())
                {
                    if (null != tmpObj.getEngineTorque().getDataType())
                        engineTorque_data_type_spinner.SetSelection((int)tmpObj.getEngineTorque().getDataType());
                    else
                        engineTorque_data_type_chk.Checked = false;

                    if (null != tmpObj.getEngineTorque().getResultCode())
                        engineTorque_result_code_spinner.SetSelection((int)tmpObj.getEngineTorque().getResultCode());
                    else
                        engineTorque_result_code_chk.Checked = false;
                }
                else
                    engineTorque_chk.Checked = false;

                if (null != tmpObj.getAccPedalPosition())
                {
                    if (null != tmpObj.getAccPedalPosition().getDataType())
                        accPedalPosition_data_type_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getDataType());
                    else
                        accPedalPosition_data_type_chk.Checked = false;

                    if (null != tmpObj.getAccPedalPosition().getResultCode())
                        accPedalPosition_result_code_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getResultCode());
                    else
                        accPedalPosition_result_code_chk.Checked = false;
                }
                else
                    accPedalPosition_chk.Checked = false;

                if (null != tmpObj.getSteeringWheelAngle())
                {
                    if (null != tmpObj.getSteeringWheelAngle().getDataType())
                        steeringWheelAngle_data_type_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getDataType());
                    else
                        steeringWheelAngle_data_type_chk.Checked = false;

                    if (null != tmpObj.getSteeringWheelAngle().getResultCode())
                        steeringWheelAngle_result_code_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getResultCode());
                    else
                        steeringWheelAngle_result_code_chk.Checked = false;
                }
                else
                    steeringWheelAngle_chk.Checked = false;

                if (null != tmpObj.getECallInfo())
                {
                    if (null != tmpObj.getECallInfo().getDataType())
                        eCallInfo_data_type_spinner.SetSelection((int)tmpObj.getECallInfo().getDataType());
                    else
                        eCallInfo_data_type_chk.Checked = false;

                    if (null != tmpObj.getECallInfo().getResultCode())
                        eCallInfo_result_code_spinner.SetSelection((int)tmpObj.getECallInfo().getResultCode());
                    else
                        eCallInfo_result_code_chk.Checked = false;
                }
                else
                    eCallInfo_chk.Checked = false;

                if (null != tmpObj.getAirbagStatus())
                {
                    if (null != tmpObj.getAirbagStatus().getDataType())
                        airbagStatus_data_type_spinner.SetSelection((int)tmpObj.getAirbagStatus().getDataType());
                    else
                        airbagStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getAirbagStatus().getResultCode())
                        airbagStatus_result_code_spinner.SetSelection((int)tmpObj.getAirbagStatus().getResultCode());
                    else
                        airbagStatus_result_code_chk.Checked = false;
                }
                else
                    airbagStatus_chk.Checked = false;

                if (null != tmpObj.getEmergencyEvent())
                {
                    if (null != tmpObj.getEmergencyEvent().getDataType())
                        emergencyEvent_data_type_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getDataType());
                    else
                        emergencyEvent_data_type_chk.Checked = false;

                    if (null != tmpObj.getEmergencyEvent().getResultCode())
                        emergencyEvent_result_code_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getResultCode());
                    else
                        emergencyEvent_result_code_chk.Checked = false;
                }
                else
                    emergencyEvent_chk.Checked = false;

                if (null != tmpObj.getClusterModes())
                {
                    if (null != tmpObj.getClusterModes().getDataType())
                        clusterModes_data_type_spinner.SetSelection((int)tmpObj.getClusterModes().getDataType());
                    else
                        clusterModes_data_type_chk.Checked = false;

                    if (null != tmpObj.getClusterModes().getResultCode())
                        clusterModes_result_code_spinner.SetSelection((int)tmpObj.getClusterModes().getResultCode());
                    else
                        clusterModes_result_code_chk.Checked = false;
                }
                else
                    clusterModes_chk.Checked = false;

                if (null != tmpObj.getMyKey())
                {
                    if (null != tmpObj.getMyKey().getDataType())
                        myKey_data_type_spinner.SetSelection((int)tmpObj.getMyKey().getDataType());
                    else
                        myKey_data_type_chk.Checked = false;

                    if (null != tmpObj.getMyKey().getResultCode())
                        myKey_result_code_spinner.SetSelection((int)tmpObj.getMyKey().getResultCode());
                    else
                        myKey_result_code_chk.Checked = false;
                }
                else
                    myKey_chk.Checked = false;

                if (null != tmpObj.getElectronicParkBrakeStatus())
                {
                    if (null != tmpObj.getElectronicParkBrakeStatus().getDataType())
                        electronicParkBrakeStatus_data_type_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getDataType());
                    else
                        electronicParkBrakeStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getElectronicParkBrakeStatus().getResultCode())
                        electronicParkBrakeStatus_result_code_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getResultCode());
                    else
                        electronicParkBrakeStatus_result_code_chk.Checked = false;
                }
                else
                    electronicParkBrakeStatus_chk.Checked = false;

                if (tmpObj.getEngineOilLife() != null)
                {
                    if (null != tmpObj.getEngineOilLife().getDataType())
                        engineOilLife_data_type_spinner.SetSelection((int)tmpObj.getEngineOilLife().getDataType());
                    else
                        engineOilLife_data_type_chk.Checked = false;

                    if (null != tmpObj.getEngineOilLife().getResultCode())
                        engineOilLife_result_code_spinner.SetSelection((int)tmpObj.getEngineOilLife().getResultCode());
                    else
                        engineOilLife_result_code_chk.Checked = false;
                }
                else
                    engineOilLife_chk.Checked = false;

                if (tmpObj.getFuelRange() != null)
                {
                    if (null != tmpObj.getFuelRange().getDataType())
                        fuelRange_data_type_spinner.SetSelection((int)tmpObj.getFuelRange().getDataType());
                    else
                        fuelRange_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelRange().getResultCode())
                        fuelRange_result_code_spinner.SetSelection((int)tmpObj.getFuelRange().getResultCode());
                    else
                        fuelRange_result_code_chk.Checked = false;
                }
                else
                    fuelRange_chk.Checked = false;
			}

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCode_chk.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;

                VehicleDataResult gps = null, speed = null, rpm = null, fuelLevel = null, fuelLevelState = null, instantFuel = null, externalTemperature = null,
                                 prndl = null, tirePressure = null, odometer = null, beltStatus = null, bodyInformation = null, deviceStatus = null, driverBraking = null,
                                 wiperStatus = null, headLampStatus = null, engineTorque = null, accPedalPosition = null, steeringWheelAngle = null, eCallInfo = null,
                				airbagStatus = null, emergencyEvent = null, clusterModes = null, myKey = null, electronicParkBrakeStatus = null, engineOilLife = null, 
                				fuelRange = null, turnSignal = null;

                if (gps_chk.Checked)
                {
                    gps = new VehicleDataResult();
                    if (gps_data_type_chk.Checked)
                        gps.dataType = (VehicleDataType)gps_data_type_spinner.SelectedItemPosition;
                    if (gps_result_code_chk.Checked)
                        gps.resultCode = (VehicleDataResultCode)gps_result_code_spinner.SelectedItemPosition;
                }

                if (speed_chk.Checked)
                {
                    speed = new VehicleDataResult();
                    if (speed_data_type_chk.Checked)
                        speed.dataType = (VehicleDataType)speed_data_type_spinner.SelectedItemPosition;
                    if (speed_result_code_chk.Checked)
                        speed.resultCode = (VehicleDataResultCode)speed_result_code_spinner.SelectedItemPosition;
                }

                if (rpm_chk.Checked)
                {
                    rpm = new VehicleDataResult();
                    if (rpm_data_type_chk.Checked)
                        rpm.dataType = (VehicleDataType)rpm_data_type_spinner.SelectedItemPosition;
                    if (rpm_result_code_chk.Checked)
                        rpm.resultCode = (VehicleDataResultCode)rpm_result_code_spinner.SelectedItemPosition;
                }

                if (fuelLevel_chk.Checked)
                {
                    fuelLevel = new VehicleDataResult();
                    if (fuelLevel_data_type_chk.Checked)
                        fuelLevel.dataType = (VehicleDataType)fuelLevel_data_type_spinner.SelectedItemPosition;
                    if (fuelLevel_result_code_chk.Checked)
                        fuelLevel.resultCode = (VehicleDataResultCode)fuelLevel_result_code_spinner.SelectedItemPosition;
                }

                if (fuelLevel_State_chk.Checked)
                {
                    fuelLevelState = new VehicleDataResult();
                    if (fuelLevel_State_data_type_chk.Checked)
                        fuelLevelState.dataType = (VehicleDataType)fuelLevel_State_data_type_spinner.SelectedItemPosition;
                    if (fuelLevel_State_result_code_chk.Checked)
                        fuelLevelState.resultCode = (VehicleDataResultCode)fuelLevel_State_result_code_spinner.SelectedItemPosition;
                }

                if (instantFuelConsumption_chk.Checked)
                {
                    instantFuel = new VehicleDataResult();
                    if (instantFuelConsumption_data_type_chk.Checked)
                        instantFuel.dataType = (VehicleDataType)instantFuelConsumption_data_type_spinner.SelectedItemPosition;
                    if (instantFuelConsumption_result_code_chk.Checked)
                        instantFuel.resultCode = (VehicleDataResultCode)instantFuelConsumption_result_code_spinner.SelectedItemPosition;
                }

                if (externalTemperature_chk.Checked)
                {
                    externalTemperature = new VehicleDataResult();
                    if (externalTemperature_data_type_chk.Checked)
                        externalTemperature.dataType = (VehicleDataType)externalTemperature_data_type_spinner.SelectedItemPosition;
                    if (externalTemperature_result_code_chk.Checked)
                        externalTemperature.resultCode = (VehicleDataResultCode)externalTemperature_result_code_spinner.SelectedItemPosition;
                }

                if (prndl_chk.Checked)
                {
                    prndl = new VehicleDataResult();
                    if (prndl_data_type_chk.Checked)
                        prndl.dataType = (VehicleDataType)prndl_data_type_spinner.SelectedItemPosition;
                    if (prndl_result_code_chk.Checked)
                        prndl.resultCode = (VehicleDataResultCode)prndl_result_code_spinner.SelectedItemPosition;
                }

                if (turnSignal_chk.Checked)
                {
                    turnSignal = new VehicleDataResult();
                	if (turnSignal_data_type_chk.Checked)
                		turnSignal.dataType = (VehicleDataType)turnSignal_data_type_spinner.SelectedItemPosition;
                	if (turnSignal_result_code_chk.Checked)
                		turnSignal.resultCode = (VehicleDataResultCode)turnSignal_result_code_spinner.SelectedItemPosition;
                }

                if (tirePressure_chk.Checked)
                {
                    tirePressure = new VehicleDataResult();
                    if (tirePressure_data_type_chk.Checked)
                        tirePressure.dataType = (VehicleDataType)tirePressure_data_type_spinner.SelectedItemPosition;
                    if (tirePressure_result_code_chk.Checked)
                        tirePressure.resultCode = (VehicleDataResultCode)tirePressure_result_code_spinner.SelectedItemPosition;
                }

                if (odometer_chk.Checked)
                {
                    odometer = new VehicleDataResult();
                    if (odometer_data_type_chk.Checked)
                        odometer.dataType = (VehicleDataType)odometer_data_type_spinner.SelectedItemPosition;
                    if (odometer_result_code_chk.Checked)
                        odometer.resultCode = (VehicleDataResultCode)odometer_result_code_spinner.SelectedItemPosition;
                }

                if (beltStatus_chk.Checked)
                {
                    beltStatus = new VehicleDataResult();
                    if (beltStatus_data_type_chk.Checked)
                        beltStatus.dataType = (VehicleDataType)beltStatus_data_type_spinner.SelectedItemPosition;
                    if (beltStatus_result_code_chk.Checked)
                        beltStatus.resultCode = (VehicleDataResultCode)beltStatus_result_code_spinner.SelectedItemPosition;
                }

                if (bodyInformation_chk.Checked)
                {
                    bodyInformation = new VehicleDataResult();
                    if (bodyInformation_data_type_chk.Checked)
                        bodyInformation.dataType = (VehicleDataType)bodyInformation_data_type_spinner.SelectedItemPosition;
                    if (bodyInformation_result_code_chk.Checked)
                        bodyInformation.resultCode = (VehicleDataResultCode)bodyInformation_result_code_spinner.SelectedItemPosition;
                }

                if (deviceStatus_chk.Checked)
                {
                    deviceStatus = new VehicleDataResult();
                    if (deviceStatus_data_type_chk.Checked)
                        deviceStatus.dataType = (VehicleDataType)deviceStatus_data_type_spinner.SelectedItemPosition;
                    if (deviceStatus_result_code_chk.Checked)
                        deviceStatus.resultCode = (VehicleDataResultCode)deviceStatus_result_code_spinner.SelectedItemPosition;
                }

                if (driverBraking_chk.Checked)
                {
                    driverBraking = new VehicleDataResult();
                    if (driverBraking_data_type_chk.Checked)
                        driverBraking.dataType = (VehicleDataType)driverBraking_data_type_spinner.SelectedItemPosition;
                    if (driverBraking_result_code_chk.Checked)
                        driverBraking.resultCode = (VehicleDataResultCode)driverBraking_result_code_spinner.SelectedItemPosition;
                }

                if (wiperStatus_chk.Checked)
                {
                    wiperStatus = new VehicleDataResult();
                    if (wiperStatus_data_type_chk.Checked)
                        wiperStatus.dataType = (VehicleDataType)wiperStatus_data_type_spinner.SelectedItemPosition;
                    if (wiperStatus_result_code_chk.Checked)
                        wiperStatus.resultCode = (VehicleDataResultCode)wiperStatus_result_code_spinner.SelectedItemPosition;
                }

                if (headLampStatus_chk.Checked)
                {
                    headLampStatus = new VehicleDataResult();
                    if (headLampStatus_data_type_chk.Checked)
                        headLampStatus.dataType = (VehicleDataType)headLampStatus_data_type_spinner.SelectedItemPosition;
                    if (headLampStatus_result_code_chk.Checked)
                        headLampStatus.resultCode = (VehicleDataResultCode)headLampStatus_result_code_spinner.SelectedItemPosition;
                }

                if (engineTorque_chk.Checked)
                {
                    engineTorque = new VehicleDataResult();
                    if (engineTorque_data_type_chk.Checked)
                        engineTorque.dataType = (VehicleDataType)engineTorque_data_type_spinner.SelectedItemPosition;
                    if (engineTorque_result_code_chk.Checked)
                        engineTorque.resultCode = (VehicleDataResultCode)engineTorque_result_code_spinner.SelectedItemPosition;
                }

                if (accPedalPosition_chk.Checked)
                {
                    accPedalPosition = new VehicleDataResult();
                    if (accPedalPosition_data_type_chk.Checked)
                        accPedalPosition.dataType = (VehicleDataType)accPedalPosition_data_type_spinner.SelectedItemPosition;
                    if (accPedalPosition_result_code_chk.Checked)
                        accPedalPosition.resultCode = (VehicleDataResultCode)accPedalPosition_result_code_spinner.SelectedItemPosition;
                }

                if (steeringWheelAngle_chk.Checked)
                {
                    steeringWheelAngle = new VehicleDataResult();
                    if (steeringWheelAngle_data_type_chk.Checked)
                        steeringWheelAngle.dataType = (VehicleDataType)steeringWheelAngle_data_type_spinner.SelectedItemPosition;
                    if (steeringWheelAngle_result_code_chk.Checked)
                        steeringWheelAngle.resultCode = (VehicleDataResultCode)steeringWheelAngle_result_code_spinner.SelectedItemPosition;
                }

                if (eCallInfo_chk.Checked)
                {
                    eCallInfo = new VehicleDataResult();
                    if (eCallInfo_data_type_chk.Checked)
                        eCallInfo.dataType = (VehicleDataType)eCallInfo_data_type_spinner.SelectedItemPosition;
                    if (eCallInfo_result_code_chk.Checked)
                        eCallInfo.resultCode = (VehicleDataResultCode)eCallInfo_result_code_spinner.SelectedItemPosition;
                }

                if (airbagStatus_chk.Checked)
                {
                    airbagStatus = new VehicleDataResult();
                    if (airbagStatus_data_type_chk.Checked)
                        airbagStatus.dataType = (VehicleDataType)airbagStatus_data_type_spinner.SelectedItemPosition;
                    if (airbagStatus_result_code_chk.Checked)
                        airbagStatus.resultCode = (VehicleDataResultCode)airbagStatus_result_code_spinner.SelectedItemPosition;
                }

                if (emergencyEvent_chk.Checked)
                {
                    emergencyEvent = new VehicleDataResult();
                    if (emergencyEvent_data_type_chk.Checked)
                        emergencyEvent.dataType = (VehicleDataType)emergencyEvent_data_type_spinner.SelectedItemPosition;
                    if (emergencyEvent_result_code_chk.Checked)
                        emergencyEvent.resultCode = (VehicleDataResultCode)emergencyEvent_result_code_spinner.SelectedItemPosition;
                }

                if (clusterModes_chk.Checked)
                {
                    clusterModes = new VehicleDataResult();
                    if (clusterModes_data_type_chk.Checked)
                        clusterModes.dataType = (VehicleDataType)clusterModes_data_type_spinner.SelectedItemPosition;
                    if (clusterModes_result_code_chk.Checked)
                        clusterModes.resultCode = (VehicleDataResultCode)clusterModes_result_code_spinner.SelectedItemPosition;
                }

                if (myKey_chk.Checked)
                {
                    myKey = new VehicleDataResult();
                    if (myKey_data_type_chk.Checked)
                        myKey.dataType = (VehicleDataType)myKey_data_type_spinner.SelectedItemPosition;
                    if (myKey_result_code_chk.Checked)
                        myKey.resultCode = (VehicleDataResultCode)myKey_result_code_spinner.SelectedItemPosition;
                }

                if (electronicParkBrakeStatus_chk.Checked)
                {
                    electronicParkBrakeStatus = new VehicleDataResult();
                    if (electronicParkBrakeStatus_data_type_chk.Checked)
                        electronicParkBrakeStatus.dataType = (VehicleDataType)electronicParkBrakeStatus_data_type_spinner.SelectedItemPosition;
                    if (electronicParkBrakeStatus_result_code_chk.Checked)
                        electronicParkBrakeStatus.resultCode = (VehicleDataResultCode)electronicParkBrakeStatus_result_code_spinner.SelectedItemPosition;
                }

                if (engineOilLife_chk.Checked)
                {
                    engineOilLife = new VehicleDataResult();
                    if (engineOilLife_data_type_chk.Checked)
                        engineOilLife.dataType = (VehicleDataType)engineOilLife_data_type_spinner.SelectedItemPosition;
                    if (engineOilLife_result_code_chk.Checked)
                        engineOilLife.resultCode = (VehicleDataResultCode)engineOilLife_result_code_spinner.SelectedItemPosition;
                }

                if (fuelRange_chk.Checked)
                {
                    fuelRange = new VehicleDataResult();
                    if (fuelRange_data_type_chk.Checked)
                        fuelRange.dataType = (VehicleDataType)fuelRange_data_type_spinner.SelectedItemPosition;
                    if (fuelRange_result_code_chk.Checked)
                        fuelRange.resultCode = (VehicleDataResultCode)fuelRange_result_code_spinner.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoUnsubscribeVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gps, speed, rpm, fuelLevel, 
                                                                                                  fuelLevelState, instantFuel, externalTemperature, prndl, turnSignal, tirePressure, odometer, beltStatus,
			                                                                                      bodyInformation, deviceStatus, driverBraking, wiperStatus, headLampStatus, engineTorque, 
                                                                                                  accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus, emergencyEvent, clusterModes, myKey, 
                                                                                                  electronicParkBrakeStatus, engineOilLife, fuelRange);

                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });


			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        void CreateVINotificationOnVehicleData()
        {
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.vi_get_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VINotificationOnVehicleData);

            rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_chk).Visibility = ViewStates.Gone;
			rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_spinner).Visibility = ViewStates.Gone;

			CheckBox gps_data_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_chk);
			Button gps_data_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_btn);

			CheckBox speed_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_chk);
			EditText speed_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_et);

			CheckBox rpm_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_chk);
			EditText rpm_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_et);

			CheckBox fuel_level_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_chk);
			EditText fuel_level_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_et);

			CheckBox fuel_level_state_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_chk);
			Spinner fuel_level_state_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_spinner);

			CheckBox instant_fuel_consumption_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_chk);
			EditText instant_fuel_consumption_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_et);

			CheckBox external_temp_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_chk);
			EditText external_temp_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_et);

			CheckBox vin_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_chk);
			EditText vin_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_et);

			CheckBox prndl_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_chk);
			Spinner prndl_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_spinner);

            CheckBox turnSignal_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_turnSignal_chk);
            Spinner turnSignal_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_turnSignal_spinner);

			CheckBox tire_pressure_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_chk);
			Button tire_pressure_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_btn);

			CheckBox odometer_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_chk);
			EditText odometer_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_et);

			CheckBox belt_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_chk);
			Button belt_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_button);

			CheckBox body_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_chk);
			Button body_info_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_button);

			CheckBox device_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_chk);
			Button device_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_button);

			CheckBox driver_braking_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_chk);
			Spinner driver_braking_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_spinner);

			CheckBox wiper_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_chk);
			Spinner wiper_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_spinner);

			CheckBox head_lamp_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_chk);
			Button head_lamp_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_btn);

			CheckBox engine_torque_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_chk);
			EditText engine_torque_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_et);

			CheckBox acc_padel_position_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_chk);
			EditText acc_padel_position_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_et);

			CheckBox steering_wheel_angle_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_chk);
			EditText steering_wheel_angle_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_et);

			CheckBox ecall_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_chk);
			Button ecall_info_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_btn);

			CheckBox airbag_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_chk);
			Button airbag_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_btn);

			CheckBox emergency_event_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_chk);
			Button emergency_event_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_btn);

			CheckBox cluster_modes_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_chk);
			Button cluster_modes_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_btn);

			CheckBox my_key_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_chk);
			Spinner my_key_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_spinner);

			CheckBox electronic_park_brake_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_chk);
			Spinner electronic_park_brake_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_spinner);

            CheckBox engine_oil_life_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_chk);
            EditText engine_oil_life_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_et);

            CheckBox fuel_range_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_chk);
            Button fuel_range_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_btn);
            ListView fuel_range_lv = (ListView)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_lv);

			var ElectronicParkBrakeStatus = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VIElectronicParkBrakeStatusArray);
			electronic_park_brake_status_spinner.Adapter = ElectronicParkBrakeStatus;

			var ComponentVolumeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, ComponentVolumeStatusArray);
			fuel_level_state_spinner.Adapter = ComponentVolumeStatusAdapter;

			var PRNDLAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PRNDLArray);
			prndl_spinner.Adapter = PRNDLAdapter;

            var TurnSignalAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, TurnSignalArray);
            turnSignal_spinner.Adapter = TurnSignalAdapter;

			var VehicleDataEventStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataEventStatusArray);
			driver_braking_spinner.Adapter = VehicleDataEventStatusAdapter;

			var WiperStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, WiperStatusArray);
			wiper_status_spinner.Adapter = WiperStatusAdapter;

			var VehicleDataStatus = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataStatusArray);
			my_key_spinner.Adapter = VehicleDataStatus;

			gps_data_chk.CheckedChange += (s, e) => gps_data_btn.Enabled = e.IsChecked;
			speed_chk.CheckedChange += (s, e) => speed_et.Enabled = e.IsChecked;
			rpm_chk.CheckedChange += (s, e) => rpm_et.Enabled = e.IsChecked;
			fuel_level_chk.CheckedChange += (s, e) => fuel_level_et.Enabled = e.IsChecked;
			fuel_level_state_chk.CheckedChange += (s, e) => fuel_level_state_spinner.Enabled = e.IsChecked;
			instant_fuel_consumption_chk.CheckedChange += (s, e) => instant_fuel_consumption_et.Enabled = e.IsChecked;
			external_temp_chk.CheckedChange += (s, e) => external_temp_et.Enabled = e.IsChecked;
			vin_chk.CheckedChange += (s, e) => vin_et.Enabled = e.IsChecked;
			prndl_chk.CheckedChange += (s, e) => prndl_spinner.Enabled = e.IsChecked;
            turnSignal_chk.CheckedChange += (s, e) => turnSignal_spinner.Enabled = e.IsChecked;
			tire_pressure_chk.CheckedChange += (s, e) => tire_pressure_btn.Enabled = e.IsChecked;
			odometer_chk.CheckedChange += (s, e) => odometer_et.Enabled = e.IsChecked;
			belt_status_chk.CheckedChange += (s, e) => belt_status_button.Enabled = e.IsChecked;
			body_info_chk.CheckedChange += (s, e) => body_info_button.Enabled = e.IsChecked;
			device_status_chk.CheckedChange += (s, e) => device_status_button.Enabled = e.IsChecked;
			driver_braking_chk.CheckedChange += (s, e) => driver_braking_spinner.Enabled = e.IsChecked;
			wiper_status_chk.CheckedChange += (s, e) => wiper_status_spinner.Enabled = e.IsChecked;
			head_lamp_status_chk.CheckedChange += (s, e) => head_lamp_status_btn.Enabled = e.IsChecked;
			engine_torque_chk.CheckedChange += (s, e) => engine_torque_et.Enabled = e.IsChecked;
			acc_padel_position_chk.CheckedChange += (s, e) => acc_padel_position_et.Enabled = e.IsChecked;
			steering_wheel_angle_chk.CheckedChange += (s, e) => steering_wheel_angle_et.Enabled = e.IsChecked;
			ecall_info_chk.CheckedChange += (s, e) => ecall_info_btn.Enabled = e.IsChecked;
			airbag_status_chk.CheckedChange += (s, e) => airbag_status_btn.Enabled = e.IsChecked;
			emergency_event_chk.CheckedChange += (s, e) => emergency_event_btn.Enabled = e.IsChecked;
			cluster_modes_chk.CheckedChange += (s, e) => cluster_modes_btn.Enabled = e.IsChecked;
			my_key_chk.CheckedChange += (s, e) => my_key_spinner.Enabled = e.IsChecked;
            electronic_park_brake_status_chk.CheckedChange += (s, e) => electronic_park_brake_status_spinner.Enabled = e.IsChecked;
            engine_oil_life_chk.CheckedChange += (s, e) => engine_oil_life_et.Enabled = e.IsChecked;
            fuel_range_chk.CheckedChange += (s, e) =>
            {
                fuel_range_btn.Enabled = e.IsChecked;
                fuel_range_lv.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData>(Activity, tmpObj.getMethod());

			GPSData viGPSData = null;
			TireStatus viTireStatus = null;
			BeltStatus viBeltStatus = null;
			BodyInformation viBodyInformation = null;
			DeviceStatus viDeviceStatus = null;
			HeadLampStatus viHeadLampStatus = null;
			ECallInfo viECallInfo = null;
			AirbagStatus viAirbagStatus = null;
			EmergencyEvent viEmergencyEvent = null;
			ClusterModeStatus viClusterModeStatus = null;
            List<FuelRange> fuelRangeList = null;

            if (null == tmpObj)
            {
                viGPSData = BuildDefaults.buildDefaultGPSData();
                viTireStatus = BuildDefaults.buildDefaultTireStatus();
                viBeltStatus = BuildDefaults.buildDefaultBeltStatus();
                viBodyInformation = BuildDefaults.buildDefaultBodyInformation();
                viDeviceStatus = BuildDefaults.buildDefaultDeviceStatus();
                viHeadLampStatus = BuildDefaults.buildDefaultHeadLampStatus();
                viECallInfo = BuildDefaults.buildDefaultECallInfo();
                viAirbagStatus = BuildDefaults.buildDefaultAirBagStaus();
                viEmergencyEvent = BuildDefaults.buildDefaultEmergencyEvent();
                viClusterModeStatus = BuildDefaults.buildDefaultClusterModeStatus();
                fuelRangeList = BuildDefaults.buildDefaultFuelRangeList();
            }

			if (tmpObj != null)
			{
                viGPSData = tmpObj.getGps();
                if (null == viGPSData)
                {
                    gps_data_chk.Checked = false;
                }
                if (tmpObj.getSpeed() != null)
                {
                    speed_et.Text = tmpObj.getSpeed().ToString();
                }
                else
                {
                    speed_chk.Checked = false;
                }
                if (tmpObj.getRpm() != null)
                {
                    rpm_et.Text = tmpObj.getRpm().ToString();
                }
                else
                {
                    rpm_chk.Checked = false;
                }
                if (tmpObj.getFuelLevel() != null)
                {
                    fuel_level_et.Text = tmpObj.getFuelLevel().ToString();
                }
                else
                {
                    fuel_level_chk.Checked = false;
                }
                if (tmpObj.getFuelLevel_State() != null)
                {
                    fuel_level_state_spinner.SetSelection((int)tmpObj.getFuelLevel_State());
                }
                else
                {
                    fuel_level_state_chk.Checked = false;
                }
                if (tmpObj.getInstantFuelConsumption() != null)
                {
                    instant_fuel_consumption_et.Text = tmpObj.getInstantFuelConsumption().ToString();
                }
                else
                {
                    instant_fuel_consumption_chk.Checked = false;
                }
                if (tmpObj.getExternalTemperature() != null)
                {
                    external_temp_et.Text = tmpObj.getExternalTemperature().ToString();
                }
                else
                {
                    external_temp_chk.Checked = false;
                }
                if (tmpObj.getVin() != null)
                {
                    vin_et.Text = tmpObj.getVin();
                }
                else
                {
                    vin_et.Enabled = false;
                    vin_chk.Checked = false;
                }

                if (tmpObj.getPrndl() != null)
                {
                    prndl_spinner.SetSelection((int)tmpObj.getPrndl());
                }
                else
                {
                    prndl_chk.Checked = false;
                }

                if (tmpObj.getTurnSignal() != null)
                {
                    turnSignal_spinner.SetSelection((int)tmpObj.getTurnSignal());
                }
                else
                {
                    turnSignal_chk.Checked = false;
                }

                viTireStatus = tmpObj.getTirePressure();
                if (null == viTireStatus)
                {
                    tire_pressure_chk.Checked = false;
                }

                if (tmpObj.getOdometer() != null)
                {
                    odometer_et.Text = tmpObj.getOdometer().ToString();
                }
                else
                {
                    odometer_chk.Checked = false;
                }

                viBeltStatus = tmpObj.getBeltStatus();
                if (null == viBeltStatus)
                {
                    belt_status_button.Enabled = false;
                    belt_status_chk.Checked = false;
                }

                viBodyInformation = tmpObj.getBodyInformation();
                if (null == viBodyInformation)
                {
                    body_info_chk.Checked = false;
                }

                viDeviceStatus = tmpObj.getDeviceStatus();
                if (null == viDeviceStatus)
                {
                    device_status_chk.Checked = false;
                }

                if (tmpObj.getDriverBraking() != null)
                {
                    driver_braking_spinner.SetSelection((int)tmpObj.getDriverBraking());
                }
                else
                {
                    driver_braking_chk.Checked = false;
                }

                if (tmpObj.getWiperStatus() != null)
                {
                    wiper_status_spinner.SetSelection((int)tmpObj.getWiperStatus());
                }
                else
                {
                    wiper_status_chk.Checked = false;
                }
                viHeadLampStatus = tmpObj.getHeadLampStatus();
                if (null == viHeadLampStatus)
                {
                    head_lamp_status_chk.Checked = false;
                }
                if (tmpObj.getEngineTorque() != null)
                {
                    engine_torque_et.Text = tmpObj.getEngineTorque().ToString();
                }
                else
                {
                    engine_torque_chk.Checked = false;
                }
                if (tmpObj.getAccPedalPosition() != null)
                {
                    acc_padel_position_et.Text = tmpObj.getAccPedalPosition().ToString();
                }
                else
                {
                    acc_padel_position_chk.Checked = false;
                }
                if (tmpObj.getSteeringWheelAngle() != null)
                {
                    steering_wheel_angle_et.Text = tmpObj.getSteeringWheelAngle().ToString();
                }
                else
                {
                    steering_wheel_angle_chk.Checked = false;
                }
                viECallInfo = tmpObj.getECallInfo();
                if (null == viECallInfo)
                {
                    ecall_info_chk.Checked = false;
                }

                viAirbagStatus = tmpObj.getAirbagStatus();
                if (null == viAirbagStatus)
                {
                    airbag_status_chk.Checked = false;
                }

                viEmergencyEvent = tmpObj.getEmergencyEvent();
                if (null == viEmergencyEvent)
                {
                    emergency_event_chk.Checked = false;
                }

                viClusterModeStatus = tmpObj.getClusterModes();
                if (null == viClusterModeStatus)
                {
                    cluster_modes_chk.Checked = false;
                }

                if (tmpObj.getMyKey() != null)
                {
                    my_key_spinner.SetSelection((int)tmpObj.getMyKey().getE911Override());
                }
                else
                {
                    my_key_chk.Checked = false;
                }

                if (tmpObj.getElectronicParkBrakeStatus() != null)
                {
                    electronic_park_brake_status_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus());
                }
                else
                {
                    electronic_park_brake_status_chk.Checked = false;
                }

                if (tmpObj.getEngineOilLife() != null)
                {
                    engine_oil_life_et.Text = ((float)tmpObj.getEngineOilLife()).ToString();
                }
                else
                {
                    engine_oil_life_chk.Checked = false;
                }
                fuelRangeList = tmpObj.getFuelRange();
			}
			if (fuelRangeList == null)
			{
				fuelRangeList = new List<FuelRange>();
				fuel_range_btn.Enabled = false;
				fuel_range_chk.Checked = false;
			}

			var fuelRangeAdapter = new FuelRangeAdapter(Activity, fuelRangeList);
				fuel_range_lv.Adapter = fuelRangeAdapter;
				Utility.setListViewHeightBasedOnChildren(fuel_range_lv);
			
			
			gps_data_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder gpsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View gpsView = layoutInflater.Inflate(Resource.Layout.vi_gps_data, null);
				gpsAlertDialog.SetView(gpsView);
				gpsAlertDialog.SetTitle("GPS Data");

				CheckBox chkLongitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_chk);
				EditText etLongitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_et);

				CheckBox chkLatitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_chk);
				EditText etLatitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_et);

				CheckBox chkUtcYear = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_chk);
				EditText etUtcYear = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_et);

				CheckBox chkUtcMonth = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_chk);
				EditText etUtcMonth = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_et);

				CheckBox chkUtcDay = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_chk);
				EditText etUtcDay = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_et);

				CheckBox chkUtcHours = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_chk);
				EditText etUtcHours = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_et);

				CheckBox chkUtcMinutes = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_chk);
				EditText etUtcMinutes = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_et);

				CheckBox chkUtcSeconds = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_chk);
				EditText etUtcSeconds = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_et);

				CheckBox chkCompassDirection = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_chk);
				Spinner spnCompassDirection = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_spinner);

				CheckBox chkpdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_chk);
				EditText etpdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_et);

				CheckBox chkhdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_chk);
				EditText ethdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_et);

				CheckBox chkvdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_chk);
				EditText etvdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_et);

				CheckBox chkActual = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_actual_chk);

				CheckBox chkSatellites = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_chk);
				EditText etSatellites = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_et);

				CheckBox chkDimension = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_chk);
				Spinner spnDimension = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_spinner);

				CheckBox chkAltitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_chk);
				EditText etAltitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_et);

				CheckBox chkHeading = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_chk);
				EditText etHeading = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_et);

				CheckBox chkSpeed = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_chk);
				EditText etSpeed = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_et);

				CheckBox chkShifted = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_shifted_chk);
				Switch tglShifted = (Switch)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_shifted_tgl);

				var CompassDirectionAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, CompassDirectionArray);
				spnCompassDirection.Adapter = CompassDirectionAdapter;

				var DimensionAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DimensionArray);
				spnDimension.Adapter = DimensionAdapter;

				chkLongitude.CheckedChange += (sender1, e1) => etLongitude.Enabled = e1.IsChecked;
				chkLatitude.CheckedChange += (sender1, e1) => etLatitude.Enabled = e1.IsChecked;
				chkUtcYear.CheckedChange += (sender1, e1) => etUtcYear.Enabled = e1.IsChecked;
				chkUtcMonth.CheckedChange += (sender1, e1) => etUtcMonth.Enabled = e1.IsChecked;
				chkUtcDay.CheckedChange += (sender1, e1) => etUtcDay.Enabled = e1.IsChecked;
				chkUtcHours.CheckedChange += (sender1, e1) => etUtcHours.Enabled = e1.IsChecked;
				chkUtcMinutes.CheckedChange += (sender1, e1) => etUtcMinutes.Enabled = e1.IsChecked;
				chkUtcSeconds.CheckedChange += (sender1, e1) => etUtcSeconds.Enabled = e1.IsChecked;
				chkCompassDirection.CheckedChange += (sender1, e1) => spnCompassDirection.Enabled = e1.IsChecked;
				chkpdop.CheckedChange += (sender1, e1) => etpdop.Enabled = e1.IsChecked;
				chkhdop.CheckedChange += (sender1, e1) => ethdop.Enabled = e1.IsChecked;
				chkvdop.CheckedChange += (sender1, e1) => etvdop.Enabled = e1.IsChecked;
				chkSatellites.CheckedChange += (sender1, e1) => etSatellites.Enabled = e1.IsChecked;
				chkDimension.CheckedChange += (sender1, e1) => spnDimension.Enabled = e1.IsChecked;
				chkAltitude.CheckedChange += (sender1, e1) => etAltitude.Enabled = e1.IsChecked;
				chkHeading.CheckedChange += (sender1, e1) => etHeading.Enabled = e1.IsChecked;
				chkSpeed.CheckedChange += (sender1, e1) => etSpeed.Enabled = e1.IsChecked;
				chkShifted.CheckedChange += (sender1, e1) => tglShifted.Enabled = e1.IsChecked;

				if (viGPSData != null)
                {
                    if (null != viGPSData.getLongitudeDegrees())
                    {
                        etLongitude.Text = viGPSData.getLongitudeDegrees().ToString();
                    }
                    else
                    {
                        etLongitude.Enabled = false;
                        chkLongitude.Checked = false;
                    }
                    if (null != viGPSData.getLatitudeDegrees())
                    {
                        etLatitude.Text = viGPSData.getLatitudeDegrees().ToString();
                    }
                    else
                    {
                        etLatitude.Enabled = false;
                        chkLatitude.Checked = false;
                    }
                    if (null != viGPSData.getUtcYear())
                    {
                        etUtcYear.Text = viGPSData.getUtcYear().ToString();
                    }
                    else
                    {
                        etUtcYear.Enabled = false;
                        chkUtcYear.Checked = false;
                    }
                    if (null != viGPSData.getUtcMonth())
                    {
                        etUtcMonth.Text = viGPSData.getUtcMonth().ToString();
                    }
                    else
                    {
                        etUtcMonth.Enabled = false;
                        chkUtcMonth.Checked = false;
                    }
                    if (null != viGPSData.getUtcDay())
                    {
                        etUtcDay.Text = viGPSData.getUtcDay().ToString();
                    }
                    else
                    {
                        etUtcDay.Enabled = false;
                        chkUtcDay.Checked = false;
                    }
                    if (null != viGPSData.getUtcHours())
                    {
                        etUtcHours.Text = viGPSData.getUtcHours().ToString();
                    }
                    else
                    {
                        etUtcHours.Enabled = false;
                        chkUtcHours.Checked = false;
                    }
                    if (null != viGPSData.getUtcMinutes())
                    {
                        etUtcMinutes.Text = viGPSData.getUtcMinutes().ToString();
                    }
                    else
                    {
                        etUtcMinutes.Enabled = false;
                        chkUtcMinutes.Checked = false;
                    }
                    if (null != viGPSData.getUtcSeconds())
                    {
                        etUtcSeconds.Text = viGPSData.getUtcSeconds().ToString();
                    }
                    else
                    {
                        etUtcSeconds.Enabled = false;
                        chkUtcSeconds.Checked = false;
                    }
                    if (null != viGPSData.getCompassDirection())
                    {
                        spnCompassDirection.SetSelection((int)viGPSData.getCompassDirection());
                    }
                    else
                    {
                        spnCompassDirection.Enabled = false;
                        chkCompassDirection.Checked = false;
                    }
                    if (null != viGPSData.getPdop())
                    {
                        etpdop.Text = viGPSData.getPdop().ToString();
                    }
                    else
                    {
                        etpdop.Enabled = false;
                        chkpdop.Checked = false;
                    }
                    if (null != viGPSData.getHdop())
                    {
                        ethdop.Text = viGPSData.getHdop().ToString();
                    }
                    else
                    {
                        ethdop.Enabled = false;
                        chkhdop.Checked = false;
                    }
                    if (null != viGPSData.getVdop())
                    {
                        etvdop.Text = viGPSData.getVdop().ToString();
                    }
                    else
                    {
                        etvdop.Enabled = false;
                        chkvdop.Checked = false;
                    }
                    if (null != viGPSData.getActual())
                        chkActual.Checked = (bool)viGPSData.getActual();
                    if (null != viGPSData.getSatellites())
                    {
                        etSatellites.Text = viGPSData.getSatellites().ToString();
                    }
                    else
                    {
                        etSatellites.Enabled = false;
                        chkSatellites.Checked = false;
                    }
                    if (null != viGPSData.getDimension())
                    {
                        spnDimension.SetSelection((int)viGPSData.getDimension());
                    }
                    else
                    {
                        spnDimension.Enabled = false;
                        chkDimension.Checked = false;
                    }
                    if (null != viGPSData.getAltitude())
                    {
                        etAltitude.Text = viGPSData.getAltitude().ToString();
                    }
                    else
                    {
                        etAltitude.Enabled = false;
                        chkAltitude.Checked = false;
                    }
                    if (null != viGPSData.getHeading())
                    {
                        etHeading.Text = viGPSData.getHeading().ToString();
                    }
                    else
                    {
                        etHeading.Enabled = false;
                        chkHeading.Checked = false;
                    }
                    if (null != viGPSData.getSpeed())
                    {
                        etSpeed.Text = viGPSData.getSpeed().ToString();
                    }
					if (null != viGPSData.getShifted())
					{
						tglShifted.Checked = (bool)viGPSData.getShifted();
					}
					else
                    {
						tglShifted.Enabled = false;
						chkShifted.Checked = false;
                    }
                }

				gpsAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					gpsAlertDialog.Dispose();
				});
                gpsAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
                {
                    viGPSData = new GPSData();
                    if (chkLongitude.Checked && etLongitude.Text != null && etLongitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.longitudeDegrees = Java.Lang.Float.ParseFloat(etLongitude.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkLatitude.Checked && etLatitude.Text != null && etLatitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.latitudeDegrees = Java.Lang.Float.ParseFloat(etLatitude.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcYear.Checked && etUtcYear.Text != null && etUtcYear.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcYear = Java.Lang.Integer.ParseInt(etUtcYear.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcMonth.Checked && etUtcMonth.Text != null && etUtcMonth.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcMonth = Java.Lang.Integer.ParseInt(etUtcMonth.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcDay.Checked && etUtcDay.Text != null && etUtcDay.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcDay = Java.Lang.Integer.ParseInt(etUtcDay.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcHours.Checked && etUtcHours.Text != null && etUtcHours.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcHours = Java.Lang.Integer.ParseInt(etUtcHours.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcMinutes.Checked && etUtcMinutes.Text != null && etUtcMinutes.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcMinutes = Java.Lang.Integer.ParseInt(etUtcMinutes.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkUtcSeconds.Checked && etUtcSeconds.Text != null && etUtcSeconds.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.utcSeconds = Java.Lang.Integer.ParseInt(etUtcSeconds.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkCompassDirection.Checked)
                    {
                        viGPSData.compassDirection = (CompassDirection)spnCompassDirection.SelectedItemPosition;
                    }
                    if (chkpdop.Checked && etpdop.Text != null && etpdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.pdop = Java.Lang.Float.ParseFloat(etpdop.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkhdop.Checked && ethdop.Text != null && ethdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.hdop = Java.Lang.Float.ParseFloat(ethdop.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkvdop.Checked && etvdop.Text != null && etvdop.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.vdop = Java.Lang.Float.ParseFloat(etvdop.Text);
                        }
                        catch (Exception ex) { }
                    }
                    viGPSData.actual = chkActual.Checked;
                    if (chkSatellites.Checked && etSatellites.Text != null && etSatellites.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.satellites = Java.Lang.Integer.ParseInt(etSatellites.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkDimension.Checked)
                    {
                        viGPSData.dimension = (Dimension)spnDimension.SelectedItemPosition;
                    }
                    if (chkAltitude.Checked && etAltitude.Text != null && etAltitude.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.altitude = Java.Lang.Float.ParseFloat(etAltitude.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkHeading.Checked && etHeading.Text != null && etHeading.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.heading = Java.Lang.Float.ParseFloat(etHeading.Text);
                        }
                        catch (Exception ex) { }
                    }
                    if (chkSpeed.Checked && etSpeed.Text != null && etSpeed.Text.Length > 0)
                    {
                        try
                        {
                            viGPSData.speed = Java.Lang.Float.ParseFloat(etSpeed.Text);
                        }
                        catch (Exception ex) { }
                    }

					if (chkShifted.Checked )
					{
						try
						{
							viGPSData.shifted = tglShifted.Checked;
						}
						catch (Exception ex) { }
					}
				});
				gpsAlertDialog.Show();
			};

			tire_pressure_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder tirePressureAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View tirePressureView = layoutInflater.Inflate(Resource.Layout.vi_tire_status, null);
				tirePressureAlertDialog.SetView(tirePressureView);
				tirePressureAlertDialog.SetTitle("Tire Pressure");

				CheckBox pressure_tell_tale_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_chk);
				Spinner pressure_tell_tale_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_spinner);

				CheckBox left_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_chk);
				Spinner left_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_spinner);

				CheckBox right_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_chk);
				Spinner right_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_spinner);

				CheckBox left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_chk);
				Spinner left_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_spinner);

				CheckBox right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_chk);
				Spinner right_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_spinner);

				CheckBox inner_left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_chk);
				Spinner inner_left_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_spinner);

				CheckBox inner_right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_chk);
				Spinner inner_right_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_spinner);

				var WarningLightStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, WarningLightStatusArray);
				pressure_tell_tale_spn.Adapter = WarningLightStatusAdapter;

				var SingleTireStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, SingleTireStatusArray);
				left_front_spn.Adapter = SingleTireStatusAdapter;
				right_front_spn.Adapter = SingleTireStatusAdapter;
				left_rear_spn.Adapter = SingleTireStatusAdapter;
				right_rear_spn.Adapter = SingleTireStatusAdapter;
				inner_left_rear_spinner.Adapter = SingleTireStatusAdapter;
				inner_right_rear_spinner.Adapter = SingleTireStatusAdapter;

				pressure_tell_tale_chk.CheckedChange += (sender1, e1) => pressure_tell_tale_spn.Enabled = e1.IsChecked;
				left_front_chk.CheckedChange += (sender1, e1) => left_front_spn.Enabled = e1.IsChecked;
				right_front_chk.CheckedChange += (sender1, e1) => right_front_spn.Enabled = e1.IsChecked;
				left_rear_chk.CheckedChange += (sender1, e1) => left_rear_spn.Enabled = e1.IsChecked;
				right_rear_chk.CheckedChange += (sender1, e1) => right_rear_spn.Enabled = e1.IsChecked;
				inner_left_rear_chk.CheckedChange += (sender1, e1) => inner_left_rear_spinner.Enabled = e1.IsChecked;
				inner_right_rear_chk.CheckedChange += (sender1, e1) => inner_right_rear_spinner.Enabled = e1.IsChecked;

                if (viTireStatus != null)
                {
                    if (null != viTireStatus.getPressureTelltale())
                    {
                        pressure_tell_tale_spn.SetSelection((int)viTireStatus.getPressureTelltale());
                    }
                    else
                    {
                        pressure_tell_tale_spn.Enabled = false;
                        pressure_tell_tale_chk.Checked = false;
                    }

                    if (viTireStatus.getleftFront() != null && (null != viTireStatus.getleftFront().getStatus()))
                    {
                        left_front_spn.SetSelection((int)viTireStatus.getleftFront().getStatus());
                    }
                    else
                    {
                        left_front_spn.Enabled = false;
                        left_front_chk.Checked = false;
                    }
                    if (viTireStatus.getRightFront() != null && (null != viTireStatus.getRightFront().getStatus()))
                    {
                        right_front_spn.SetSelection((int)viTireStatus.getRightFront().getStatus());
                    }
                    else
                    {
                        right_front_spn.Enabled = false;
                        right_front_chk.Checked = false;
                    }
                    if (viTireStatus.getLeftRear() != null && (null != viTireStatus.getLeftRear().getStatus()))
                    {
                        left_rear_spn.SetSelection((int)viTireStatus.getLeftRear().getStatus());
                    }
                    else
                    {
                        left_rear_spn.Enabled = false;
                        left_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getRightRear() != null && (null != viTireStatus.getRightRear().getStatus()))
                    {
                        right_rear_spn.SetSelection((int)viTireStatus.getRightRear().getStatus());
                    }
                    else
                    {
                        right_rear_spn.Enabled = false;
                        right_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getInnerLeftRear() != null && (null != viTireStatus.getInnerLeftRear().getStatus()))
                    {
                        inner_left_rear_spinner.SetSelection((int)viTireStatus.getInnerLeftRear().getStatus());
                    }
                    else
                    {
                        inner_left_rear_spinner.Enabled = false;
                        inner_left_rear_chk.Checked = false;
                    }
                    if (viTireStatus.getInnerRightRear() != null && (null != viTireStatus.getInnerRightRear().getStatus()))
                    {
                        inner_right_rear_spinner.SetSelection((int)viTireStatus.getInnerRightRear().getStatus());
                    }
                    else
                    {
                        inner_right_rear_spinner.Enabled = false;
                        inner_right_rear_chk.Checked = false;
                    }
                }

				tirePressureAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					tirePressureAlertDialog.Dispose();
				});
				tirePressureAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viTireStatus = new TireStatus();
                    if (pressure_tell_tale_chk.Checked)
                    {
                        viTireStatus.pressureTelltale = (WarningLightStatus)pressure_tell_tale_spn.SelectedItemPosition;
                    }
                    if (left_front_chk.Checked)
                    {
                        SingleTireStatus leftFront = new SingleTireStatus();
                        leftFront.status = (ComponentVolumeStatus)left_front_spn.SelectedItemPosition;
                        viTireStatus.leftFront = leftFront;
                    }
                    if (right_front_chk.Checked)
                    {
                        SingleTireStatus right_front = new SingleTireStatus();
                        right_front.status = (ComponentVolumeStatus)right_front_spn.SelectedItemPosition;
                        viTireStatus.rightFront = right_front;
                    }
                    if (left_rear_chk.Checked)
                    {
                        SingleTireStatus left_rear = new SingleTireStatus();
                        left_rear.status = (ComponentVolumeStatus)left_rear_spn.SelectedItemPosition;
                        viTireStatus.leftRear = left_rear;
                    }
                    if (right_rear_chk.Checked)
                    {
                        SingleTireStatus right_rear = new SingleTireStatus();
                        right_rear.status = (ComponentVolumeStatus)right_rear_spn.SelectedItemPosition;
                        viTireStatus.rightRear = right_rear;
                    }
                    if (inner_left_rear_chk.Checked)
                    {
                        SingleTireStatus inner_left_rear = new SingleTireStatus();
                        inner_left_rear.status = (ComponentVolumeStatus)inner_left_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerLeftRear = inner_left_rear;
                    }
                    if (inner_right_rear_chk.Checked)
                    {
                        SingleTireStatus inner_right_rear = new SingleTireStatus();
                        inner_right_rear.status = (ComponentVolumeStatus)inner_right_rear_spinner.SelectedItemPosition;
                        viTireStatus.innerRightRear = inner_right_rear;
                    }
				});
				tirePressureAlertDialog.Show();
			};

			belt_status_button.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder beltStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View beltStatusView = layoutInflater.Inflate(Resource.Layout.vi_belt_status, null);
				beltStatusAlertDialog.SetView(beltStatusView);
				beltStatusAlertDialog.SetTitle("Belt Status");

				CheckBox driver_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_chk);
				Spinner driver_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_spinner);

				CheckBox passenger_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_chk);
				Spinner passenger_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_spinner);

				CheckBox passenger_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_chk);
				Spinner passenger_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_spinner);

				CheckBox driver_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_chk);
				Spinner driver_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_spinner);

				CheckBox left_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_chk);
				Spinner left_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_spinner);

				CheckBox passenger_child_detected_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_chk);
				Spinner passenger_child_detected_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_spinner);

				CheckBox right_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_chk);
				Spinner right_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_spinner);

				CheckBox middle_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_chk);
				Spinner middle_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_spinner);

				CheckBox middle_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_chk);
				Spinner middle_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_spinner);

				CheckBox left_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_chk);
				Spinner left_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_spinner);

				CheckBox right_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_chk);
				Spinner right_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_spinner);

				CheckBox left_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_chk);
				Spinner left_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_spinner);

				CheckBox right_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_chk);
				Spinner right_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_spinner);

				CheckBox middle_row_1_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_chk);
				Spinner middle_row_1_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_spinner);

				CheckBox middle_row_1_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_chk);
				Spinner middle_row_1_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_spinner);

				driver_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_child_detected_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_1_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_1_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;

                if (viBeltStatus != null)
                {
                    if (null != viBeltStatus.getDriverBeltDeployed())
                    {
                        driver_belt_deployed_spinner.SetSelection((int)viBeltStatus.getDriverBeltDeployed());
                    }
                    else
                    {
                        driver_belt_deployed_spinner.Enabled = false;
                        driver_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerBeltDeployed())
                    {
                        passenger_belt_deployed_spinner.SetSelection((int)viBeltStatus.getPassengerBeltDeployed());
                    }
                    else
                    {
                        passenger_belt_deployed_spinner.Enabled = false;
                        passenger_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerBuckleBelted())
                    {
                        passenger_buckle_belted_spinner.SetSelection((int)viBeltStatus.getPassengerBuckleBelted());
                    }
                    else
                    {
                        passenger_buckle_belted_spinner.Enabled = false;
                        passenger_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getDriverBuckleBelted())
                    {
                        driver_buckle_belted_spinner.SetSelection((int)viBeltStatus.getDriverBuckleBelted());
                    }
                    else
                    {
                        driver_buckle_belted_spinner.Enabled = false;
                        driver_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRow2BuckleBelted())
                    {
                        left_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow2BuckleBelted());
                    }
                    else
                    {
                        left_row_2_buckle_belted_spinner.Enabled = false;
                        left_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getPassengerChildDetected())
                    {
                        passenger_child_detected_spinner.SetSelection((int)viBeltStatus.getPassengerChildDetected());
                    }
                    else
                    {
                        passenger_child_detected_spinner.Enabled = false;
                        passenger_child_detected_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRow2BuckleBelted())
                    {
                        right_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow2BuckleBelted());
                    }
                    else
                    {
                        right_row_2_buckle_belted_spinner.Enabled = false;
                        right_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow2BuckleBelted())
                    {
                        middle_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow2BuckleBelted());
                    }
                    else
                    {
                        middle_row_2_buckle_belted_spinner.Enabled = false;
                        middle_row_2_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow3BuckleBelted())
                    {
                        middle_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow3BuckleBelted());
                    }
                    else
                    {
                        middle_row_3_buckle_belted_spinner.Enabled = false;
                        middle_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRow3BuckleBelted())
                    {
                        left_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow3BuckleBelted());
                    }
                    else
                    {
                        left_row_3_buckle_belted_spinner.Enabled = false;
                        left_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRow3BuckleBelted())
                    {
                        right_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow3BuckleBelted());
                    }
                    else
                    {
                        right_row_3_buckle_belted_spinner.Enabled = false;
                        right_row_3_buckle_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getLeftRearInflatableBelted())
                    {
                        left_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getLeftRearInflatableBelted());
                    }
                    else
                    {
                        left_rear_inflatable_belted_spinner.Enabled = false;
                        left_rear_inflatable_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getRightRearInflatableBelted())
                    {
                        right_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getRightRearInflatableBelted());
                    }
                    else
                    {
                        right_rear_inflatable_belted_spinner.Enabled = false;
                        right_rear_inflatable_belted_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow1BeltDeployed())
                    {
                        middle_row_1_belt_deployed_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BeltDeployed());
                    }
                    else
                    {
                        middle_row_1_belt_deployed_spinner.Enabled = false;
                        middle_row_1_belt_deployed_chk.Checked = false;
                    }
                    if (null != viBeltStatus.getMiddleRow1BuckleBelted())
                    {
                        middle_row_1_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BuckleBelted());
                    }
                    else
                    {
                        middle_row_1_buckle_belted_spinner.Enabled = false;
                        middle_row_1_buckle_belted_chk.Checked = false;
                    }
                }

				driver_belt_deployed_chk.CheckedChange += (sender1, e1) => driver_belt_deployed_spinner.Enabled = e1.IsChecked;
				passenger_belt_deployed_chk.CheckedChange += (sender1, e1) => passenger_belt_deployed_spinner.Enabled = e1.IsChecked;
				passenger_buckle_belted_chk.CheckedChange += (sender1, e1) => passenger_buckle_belted_spinner.Enabled = e1.IsChecked;
				driver_buckle_belted_chk.CheckedChange += (sender1, e1) => driver_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				passenger_child_detected_chk.CheckedChange += (sender1, e1) => passenger_child_detected_spinner.Enabled = e1.IsChecked;
				right_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				middle_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				middle_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				right_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => left_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
				right_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => right_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
				middle_row_1_belt_deployed_chk.CheckedChange += (sender1, e1) => middle_row_1_belt_deployed_spinner.Enabled = e1.IsChecked;
				middle_row_1_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_1_buckle_belted_spinner.Enabled = e1.IsChecked;

				beltStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					beltStatusAlertDialog.Dispose();
				});
				beltStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viBeltStatus = new BeltStatus();
					if (driver_belt_deployed_chk.Checked)
					{
						viBeltStatus.driverBeltDeployed = (VehicleDataEventStatus)driver_belt_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_belt_deployed_chk.Checked)
					{
						viBeltStatus.passengerBeltDeployed = (VehicleDataEventStatus)passenger_belt_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_buckle_belted_chk.Checked)
					{
						viBeltStatus.passengerBuckleBelted = (VehicleDataEventStatus)passenger_buckle_belted_spinner.SelectedItemPosition;
					}
					if (driver_buckle_belted_chk.Checked)
					{
						viBeltStatus.driverBuckleBelted = (VehicleDataEventStatus)driver_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.leftRow2BuckleBelted = (VehicleDataEventStatus)left_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (passenger_child_detected_chk.Checked)
					{
						viBeltStatus.passengerChildDetected = (VehicleDataEventStatus)passenger_child_detected_spinner.SelectedItemPosition;
					}
					if (right_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.rightRow2BuckleBelted = (VehicleDataEventStatus)right_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow2BuckleBelted = (VehicleDataEventStatus)middle_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow3BuckleBelted = (VehicleDataEventStatus)middle_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.leftRow3BuckleBelted = (VehicleDataEventStatus)left_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (right_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.rightRow3BuckleBelted = (VehicleDataEventStatus)right_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_rear_inflatable_belted_chk.Checked)
					{
						viBeltStatus.leftRearInflatableBelted = (VehicleDataEventStatus)left_rear_inflatable_belted_spinner.SelectedItemPosition;
					}
					if (right_rear_inflatable_belted_chk.Checked)
					{
						viBeltStatus.rightRearInflatableBelted = (VehicleDataEventStatus)right_rear_inflatable_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_1_belt_deployed_chk.Checked)
					{
						viBeltStatus.middleRow1BeltDeployed = (VehicleDataEventStatus)middle_row_1_belt_deployed_spinner.SelectedItemPosition;
					}
					if (middle_row_1_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow1BuckleBelted = (VehicleDataEventStatus)middle_row_1_buckle_belted_spinner.SelectedItemPosition;
					}
				});
				beltStatusAlertDialog.Show();
			};

			body_info_button.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder bodyInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View bodyInfoView = layoutInflater.Inflate(Resource.Layout.vi_body_information, null);
				bodyInfoAlertDialog.SetView(bodyInfoView);
				bodyInfoAlertDialog.SetTitle("Body Info");

				CheckBox park_brake_active = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active);
                Switch park_brake_active_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active_tgl);

				CheckBox ignition_stable_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_chk);
				Spinner ignition_stable_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_spinner);

				CheckBox ignition_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_chk);
				Spinner ignition_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_spinner);

                CheckBox driver_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar);
                Switch driver_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar_tgl);

                CheckBox passenger_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar);
                Switch passenger_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar_tgl);

                CheckBox rear_left_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar);
                Switch rear_left_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar_tgl);

                CheckBox rear_right_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar);
                Switch rear_right_door_ajar_tgl = (Switch)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar_tgl);

				var IgnitionStableStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStableStatusArray);
				ignition_stable_status_spinner.Adapter = IgnitionStableStatusAdapter;

				var IgnitionStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStatusArray);
				ignition_status_spinner.Adapter = IgnitionStatusAdapter;

				park_brake_active.CheckedChange += (sender1, e1) =>
				{		
					park_brake_active_tgl.Enabled = e1.IsChecked;
				};
                ignition_stable_status_chk.CheckedChange += (sender1, e1) =>ignition_stable_status_spinner.Enabled = e1.IsChecked;
                ignition_status_chk.CheckedChange += (sender1, e1) => ignition_status_spinner.Enabled = e1.IsChecked;
				driver_door_ajar.CheckedChange += (sender1, e1) =>
				{
					driver_door_ajar_tgl.Enabled = e1.IsChecked;
				};
				passenger_door_ajar.CheckedChange += (sender1, e1) =>
				{
					passenger_door_ajar_tgl.Enabled = e1.IsChecked; };
				rear_left_door_ajar.CheckedChange += (sender1, e1) =>
				{
					rear_left_door_ajar_tgl.Enabled = e1.IsChecked;
				};
				rear_right_door_ajar.CheckedChange += (sender1, e1) =>
				{
					rear_right_door_ajar_tgl.Enabled = e1.IsChecked;
				};

                if (viBodyInformation != null)
                {
                    if (null != viBodyInformation.getParkBrakeActive())
                    {
                        park_brake_active_tgl.Checked = (bool)viBodyInformation.getParkBrakeActive();
                    }
                    else
                    {
                        park_brake_active.Checked = false;
                        park_brake_active_tgl.Enabled = false;
                    }

                    if (null != viBodyInformation.getIgnitionStableStatus())
                    {
                        ignition_stable_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStableStatus());
                    }
                    else
                    {
                        ignition_stable_status_chk.Checked = false;
                        ignition_stable_status_spinner.Enabled = false;
                    }

                    if (null != viBodyInformation.getIgnitionStatus())
                    {
                        ignition_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStatus());
                    }
                    else
                    {
                        ignition_status_spinner.Enabled = false;
                        ignition_status_chk.Checked = false;
                    }

                    if (null != viBodyInformation.getDriverDoorAjar())
                    {
                        driver_door_ajar_tgl.Checked = (bool)viBodyInformation.getDriverDoorAjar();
                    }
                    else
                    {
                        driver_door_ajar.Checked = false;
                        driver_door_ajar_tgl.Enabled = false;
                    }

                    if (null != viBodyInformation.getPassengerDoorAjar())
                    {
                        passenger_door_ajar_tgl.Checked = (bool)viBodyInformation.getPassengerDoorAjar();
                    }
                    else
                    {
                        passenger_door_ajar.Checked = false;
                        passenger_door_ajar_tgl.Enabled = false;
                    }


                    if (null != viBodyInformation.getRearLeftDoorAjar())
                    {
                        rear_left_door_ajar_tgl.Checked = (bool)viBodyInformation.getRearLeftDoorAjar();
                    }
                    else
                    {
                        rear_left_door_ajar.Checked = false;
                        rear_left_door_ajar_tgl.Enabled = false;
                    }

                    if (null != viBodyInformation.getRearRightDoorAjar())
                    {
                        rear_right_door_ajar_tgl.Checked = (bool)viBodyInformation.getRearRightDoorAjar();
                    }
                    else
                    {
                        rear_right_door_ajar.Checked = false;
                        rear_right_door_ajar_tgl.Enabled = false;
                    }
                }

				bodyInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					bodyInfoAlertDialog.Dispose();
				});
				bodyInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viBodyInformation = new BodyInformation();
                    if (park_brake_active.Checked)
                        viBodyInformation.parkBrakeActive = park_brake_active_tgl.Checked;
					if (ignition_stable_status_chk.Checked)
					{
						viBodyInformation.ignitionStableStatus = (IgnitionStableStatus)ignition_stable_status_spinner.SelectedItemPosition;
					}
					if (ignition_status_chk.Checked)
					{
						viBodyInformation.ignitionStatus = (IgnitionStatus)ignition_status_spinner.SelectedItemPosition;
					}
                    if (driver_door_ajar.Checked)
                        viBodyInformation.driverDoorAjar = driver_door_ajar_tgl.Checked;
                    if (passenger_door_ajar.Checked)
                        viBodyInformation.passengerDoorAjar = passenger_door_ajar_tgl.Checked;
                    if (rear_left_door_ajar.Checked)
                        viBodyInformation.rearLeftDoorAjar = rear_left_door_ajar_tgl.Checked;
                    if (rear_right_door_ajar.Checked)
                        viBodyInformation.rearRightDoorAjar = rear_right_door_ajar_tgl.Checked;
				});
				bodyInfoAlertDialog.Show();
			};

			device_status_button.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder deviceStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View deviceStatusView = layoutInflater.Inflate(Resource.Layout.vi_device_status, null);
				deviceStatusAlertDialog.SetView(deviceStatusView);
				deviceStatusAlertDialog.SetTitle("Device Status");

                CheckBox voice_rec_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on);
                Switch voice_rec_on_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on_tgl);

                CheckBox bt_icon_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on);
                Switch bt_icon_on_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on_tgl);

                CheckBox call_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_call_active);
                Switch call_active_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_call_active_tgl);

                CheckBox phone_roaming = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming);
                Switch phone_roaming_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming_tgl);

                CheckBox text_msg_available = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available);
                Switch text_msg_available_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available_tgl);

                CheckBox batt_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_chk);
                Spinner batt_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_spinner);

                CheckBox stereo_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted);
                Switch stereo_audio_output_muted_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted_tgl);

                CheckBox mono_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted);
                Switch mono_audio_output_muted_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted_tgl);

                CheckBox signal_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_chk);
                Spinner signal_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_spinner);

                CheckBox primary_audio_source_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_chk);
                Spinner primary_audio_source_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_spinner);

                CheckBox ecall_event_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active);
                Switch ecall_event_active_tgl = (Switch)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active_tgl);

				var battLevelStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DeviceLevelStatusArray);
				batt_level_status_spinner.Adapter = battLevelStatusAdapter;
				signal_level_status_spinner.Adapter = battLevelStatusAdapter;

				var PrimaryAudioSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PrimaryAudioSourceArray);
				primary_audio_source_spinner.Adapter = PrimaryAudioSourceAdapter;

                voice_rec_on.CheckedChange += (sender1, e1) => voice_rec_on_tgl.Enabled = e1.IsChecked;
                bt_icon_on.CheckedChange += (sender1, e1) => bt_icon_on_tgl.Enabled = e1.IsChecked;
                call_active.CheckedChange += (sender1, e1) => call_active_tgl.Enabled = e1.IsChecked;
                phone_roaming.CheckedChange += (sender1, e1) => phone_roaming_tgl.Enabled = e1.IsChecked;
                text_msg_available.CheckedChange += (sender1, e1) => text_msg_available_tgl.Enabled = e1.IsChecked;
                batt_level_status_chk.CheckedChange += (sender1, e1) => batt_level_status_spinner.Enabled = e1.IsChecked;
                stereo_audio_output_muted.CheckedChange += (sender1, e1) => stereo_audio_output_muted_tgl.Enabled = e1.IsChecked;
                mono_audio_output_muted.CheckedChange += (sender1, e1) => mono_audio_output_muted_tgl.Enabled = e1.IsChecked;
                signal_level_status_chk.CheckedChange += (sender1, e1) => signal_level_status_spinner.Enabled = e1.IsChecked;
                primary_audio_source_chk.CheckedChange += (sender1, e1) => primary_audio_source_spinner.Enabled = e1.IsChecked;
                ecall_event_active.CheckedChange += (sender1, e1) => ecall_event_active_tgl.Enabled = e1.IsChecked;

                if (viDeviceStatus != null)
                {
                    if (null != viDeviceStatus.voiceRecOn)
                        voice_rec_on_tgl.Checked = (bool)viDeviceStatus.voiceRecOn;
                    else
                        voice_rec_on.Checked = false;

                    if (null != viDeviceStatus.btIconOn)
                        bt_icon_on_tgl.Checked = (bool)viDeviceStatus.btIconOn;
                    else
                        bt_icon_on.Checked = false;

                    if (null != viDeviceStatus.callActive)
                        call_active_tgl.Checked = (bool)viDeviceStatus.callActive;
                    else
                        call_active.Checked = false;

                    if (null != viDeviceStatus.phoneRoaming)
                        phone_roaming_tgl.Checked = (bool)viDeviceStatus.phoneRoaming;
                    else
                        phone_roaming.Checked = false;

                    if (null != viDeviceStatus.textMsgAvailable)
                        text_msg_available_tgl.Checked = (bool)viDeviceStatus.textMsgAvailable;
                    else
                        text_msg_available.Checked = false;

                    if (null != viDeviceStatus.getBattLevelStatus())
                        batt_level_status_spinner.SetSelection((int)viDeviceStatus.getBattLevelStatus());
                    else
                        batt_level_status_chk.Checked = false;

                    if (null != viDeviceStatus.stereoAudioOutputMuted)
                        stereo_audio_output_muted_tgl.Checked = (bool)viDeviceStatus.stereoAudioOutputMuted;
                    else
                        stereo_audio_output_muted.Checked = false;

                    if (null != viDeviceStatus.monoAudioOutputMuted)
                        mono_audio_output_muted_tgl.Checked = (bool)viDeviceStatus.monoAudioOutputMuted;
                    else
                        mono_audio_output_muted.Checked = false;

                    if (null != viDeviceStatus.getSignalLevelStatus())
                        signal_level_status_spinner.SetSelection((int)viDeviceStatus.getSignalLevelStatus());
                    else
                        signal_level_status_chk.Checked = false;

                    if (null != viDeviceStatus.getPrimaryAudioSource())
                        primary_audio_source_spinner.SetSelection((int)viDeviceStatus.getPrimaryAudioSource());
                    else
                        primary_audio_source_chk.Checked = false;

                    if (null != viDeviceStatus.eCallEventActive)
                        ecall_event_active_tgl.Checked = (bool)viDeviceStatus.eCallEventActive;
                    else
                        ecall_event_active.Checked = false;
                }

				deviceStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					deviceStatusAlertDialog.Dispose();
				});
				deviceStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viDeviceStatus = new DeviceStatus();
                    if (voice_rec_on.Checked)
                        viDeviceStatus.voiceRecOn = voice_rec_on_tgl.Checked;
                    if (bt_icon_on.Checked)
                        viDeviceStatus.btIconOn = bt_icon_on_tgl.Checked;
                    if (call_active.Checked)
                        viDeviceStatus.callActive = call_active_tgl.Checked;
                    if (phone_roaming.Checked)
                        viDeviceStatus.phoneRoaming = phone_roaming_tgl.Checked;
                    if (text_msg_available.Checked)
                        viDeviceStatus.textMsgAvailable = text_msg_available_tgl.Checked;
                    if (batt_level_status_chk.Checked)
                        viDeviceStatus.battLevelStatus = (DeviceLevelStatus)batt_level_status_spinner.SelectedItemPosition;
                    if (stereo_audio_output_muted.Checked)
                        viDeviceStatus.stereoAudioOutputMuted = stereo_audio_output_muted_tgl.Checked;
                    if (mono_audio_output_muted.Checked)
                        viDeviceStatus.monoAudioOutputMuted = mono_audio_output_muted_tgl.Checked;
                    if (signal_level_status_chk.Checked)
                        viDeviceStatus.signalLevelStatus = (DeviceLevelStatus)signal_level_status_spinner.SelectedItemPosition;
                    if (primary_audio_source_chk.Checked)
                        viDeviceStatus.primaryAudioSource = (PrimaryAudioSource)primary_audio_source_spinner.SelectedItemPosition;
                    if (ecall_event_active.Checked)
                        viDeviceStatus.eCallEventActive = ecall_event_active_tgl.Checked;
				});
				deviceStatusAlertDialog.Show();
			};

			head_lamp_status_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder headLampAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View headLampView = layoutInflater.Inflate(Resource.Layout.vi_head_lamp_status, null);
				headLampAlertDialog.SetView(headLampView);
				headLampAlertDialog.SetTitle("Head Lamp Status");

                CheckBox low_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on_chk);
				CheckBox high_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on_chk);

                Switch low_beams_on_tgl = (Switch)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on_tgl);
                Switch high_beams_on_tgl = (Switch)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on_tgl);

				CheckBox ambient_light_sensor_status_chk = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_chk);
				Spinner ambient_light_sensor_status_spinner = (Spinner)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_spinner);

                var AmbientLightStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, AmbientLightStatusArray);
				ambient_light_sensor_status_spinner.Adapter = AmbientLightStatusAdapter;

				low_beams_on.CheckedChange += (sender1, e1) =>
				{
					low_beams_on_tgl.Enabled = e1.IsChecked;
				};
				high_beams_on.CheckedChange += (sender1, e1) =>
				{
					high_beams_on_tgl.Enabled = e1.IsChecked;
				};
				ambient_light_sensor_status_chk.CheckedChange += (sender1, e1) => ambient_light_sensor_status_spinner.Enabled = e1.IsChecked;

                if (viHeadLampStatus != null)
                {
                    if (null == viHeadLampStatus.lowBeamsOn)
					{
						low_beams_on.Checked = false;
						low_beams_on_tgl.Enabled = false;
					}
					else
					{
						low_beams_on.Checked = true;
						low_beams_on_tgl.Checked = (bool)viHeadLampStatus.lowBeamsOn;
					}
					if (null == viHeadLampStatus.highBeamsOn)
					{
						high_beams_on.Checked = false;
						high_beams_on_tgl.Enabled = false;
					}
					else
					{
						high_beams_on.Checked = true;
						high_beams_on_tgl.Checked = (bool)viHeadLampStatus.highBeamsOn;
					}
					if (null != viHeadLampStatus.getAmbientLightSensorStatus())
						ambient_light_sensor_status_spinner.SetSelection((int)viHeadLampStatus.getAmbientLightSensorStatus());
					else
						ambient_light_sensor_status_chk.Checked = false;


				}

				headLampAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					headLampAlertDialog.Dispose();
				});
				headLampAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viHeadLampStatus = new HeadLampStatus();
                    if (low_beams_on.Checked)
                        viHeadLampStatus.lowBeamsOn = low_beams_on_tgl.Checked;
                    if (high_beams_on.Checked)
                        viHeadLampStatus.highBeamsOn = high_beams_on_tgl.Checked;
                    if (ambient_light_sensor_status_chk.Checked)
                    {
                        viHeadLampStatus.ambientLightSensorStatus = (AmbientLightStatus)ambient_light_sensor_status_spinner.SelectedItemPosition;
                    }
				});
				headLampAlertDialog.Show();
			};

			ecall_info_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder eCallInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View eCallInfoView = layoutInflater.Inflate(Resource.Layout.vi_ecall_info, null);
				eCallInfoAlertDialog.SetView(eCallInfoView);
				eCallInfoAlertDialog.SetTitle("ECall Info");

				CheckBox ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_chk);
				Spinner ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_spinner);

				CheckBox aux_ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_chk);
				Spinner aux_ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_spinner);

				CheckBox ecall_confirmation_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_chk);
				Spinner ecall_confirmation_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_spinner);

				ecall_notification_status_chk.CheckedChange += (sender1, e1) => ecall_notification_status_spinner.Enabled = e1.IsChecked;
				aux_ecall_notification_status_chk.CheckedChange += (sender1, e1) => aux_ecall_notification_status_spinner.Enabled = e1.IsChecked;
				ecall_confirmation_status_chk.CheckedChange += (sender1, e1) => ecall_confirmation_status_spinner.Enabled = e1.IsChecked;

				var vehicleDataNotificationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataNotificationStatusArray);
				ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;
				aux_ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;

				var ECallConfirmationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, ECallConfirmationStatusArray);
				ecall_confirmation_status_spinner.Adapter = ECallConfirmationStatusAdapter;

				if (null != viECallInfo)
				{
                    if (null != viECallInfo.getECallNotificationStatus())
                        ecall_notification_status_spinner.SetSelection((int)viECallInfo.getECallNotificationStatus());
                    else
                        ecall_notification_status_chk.Checked = false;
                    
                    if (null != viECallInfo.getAuxECallNotificationStatus())
                        aux_ecall_notification_status_spinner.SetSelection((int)viECallInfo.getAuxECallNotificationStatus());
                    else
                        aux_ecall_notification_status_chk.Checked = false;
                    
                    if (null != viECallInfo.getECallConfirmationStatus())
                        ecall_confirmation_status_spinner.SetSelection((int)viECallInfo.getECallConfirmationStatus());
                    else
                        ecall_confirmation_status_chk.Checked = false;
				}

				eCallInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					eCallInfoAlertDialog.Dispose();
				});
				eCallInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viECallInfo = new ECallInfo();
					if (ecall_notification_status_chk.Checked)
					{
						viECallInfo.eCallNotificationStatus = (VehicleDataNotificationStatus)ecall_notification_status_spinner.SelectedItemPosition;
					}
					if (aux_ecall_notification_status_chk.Checked)
					{
						viECallInfo.auxECallNotificationStatus = (VehicleDataNotificationStatus)aux_ecall_notification_status_spinner.SelectedItemPosition;
					}
					if (ecall_confirmation_status_chk.Checked)
					{
						viECallInfo.eCallConfirmationStatus = (ECallConfirmationStatus)ecall_confirmation_status_spinner.SelectedItemPosition;
					}
				});
				eCallInfoAlertDialog.Show();
			};

			airbag_status_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder airbagStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View airbagStatusView = layoutInflater.Inflate(Resource.Layout.vi_airbag_status, null);
				airbagStatusAlertDialog.SetView(airbagStatusView);
				airbagStatusAlertDialog.SetTitle("Airbag Status");

				CheckBox driver_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_chk);
				Spinner driver_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_spinner);

				CheckBox driver_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_chk);
				Spinner driver_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_spinner);

				CheckBox driver_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_chk);
				Spinner driver_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_spinner);

				CheckBox passenger_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_chk);
				Spinner passenger_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_spinner);

				CheckBox passenger_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_chk);
				Spinner passenger_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_spinner);

				CheckBox driver_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_chk);
				Spinner driver_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_spinner);

				CheckBox passenger_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_chk);
				Spinner passenger_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_spinner);

				CheckBox passenger_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_chk);
				Spinner passenger_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_spinner);

				driver_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;

				driver_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;

                if (null != viAirbagStatus)
                {
                    if (null != viAirbagStatus.getDriverAirbagDeployed())
                    {
                        driver_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverAirbagDeployed());
                    }
                    else
                    {
                        driver_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getDriverSideAirbagDeployed())
                    {
                        driver_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverSideAirbagDeployed());
                    }
                    else
                    {
                        driver_side_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getDriverCurtainAirbagDeployed())
                    {
                        driver_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverCurtainAirbagDeployed());
                    }
                    else
                    {
                        driver_curtain_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getPassengerAirbagDeployed())
                    {
                        passenger_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerAirbagDeployed());
                    }
                    else
                    {
                        passenger_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getPassengerCurtainAirbagDeployed())
                    {
                        passenger_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerCurtainAirbagDeployed());
                    }
                    else
                    {
                        passenger_curtain_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getDriverKneeAirbagDeployed())
                    {
                        driver_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverKneeAirbagDeployed());
                    }
                    else
                    {
                        driver_knee_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getPassengerSideAirbagDeployed())
                    {
                        passenger_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerSideAirbagDeployed());
                    }
                    else
                    {
                        passenger_side_airbag_deployed_chk.Checked = false;
                    }

                    if (null != viAirbagStatus.getPassengerKneeAirbagDeployed())
                    {
                        passenger_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerKneeAirbagDeployed());
                    }
                    else
                    {
                        passenger_knee_airbag_deployed_chk.Checked = false;
                    }
                }

				airbagStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					airbagStatusAlertDialog.Dispose();
				});
				airbagStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viAirbagStatus = new AirbagStatus();
					if (driver_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverAirbagDeployed = (VehicleDataEventStatus)driver_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_side_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverSideAirbagDeployed = (VehicleDataEventStatus)driver_side_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_curtain_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverCurtainAirbagDeployed = (VehicleDataEventStatus)driver_curtain_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerAirbagDeployed = (VehicleDataEventStatus)passenger_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_curtain_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerCurtainAirbagDeployed = (VehicleDataEventStatus)passenger_curtain_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_knee_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverKneeAirbagDeployed = (VehicleDataEventStatus)driver_knee_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_side_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerSideAirbagDeployed = (VehicleDataEventStatus)passenger_side_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_knee_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerKneeAirbagDeployed = (VehicleDataEventStatus)passenger_knee_airbag_deployed_spinner.SelectedItemPosition;
					}
				});
				airbagStatusAlertDialog.Show();
			};

			emergency_event_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder emergencyEventAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View emergencyEventView = layoutInflater.Inflate(Resource.Layout.vi_emergency_event, null);
				emergencyEventAlertDialog.SetView(emergencyEventView);
				emergencyEventAlertDialog.SetTitle("Emergency Event");

				CheckBox emergency_event_type_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_chk);
				Spinner emergency_event_type_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_spinner);

				CheckBox fuel_cutoff_status_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_chk);
				Spinner fuel_cutoff_status_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_spinner);

				CheckBox rollover_event_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_chk);
				Spinner rollover_event_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_spinner);

				CheckBox maximum_change_velocity_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_chk);
				Spinner maximum_change_velocity_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_spinner);

				CheckBox multiple_events_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_chk);
				Spinner multiple_events_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_spinner);

                var EmergencyEventTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, EmergencyEventTypeArray);
				emergency_event_type_spinner.Adapter = EmergencyEventTypeAdapter;

				var FuelCutoffStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, FuelCutoffStatusArray);
				fuel_cutoff_status_spinner.Adapter = FuelCutoffStatusAdapter;

				rollover_event_spinner.Adapter = VehicleDataEventStatusAdapter;
				maximum_change_velocity_spinner.Adapter = VehicleDataEventStatusAdapter;
				multiple_events_spinner.Adapter = VehicleDataEventStatusAdapter;

                emergency_event_type_chk.CheckedChange += (sender1, e1) => emergency_event_type_spinner.Enabled = e1.IsChecked;
                fuel_cutoff_status_chk.CheckedChange += (sender1, e1) => fuel_cutoff_status_spinner.Enabled = e1.IsChecked;
                rollover_event_chk.CheckedChange += (sender1, e1) => rollover_event_spinner.Enabled = e1.IsChecked;
                maximum_change_velocity_chk.CheckedChange += (sender1, e1) => maximum_change_velocity_spinner.Enabled = e1.IsChecked;
                multiple_events_chk.CheckedChange += (sender1, e1) => multiple_events_spinner.Enabled = e1.IsChecked;

                if (null != viEmergencyEvent)
                {
                    if (null != viEmergencyEvent.getEmergencyEventType())
                    {
                        emergency_event_type_spinner.SetSelection((int)viEmergencyEvent.getEmergencyEventType());
                    }
                    else
                    {
                        emergency_event_type_chk.Checked = false;
                    }

                    if (null != viEmergencyEvent.getFuelCutoffStatus())
                    {
                        fuel_cutoff_status_spinner.SetSelection((int)viEmergencyEvent.getFuelCutoffStatus());
                    }
                    else
                    {
                        fuel_cutoff_status_chk.Checked = false;
                    }

                    if (null != viEmergencyEvent.getRolloverEvent())
                    {
                        rollover_event_spinner.SetSelection((int)viEmergencyEvent.getRolloverEvent());
                    }
                    else
                    {
                        rollover_event_chk.Checked = false;
                    }

                    if (null != viEmergencyEvent.getMaximumChangeVelocity())
                    {
                        maximum_change_velocity_spinner.SetSelection((int)viEmergencyEvent.getMaximumChangeVelocity());
                    }
                    else
                    {
                        maximum_change_velocity_chk.Checked = false;
                    }

                    if (null != viEmergencyEvent.getMultipleEvents())
                    {
                        multiple_events_spinner.SetSelection((int)viEmergencyEvent.getMultipleEvents());
                    }
                    else
                    {
                        multiple_events_chk.Checked = false;
                    }
                }

				emergencyEventAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					emergencyEventAlertDialog.Dispose();
				});
				emergencyEventAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viEmergencyEvent = new EmergencyEvent();
					if (emergency_event_type_chk.Checked)
					{
						viEmergencyEvent.emergencyEventType = (EmergencyEventType)emergency_event_type_spinner.SelectedItemPosition;
					}
					if (fuel_cutoff_status_chk.Checked)
					{
						viEmergencyEvent.fuelCutoffStatus = (FuelCutoffStatus)fuel_cutoff_status_spinner.SelectedItemPosition;
					}
					if (rollover_event_chk.Checked)
					{
						viEmergencyEvent.rolloverEvent = (VehicleDataEventStatus)rollover_event_spinner.SelectedItemPosition;
					}
					if (maximum_change_velocity_chk.Checked)
					{
						viEmergencyEvent.maximumChangeVelocity = (VehicleDataEventStatus)maximum_change_velocity_spinner.SelectedItemPosition;
					}
					if (multiple_events_chk.Checked)
					{
						viEmergencyEvent.multipleEvents = (VehicleDataEventStatus)multiple_events_spinner.SelectedItemPosition;
					}
				});
				emergencyEventAlertDialog.Show();
			};

			cluster_modes_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder clusterModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View clusterModeView = layoutInflater.Inflate(Resource.Layout.vi_cluster_mode_status, null);
				clusterModeAlertDialog.SetView(clusterModeView);
				clusterModeAlertDialog.SetTitle("Cluster Mode Status");

				CheckBox power_mode_active = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active);
                Switch power_mode_active_tgl = (Switch)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active_tgl);

				CheckBox power_mode_qualification_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_chk);
				Spinner power_mode_qualification_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_spinner);

				CheckBox car_mode_status_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_chk);
				Spinner car_mode_status_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_spinner);

				CheckBox power_mode_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_chk);
				Spinner power_mode_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_spinner);

				var PowerModeQualificationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeQualificationStatusArray);
				power_mode_qualification_status_spinner.Adapter = PowerModeQualificationStatusAdapter;

				var CarModeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, CarModeStatusArray);
				car_mode_status_status_spinner.Adapter = CarModeStatusAdapter;

				var PowerModeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeStatusArray);
				power_mode_status_spinner.Adapter = PowerModeStatusAdapter;

				power_mode_active.CheckedChange += (sender1, e1) =>
				{
					power_mode_active_tgl.Enabled = e1.IsChecked;
				};
                power_mode_qualification_status_chk.CheckedChange += (sender1, e1) => power_mode_qualification_status_spinner.Enabled = e1.IsChecked;
                car_mode_status_status_chk.CheckedChange += (sender1, e1) => car_mode_status_status_spinner.Enabled = e1.IsChecked;
                power_mode_status_chk.CheckedChange += (sender1, e1) => power_mode_status_spinner.Enabled = e1.IsChecked;

                if (null != viClusterModeStatus)
                {
					if (null != viClusterModeStatus.powerModeActive)
					{
						power_mode_active.Checked = (bool)viClusterModeStatus.powerModeActive;
					}
					else
					{
						power_mode_active.Checked = false;
					}

                    if (null != viClusterModeStatus.getPowerModeQualificationStatus())
                    {
                        power_mode_qualification_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeQualificationStatus());
                    }
                    else
                    {
                        power_mode_qualification_status_chk.Checked = false;
                    }

                    if (null != viClusterModeStatus.getCarModeStatus())
                    {
                        car_mode_status_status_spinner.SetSelection((int)viClusterModeStatus.getCarModeStatus());
                    }
                    else
                    {
                        car_mode_status_status_chk.Checked = false;
                    }

                    if (null != viClusterModeStatus.getPowerModeStatus())
                    {
                        power_mode_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeStatus());
                    }
                    else
                    {
                        power_mode_status_chk.Checked = false;
                    }
                }

				clusterModeAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					clusterModeAlertDialog.Dispose();
				});
				clusterModeAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
                    viClusterModeStatus = new ClusterModeStatus();
                    if (power_mode_active.Checked)
                        viClusterModeStatus.powerModeActive = power_mode_active_tgl.Checked;
                    
					if (power_mode_qualification_status_chk.Checked)
					{
						viClusterModeStatus.powerModeQualificationStatus = (PowerModeQualificationStatus)power_mode_qualification_status_spinner.SelectedItemPosition;
					}
					if (car_mode_status_status_chk.Checked)
					{
						viClusterModeStatus.carModeStatus = (CarModeStatus)car_mode_status_status_spinner.SelectedItemPosition;
					}
                    if (power_mode_status_chk.Checked)
					{
						viClusterModeStatus.powerModeStatus = (PowerModeStatus)power_mode_status_spinner.SelectedItemPosition;
					}
				});
				clusterModeAlertDialog.Show();
			};

            fuel_range_btn.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder fuelRangeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View fuelRangeView = layoutInflater.Inflate(Resource.Layout.add_fuel_range, null);
                fuelRangeAlertDialog.SetView(fuelRangeView);
                fuelRangeAlertDialog.SetTitle("Add Fuel Range");

                CheckBox checkBoxName = (CheckBox)fuelRangeView.FindViewById(Resource.Id.fuel_type_check);
                Spinner spnName = (Spinner)fuelRangeView.FindViewById(Resource.Id.fuel_type_spinner);

                string[] fuelTypeNames = Enum.GetNames(typeof(FuelType));
                var fuelTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, fuelTypeNames);
                spnName.Adapter = fuelTypeAdapter;

                CheckBox rangeChk = (CheckBox)fuelRangeView.FindViewById(Resource.Id.range_check);
                EditText rangeET = (EditText)fuelRangeView.FindViewById(Resource.Id.range_et);

                checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                rangeChk.CheckedChange += (sender, e) => rangeET.Enabled = e.IsChecked;

                fuelRangeAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    FuelRange fuelRange = new FuelRange();

                    if (checkBoxName.Checked)
                    {
                        fuelRange.type = (FuelType)spnName.SelectedItemPosition;
                    }
                    if (rangeChk.Checked)
                    {
                        try
                        {
                            fuelRange.range = Java.Lang.Float.ParseFloat(rangeET.Text);
                        }
                        catch (Exception e)
                        {
                            fuelRange.range = 0;
                        }
                    }

                   fuelRangeList.Add(fuelRange);
                    fuelRangeAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(fuel_range_lv);
                });

                fuelRangeAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    fuelRangeAlertDialog.Dispose();
                });
                fuelRangeAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				GPSData gpsdata = null;
				if (gps_data_chk.Checked)
				{
					gpsdata = viGPSData;
				}

				float? speed = null;
				if (speed_chk.Checked && speed_et.Text != null && speed_et.Text.Length > 0)
				{
					speed = Java.Lang.Float.ParseFloat(speed_et.Text);
				}

				int? rpm = null;
				if (rpm_chk.Checked && rpm_et.Text != null && rpm_et.Text.Length > 0)
				{
					rpm = Java.Lang.Integer.ParseInt(rpm_et.Text);
				}

				float? fuelLevel = null;
				if (fuel_level_chk.Checked && fuel_level_et.Text != null && fuel_level_et.Text.Length > 0)
				{
					fuelLevel = Java.Lang.Float.ParseFloat(fuel_level_et.Text);
				}

				ComponentVolumeStatus? fuelLevel_State = null;
				if (fuel_level_state_chk.Checked)
				{
					fuelLevel_State = (ComponentVolumeStatus)fuel_level_state_spinner.SelectedItemPosition;
				}

				float? instantFuelConsumption = null;
				if (instant_fuel_consumption_chk.Checked && instant_fuel_consumption_et.Text != null && instant_fuel_consumption_et.Text.Length > 0)
				{
					instantFuelConsumption = Java.Lang.Float.ParseFloat(instant_fuel_consumption_et.Text);
				}

				float? externalTemperature = null;
				if (external_temp_chk.Checked && external_temp_et.Text != null && external_temp_et.Text.Length > 0)
				{
					externalTemperature = Java.Lang.Float.ParseFloat(external_temp_et.Text);
				}

				string vin = null;
				if (vin_chk.Checked)
				{
					vin = vin_et.Text;
				}

				PRNDL? prndl = null;
				if (prndl_chk.Checked)
				{
					prndl = (PRNDL)prndl_spinner.SelectedItemPosition;
				}

                TurnSignal? turnSignal = null;
				if (turnSignal_chk.Checked)
				{
					turnSignal = (TurnSignal)turnSignal_spinner.SelectedItemPosition;
				}

				TireStatus tirePressure = null;
				if (tire_pressure_chk.Checked)
				{
					tirePressure = viTireStatus;
				}

				int? odometer = null;
				if (odometer_chk.Checked && odometer_et.Text != null && odometer_et.Text.Length > 0)
				{
					odometer = Java.Lang.Integer.ParseInt(odometer_et.Text);
				}

				BeltStatus beltStatus = null;
				if (belt_status_chk.Checked)
				{
					beltStatus = viBeltStatus;
				}

				BodyInformation bodyInformation = null;
				if (body_info_chk.Checked)
				{
					bodyInformation = viBodyInformation;
				}

				DeviceStatus deviceStatus = null;
				if (device_status_chk.Checked)
				{
					deviceStatus = viDeviceStatus;
				}

				VehicleDataEventStatus? driverBraking = null;
				if (driver_braking_chk.Checked)
				{
					driverBraking = (VehicleDataEventStatus)driver_braking_spinner.SelectedItemPosition;
				}

				WiperStatus? wiperStatus = null;
				if (wiper_status_chk.Checked)
				{
					wiperStatus = (WiperStatus)wiper_status_spinner.SelectedItemPosition;
				}

				HeadLampStatus headLampStatus = null;
				if (head_lamp_status_chk.Checked)
				{
					headLampStatus = viHeadLampStatus;
				}

				float? engineTorque = null;
				if (engine_torque_chk.Checked && engine_torque_et.Text != null && engine_torque_et.Text.Length > 0)
				{
					engineTorque = Java.Lang.Float.ParseFloat(engine_torque_et.Text);
				}

				float? accPedalPosition = null;
				if (acc_padel_position_chk.Checked && acc_padel_position_et.Text != null && acc_padel_position_et.Text.Length > 0)
				{
					accPedalPosition = Java.Lang.Float.ParseFloat(acc_padel_position_et.Text);
				}

				float? steeringWheelAngle = null;
				if (steering_wheel_angle_chk.Checked && steering_wheel_angle_et.Text != null && steering_wheel_angle_et.Text.Length > 0)
				{
					steeringWheelAngle = Java.Lang.Float.ParseFloat(steering_wheel_angle_et.Text);
				}

				ECallInfo eCallInfo = null;
				if (ecall_info_chk.Checked)
				{
					eCallInfo = viECallInfo;
				}

				AirbagStatus airbagStatus = null;
				if (airbag_status_chk.Checked)
				{
					airbagStatus = viAirbagStatus;
				}

				EmergencyEvent emergencyEvent = null;
				if (emergency_event_chk.Checked)
				{
					emergencyEvent = viEmergencyEvent;
				}

				ClusterModeStatus clusterModeStatus = null;
				if (cluster_modes_chk.Checked)
				{
					clusterModeStatus = viClusterModeStatus;
				}

				MyKey myKey = null;
				if (my_key_chk.Checked)
				{
					myKey = new MyKey();
					myKey.e911Override = (VehicleDataStatus)my_key_spinner.SelectedItemPosition;
				}

                ElectronicParkBrakeStatus? electronicParkBrakeStatus = null;

                if (electronic_park_brake_status_chk.Checked)
				{
					electronicParkBrakeStatus = (ElectronicParkBrakeStatus)electronic_park_brake_status_spinner.SelectedItemPosition;
				}

                float? engineOilLife = null;
                if (engine_oil_life_chk.Checked)
                {
                    try
                    {
                        engineOilLife = Java.Lang.Float.ParseFloat(engine_oil_life_et.Text);
                    }
                    catch (Exception e)
                    {
                        engineOilLife = null;
                    }
                }

                List<FuelRange> fuelRange = null;
                if (fuel_range_chk.Checked)
                {
                    fuelRange = fuelRangeList;
                }

				RequestNotifyMessage rpcMessage = BuildRpc.buildVehicleInfoOnVehicleData(gpsdata, speed, rpm, fuelLevel,
																						 fuelLevel_State, instantFuelConsumption, externalTemperature, vin,
                                                                                         prndl, turnSignal, tirePressure, odometer, beltStatus, bodyInformation,
																						deviceStatus, driverBraking, wiperStatus, headLampStatus,
																						engineTorque, accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus,
																						emergencyEvent, clusterModeStatus, myKey, electronicParkBrakeStatus,
                                                                                         engineOilLife, fuelRange);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
        }

        void CreateVIResponseSubscribeVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.subscribe_vehicle_data_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VIResponseSubscribeVehicleData);

            var resultCode_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_result_code_chk);
            var gps_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps);
			var speed_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed);
			var rpm_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm);
			var fuelLevel_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level);
			var fuelLevel_State_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state);
			var instantFuelConsumption_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption);
			var externalTemperature_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature);
			var prndl_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl);
            var turnSignal_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal);
			var tirePressure_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure);
			var odometer_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer);
			var beltStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status);
			var bodyInformation_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info);
			var deviceStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status);
			var driverBraking_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking);
			var wiperStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status);
			var headLampStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status);
			var engineTorque_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque);
			var accPedalPosition_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos);
			var steeringWheelAngle_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle);
			var eCallInfo_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info);
			var airbagStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status);
			var emergencyEvent_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event);
			var clusterModes_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes);
			var myKey_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key);
			var electronicParkBrakeStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status);
            var engineOilLife_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life);
            var fuelRange_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range);

			var gps_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_data_type_chk);
            var speed_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_data_type_chk);
            var rpm_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_date_type_chk);
            var fuelLevel_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_data_type_chk);
            var fuelLevel_State_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_chk);
            var instantFuelConsumption_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_chk);
            var externalTemperature_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_data_type_chk);
            var prndl_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_data_type_chk);
            var turnSignal_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal_data_type_chk);
            var tirePressure_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_chk);
            var odometer_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_data_type_chk);
            var beltStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_data_type_chk);
            var bodyInformation_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_data_type_chk);
            var deviceStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_data_type_chk);
            var driverBraking_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_data_type_chk);
            var wiperStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_data_type_chk);
            var headLampStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_chk);
            var engineTorque_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_data_type_chk);
            var accPedalPosition_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_chk);
            var steeringWheelAngle_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_chk);
            var eCallInfo_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_data_type_chk);
            var airbagStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_data_type_chk);
            var emergencyEvent_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_data_type_chk);
            var clusterModes_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_chk);
            var myKey_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_data_type_chk);
			var electronicParkBrakeStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_chk);
            var engineOilLife_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_chk);
            var fuelRange_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_data_type_chk);

			var gps_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_result_code_chk);
            var speed_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_result_code_chk);
            var rpm_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_result_code_chk);
            var fuelLevel_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_result_code_chk);
            var fuelLevel_State_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_chk);
            var instantFuelConsumption_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_chk);
            var externalTemperature_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_result_code_chk);
            var prndl_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_result_code_chk);
            var turnSignal_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_turnSignal_result_code_chk);
            var tirePressure_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_chk);
            var odometer_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_result_code_chk);
            var beltStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_result_code_chk);
            var bodyInformation_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_result_code_chk);
            var deviceStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_result_code_chk);
            var driverBraking_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_result_code_chk);
            var wiperStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_result_code_chk);
            var headLampStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_chk);
            var engineTorque_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_result_code_chk);
            var accPedalPosition_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_chk);
            var steeringWheelAngle_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_chk);
            var eCallInfo_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_result_code_chk);
            var airbagStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_result_code_chk);
            var emergencyEvent_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_result_code_chk);
            var clusterModes_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_chk);
            var myKey_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_result_code_chk);
			var electronicParkBrakeStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_chk);
            var engineOilLife_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_chk);
            var fuelRange_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_result_code_chk);

			var gps_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_data_type_spinner);
			var speed_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_data_type_spinner);
			var rpm_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_date_type_spinner);
			var fuelLevel_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_data_type_spinner);
			var fuelLevel_State_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_spinner);
			var instantFuelConsumption_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_spinner);
			var externalTemperature_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_data_type_spinner);
			var prndl_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_data_type_spinner);
            var turnSignal_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_turnSignal_data_type_spinner);
			var tirePressure_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_spinner);
			var odometer_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_data_type_spinner);
			var beltStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_data_type_spinner);
			var bodyInformation_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_data_type_spinner);
			var deviceStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_data_type_spinner);
			var driverBraking_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_data_type_spinner);
			var wiperStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_data_type_spinner);
			var headLampStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_spinner);
			var engineTorque_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_data_type_spinner);
			var accPedalPosition_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_spinner);
			var steeringWheelAngle_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_spinner);
			var eCallInfo_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_data_type_spinner);
			var airbagStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_data_type_spinner);
			var emergencyEvent_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_data_type_spinner);
			var clusterModes_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_spinner);
			var myKey_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_data_type_spinner);
            var electronicParkBrakeStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_spinner);
            var engineOilLife_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_spinner);
            var fuelRange_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_data_type_spinner);

			var result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_result_code_spinner);
			var gps_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_result_code_spinner);
			var speed_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_result_code_spinner);
			var rpm_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_result_code_spinner);
			var fuelLevel_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_result_code_spinner);
			var fuelLevel_State_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_spinner);
			var instantFuelConsumption_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_spinner);
			var externalTemperature_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_result_code_spinner);
			var prndl_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_result_code_spinner);
            var turnSignal_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_turnSignal_result_code_spinner);
			var tirePressure_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_spinner);
			var odometer_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_result_code_spinner);
			var beltStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_result_code_spinner);
			var bodyInformation_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_result_code_spinner);
			var deviceStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_result_code_spinner);
			var driverBraking_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_result_code_spinner);
			var wiperStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_result_code_spinner);
			var headLampStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_spinner);
			var engineTorque_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_result_code_spinner);
			var accPedalPosition_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_spinner);
			var steeringWheelAngle_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_spinner);
			var eCallInfo_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_result_code_spinner);
			var airbagStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_result_code_spinner);
			var emergencyEvent_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_result_code_spinner);
			var clusterModes_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_spinner);
			var myKey_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_result_code_spinner);
			var electronicParkBrakeStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_spinner);
            var engineOilLife_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_spinner);
            var fuelRange_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_result_code_spinner);

			resultCode_chk.CheckedChange += (sender, e) => result_code_spinner.Enabled = e.IsChecked;

            gps_chk.CheckedChange += (sender, e) =>
            {
                gps_data_type_chk.Enabled = e.IsChecked;
                gps_result_code_chk.Enabled = e.IsChecked;
                gps_data_type_spinner.Enabled = e.IsChecked && gps_data_type_chk.Checked;
                gps_result_code_spinner.Enabled = e.IsChecked && gps_result_code_chk.Checked;
            };

            speed_chk.CheckedChange += (sender, e) =>
            {
                speed_data_type_chk.Enabled = e.IsChecked;
                speed_result_code_chk.Enabled = e.IsChecked;
                speed_data_type_spinner.Enabled = e.IsChecked && speed_data_type_chk.Checked;
                speed_result_code_spinner.Enabled = e.IsChecked && speed_result_code_chk.Checked;
            };

            rpm_chk.CheckedChange += (sender, e) =>
            {
                rpm_data_type_chk.Enabled = e.IsChecked;
                rpm_result_code_chk.Enabled = e.IsChecked;
                rpm_data_type_spinner.Enabled = e.IsChecked && rpm_data_type_chk.Checked;
                rpm_result_code_spinner.Enabled = e.IsChecked && rpm_result_code_chk.Checked;
            };

            fuelLevel_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_data_type_spinner.Enabled = e.IsChecked && fuelLevel_data_type_chk.Checked;
                fuelLevel_result_code_spinner.Enabled = e.IsChecked && fuelLevel_result_code_chk.Checked;
            };

            fuelLevel_State_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_State_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_State_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_State_data_type_spinner.Enabled = e.IsChecked && fuelLevel_State_data_type_chk.Checked;
                fuelLevel_State_result_code_spinner.Enabled = e.IsChecked && fuelLevel_State_result_code_chk.Checked;
            };

            instantFuelConsumption_chk.CheckedChange += (sender, e) =>
            {
                instantFuelConsumption_data_type_chk.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_chk.Enabled = e.IsChecked;
                instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked && instantFuelConsumption_data_type_chk.Checked;
                instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked && instantFuelConsumption_result_code_chk.Checked;
            };

            externalTemperature_chk.CheckedChange += (sender, e) =>
            {
                externalTemperature_data_type_chk.Enabled = e.IsChecked;
                externalTemperature_result_code_chk.Enabled = e.IsChecked;
                externalTemperature_data_type_spinner.Enabled = e.IsChecked && externalTemperature_data_type_chk.Checked;
                externalTemperature_result_code_spinner.Enabled = e.IsChecked && externalTemperature_result_code_chk.Checked;
            };

            prndl_chk.CheckedChange += (sender, e) =>
            {
                prndl_data_type_chk.Enabled = e.IsChecked;
                prndl_result_code_chk.Enabled = e.IsChecked;
                prndl_data_type_spinner.Enabled = e.IsChecked && prndl_data_type_chk.Checked;
                prndl_result_code_spinner.Enabled = e.IsChecked && prndl_result_code_chk.Checked;
            };

            turnSignal_chk.CheckedChange += (sender, e) =>
			{
                turnSignal_data_type_chk.Enabled = e.IsChecked;
				turnSignal_result_code_chk.Enabled = e.IsChecked;
                turnSignal_data_type_spinner.Enabled = e.IsChecked && turnSignal_data_type_chk.Checked;
                turnSignal_result_code_spinner.Enabled = e.IsChecked && turnSignal_result_code_chk.Checked;
			};

            tirePressure_chk.CheckedChange += (sender, e) =>
            {
                tirePressure_data_type_chk.Enabled = e.IsChecked;
                tirePressure_result_code_chk.Enabled = e.IsChecked;
                tirePressure_data_type_spinner.Enabled = e.IsChecked && tirePressure_data_type_chk.Checked;
                tirePressure_result_code_spinner.Enabled = e.IsChecked && tirePressure_result_code_chk.Checked;
            };

            odometer_chk.CheckedChange += (sender, e) =>
            {
                odometer_data_type_chk.Enabled = e.IsChecked;
                odometer_result_code_chk.Enabled = e.IsChecked;
                odometer_data_type_spinner.Enabled = e.IsChecked && odometer_data_type_chk.Checked;
                odometer_result_code_spinner.Enabled = e.IsChecked && odometer_result_code_chk.Checked;
            };

            beltStatus_chk.CheckedChange += (sender, e) =>
            {
                beltStatus_data_type_chk.Enabled = e.IsChecked;
                beltStatus_result_code_chk.Enabled = e.IsChecked;
                beltStatus_data_type_spinner.Enabled = e.IsChecked && beltStatus_data_type_chk.Checked;
                beltStatus_result_code_spinner.Enabled = e.IsChecked && beltStatus_result_code_chk.Checked;
            };

            bodyInformation_chk.CheckedChange += (sender, e) =>
            {
                bodyInformation_data_type_chk.Enabled = e.IsChecked;
                bodyInformation_result_code_chk.Enabled = e.IsChecked;
                bodyInformation_data_type_spinner.Enabled = e.IsChecked && bodyInformation_data_type_chk.Checked;
                bodyInformation_result_code_spinner.Enabled = e.IsChecked && bodyInformation_result_code_chk.Checked;
            };

            deviceStatus_chk.CheckedChange += (sender, e) =>
            {
                deviceStatus_data_type_chk.Enabled = e.IsChecked;
                deviceStatus_result_code_chk.Enabled = e.IsChecked;
                deviceStatus_data_type_spinner.Enabled = e.IsChecked && deviceStatus_data_type_chk.Checked;
                deviceStatus_result_code_spinner.Enabled = e.IsChecked && deviceStatus_result_code_chk.Checked;
            };

            driverBraking_chk.CheckedChange += (sender, e) =>
            {
                driverBraking_data_type_chk.Enabled = e.IsChecked;
                driverBraking_result_code_chk.Enabled = e.IsChecked;
                driverBraking_data_type_spinner.Enabled = e.IsChecked && driverBraking_data_type_chk.Checked;
                driverBraking_result_code_spinner.Enabled = e.IsChecked && driverBraking_result_code_chk.Checked;
            };

            wiperStatus_chk.CheckedChange += (sender, e) =>
            {
                wiperStatus_data_type_chk.Enabled = e.IsChecked;
                wiperStatus_result_code_chk.Enabled = e.IsChecked;
                wiperStatus_data_type_spinner.Enabled = e.IsChecked && wiperStatus_data_type_chk.Checked;
                wiperStatus_result_code_spinner.Enabled = e.IsChecked && wiperStatus_result_code_chk.Checked;
            };

            headLampStatus_chk.CheckedChange += (sender, e) =>
            {
                headLampStatus_data_type_chk.Enabled = e.IsChecked;
                headLampStatus_result_code_chk.Enabled = e.IsChecked;
                headLampStatus_data_type_spinner.Enabled = e.IsChecked && headLampStatus_data_type_chk.Checked;
                headLampStatus_result_code_spinner.Enabled = e.IsChecked && headLampStatus_result_code_chk.Checked;
            };

            engineTorque_chk.CheckedChange += (sender, e) =>
            {
                engineTorque_data_type_chk.Enabled = e.IsChecked;
                engineTorque_result_code_chk.Enabled = e.IsChecked;
                engineTorque_data_type_spinner.Enabled = e.IsChecked && engineTorque_data_type_chk.Checked;
                engineTorque_result_code_spinner.Enabled = e.IsChecked && engineTorque_result_code_chk.Checked;
            };

            accPedalPosition_chk.CheckedChange += (sender, e) =>
            {
                accPedalPosition_data_type_chk.Enabled = e.IsChecked;
                accPedalPosition_result_code_chk.Enabled = e.IsChecked;
                accPedalPosition_data_type_spinner.Enabled = e.IsChecked && accPedalPosition_data_type_chk.Checked;
                accPedalPosition_result_code_spinner.Enabled = e.IsChecked && accPedalPosition_result_code_chk.Checked;
            };

            steeringWheelAngle_chk.CheckedChange += (sender, e) =>
            {
                steeringWheelAngle_data_type_chk.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_chk.Enabled = e.IsChecked;
                steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked && steeringWheelAngle_data_type_chk.Checked;
                steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked && steeringWheelAngle_result_code_chk.Checked;
            };

            eCallInfo_chk.CheckedChange += (sender, e) =>
            {
                eCallInfo_data_type_chk.Enabled = e.IsChecked;
                eCallInfo_result_code_chk.Enabled = e.IsChecked;
                eCallInfo_data_type_spinner.Enabled = e.IsChecked && eCallInfo_data_type_chk.Checked;
                eCallInfo_result_code_spinner.Enabled = e.IsChecked && eCallInfo_result_code_chk.Checked;
            };

            airbagStatus_chk.CheckedChange += (sender, e) =>
            {
                airbagStatus_data_type_chk.Enabled = e.IsChecked;
                airbagStatus_result_code_chk.Enabled = e.IsChecked;
                airbagStatus_data_type_spinner.Enabled = e.IsChecked && airbagStatus_data_type_chk.Checked;
                airbagStatus_result_code_spinner.Enabled = e.IsChecked && airbagStatus_result_code_chk.Checked;
            };

            emergencyEvent_chk.CheckedChange += (sender, e) =>
            {
                emergencyEvent_data_type_chk.Enabled = e.IsChecked;
                emergencyEvent_result_code_chk.Enabled = e.IsChecked;
                emergencyEvent_data_type_spinner.Enabled = e.IsChecked && emergencyEvent_data_type_chk.Checked;
                emergencyEvent_result_code_spinner.Enabled = e.IsChecked && emergencyEvent_result_code_chk.Checked;
            };

            clusterModes_chk.CheckedChange += (sender, e) =>
            {
                clusterModes_data_type_chk.Enabled = e.IsChecked;
                clusterModes_result_code_chk.Enabled = e.IsChecked;
                clusterModes_data_type_spinner.Enabled = e.IsChecked && clusterModes_data_type_chk.Checked;
                clusterModes_result_code_spinner.Enabled = e.IsChecked && clusterModes_result_code_chk.Checked;
            };

            myKey_chk.CheckedChange += (sender, e) =>
            {
                myKey_data_type_chk.Enabled = e.IsChecked;
                myKey_result_code_chk.Enabled = e.IsChecked;
                myKey_data_type_spinner.Enabled = e.IsChecked && myKey_data_type_chk.Checked;
                myKey_result_code_spinner.Enabled = e.IsChecked && myKey_result_code_chk.Checked;
            };

			electronicParkBrakeStatus_chk.CheckedChange += (sender, e) =>
			{
				electronicParkBrakeStatus_data_type_chk.Enabled = e.IsChecked;
				electronicParkBrakeStatus_result_code_chk.Enabled = e.IsChecked;
                electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked && electronicParkBrakeStatus_data_type_chk.Checked;
                electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked && electronicParkBrakeStatus_result_code_chk.Checked;
			};

            engineOilLife_chk.CheckedChange += (sender, e) =>
            {
                engineOilLife_data_type_chk.Enabled = e.IsChecked;
                engineOilLife_result_code_chk.Enabled = e.IsChecked;
                engineOilLife_data_type_spinner.Enabled = e.IsChecked && engineOilLife_data_type_chk.Checked;
                engineOilLife_result_code_spinner.Enabled = e.IsChecked && engineOilLife_result_code_chk.Checked;
            };

            fuelRange_chk.CheckedChange += (sender, e) =>
            {
                fuelRange_data_type_chk.Enabled = e.IsChecked;
                fuelRange_result_code_chk.Enabled = e.IsChecked;
                fuelRange_data_type_spinner.Enabled = e.IsChecked && fuelRange_data_type_chk.Checked;
                fuelRange_result_code_spinner.Enabled = e.IsChecked && fuelRange_result_code_chk.Checked;
            };

            gps_data_type_chk.CheckedChange += (sender, e) => gps_data_type_spinner.Enabled = e.IsChecked;
            speed_data_type_chk.CheckedChange += (sender, e) => speed_data_type_spinner.Enabled = e.IsChecked;
            rpm_data_type_chk.CheckedChange += (sender, e) => rpm_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_data_type_chk.CheckedChange += (sender, e) => fuelLevel_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_State_data_type_chk.CheckedChange += (sender, e) => fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_data_type_chk.CheckedChange += (sender, e) => instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
            externalTemperature_data_type_chk.CheckedChange += (sender, e) => externalTemperature_data_type_spinner.Enabled = e.IsChecked;
            prndl_data_type_chk.CheckedChange += (sender, e) => prndl_data_type_spinner.Enabled = e.IsChecked;
            turnSignal_data_type_chk.CheckedChange += (sender, e) => turnSignal_data_type_spinner.Enabled = e.IsChecked;
            tirePressure_data_type_chk.CheckedChange += (sender, e) => tirePressure_data_type_spinner.Enabled = e.IsChecked;
            odometer_data_type_chk.CheckedChange += (sender, e) => odometer_data_type_spinner.Enabled = e.IsChecked;
            beltStatus_data_type_chk.CheckedChange += (sender, e) => beltStatus_data_type_spinner.Enabled = e.IsChecked;
            bodyInformation_data_type_chk.CheckedChange += (sender, e) => bodyInformation_data_type_spinner.Enabled = e.IsChecked;
            deviceStatus_data_type_chk.CheckedChange += (sender, e) => deviceStatus_data_type_spinner.Enabled = e.IsChecked;
            driverBraking_data_type_chk.CheckedChange += (sender, e) => driverBraking_data_type_spinner.Enabled = e.IsChecked;
            wiperStatus_data_type_chk.CheckedChange += (sender, e) => wiperStatus_data_type_spinner.Enabled = e.IsChecked;
            headLampStatus_data_type_chk.CheckedChange += (sender, e) => headLampStatus_data_type_spinner.Enabled = e.IsChecked;
            engineTorque_data_type_chk.CheckedChange += (sender, e) => engineTorque_data_type_spinner.Enabled = e.IsChecked;
            accPedalPosition_data_type_chk.CheckedChange += (sender, e) => accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_data_type_chk.CheckedChange += (sender, e) => steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
            eCallInfo_data_type_chk.CheckedChange += (sender, e) => eCallInfo_data_type_spinner.Enabled = e.IsChecked;
            airbagStatus_data_type_chk.CheckedChange += (sender, e) => airbagStatus_data_type_spinner.Enabled = e.IsChecked;
            emergencyEvent_data_type_chk.CheckedChange += (sender, e) => emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
            clusterModes_data_type_chk.CheckedChange += (sender, e) => clusterModes_data_type_spinner.Enabled = e.IsChecked;
            myKey_data_type_chk.CheckedChange += (sender, e) => myKey_data_type_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_data_type_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
            engineOilLife_data_type_chk.CheckedChange += (sender, e) => engineOilLife_data_type_spinner.Enabled = e.IsChecked;
            fuelRange_data_type_chk.CheckedChange += (sender, e) => fuelRange_data_type_spinner.Enabled = e.IsChecked;

			gps_result_code_chk.CheckedChange += (sender, e) => gps_result_code_spinner.Enabled = e.IsChecked;
            speed_result_code_chk.CheckedChange += (sender, e) => speed_result_code_spinner.Enabled = e.IsChecked;
            rpm_result_code_chk.CheckedChange += (sender, e) => rpm_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_result_code_chk.CheckedChange += (sender, e) => fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_State_result_code_chk.CheckedChange += (sender, e) => fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_result_code_chk.CheckedChange += (sender, e) => instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            externalTemperature_result_code_chk.CheckedChange += (sender, e) => externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            prndl_result_code_chk.CheckedChange += (sender, e) => prndl_result_code_spinner.Enabled = e.IsChecked;
            turnSignal_result_code_chk.CheckedChange += (sender, e) => turnSignal_result_code_spinner.Enabled = e.IsChecked;
            tirePressure_result_code_chk.CheckedChange += (sender, e) => tirePressure_result_code_spinner.Enabled = e.IsChecked;
            odometer_result_code_chk.CheckedChange += (sender, e) => odometer_result_code_spinner.Enabled = e.IsChecked;
            beltStatus_result_code_chk.CheckedChange += (sender, e) => beltStatus_result_code_spinner.Enabled = e.IsChecked;
            bodyInformation_result_code_chk.CheckedChange += (sender, e) => bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            deviceStatus_result_code_chk.CheckedChange += (sender, e) => deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            driverBraking_result_code_chk.CheckedChange += (sender, e) => driverBraking_result_code_spinner.Enabled = e.IsChecked;
            wiperStatus_result_code_chk.CheckedChange += (sender, e) => wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            headLampStatus_result_code_chk.CheckedChange += (sender, e) => headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            engineTorque_result_code_chk.CheckedChange += (sender, e) => engineTorque_result_code_spinner.Enabled = e.IsChecked;
            accPedalPosition_result_code_chk.CheckedChange += (sender, e) => accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_result_code_chk.CheckedChange += (sender, e) => steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            eCallInfo_result_code_chk.CheckedChange += (sender, e) => eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            airbagStatus_result_code_chk.CheckedChange += (sender, e) => airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            emergencyEvent_result_code_chk.CheckedChange += (sender, e) => emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            clusterModes_result_code_chk.CheckedChange += (sender, e) => clusterModes_result_code_spinner.Enabled = e.IsChecked;
            myKey_result_code_chk.CheckedChange += (sender, e) => myKey_result_code_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_result_code_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
            engineOilLife_result_code_chk.CheckedChange += (sender, e) => engineOilLife_result_code_spinner.Enabled = e.IsChecked;
            fuelRange_result_code_chk.CheckedChange += (sender, e) => fuelRange_result_code_spinner.Enabled = e.IsChecked;

			var vehicleDataTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataType);
			gps_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			speed_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			rpm_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			fuelLevel_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			fuelLevel_State_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			instantFuelConsumption_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			externalTemperature_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			prndl_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            turnSignal_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			tirePressure_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			odometer_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			beltStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			bodyInformation_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			deviceStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			driverBraking_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			wiperStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			headLampStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			engineTorque_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			accPedalPosition_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			steeringWheelAngle_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			eCallInfo_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			airbagStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			emergencyEvent_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			clusterModes_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			myKey_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			electronicParkBrakeStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineOilLife_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelRange_data_type_spinner.Adapter = vehicleDataTypeAdapter;

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCode);
			result_code_spinner.Adapter = resultCodeAdapter;
			gps_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			speed_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			rpm_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			fuelLevel_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			fuelLevel_State_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			instantFuelConsumption_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			externalTemperature_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			prndl_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            turnSignal_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			tirePressure_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			odometer_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			beltStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			bodyInformation_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			deviceStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			driverBraking_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			wiperStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			headLampStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			engineTorque_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			accPedalPosition_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			steeringWheelAngle_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			eCallInfo_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			airbagStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			emergencyEvent_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			clusterModes_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			myKey_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			electronicParkBrakeStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineOilLife_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelRange_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;

			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)BuildDefaults.buildDefaultMessage(type, 0);
            }

            if (tmpObj != null)
            {
                result_code_spinner.SetSelection((int)tmpObj.getResultCode());

                if (null != tmpObj.getGps())
                {
                    if (null != tmpObj.getGps().getDataType())
                        gps_data_type_spinner.SetSelection((int)tmpObj.getGps().getDataType());
                    else
                        gps_data_type_chk.Checked = false;

                    if (null != tmpObj.getGps().getResultCode())
                        gps_result_code_spinner.SetSelection((int)tmpObj.getGps().getResultCode());
                    else
                        gps_result_code_chk.Checked = false;
                }
                else
                    gps_chk.Checked = false;

                if (null != tmpObj.getSpeed())
                {
                    if (null != tmpObj.getSpeed().getDataType())
                        speed_data_type_spinner.SetSelection((int)tmpObj.getSpeed().getDataType());
                    else
                        speed_data_type_chk.Checked = false;

                    if (null != tmpObj.getSpeed().getResultCode())
                        speed_result_code_spinner.SetSelection((int)tmpObj.getSpeed().getResultCode());
                    else
                        speed_result_code_chk.Checked = false;
                }
                else
                    speed_chk.Checked = false;

                if (null != tmpObj.getRpm())
                {
                    if (null != tmpObj.getRpm().getDataType())
                        rpm_data_type_spinner.SetSelection((int)tmpObj.getRpm().getDataType());
                    else
                        rpm_data_type_chk.Checked = false;

                    if (null != tmpObj.getRpm().getResultCode())
                        rpm_result_code_spinner.SetSelection((int)tmpObj.getRpm().getResultCode());
                    else
                        rpm_result_code_chk.Checked = false;
                }
                else
                    rpm_chk.Checked = false;

                if (null != tmpObj.getFuelLevel())
                {
                    if (null != tmpObj.getFuelLevel().getDataType())
                        fuelLevel_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel().getDataType());
                    else
                        fuelLevel_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelLevel().getResultCode())
                        fuelLevel_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel().getResultCode());
                    else
                        fuelLevel_result_code_chk.Checked = false;
                }
                else
                    fuelLevel_chk.Checked = false;

                if (null != tmpObj.getFuelLevel_State())
                {
                    if (null != tmpObj.getFuelLevel_State().getDataType())
                        fuelLevel_State_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getDataType());
                    else
                        fuelLevel_State_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelLevel_State().getResultCode())
                        fuelLevel_State_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());
                    else
                        fuelLevel_State_result_code_chk.Checked = false;
                }
                else
                    fuelLevel_State_chk.Checked = false;

                if (null != tmpObj.getInstantFuelConsumption())
                {
                    if (null != tmpObj.getInstantFuelConsumption().getDataType())
                        instantFuelConsumption_data_type_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getDataType());
                    else
                        instantFuelConsumption_data_type_chk.Checked = false;

                    if (null != tmpObj.getInstantFuelConsumption().getResultCode())
                        instantFuelConsumption_result_code_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getResultCode());
                    else
                        instantFuelConsumption_result_code_chk.Checked = false;
                }
                else
                    instantFuelConsumption_chk.Checked = false;

                if (null != tmpObj.getExternalTemperature())
                {
                    if (null != tmpObj.getExternalTemperature().getDataType())
                        externalTemperature_data_type_spinner.SetSelection((int)tmpObj.getExternalTemperature().getDataType());
                    else
                        externalTemperature_data_type_chk.Checked = false;

                    if (null != tmpObj.getExternalTemperature().getResultCode())
                        externalTemperature_result_code_spinner.SetSelection((int)tmpObj.getExternalTemperature().getResultCode());
                    else
                        externalTemperature_result_code_chk.Checked = false;
                }
                else
                    externalTemperature_chk.Checked = false;

                if (null != tmpObj.getPrndl())
                {
                    if (null != tmpObj.getPrndl().getDataType())
                        prndl_data_type_spinner.SetSelection((int)tmpObj.getPrndl().getDataType());
                    else
                        prndl_data_type_chk.Checked = false;

                    if (null != tmpObj.getPrndl().getResultCode())
                        prndl_result_code_spinner.SetSelection((int)tmpObj.getPrndl().getResultCode());
                    else
                        prndl_result_code_chk.Checked = false;
                }
                else
                    prndl_chk.Checked = false;

                if (null != tmpObj.getTurnSignal())
                {
                    if (null != tmpObj.getTurnSignal().getDataType())
                        turnSignal_data_type_spinner.SetSelection((int)tmpObj.getTurnSignal().getDataType());
                    else
                        turnSignal_data_type_chk.Checked = false;

                    if (null != tmpObj.getTurnSignal().getResultCode())
                        turnSignal_result_code_spinner.SetSelection((int)tmpObj.getTurnSignal().getResultCode());
                    else
                        turnSignal_result_code_chk.Checked = false;
                }
                else
                    turnSignal_chk.Checked = false;

                if (null != tmpObj.getTirePressure())
                {
                    if (null != tmpObj.getTirePressure().getDataType())
                        tirePressure_data_type_spinner.SetSelection((int)tmpObj.getTirePressure().getDataType());
                    else
                        tirePressure_data_type_chk.Checked = false;

                    if (null != tmpObj.getTirePressure().getResultCode())
                        tirePressure_result_code_spinner.SetSelection((int)tmpObj.getTirePressure().getResultCode());
                    else
                        tirePressure_result_code_chk.Checked = false;
                }
                else
                    tirePressure_chk.Checked = false;

                if (null != tmpObj.getOdometer())
                {
                    if (null != tmpObj.getOdometer().getDataType())
                        odometer_data_type_spinner.SetSelection((int)tmpObj.getOdometer().getDataType());
                    else
                        odometer_data_type_chk.Checked = false;

                    if (null != tmpObj.getOdometer().getResultCode())
                        odometer_result_code_spinner.SetSelection((int)tmpObj.getOdometer().getResultCode());
                    else
                        odometer_result_code_chk.Checked = false;
                }
                else
                    odometer_chk.Checked = false;

                if (null != tmpObj.getBeltStatus())
                {
                    if (null != tmpObj.getBeltStatus().getDataType())
                        beltStatus_data_type_spinner.SetSelection((int)tmpObj.getBeltStatus().getDataType());
                    else
                        beltStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getBeltStatus().getResultCode())
                        beltStatus_result_code_spinner.SetSelection((int)tmpObj.getBeltStatus().getResultCode());
                    else
                        beltStatus_result_code_chk.Checked = false;
                }
                else
                    beltStatus_chk.Checked = false;

                if (null != tmpObj.getBodyInformation())
                {
                    if (null != tmpObj.getBodyInformation().getDataType())
                        bodyInformation_data_type_spinner.SetSelection((int)tmpObj.getBodyInformation().getDataType());
                    else
                        bodyInformation_data_type_chk.Checked = false;

                    if (null != tmpObj.getBodyInformation().getResultCode())
                        bodyInformation_result_code_spinner.SetSelection((int)tmpObj.getBodyInformation().getResultCode());
                    else
                        bodyInformation_result_code_chk.Checked = false;
                }
                else
                    bodyInformation_chk.Checked = false;

                if (null != tmpObj.getDeviceStatus())
                {
                    if (null != tmpObj.getDeviceStatus().getDataType())
                        deviceStatus_data_type_spinner.SetSelection((int)tmpObj.getDeviceStatus().getDataType());
                    else
                        deviceStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getDeviceStatus().getResultCode())
                        deviceStatus_result_code_spinner.SetSelection((int)tmpObj.getDeviceStatus().getResultCode());
                    else
                        deviceStatus_result_code_chk.Checked = false;
                }
                else
                    deviceStatus_chk.Checked = false;

                if (null != tmpObj.getdriverBraking())
                {
                    if (null != tmpObj.getdriverBraking().getDataType())
                        driverBraking_data_type_spinner.SetSelection((int)tmpObj.getdriverBraking().getDataType());
                    else
                        driverBraking_data_type_chk.Checked = false;

                    if (null != tmpObj.getdriverBraking().getResultCode())
                        driverBraking_result_code_spinner.SetSelection((int)tmpObj.getdriverBraking().getResultCode());
                    else
                        driverBraking_result_code_chk.Checked = false;
                }
                else
                    driverBraking_chk.Checked = false;

                if (null != tmpObj.getWiperStatus())
                {
                    if (null != tmpObj.getWiperStatus().getDataType())
                        wiperStatus_data_type_spinner.SetSelection((int)tmpObj.getWiperStatus().getDataType());
                    else
                        wiperStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getWiperStatus().getResultCode())
                        wiperStatus_result_code_spinner.SetSelection((int)tmpObj.getWiperStatus().getResultCode());
                    else
                        wiperStatus_result_code_chk.Checked = false;
                }
                else
                    wiperStatus_chk.Checked = false;

                if (null != tmpObj.getHeadLampStatus())
                {
                    if (null != tmpObj.getHeadLampStatus().getDataType())
                        headLampStatus_data_type_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getDataType());
                    else
                        headLampStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getHeadLampStatus().getResultCode())
                        headLampStatus_result_code_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getResultCode());
                    else
                        headLampStatus_result_code_chk.Checked = false;
                }
                else
                    headLampStatus_chk.Checked = false;

                if (null != tmpObj.getEngineTorque())
                {
                    if (null != tmpObj.getEngineTorque().getDataType())
                        engineTorque_data_type_spinner.SetSelection((int)tmpObj.getEngineTorque().getDataType());
                    else
                        engineTorque_data_type_chk.Checked = false;

                    if (null != tmpObj.getEngineTorque().getResultCode())
                        engineTorque_result_code_spinner.SetSelection((int)tmpObj.getEngineTorque().getResultCode());
                    else
                        engineTorque_result_code_chk.Checked = false;
                }
                else
                    engineTorque_chk.Checked = false;

                if (null != tmpObj.getAccPedalPosition())
                {
                    if (null != tmpObj.getAccPedalPosition().getDataType())
                        accPedalPosition_data_type_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getDataType());
                    else
                        accPedalPosition_data_type_chk.Checked = false;

                    if (null != tmpObj.getAccPedalPosition().getResultCode())
                        accPedalPosition_result_code_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getResultCode());
                    else
                        accPedalPosition_result_code_chk.Checked = false;
                }
                else
                    accPedalPosition_chk.Checked = false;

                if (null != tmpObj.getSteeringWheelAngle())
                {
                    if (null != tmpObj.getSteeringWheelAngle().getDataType())
                        steeringWheelAngle_data_type_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getDataType());
                    else
                        steeringWheelAngle_data_type_chk.Checked = false;

                    if (null != tmpObj.getSteeringWheelAngle().getResultCode())
                        steeringWheelAngle_result_code_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getResultCode());
                    else
                        steeringWheelAngle_result_code_chk.Checked = false;
                }
                else
                    steeringWheelAngle_chk.Checked = false;

                if (null != tmpObj.getECallInfo())
                {
                    if (null != tmpObj.getECallInfo().getDataType())
                        eCallInfo_data_type_spinner.SetSelection((int)tmpObj.getECallInfo().getDataType());
                    else
                        eCallInfo_data_type_chk.Checked = false;

                    if (null != tmpObj.getECallInfo().getResultCode())
                        eCallInfo_result_code_spinner.SetSelection((int)tmpObj.getECallInfo().getResultCode());
                    else
                        eCallInfo_result_code_chk.Checked = false;
                }
                else
                    eCallInfo_chk.Checked = false;

                if (null != tmpObj.getAirbagStatus())
                {
                    if (null != tmpObj.getAirbagStatus().getDataType())
                        airbagStatus_data_type_spinner.SetSelection((int)tmpObj.getAirbagStatus().getDataType());
                    else
                        airbagStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getAirbagStatus().getResultCode())
                        airbagStatus_result_code_spinner.SetSelection((int)tmpObj.getAirbagStatus().getResultCode());
                    else
                        airbagStatus_result_code_chk.Checked = false;
                }
                else
                    airbagStatus_chk.Checked = false;

                if (null != tmpObj.getEmergencyEvent())
                {
                    if (null != tmpObj.getEmergencyEvent().getDataType())
                        emergencyEvent_data_type_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getDataType());
                    else
                        emergencyEvent_data_type_chk.Checked = false;

                    if (null != tmpObj.getEmergencyEvent().getResultCode())
                        emergencyEvent_result_code_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getResultCode());
                    else
                        emergencyEvent_result_code_chk.Checked = false;
                }
                else
                    emergencyEvent_chk.Checked = false;

                if (null != tmpObj.getClusterModes())
                {
                    if (null != tmpObj.getClusterModes().getDataType())
                        clusterModes_data_type_spinner.SetSelection((int)tmpObj.getClusterModes().getDataType());
                    else
                        clusterModes_data_type_chk.Checked = false;

                    if (null != tmpObj.getClusterModes().getResultCode())
                        clusterModes_result_code_spinner.SetSelection((int)tmpObj.getClusterModes().getResultCode());
                    else
                        clusterModes_result_code_chk.Checked = false;
                }
                else
                    clusterModes_chk.Checked = false;

                if (null != tmpObj.getMyKey())
                {
                    if (null != tmpObj.getMyKey().getDataType())
                        myKey_data_type_spinner.SetSelection((int)tmpObj.getMyKey().getDataType());
                    else
                        myKey_data_type_chk.Checked = false;

                    if (null != tmpObj.getMyKey().getResultCode())
                        myKey_result_code_spinner.SetSelection((int)tmpObj.getMyKey().getResultCode());
                    else
                        myKey_result_code_chk.Checked = false;
                }
                else
                    myKey_chk.Checked = false;

                if (null != tmpObj.getElectronicParkBrakeStatus())
                {
                    if (null != tmpObj.getElectronicParkBrakeStatus().getDataType())
                        electronicParkBrakeStatus_data_type_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getDataType());
                    else
                        electronicParkBrakeStatus_data_type_chk.Checked = false;

                    if (null != tmpObj.getElectronicParkBrakeStatus().getResultCode())
                        electronicParkBrakeStatus_result_code_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getResultCode());
                    else
                        electronicParkBrakeStatus_result_code_chk.Checked = false;
                }
                else
                    electronicParkBrakeStatus_chk.Checked = false;

                if (tmpObj.getEngineOilLife() != null)
                {
                    if (null != tmpObj.getEngineOilLife().getDataType())
                        engineOilLife_data_type_spinner.SetSelection((int)tmpObj.getEngineOilLife().getDataType());
                    else
                        engineOilLife_data_type_chk.Checked = false;

                    if (null != tmpObj.getEngineOilLife().getResultCode())
                        engineOilLife_result_code_spinner.SetSelection((int)tmpObj.getEngineOilLife().getResultCode());
                    else
                        engineOilLife_result_code_chk.Checked = false;
                }
                else
                    engineOilLife_chk.Checked = false;

                if (tmpObj.getFuelRange() != null)
                {
                    if (null != tmpObj.getFuelRange().getDataType())
                        fuelRange_data_type_spinner.SetSelection((int)tmpObj.getFuelRange().getDataType());
                    else
                        fuelRange_data_type_chk.Checked = false;

                    if (null != tmpObj.getFuelRange().getResultCode())
                        fuelRange_result_code_spinner.SetSelection((int)tmpObj.getFuelRange().getResultCode());
                    else
                        fuelRange_result_code_chk.Checked = false;
                }
                else
                    fuelRange_chk.Checked = false;
            }

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCode_chk.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;

                VehicleDataResult gps = null, speed = null, rpm = null, fuelLevel = null, fuelLevelState = null, instantFuel = null, externalTemperature = null,
                prndl = null, turnSignal = null, tirePressure = null, odometer = null, beltStatus = null, bodyInformation = null, deviceStatus = null, driverBraking = null,
                                 wiperStatus = null, headLampStatus = null, engineTorque = null, accPedalPosition = null, steeringWheelAngle = null, eCallInfo = null,
                airbagStatus = null, emergencyEvent = null, clusterModes = null, myKey = null, electronicParkBrakeStatus = null, engineOilLife = null, fuelRange = null;

                if (gps_chk.Checked)
                {
                    gps = new VehicleDataResult();
                    if (gps_data_type_chk.Checked)
                        gps.dataType = (VehicleDataType)gps_data_type_spinner.SelectedItemPosition;
                    if (gps_result_code_chk.Checked)
                        gps.resultCode = (VehicleDataResultCode)gps_result_code_spinner.SelectedItemPosition;
                }

                if (speed_chk.Checked)
                {
                    speed = new VehicleDataResult();
                    if (speed_data_type_chk.Checked)
                        speed.dataType = (VehicleDataType)speed_data_type_spinner.SelectedItemPosition;
                    if (speed_result_code_chk.Checked)
                        speed.resultCode = (VehicleDataResultCode)speed_result_code_spinner.SelectedItemPosition;
                }

                if (rpm_chk.Checked)
                {
                    rpm = new VehicleDataResult();
                    if (rpm_data_type_chk.Checked)
                        rpm.dataType = (VehicleDataType)rpm_data_type_spinner.SelectedItemPosition;
                    if (rpm_result_code_chk.Checked)
                        rpm.resultCode = (VehicleDataResultCode)rpm_result_code_spinner.SelectedItemPosition;
                }

                if (fuelLevel_chk.Checked)
                {
                    fuelLevel = new VehicleDataResult();
                    if (fuelLevel_data_type_chk.Checked)
                        fuelLevel.dataType = (VehicleDataType)fuelLevel_data_type_spinner.SelectedItemPosition;
                    if (fuelLevel_result_code_chk.Checked)
                        fuelLevel.resultCode = (VehicleDataResultCode)fuelLevel_result_code_spinner.SelectedItemPosition;
                }

                if (fuelLevel_State_chk.Checked)
                {
                    fuelLevelState = new VehicleDataResult();
                    if (fuelLevel_State_data_type_chk.Checked)
                        fuelLevelState.dataType = (VehicleDataType)fuelLevel_State_data_type_spinner.SelectedItemPosition;
                    if (fuelLevel_State_result_code_chk.Checked)
                        fuelLevelState.resultCode = (VehicleDataResultCode)fuelLevel_State_result_code_spinner.SelectedItemPosition;
                }

                if (instantFuelConsumption_chk.Checked)
                {
                    instantFuel = new VehicleDataResult();
                    if (instantFuelConsumption_data_type_chk.Checked)
                        instantFuel.dataType = (VehicleDataType)instantFuelConsumption_data_type_spinner.SelectedItemPosition;
                    if (instantFuelConsumption_result_code_chk.Checked)
                        instantFuel.resultCode = (VehicleDataResultCode)instantFuelConsumption_result_code_spinner.SelectedItemPosition;
                }

                if (externalTemperature_chk.Checked)
                {
                    externalTemperature = new VehicleDataResult();
                    if (externalTemperature_data_type_chk.Checked)
                        externalTemperature.dataType = (VehicleDataType)externalTemperature_data_type_spinner.SelectedItemPosition;
                    if (externalTemperature_result_code_chk.Checked)
                        externalTemperature.resultCode = (VehicleDataResultCode)externalTemperature_result_code_spinner.SelectedItemPosition;
                }

                if (prndl_chk.Checked)
                {
                    prndl = new VehicleDataResult();
                    if (prndl_data_type_chk.Checked)
                        prndl.dataType = (VehicleDataType)prndl_data_type_spinner.SelectedItemPosition;
                    if (prndl_result_code_chk.Checked)
                        prndl.resultCode = (VehicleDataResultCode)prndl_result_code_spinner.SelectedItemPosition;
                }

                if (tirePressure_chk.Checked)
                {
                    tirePressure = new VehicleDataResult();
                    if (tirePressure_data_type_chk.Checked)
                        tirePressure.dataType = (VehicleDataType)tirePressure_data_type_spinner.SelectedItemPosition;
                    if (tirePressure_result_code_chk.Checked)
                        tirePressure.resultCode = (VehicleDataResultCode)tirePressure_result_code_spinner.SelectedItemPosition;
                }

                if (odometer_chk.Checked)
                {
                    odometer = new VehicleDataResult();
                    if (odometer_data_type_chk.Checked)
                        odometer.dataType = (VehicleDataType)odometer_data_type_spinner.SelectedItemPosition;
                    if (odometer_result_code_chk.Checked)
                        odometer.resultCode = (VehicleDataResultCode)odometer_result_code_spinner.SelectedItemPosition;
                }

                if (beltStatus_chk.Checked)
                {
                    beltStatus = new VehicleDataResult();
                    if (beltStatus_data_type_chk.Checked)
                        beltStatus.dataType = (VehicleDataType)beltStatus_data_type_spinner.SelectedItemPosition;
                    if (beltStatus_result_code_chk.Checked)
                        beltStatus.resultCode = (VehicleDataResultCode)beltStatus_result_code_spinner.SelectedItemPosition;
                }

                if (bodyInformation_chk.Checked)
                {
                    bodyInformation = new VehicleDataResult();
                    if (bodyInformation_data_type_chk.Checked)
                        bodyInformation.dataType = (VehicleDataType)bodyInformation_data_type_spinner.SelectedItemPosition;
                    if (bodyInformation_result_code_chk.Checked)
                        bodyInformation.resultCode = (VehicleDataResultCode)bodyInformation_result_code_spinner.SelectedItemPosition;
                }

                if (deviceStatus_chk.Checked)
                {
                    deviceStatus = new VehicleDataResult();
                    if (deviceStatus_data_type_chk.Checked)
                        deviceStatus.dataType = (VehicleDataType)deviceStatus_data_type_spinner.SelectedItemPosition;
                    if (deviceStatus_result_code_chk.Checked)
                        deviceStatus.resultCode = (VehicleDataResultCode)deviceStatus_result_code_spinner.SelectedItemPosition;
                }

                if (driverBraking_chk.Checked)
                {
                    driverBraking = new VehicleDataResult();
                    if (driverBraking_data_type_chk.Checked)
                        driverBraking.dataType = (VehicleDataType)driverBraking_data_type_spinner.SelectedItemPosition;
                    if (driverBraking_result_code_chk.Checked)
                        driverBraking.resultCode = (VehicleDataResultCode)driverBraking_result_code_spinner.SelectedItemPosition;
                }

                if (wiperStatus_chk.Checked)
                {
                    wiperStatus = new VehicleDataResult();
                    if (wiperStatus_data_type_chk.Checked)
                        wiperStatus.dataType = (VehicleDataType)wiperStatus_data_type_spinner.SelectedItemPosition;
                    if (wiperStatus_result_code_chk.Checked)
                        wiperStatus.resultCode = (VehicleDataResultCode)wiperStatus_result_code_spinner.SelectedItemPosition;
                }

                if (headLampStatus_chk.Checked)
                {
                    headLampStatus = new VehicleDataResult();
                    if (headLampStatus_data_type_chk.Checked)
                        headLampStatus.dataType = (VehicleDataType)headLampStatus_data_type_spinner.SelectedItemPosition;
                    if (headLampStatus_result_code_chk.Checked)
                        headLampStatus.resultCode = (VehicleDataResultCode)headLampStatus_result_code_spinner.SelectedItemPosition;
                }

                if (engineTorque_chk.Checked)
                {
                    engineTorque = new VehicleDataResult();
                    if (engineTorque_data_type_chk.Checked)
                        engineTorque.dataType = (VehicleDataType)engineTorque_data_type_spinner.SelectedItemPosition;
                    if (engineTorque_result_code_chk.Checked)
                        engineTorque.resultCode = (VehicleDataResultCode)engineTorque_result_code_spinner.SelectedItemPosition;
                }

                if (accPedalPosition_chk.Checked)
                {
                    accPedalPosition = new VehicleDataResult();
                    if (accPedalPosition_data_type_chk.Checked)
                        accPedalPosition.dataType = (VehicleDataType)accPedalPosition_data_type_spinner.SelectedItemPosition;
                    if (accPedalPosition_result_code_chk.Checked)
                        accPedalPosition.resultCode = (VehicleDataResultCode)accPedalPosition_result_code_spinner.SelectedItemPosition;
                }

                if (steeringWheelAngle_chk.Checked)
                {
                    steeringWheelAngle = new VehicleDataResult();
                    if (steeringWheelAngle_data_type_chk.Checked)
                        steeringWheelAngle.dataType = (VehicleDataType)steeringWheelAngle_data_type_spinner.SelectedItemPosition;
                    if (steeringWheelAngle_result_code_chk.Checked)
                        steeringWheelAngle.resultCode = (VehicleDataResultCode)steeringWheelAngle_result_code_spinner.SelectedItemPosition;
                }

                if (eCallInfo_chk.Checked)
                {
                    eCallInfo = new VehicleDataResult();
                    if (eCallInfo_data_type_chk.Checked)
                        eCallInfo.dataType = (VehicleDataType)eCallInfo_data_type_spinner.SelectedItemPosition;
                    if (eCallInfo_result_code_chk.Checked)
                        eCallInfo.resultCode = (VehicleDataResultCode)eCallInfo_result_code_spinner.SelectedItemPosition;
                }

                if (airbagStatus_chk.Checked)
                {
                    airbagStatus = new VehicleDataResult();
                    if (airbagStatus_data_type_chk.Checked)
                        airbagStatus.dataType = (VehicleDataType)airbagStatus_data_type_spinner.SelectedItemPosition;
                    if (airbagStatus_result_code_chk.Checked)
                        airbagStatus.resultCode = (VehicleDataResultCode)airbagStatus_result_code_spinner.SelectedItemPosition;
                }

                if (emergencyEvent_chk.Checked)
                {
                    emergencyEvent = new VehicleDataResult();
                    if (emergencyEvent_data_type_chk.Checked)
                        emergencyEvent.dataType = (VehicleDataType)emergencyEvent_data_type_spinner.SelectedItemPosition;
                    if (emergencyEvent_result_code_chk.Checked)
                        emergencyEvent.resultCode = (VehicleDataResultCode)emergencyEvent_result_code_spinner.SelectedItemPosition;
                }

                if (clusterModes_chk.Checked)
                {
                    clusterModes = new VehicleDataResult();
                    if (clusterModes_data_type_chk.Checked)
                        clusterModes.dataType = (VehicleDataType)clusterModes_data_type_spinner.SelectedItemPosition;
                    if (clusterModes_result_code_chk.Checked)
                        clusterModes.resultCode = (VehicleDataResultCode)clusterModes_result_code_spinner.SelectedItemPosition;
                }

                if (myKey_chk.Checked)
                {
                    myKey = new VehicleDataResult();
                    if (myKey_data_type_chk.Checked)
                        myKey.dataType = (VehicleDataType)myKey_data_type_spinner.SelectedItemPosition;
                    if (myKey_result_code_chk.Checked)
                        myKey.resultCode = (VehicleDataResultCode)myKey_result_code_spinner.SelectedItemPosition;
                }

                if (electronicParkBrakeStatus_chk.Checked)
                {
                    electronicParkBrakeStatus = new VehicleDataResult();
                    if (electronicParkBrakeStatus_data_type_chk.Checked)
                        electronicParkBrakeStatus.dataType = (VehicleDataType)electronicParkBrakeStatus_data_type_spinner.SelectedItemPosition;
                    if (electronicParkBrakeStatus_result_code_chk.Checked)
                        electronicParkBrakeStatus.resultCode = (VehicleDataResultCode)electronicParkBrakeStatus_result_code_spinner.SelectedItemPosition;
                }

                if (engineOilLife_chk.Checked)
                {
                    engineOilLife = new VehicleDataResult();
                    if (engineOilLife_data_type_chk.Checked)
                        engineOilLife.dataType = (VehicleDataType)engineOilLife_data_type_spinner.SelectedItemPosition;
                    if (engineOilLife_result_code_chk.Checked)
                        engineOilLife.resultCode = (VehicleDataResultCode)engineOilLife_result_code_spinner.SelectedItemPosition;
                }

                if (fuelRange_chk.Checked)
                {
                    fuelRange = new VehicleDataResult();
                    if (fuelRange_data_type_chk.Checked)
                        fuelRange.dataType = (VehicleDataType)fuelRange_data_type_spinner.SelectedItemPosition;
                    if (fuelRange_result_code_chk.Checked)
                        fuelRange.resultCode = (VehicleDataResultCode)fuelRange_result_code_spinner.SelectedItemPosition;
                }

                if (turnSignal_chk.Checked)
                {
                    turnSignal = new VehicleDataResult();
                    if (turnSignal_data_type_chk.Checked)
                        turnSignal.dataType = (VehicleDataType)turnSignal_data_type_spinner.SelectedItemPosition;
                    if (turnSignal_result_code_chk.Checked)
                        turnSignal.resultCode = (VehicleDataResultCode)turnSignal_result_code_spinner.SelectedItemPosition;
                }

				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildVehicleInfoSubscribeVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gps, speed, rpm, fuelLevel, fuelLevelState, instantFuel, 
                																	externalTemperature, prndl, turnSignal,tirePressure, odometer, beltStatus,
                                                                                    bodyInformation, deviceStatus, driverBraking, wiperStatus, headLampStatus, engineTorque, 
                                                                                    accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus,
                                                                                    emergencyEvent, clusterModes, myKey, electronicParkBrakeStatus, engineOilLife, fuelRange);
                
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateVIResponseReadDID()
		{			
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.read_did_response, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox did_result_cb = (CheckBox)rpcView.FindViewById(Resource.Id.read_did_result_cb);
			Button createDidResultBtn = (Button)rpcView.FindViewById(Resource.Id.create_did_result_button);
			ListView didResultListView = (ListView)rpcView.FindViewById(Resource.Id.read_did_result_listview);

			CheckBox result_code_spinner = (CheckBox)rpcView.FindViewById(Resource.Id.read_did_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.read_did_result_code_spinner);

            List<DIDResult> didResultList = new List<DIDResult>();
			var didResultAdapter = new DIDResultAdapter(Activity, didResultList);
			didResultListView.Adapter = didResultAdapter;

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            result_code_spinner.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
            did_result_cb.CheckedChange += (sender, e) => createDidResultBtn.Enabled = e.IsChecked;

            rpcAlertDialog.SetTitle(VIResponseReadDID);

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getDidResult() != null)
				{
					didResultList.AddRange(tmpObj.getDidResult());
					didResultAdapter.NotifyDataSetChanged();
				}
				else
				{
					did_result_cb.Checked = false;
				}
            }

			createDidResultBtn.Click += (sender, e) =>
			{
				AlertDialog.Builder didResultAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
				View didResultView = layoutInflater.Inflate(Resource.Layout.did_result_response, null);
				didResultAlertDialog.SetView(didResultView);
				didResultAlertDialog.SetTitle("DID Result");

				CheckBox vehicleDataResultCodeCB = (CheckBox)didResultView.FindViewById(Resource.Id.vehicle_data_result_code_cb);
				Spinner vehicleDataResultCodeSpinner = (Spinner)didResultView.FindViewById(Resource.Id.vehicle_data_result_code_spinner);
				CheckBox didLocationCB = (CheckBox)didResultView.FindViewById(Resource.Id.did_location_cb);
				EditText didLocationET = (EditText)didResultView.FindViewById(Resource.Id.did_location_et);
				CheckBox dataCB = (CheckBox)didResultView.FindViewById(Resource.Id.data_cb);
				EditText dataET = (EditText)didResultView.FindViewById(Resource.Id.data_et);

				string[] vehicleDataResultCodeEnum = Enum.GetNames(typeof(VehicleDataResultCode));
				var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCodeEnum);
				vehicleDataResultCodeSpinner.Adapter = vehicleDataResultCodeAdapter;

                vehicleDataResultCodeCB.CheckedChange += (sndr, evnt) => vehicleDataResultCodeSpinner.Enabled = evnt.IsChecked;
                didLocationCB.CheckedChange += (sndr, evnt) => didLocationET.Enabled = evnt.IsChecked;
                dataCB.CheckedChange += (sndr, evnt) => dataET.Enabled = evnt.IsChecked;

				didResultAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					didResultAlertDialog.Dispose();
				});

				didResultAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
				{
					DIDResult didRslt = new DIDResult();
					didRslt.data = dataET.Text;
					didRslt.resultCode = (VehicleDataResultCode)vehicleDataResultCodeSpinner.SelectedItemPosition;
					try
					{
						didRslt.didLocation = Int32.Parse(didLocationET.Text.ToString());
					}
					catch (Exception e1)
					{

					}

					didResultList.Add(didRslt);
					didResultAdapter.NotifyDataSetChanged();
				});

				didResultAlertDialog.Show();
			};

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_spinner.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                if (!did_result_cb.Checked)
                    didResultList = null;

                RpcResponse rpcResponse = null;
				rpcResponse = BuildRpc.buildVehicleInfoReadDIDResponse(BuildRpc.getNextId(), rsltCode, didResultList);
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

        private void CreateNavigationResponseSetVideoConfig()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.set_video_config, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox rejectedParamsCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.rejectedParams_checkbox);
            EditText rejectedParamsEdittext = (EditText)rpcView.FindViewById(Resource.Id.rejectedParams_edittext);

            CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.set_video_config_result_code_checkbox);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.set_video_config_result_code_spinner);

            rpcAlertDialog.SetTitle(NavigationResponseSetVideoConfig);

            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            rejectedParamsCheckbox.CheckedChange += (s, e) => rejectedParamsEdittext.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());

                List<string> rejectedParamsList = tmpObj.getRejectedParams();
                if (null != rejectedParamsList)
                {
                    string[] rejectedParamsListArray = rejectedParamsList.ToArray();

                    for (int i = 0; i < rejectedParamsList.Count; i++)
                    {
                        if (i == rejectedParamsList.Count - 1)
                        {
                            rejectedParamsEdittext.Append(rejectedParamsListArray[i]);
                            break;
                        }
                        rejectedParamsEdittext.Append(rejectedParamsListArray[i] + ",");
                    }
                    rejectedParamsCheckbox.Checked = true;
                }
                else
                {
                    rejectedParamsCheckbox.Checked = false;
                    rejectedParamsEdittext.Enabled = false;
                }
            }

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                List<string> rejectedParamsList = null;

                if (rejectedParamsCheckbox.Checked)
                {
                    string[] rejectedParams = rejectedParamsEdittext.Text.Split(',');
                    rejectedParamsList = new List<string>(rejectedParams);
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildNavSetVideoConfigResponse(BuildRpc.getNextId(), rsltCode, rejectedParamsList);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        private void CreateVIResponseGetDTCs()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.get_dtc_response, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox ecuHeaderCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_ecu_header_checkbox);
			EditText ecuHeaderEdittext = (EditText)rpcView.FindViewById(Resource.Id.dtc_ecu_header_edittext);

			CheckBox dtcCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_checkbox);
			EditText dtcEdittext = (EditText)rpcView.FindViewById(Resource.Id.dtc_edittext);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_result_code_checkbox);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.dtc_result_code_spinner);

			rpcAlertDialog.SetTitle(VIResponseGetDTCs);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            ecuHeaderCheckbox.CheckedChange += (s, e) => ecuHeaderEdittext.Enabled = e.IsChecked;
            dtcCheckbox.CheckedChange += (s, e) => dtcEdittext.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());

                if(tmpObj.getEcuHeader() != null)
                {
                    ecuHeaderCheckbox.Checked = true;
                    ecuHeaderEdittext.Text = tmpObj.getEcuHeader().ToString();
                }
                else
                {
                    ecuHeaderCheckbox.Checked = false;
                    ecuHeaderEdittext.Enabled = false;
                }

                List<string> dtcList = tmpObj.getDtc();
                if (null != dtcList)
                {
					string[] messageDataRsltArray = dtcList.ToArray();

					for (int i = 0; i < dtcList.Count; i++)
					{
						if (i == dtcList.Count - 1)
						{
							dtcEdittext.Append(messageDataRsltArray[i]);
							break;
						}
						dtcEdittext.Append(messageDataRsltArray[i] + ",");
					}
                    dtcCheckbox.Checked = true;
                }
                else
                {
                    dtcCheckbox.Checked = false;
                    dtcEdittext.Enabled = false;
                }
            }

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				List<string> dtcList = null;

                int? ecuHeader = null;
                if(ecuHeaderCheckbox.Checked && ecuHeaderEdittext.Text != null && ecuHeaderEdittext.Text.Length > 0)
				    ecuHeader = Int32.Parse(ecuHeaderEdittext.Text);

                if (dtcCheckbox.Checked)
                {
                    string[] dtc = dtcEdittext.Text.Split(',');
                    dtcList = new List<string>(dtc);
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetDTCsResponse(BuildRpc.getNextId(), rsltCode,ecuHeader, dtcList);
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateVIResponseDiagnosticMessage()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.diagnostic_message, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox messageDataCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.diagnostic_messgae_data_check);
			EditText messageDataEdittext = (EditText)rpcView.FindViewById(Resource.Id.diagnostic_messgae_data__et);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.diagnostic_messgae_result_code_checkbox);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.diagnostic_messgae_result_code_spinner);

			rpcAlertDialog.SetTitle(VIResponseDiagnosticMessage);
			
            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            messageDataCheckbox.CheckedChange += (s, e) => messageDataEdittext.Enabled = e.IsChecked;
			resultCodeCheckbox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
            		
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage);
                tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());

                List<int> messageDataRsltList = tmpObj.getMessageDataResult();
                if (messageDataRsltList != null)
                {
                    int[] messageDataRsltArray = messageDataRsltList.ToArray();

                    for (int i = 0; i < messageDataRsltList.Count; i++)
                    {
                        if (i == messageDataRsltList.Count - 1)
                        {
                            messageDataEdittext.Append(messageDataRsltArray[i].ToString());
                            break;
                        }
                        messageDataEdittext.Append(messageDataRsltArray[i].ToString() + ",");
                    }
                    messageDataCheckbox.Checked = true;
                }
                else
                {
                    messageDataCheckbox.Checked = false;
					messageDataEdittext.Enabled = false;
				}
            }

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				List<int> messageDataResultList = null;
				RpcResponse rpcResponse = null;

				if (messageDataCheckbox.Checked)
				{
					messageDataResultList = new List<int>();
					string[] t = messageDataEdittext.Text.Split(',');
					foreach (string ts in t)
					{
						try
						{
							messageDataResultList.Add(Int32.Parse(ts));
						}
						catch (Exception e)
						{

						}
					}
				}

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }                

                rpcResponse = BuildRpc.buildVehicleInfoDiagnosticMessageResponse(BuildRpc.getNextId(), rsltCode, messageDataResultList);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);				
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

        private void CreateBCResponseGetSystemTime()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.bc_get_systemTime, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(BCResponseGetSystemTime);

            CheckBox getSystemTimeCB = (CheckBox)rpcView.FindViewById(Resource.Id.getSystemTime_chk);

            CheckBox millisecondCB = (CheckBox)rpcView.FindViewById(Resource.Id.millisecond_cb);
            EditText millisecondET = (EditText)rpcView.FindViewById(Resource.Id.millisecond_et);

            CheckBox secondCB = (CheckBox)rpcView.FindViewById(Resource.Id.second_cb);
            EditText secondET = (EditText)rpcView.FindViewById(Resource.Id.second_et);

            CheckBox minuteCB = (CheckBox)rpcView.FindViewById(Resource.Id.minute_cb);
            EditText minuteET = (EditText)rpcView.FindViewById(Resource.Id.minute_et);

            CheckBox hourCB = (CheckBox)rpcView.FindViewById(Resource.Id.hour_cb);
            EditText hourET = (EditText)rpcView.FindViewById(Resource.Id.hour_et);

            CheckBox dayCB = (CheckBox)rpcView.FindViewById(Resource.Id.day_cb);
            EditText dayET = (EditText)rpcView.FindViewById(Resource.Id.day_et);

            CheckBox monthCB = (CheckBox)rpcView.FindViewById(Resource.Id.month_cb);
            EditText monthET = (EditText)rpcView.FindViewById(Resource.Id.month_et);

            CheckBox yearCB = (CheckBox)rpcView.FindViewById(Resource.Id.year_cb);
            EditText yearET = (EditText)rpcView.FindViewById(Resource.Id.year_et);

            CheckBox tzHourCB = (CheckBox)rpcView.FindViewById(Resource.Id.tz_hour_cb);
            EditText tzHourET = (EditText)rpcView.FindViewById(Resource.Id.tz_hour_et);

            CheckBox tzMinuteCB = (CheckBox)rpcView.FindViewById(Resource.Id.tz_minute_cb);
            EditText tzMinuteET = (EditText)rpcView.FindViewById(Resource.Id.tz_minute_et);

            CheckBox checkBoxResultCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_code_spn);

            getSystemTimeCB.CheckedChange += (sender, e) =>
            {
                millisecondCB.Enabled = e.IsChecked;
                millisecondET.Enabled = e.IsChecked && millisecondCB.Checked;
                secondCB.Enabled = e.IsChecked;
                secondET.Enabled = e.IsChecked && secondCB.Checked;
                minuteCB.Enabled = e.IsChecked;
                minuteET.Enabled = e.IsChecked && minuteCB.Checked;
                hourCB.Enabled = e.IsChecked;
                hourET.Enabled = e.IsChecked && hourCB.Checked;
                dayCB.Enabled = e.IsChecked;
                dayET.Enabled = e.IsChecked && dayCB.Checked;
                monthCB.Enabled = e.IsChecked;
                monthET.Enabled = e.IsChecked && monthCB.Checked;
                yearCB.Enabled = e.IsChecked;
                yearET.Enabled = e.IsChecked && yearCB.Checked;
                tzHourCB.Enabled = e.IsChecked;
                tzHourET.Enabled = e.IsChecked && tzHourCB.Checked;
                tzMinuteCB.Enabled = e.IsChecked;
                tzMinuteET.Enabled = e.IsChecked && tzMinuteCB.Checked;
            };

            millisecondCB.CheckedChange += (s, e) => millisecondET.Enabled = e.IsChecked;
            secondCB.CheckedChange += (s, e) => secondET.Enabled = e.IsChecked;
            minuteCB.CheckedChange += (s, e) => minuteET.Enabled = e.IsChecked;
            hourCB.CheckedChange += (s, e) => hourET.Enabled = e.IsChecked;
            dayCB.CheckedChange += (s, e) => dayET.Enabled = e.IsChecked;
            monthCB.CheckedChange += (s, e) => monthET.Enabled = e.IsChecked;
            yearCB.CheckedChange += (s, e) => yearET.Enabled = e.IsChecked;
            tzHourCB.CheckedChange += (s, e) => tzHourET.Enabled = e.IsChecked;
            tzMinuteCB.CheckedChange += (s, e) => tzMinuteET.Enabled = e.IsChecked;


            checkBoxResultCode.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
            var resultCodeAdapter = new ArrayAdapter<String>(this.Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = resultCodeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (null != tmpObj.getSystemTime())
                {
                    if (tmpObj.getSystemTime().getMillisecond() == null)
                        millisecondCB.Checked = false;
                    else
                        millisecondET.Text = tmpObj.getSystemTime().getMillisecond().ToString();

                    if (tmpObj.getSystemTime().getSecond() == null)
                        secondCB.Checked = false;
                    else
                        secondET.Text = tmpObj.getSystemTime().getSecond().ToString();
                    
                    if (tmpObj.getSystemTime().getMinute() == null)
                        minuteCB.Checked = false;
                    else
                        minuteET.Text = tmpObj.getSystemTime().getMinute().ToString();
                    
                    if (tmpObj.getSystemTime().getHour() == null)
                        hourCB.Checked = false;
                    else
                        hourET.Text = tmpObj.getSystemTime().getHour().ToString();
                    
                    if (tmpObj.getSystemTime().getDay() == null)
                        dayCB.Checked = false;
                    else
                        dayET.Text = tmpObj.getSystemTime().getDay().ToString();
                    
                    if (tmpObj.getSystemTime().getMonth() == null)
                        monthCB.Checked = false;
                    else
                        monthET.Text = tmpObj.getSystemTime().getMonth().ToString();
                    
                    if (tmpObj.getSystemTime().getYear() == null)
                        yearCB.Checked = false;
                    else
                        yearET.Text = tmpObj.getSystemTime().getYear().ToString();
                    
                    if (tmpObj.getSystemTime().getTz_hour() == null)
                        tzHourCB.Checked = false;
                    else
                        tzHourET.Text = tmpObj.getSystemTime().getTz_hour().ToString();
                    
                    if (tmpObj.getSystemTime().getTz_minute() == null)
                        tzMinuteCB.Checked = false;
                    else
                        tzMinuteET.Text = tmpObj.getSystemTime().getTz_minute().ToString();

                    spnResultCode.SetSelection((int)tmpObj.getResultCode());
                }
                else
                    getSystemTimeCB.Checked = false;
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Structs.DateTime systemTime = null;
                if (getSystemTimeCB.Checked)
                {
                    systemTime = new HmiApiLib.Common.Structs.DateTime();

                    if (millisecondCB.Checked)
                    {
                        if (millisecondET.Text != null && millisecondET.Text.Length > 0)
                            systemTime.millisecond = Java.Lang.Integer.ParseInt(millisecondET.Text);
                    }                    
                    if (secondCB.Checked)
                    {
                        if (secondET.Text != null && secondET.Text.Length > 0)
                            systemTime.second = Java.Lang.Integer.ParseInt(secondET.Text);
                    }                  
                    if (minuteCB.Checked)
                    {
                        if (minuteET.Text != null && minuteET.Text.Length > 0)
                            systemTime.minute = Java.Lang.Integer.ParseInt(minuteET.Text);
                    }                  
                    if (hourCB.Checked)
                    {
                        if (hourET.Text != null && hourET.Text.Length > 0)
                            systemTime.hour = Java.Lang.Integer.ParseInt(hourET.Text);
                    }                  
                    if (dayCB.Checked)
                    {
                        if (dayET.Text != null && dayET.Text.Length > 0)
                            systemTime.day = Java.Lang.Integer.ParseInt(dayET.Text);
                    }                  
                    if (monthCB.Checked)
                    {
                        if (monthET.Text != null && monthET.Text.Length > 0)
                            systemTime.month = Java.Lang.Integer.ParseInt(monthET.Text);
                    }                  
                    if (yearCB.Checked)
                    {
                        if (yearET.Text != null && yearET.Text.Length > 0)
                            systemTime.year = Java.Lang.Integer.ParseInt(yearET.Text);
                    }                  
                    if (tzHourCB.Checked)
                    {
                        if (tzHourET.Text != null && tzHourET.Text.Length > 0)
                            systemTime.tz_hour = Java.Lang.Integer.ParseInt(tzHourET.Text);
                    }                  
                    if (tzMinuteCB.Checked)
                    {
                        if (tzMinuteET.Text != null && tzMinuteET.Text.Length > 0)
                            systemTime.tz_minute = Java.Lang.Integer.ParseInt(tzMinuteET.Text);
                    } 
                }

                HmiApiLib.Common.Enums.Result? resltCode = null;
                if (checkBoxResultCode.Checked)
                {
                    resltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildBasicCommunicationGetSystemTimeResponse(BuildRpc.getNextId(), resltCode, systemTime);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateBCResponseUpdateDeviceList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseUpdateDeviceList);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});


            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateDeviceListResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponseUpdateAppList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseUpdateAppList);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
		

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateAppListResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponseSystemRequest()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseSystemRequest);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
		

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationSystemRequestResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponsePolicyUpdate()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponsePolicyUpdate);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(adapter.Context, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});


            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationPolicyUpdateResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		void CreateBCResponseDialNumber()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseDialNumber);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
            resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
				

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				} 
				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationDialNumberResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }

			});

			rpcAlertDialog.Show();
		}


        void CreateBCResponseDecryptCertificate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseDecryptCertificate);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }
			
            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationDecryptCertificateResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }

            });

            rpcAlertDialog.Show();
        }

		private void CreateBCResponseAllowDeviceToConnect()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseAllowDeviceToConnect);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);
			Switch switchAllow= (Switch)rpcView.FindViewById(Resource.Id.toggle_button);
			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);
			
			
			rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAllow.CheckedChange += (s, e) =>
			{
				switchAllow.Enabled = e.IsChecked;
			};
			checkBoxAllow.Text = ("Allow");
			rsltCodeCheckBox.Text = "Result Code";

			var adapter1 = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter1;

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
				if (tmpObj.getAllow() == null)
				{
					checkBoxAllow.Checked = false;
					switchAllow.Enabled = false;
				}
				else
				{
					switchAllow.Checked = (bool)tmpObj.getAllow();
				}
			}
			

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				RpcResponse rpcResponse;
				rpcResponse = BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(BuildRpc.getNextId(), rsltCode, null);

				if (checkBoxAllow.Checked)
				{
					
					rpcResponse = BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(BuildRpc.getNextId(), rsltCode, switchAllow.Checked);

				}
				
				
				AppUtils.savePreferenceValueForRpc(adapter1.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
				}
			});

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}


		void CreateBCResponseActivateApp()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseActivateApp);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp);
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp)BuildDefaults.buildDefaultMessage(type, 0);
            }
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationActivateAppResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}

			});

			rpcAlertDialog.Show();
		}

		void CreateButtonsResponseButtonPress()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(ButtonsResponseButtonPress);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress>(Activity, tmpObj.getMethod());
            if (tmpObj == null)
            {
                Type type = typeof(HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress);
                tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)BuildDefaults.buildDefaultMessage(type, 0);
            }
            if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildButtonsButtonPressResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}

			});

			rpcAlertDialog.Show();
		}
	}
}
