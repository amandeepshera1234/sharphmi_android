﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Org.Videolan.Libvlc;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Runtime.CompilerServices;

namespace SharpHmiAndroid
{
    public class VlcPlayer : Fragment, IVLCVoutCallback, View.IOnTouchListener, VlcPlayerCallback
    {
        private SurfaceView mSurface = null;
        private ISurfaceHolder holder = null;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.Inflate(Resource.Layout.vlc_player, container,
                false);

            mSurface = (SurfaceView)rootView.FindViewById(Resource.Id.surface);
            mSurface.SetOnTouchListener(this);
            holder = mSurface.Holder;
            holder.SetFixedSize(800, 480);
            holder.SetKeepScreenOn(true);

            Button btn = (Button)rootView.FindViewById(Resource.Id.playVideo);
            btn.Click += delegate
            {
                startPlayingVideo();
            };
            Button audioBtn = (Button)rootView.FindViewById(Resource.Id.playAudio);
            audioBtn.Click += delegate
            {
                startPlayingAudio();
            };

            return rootView;
        }


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
            videoPlay();
            AppInstanceManager.Instance.setVlcPlayerUiCallback(this);
        }

        public override void OnPause()
        {
            base.OnPause();
            AppInstanceManager.Instance.setVlcPlayerUiCallback(null);
            videoPause();
        }

        public override void OnStop()
        {
            base.OnStop();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            mSurface = null;
            holder = null;
        }

        private void clearVout(){
            if(null != AppInstanceManager.Instance.mVideoMediaPlayer)
            {
                IVLCVout vout = AppInstanceManager.Instance.mVideoMediaPlayer.VLCVout;
                vout.RemoveCallback(this);
                if (vout.AreViewsAttached()) vout.DetachViews();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void videoPause(){
            if (AppInstanceManager.Instance.mVideoMediaPlayer != null && AppInstanceManager.Instance.mVideoMediaPlayer.IsPlaying)
            {
                AppInstanceManager.Instance.mVideoMediaPlayer.Pause();
            }

            clearVout();
        }

        private void initializeVout(){

            if (AppInstanceManager.Instance.mVideoMediaPlayer != null)
            {
                // Set up video output
                IVLCVout vout = AppInstanceManager.Instance.mVideoMediaPlayer.VLCVout;
                vout.SetVideoView(mSurface);
                vout.AddCallback(this);
                vout.AttachViews();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void videoPlay(){

            if ((AppInstanceManager.Instance.mVideoMediaPlayer != null) && AppInstanceManager.Instance.isVideoStreaming && !AppInstanceManager.Instance.mVideoMediaPlayer.IsPlaying && !AppInstanceManager.Instance.mVideoMediaPlayer.VLCVout.AreViewsAttached())
            {
                initializeVout();
                if (AppInstanceManager.Instance.mVideoMediaPlayer.PlayerState == 4) // Resume if Paused.
                {
                    Toast.MakeText(Application.Context, "Player Resuming.", ToastLength.Short).Show();
                }
                else
                {

                    Toast.MakeText(Application.Context, "Playing Video Player.", ToastLength.Short).Show();
                }
                AppInstanceManager.Instance.mVideoMediaPlayer.Play();
            }
            else{
                Toast.MakeText(Application.Context, "Video Player already Playing.", ToastLength.Short).Show();
            }
        }

        public void OnHardwareAccelerationError(IVLCVout p0)
        {

        }

        public void OnNewLayout(IVLCVout p0, int p1, int p2, int p3, int p4, int p5, int p6)
        {
            if (p1 * p2 == 0)
                return;
        }

        public void OnSurfacesCreated(IVLCVout p0)
        {

        }

        public void OnSurfacesDestroyed(IVLCVout p0)
        {

        }

        public void startPlayingAudio()
        {
            if ((AppInstanceManager.Instance.mAudioMediaPlayer != null) && (AppInstanceManager.Instance.isAudioStreaming))
            {
                if (!AppInstanceManager.Instance.mAudioMediaPlayer.IsPlaying)
                {
                    Toast.MakeText(Application.Context, "Starting Audio Player.", ToastLength.Short).Show();
                    AppInstanceManager.Instance.mAudioMediaPlayer.Play();
                }
                else
                {
                    Toast.MakeText(Application.Context, "Audio Player already started.", ToastLength.Short).Show();
                }
            }
            else
            {
                Toast.MakeText(Application.Context, "No Stream in Progress for Audio Player to play.", ToastLength.Short).Show();
            }

        }

        public void startPlayingVideo()
        {
            if ((AppInstanceManager.Instance.mVideoMediaPlayer != null) && (AppInstanceManager.Instance.isVideoStreaming))
            {
                if(!AppInstanceManager.Instance.mVideoMediaPlayer.IsPlaying){
                    videoPlay();
                } else {
                    Toast.MakeText(Application.Context, "Video Player already started.", ToastLength.Short).Show();
                }
            }
            else {
                Toast.MakeText(Application.Context, "No Stream in Progress for Video Player to play.", ToastLength.Short).Show();   
            }
        }

        int touchid = 0;

        public bool OnTouch(View v, MotionEvent e)
        {

            float screenX = e.RawX;
            float screenY = e.RawY;

            float viewX = (e.GetX() * 800 / v.Width);
            float viewY = (e.GetY() * 480 / v.Height);

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent myTouchEvent = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();
            if (e.Action == MotionEventActions.Up || e.Action == MotionEventActions.Cancel)
                myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.END);
            else if (e.Action == MotionEventActions.Down)
                myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.BEGIN);
            else if (e.Action == MotionEventActions.Move)
                myTouchEvent.setTouchType(HmiApiLib.Common.Enums.TouchType.MOVE);

            List<TouchCoord> touchCoordList = new List<TouchCoord>();
            TouchCoord touchCoord = new TouchCoord();
            touchCoord.x = (int)viewX;
            touchCoord.y = (int)viewY;
            touchCoordList.Add(touchCoord);

            List<TouchEvent> touchEventsList = new List<TouchEvent>();
            TouchEvent touchEvent = new TouchEvent();
            touchEvent.c = touchCoordList;
            touchEvent.id = touchid;
            List<int> tsList = new List<int>();
            tsList.Add((int)SystemClock.UptimeMillis());
            touchEvent.ts = tsList;
            touchEventsList.Add(touchEvent);

            myTouchEvent.setTouchEvent(touchEventsList);
            AppInstanceManager.Instance.sendRpc(myTouchEvent);

            return true;            
        }

        public void OnVideoStreaming()
        {
            if (AppInstanceManager.Instance.isVideoStreaming)
            {
                videoPlay();
            } else {
                Toast.MakeText(Application.Context, "No Stream in Progress for Video Player to play.", ToastLength.Short).Show();
            }
        }
    }
}