using System;
using System.Runtime.CompilerServices;
using HmiApiLib.Interfaces;
using HmiApiLib.Proxy;
using HmiApiLib;
using System.Collections.Generic;
using HmiApiLib.Base;
using Android.Graphics;
using Java.IO;
using System.IO;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Controllers.Navigation.IncomingNotifications;
using HmiApiLib.Controllers.SDL.IncomingNotifications;
using HmiApiLib.Controllers.SDL.IncomingResponses;
using System.Linq;
using HmiApiLib.Controllers.RC.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.BasicCommunication.IncomingRequests;
using Org.Videolan.Libvlc;
using Android.Widget;
using Android.App;
using Android.OS;
using HmiApiLib.Controllers.UI.IncomingRequests;

namespace SharpHmiAndroid
{
	public class AppInstanceManager : ProxyHelper, IConnectionListener, IDispatchingHelper<LogMessage>
	{
		private static volatile AppInstanceManager instance;
		private static object syncRoot = new Object();
		public MessageAdapter _msgAdapter = null;
		public static Boolean bRecycled = false;

		public static Boolean appResumed = false;

		private AppSetting appSetting = null;
		public static List<AppItem> appList = new List<AppItem>();
        public static AppItem lastFullApp;

        AppUiCallback appUiCallback;
		public static Dictionary<int, List<RpcRequest>> menuOptionListUi = new Dictionary<int, List<RpcRequest>>();
		public static Dictionary<int, List<string>> appIdPutfileList = new Dictionary<int, List<string>>();
		public static Dictionary<int, string> appIdPolicyIdDictionary = new Dictionary<int, string>();
        public static Dictionary<int, List<int?>> commandIdList = new Dictionary<int, List<int?>>();
        public enum ConnectionState {CONNECTED, DISCONNECTED, NOT_INITIALIZED }
        public static ConnectionState currentState;

		public enum SelectionMode { NONE, DEBUG_MODE, BASIC_MODE };
		private SelectionMode UserSelectedMode = SelectionMode.NONE;

        public LibVLCLibVLC mVideoLibVLC = null;
        public LibVLCLibVLC mAudioLibVLC = null;
        public MediaPlayer mVideoMediaPlayer = null;
        public MediaPlayer mAudioMediaPlayer = null;
        public VlcPlayerCallback vlcPlayerCallback = null;
        public bool isVideoStreaming = false;
        public bool isAudioStreaming = false;
        Handler handler = null;
        public bool userMovedAwayFromMediaProj = false;

        public static AppInstanceManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
                        if (instance == null) {
							
                            currentState = ConnectionState.NOT_INITIALIZED;
							instance = new AppInstanceManager();
						}
					}
				}
				else
				{
					bRecycled = true;
				}

				return instance;
			}
		}

		private AppInstanceManager()
		{
            handler = new Handler(Looper.MainLooper);
		}

		internal void setAppUiCallback(AppUiCallback callback)
		{
			appUiCallback = callback;
		}

        internal void setVlcPlayerUiCallback(VlcPlayerCallback callback)
        {
            vlcPlayerCallback = callback;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
		public void setAppSetting(AppSetting appSetting)
		{
			this.appSetting = appSetting;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public AppSetting getAppSetting()
		{
			return this.appSetting;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public void setMsgAdapter(MessageAdapter messageAdapter)
		{
			this._msgAdapter = messageAdapter;
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		public MessageAdapter getMsgAdapter()
		{
			return this._msgAdapter;
		}

        public void initializeLibVlc()
        {
            try
            {
                if (mVideoLibVLC == null)
                {
                    List<String> options = new List<String>();
                    options.Add("--demux=h264");
                    //options.Add("-vvv");
                    options.Add("--h264-fps=60");
                    options.Add("--network-caching=240");
                    options.Add("--clock-jitter=0");
                    options.Add("--clock-synchro=1");
                    mVideoLibVLC = new LibVLCLibVLC(Application.Context, options);
                    addMessageToUI(new StringLogMessage("libVLC Initialized (video)"));
                }
                if (mAudioLibVLC == null)
                {
                    List<String> options = new List<String>();
                    options.Add("--demux=rawaud");
                    options.Add("--rawaud-channels=1");
                    options.Add("--rawaud-samplerate=16000");
                    options.Add("--network-caching=1000");
                    mAudioLibVLC = new LibVLCLibVLC(Application.Context, options);
                    addMessageToUI(new StringLogMessage("libVLC Initialized (audio)"));
                }                
            }
            catch (Exception e)
            {
                addMessageToUI(new StringLogMessage("Error initializing the libVLC!"));
            }
        }
        public void initializeAudioPlayer()
        {
            if ( mAudioLibVLC == null || getAppSetting() == null) return;

            if (mAudioMediaPlayer == null)
            {
                String media = "http://" + getAppSetting().getServerAddress() + ":" + getAppSetting().getAudioStreamingPort();
                MediaLibVLC m = new MediaLibVLC(mAudioLibVLC, Android.Net.Uri.Parse(media));

                try
                {
                    // Create media player
                    mAudioMediaPlayer = new MediaPlayer(mAudioLibVLC);
                    mAudioMediaPlayer.Media = m;
                    addMessageToUI(new StringLogMessage("Audio Player configured for: " + media));

                    mAudioMediaPlayer.Play();
                    addMessageToUI(new StringLogMessage("Starting Audio Player."));
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error configuring audio Player."));
                }
            }
        }

        public void initializeVideoPlayer()
        {
            if (mVideoLibVLC == null || getAppSetting() == null) return;

            if (mVideoMediaPlayer == null)
            {
                String media = "http://" + getAppSetting().getServerAddress() + ":" + getAppSetting().getVideoStreamingPort();
                MediaLibVLC m = new MediaLibVLC(mVideoLibVLC, Android.Net.Uri.Parse(media));
                m.SetHWDecoderEnabled(true, false);
                try
                {
                    // Create media player
                    mVideoMediaPlayer = new MediaPlayer(mVideoLibVLC);
                    mVideoMediaPlayer.Media = m;
                    addMessageToUI(new StringLogMessage("Video Player configured for: " + media));
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error configuring Video Player."));
                }
            }
        }

        public void stopAudioPlayer(){
            if (mAudioMediaPlayer != null)
            {
                try
                {
                    if (mAudioMediaPlayer.IsPlaying)
                    {
                        mAudioMediaPlayer.Stop();
                        addMessageToUI(new StringLogMessage("Stopping Audio Player."));
                    }
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error stopping audio player!"));
                }
            }
        }

        public void releaseAudioPlayer()
        {
            if (mAudioMediaPlayer != null)
            {
                try
                {
                    stopAudioPlayer();
                    mAudioMediaPlayer.Release();

                    addMessageToUI(new StringLogMessage("Releasing Audio Player."));
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error releasing audio player!"));
                }
                finally
                {
                    mAudioMediaPlayer = null;
                }

            }
        }

        public void stopVideoPlayer(){
            if (mVideoMediaPlayer != null)
            {
                try
                {
                    if (mVideoMediaPlayer.IsPlaying)
                    {
                        mVideoMediaPlayer.Stop();
                        addMessageToUI(new StringLogMessage("Stopping Video Player."));
                    }

                    IVLCVout vout = mVideoMediaPlayer.VLCVout;

                    if (vout.AreViewsAttached()) vout.DetachViews();
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error stopping video player!"));
                }
            }
        }

        public void releaseVideoPlayer()
        {
            if (mVideoMediaPlayer != null)
            {
                try
                {
                    stopVideoPlayer();
                    mVideoMediaPlayer.Release();

                    addMessageToUI(new StringLogMessage("Releasing Video Player."));
                }
                catch (Exception e)
                {
                    addMessageToUI(new StringLogMessage("Error releasing video player!"));
                }
                finally
                {
                    mVideoMediaPlayer = null;
                }
            }
        }

        public void releaseVideoLibVlc()
        {
            if (mVideoLibVLC != null)
            {
                mVideoLibVLC.Release();

                addMessageToUI(new StringLogMessage("Releasing video libVLC."));

                mVideoLibVLC = null;
            }
        }

        public void releaseAudioLibVlc()
        {
            if (mAudioLibVLC != null)
            {
                mAudioLibVLC.Release();

                addMessageToUI(new StringLogMessage("Releasing audio libVLC."));

                mAudioLibVLC = null;
            }
        }

        public void setupConnection(String ipAddr, int portNum, InitialConnectionCommandConfig initialConnectionCommandConfig)
		{
            UserSelectedMode = appSetting.getSelectedMode();
            if (UserSelectedMode == SelectionMode.BASIC_MODE)
            {
                initConnectionManager(ipAddr, portNum, this, this, null, initialConnectionCommandConfig);
            }
            else if (UserSelectedMode == SelectionMode.DEBUG_MODE)
            {
                initConnectionManager(ipAddr, portNum, this, this, this, initialConnectionCommandConfig);
            }
		}

		public void onOpen()
		{
            // Handle logic for Callback triggered when Socket is Opened.
            //Console.WriteLine("Debug: onOpen()");
            currentState = ConnectionState.CONNECTED;

            if (appSetting != null && appUiCallback != null)
            {
                appUiCallback.sendRcOnRemoteControlSettings();
            }
		}

		public void onClose()
		{
			// Handle logic for Callback triggered when Socket is Opened.
			//Console.WriteLine("Debug: onClose()");
            currentState = ConnectionState.DISCONNECTED;
		}

		public void onError()
		{
			// Handle logic for Callback triggered when Socket is Opened.
			//Console.WriteLine("Debug: onError()");
			currentState = ConnectionState.DISCONNECTED;
		}

		private void addMessageToUI(LogMessage message)
		{
			if (_msgAdapter == null) return;

			_msgAdapter.addMessage(message);
		}

		public void dispatch(LogMessage message)
		{
			addMessageToUI(message);
		}

		public void handleDispatchingError(string info, Exception ex)
		{
			LogMessage logMessage = new StringLogMessage(info);
			addMessageToUI(logMessage);
		}

		public void handleQueueingError(string info, Exception ex)
		{
			LogMessage logMessage = new StringLogMessage(info);
			addMessageToUI(logMessage);
		}

		//UI interface callbacks
		public override void onUiSetAppIconRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetAppIcon msg)
		{
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();

            if (null == tmpObj)
			{
				if (null != appUiCallback)
				{
					int appId = -1;
					if (appIdPutfileList.ContainsKey((int)msg.getAppId()))
					{
						appId = (int)msg.getAppId();
					}
					else
					{
						appId = getCorrectAppId(msg.getAppId());
					}
					if (appId != -1 && selectionMode == SelectionMode.BASIC_MODE)
					{
						for (int i = 0; i < appIdPutfileList[appId].Count; i++)
						{
							if (appIdPutfileList[appId].Contains(msg.getAppIcon().getValue()))
							{
								for (int j = 0; j < appList.Count; j++)
								{
									if ((appList[j].getAppID() == appId) || (appList[j].getAppID() == msg.getAppId()))
									{
										Bitmap image = null;
										try
										{
											image = BitmapFactory.DecodeStream(getPutfile(msg.getAppIcon().getValue()));
											appList[j].setAppIcon(image);
											appUiCallback.setDownloadedAppIcon();
											sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
										}
										catch (Exception ex)
										{
											sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.INVALID_DATA));
										}
										return;
									}
								}
								break;
							}
						}
					}
					else
					{
                        Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon);
                        sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
						return;
					}
					sendRpc(BuildRpc.buildUiSetAppIconResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public int getCorrectAppId(int? matchValue)
		{
			int appId = -1;

			if (matchValue == null) return appId;

			if (appIdPolicyIdDictionary.ContainsKey(matchValue.Value))
			{
                try
                { 
                    appId = int.Parse(appIdPolicyIdDictionary[matchValue.Value]);
                }
                catch(Exception ex)
                {
                    return -1;
                }
            }

			if (appIdPolicyIdDictionary.ContainsValue(matchValue.Value.ToString()))
			{
                appId = appIdPolicyIdDictionary.Where(x => x.Value == matchValue.Value.ToString()).FirstOrDefault().Key;
			}

			return appId;
		}

		public override void onUiShowRequest(HmiApiLib.Controllers.UI.IncomingRequests.Show msg)
		{
			int corrId = msg.getId();
            HmiApiLib.Controllers.UI.OutgoingResponses.Show tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Show();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Show>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.Show);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                if(null != appUiCallback){
                    appUiCallback.onUiShowRequestCallback(msg);
                }
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
		}

		public override void onUiAddCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddCommand msg)
		{
            int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                int appID = (int)msg.getAppId();
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data;
				    List<int?> cmdIdList;
				    if (menuOptionListUi.ContainsKey(appID))
				    {
					    data = menuOptionListUi[appID];
					    data.Add(msg);
					    menuOptionListUi.Remove(appID);
				    }
				    else
				    {
					    data = new List<RpcRequest>();
					    data.Add(msg);
				    }
				    menuOptionListUi.Add(appID, data);

				    if (commandIdList.ContainsKey(appID))
				    {
					    cmdIdList = commandIdList[appID];
					    cmdIdList.Add(msg.getCmdId());
					    commandIdList.Remove(appID);
				    }
				    else
				    {
					    cmdIdList = new List<int?>();
					    cmdIdList.Add(msg.getCmdId());
				    }
				    commandIdList.Add(appID, cmdIdList);

                    if(null != appUiCallback) {
                        appUiCallback.refreshOptionsMenu();
                    }
                }
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
            }
		}

		public override void onUiAlertRequest(HmiApiLib.Controllers.UI.IncomingRequests.Alert msg)
		{
			int corrId = msg.getId();
			int? appId = msg.getAppId();
			int? windowId = msg.getWindowId();

			sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.ALERT, appId, windowId));

			HmiApiLib.Controllers.UI.OutgoingResponses.Alert tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Alert();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Alert>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				if (null != appUiCallback)
				{
					appUiCallback.onUiAlertRequestCallback(msg);
				}

				sendRpc(BuildRpc.buildUiAlertResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS, null));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
			sendRpc(BuildRpc.buildUiOnSystemContext(SystemContext.MAIN, appId, windowId));
		}

		public override void onUiPerformInteractionRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformInteraction msg)
		{
            int corrId = msg.getId();
            if (null != msg.getChoiceSet())
            {
				HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    if(null != appUiCallback) appUiCallback.onUiPerformInteractionRequestCallback(msg);
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
            else
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
		}

		public override void onUiGetLanguageRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getUIGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiDeleteCommandRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteCommand msg)
		{
			int corrId = msg.getId();
			
            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                SelectionMode selectionMode = getAppSetting().getSelectedMode();

                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data = new List<RpcRequest>();
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        foreach (RpcRequest req in data)
                        {
                            if (req is HmiApiLib.Controllers.UI.IncomingRequests.AddCommand)
                            {
                                if (((HmiApiLib.Controllers.UI.IncomingRequests.AddCommand)req).getCmdId() == msg.getCmdId())
                                {
                                    data.Remove(req);
                                    break;
                                }
                            }
                        }
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);

                    if(null != appUiCallback) appUiCallback.refreshOptionsMenu();
                }
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiIsReadyRequest(HmiApiLib.Controllers.UI.IncomingRequests.IsReady msg)
		{
            if (appSetting.getUIIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//TTS interface callbacks
		public override void onTtsSpeakRequest(HmiApiLib.Controllers.TTS.IncomingRequests.Speak msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.TTS.OutgoingResponses.Speak tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.Speak();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.Speak>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.Speak);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                if(null != appUiCallback) appUiCallback.onTtsSpeakRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsStopSpeakingRequest(HmiApiLib.Controllers.TTS.IncomingRequests.StopSpeaking msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsGetLanguageRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getTTSGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsIsReadyRequest(HmiApiLib.Controllers.TTS.IncomingRequests.IsReady msg)
		{
            if (appSetting.getTTSIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//VR interface callbacks
		public override void onVrAddCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.AddCommand msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            SelectionMode selectionMode = getAppSetting().getSelectedMode();

            if (null == tmpObj)
			{
				int? cmdId = msg.getCmdId();
				int? grammerId = msg.getGrammarId();

				if ((vrGrammerAddCommandDictionary != null) && (grammerId != null)
					&& (cmdId != null) && (grammerId != -1) && selectionMode == SelectionMode.BASIC_MODE)
				{
					vrGrammerAddCommandDictionary.Add((int)grammerId, (int)cmdId);
				}
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrGetLanguageRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetLanguage msg)
		{
            if (appSetting.getVRGetLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetLanguage);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrDeleteCommandRequest(HmiApiLib.Controllers.VR.IncomingRequests.DeleteCommand msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrIsReadyRequest(HmiApiLib.Controllers.VR.IncomingRequests.IsReady msg)
		{
            if (appSetting.getVRIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.IsReady);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrPerformInteractionRequest(HmiApiLib.Controllers.VR.IncomingRequests.PerformInteraction msg)
		{
            int corrId = msg.getId();
			HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				if (null != msg.getGrammarID())
				{
					int returnVal = getVrAddCommandId(msg.getGrammarID());

					if (returnVal != -1)
					{
						sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, returnVal, HmiApiLib.Common.Enums.Result.SUCCESS));
					}
					else
					{
						sendRpc(BuildRpc.buildVrPerformInteractionResponse(corrId, null, HmiApiLib.Common.Enums.Result.GENERIC_ERROR));
					}
				}
				else
				{
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		//Navigation interface callbacks
		public override void onNavIsReadyRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.IsReady msg)
		{
            if (appSetting.getNavigationIsReady()) 
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.Navigation, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//VehicleInfo interface callbacks
		public override void onVehicleInfoIsReadyRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.IsReady msg)
		{
            if (appSetting.getVIIsReady())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
					sendRpc(BuildRpc.buildIsReadyResponse(corrId, HmiApiLib.Types.InterfaceType.VehicleInfo, true, HmiApiLib.Common.Enums.Result.SUCCESS));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		//Bc interface callbacks
		public override void onBcAppRegisteredNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnAppRegistered msg)
		{
            if (null != msg.getApplication().appID)
            {
                appList.Add(new AppItem(msg.getApplication().appName, (int)msg.getApplication().appID));
                appIdPolicyIdDictionary.Add((int)msg.getApplication().getAppID(), msg.getApplication().getPolicyAppID());
                if (null != appUiCallback) appUiCallback.onBcAppRegisteredNotificationCallback(true);
            }
		}

		public override void onBcAppUnRegisteredNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnAppUnregistered msg)
		{
			int appID = (int)msg.getAppId();
			for (int i = 0; i < appList.Count; i++)
			{
				if ((appList[i].getAppID() == appID) || (appList[i].getAppID() == getCorrectAppId(appID)))
				{
					int tmpAppId = appID;

					if (appList[i].getAppID() == getCorrectAppId(tmpAppId))
					{
						tmpAppId = getCorrectAppId(tmpAppId);
					}

					appList.RemoveAt(i);
					i--;
				}
			}

			if (appIdPutfileList.ContainsKey(appID) || appIdPutfileList.ContainsKey(getCorrectAppId(appID)))
			{
				int tmpAppId = appID;
				if (appIdPutfileList.ContainsKey(getCorrectAppId(tmpAppId)))
				{
					tmpAppId = getCorrectAppId(tmpAppId);
				}

				deletePutfileDirectory(appIdPutfileList[tmpAppId][0]);
				appIdPutfileList[tmpAppId].Clear();
				appIdPutfileList.Remove(tmpAppId);
				appIdPolicyIdDictionary.Remove(tmpAppId);
			}

			if (null != appUiCallback) appUiCallback.onBcAppRegisteredNotificationCallback(false);
		}

		public override void onBcMixingAudioSupportedRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.MixingAudioSupported msg)
		{
            if (appSetting.getBCMixAudioSupport()) {
                int corrId = msg.getId();
				HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported();
				tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void OnButtonSubscriptionNotification(OnButtonSubscription msg)
		{
            if(null != appUiCallback) appUiCallback.OnButtonSubscriptionNotificationCallback(msg);
		}

		public override void onUiAddSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu msg)
		{
			int corrId = msg.getId();
            int appID = (int)msg.getAppId();
            HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                { 
                    List<RpcRequest> data;
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        data.Add(msg);
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    else
                    {
                        data = new List<RpcRequest>();
                        data.Add(msg);
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);
                    if(null != appUiCallback) appUiCallback.refreshOptionsMenu();
                }
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
		}

		public override void onUiChangeRegistrationRequest(HmiApiLib.Controllers.UI.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
            HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiClosePopUpRequest(HmiApiLib.Controllers.UI.IncomingRequests.ClosePopUp msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiDeleteSubMenuRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteSubMenu msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                AppInstanceManager.SelectionMode selectionMode = getAppSetting().getSelectedMode();
                if (selectionMode == SelectionMode.BASIC_MODE)
                {
                    List<RpcRequest> data = new List<RpcRequest>();
                    if (menuOptionListUi.ContainsKey((int)msg.getAppId()))
                    {
                        data = menuOptionListUi[(int)msg.getAppId()];
                        foreach (RpcRequest req in data)
                        {
                            if (req is HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu)
                            {
                                if (((HmiApiLib.Controllers.UI.IncomingRequests.AddSubMenu)req).getMenuID() == msg.getMenuID())
                                {
                                    data.Remove(req);
                                    break;
                                }
                            }
                        }
                        menuOptionListUi.Remove((int)msg.getAppId());
                    }
                    menuOptionListUi.Add((int)msg.getAppId(), data);
                    if(null != appUiCallback) appUiCallback.refreshOptionsMenu();
                }
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiEndAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.EndAudioPassThru msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiGetCapabilitiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getUIGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiGetSupportedLanguagesRequest(HmiApiLib.Controllers.UI.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getUIGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onUiPerformAudioPassThruRequest(HmiApiLib.Controllers.UI.IncomingRequests.PerformAudioPassThru msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiScrollableMessageRequest(HmiApiLib.Controllers.UI.IncomingRequests.ScrollableMessage msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                if(null != appUiCallback) appUiCallback.onUiScrollableMessageRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetDisplayLayoutRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetDisplayLayout msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetGlobalPropertiesRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetGlobalProperties msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties>(((MainActivity)appUiCallback), tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSetMediaClockTimerRequest(HmiApiLib.Controllers.UI.IncomingRequests.SetMediaClockTimer msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                if(null != appUiCallback) appUiCallback.onUiSetMediaClockTimerRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiShowCustomFormRequest(HmiApiLib.Controllers.UI.IncomingRequests.ShowCustomForm msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onUiSliderRequest(HmiApiLib.Controllers.UI.IncomingRequests.Slider msg)
		{
            int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                if(null != appUiCallback) appUiCallback.onUiSliderRequestCallback(msg);
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsChangeRegistrationRequest(HmiApiLib.Controllers.TTS.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onTtsGetCapabilitiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getTTSGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsGetSupportedLanguagesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getTTSGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId)); 
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onTtsSetGlobalPropertiesRequest(HmiApiLib.Controllers.TTS.IncomingRequests.SetGlobalProperties msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrChangeRegistrationRequest(HmiApiLib.Controllers.VR.IncomingRequests.ChangeRegistration msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVrGetCapabilitiesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetCapabilities msg)
		{
            if (appSetting.getVRGetCapabilities())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetCapabilities);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVrGetSupportedLanguagesRequest(HmiApiLib.Controllers.VR.IncomingRequests.GetSupportedLanguages msg)
		{
            if (appSetting.getVRGetSupportedLanguage())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages();
				tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VR.OutgoingResponses.GetSupportedLanguages);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onNavAlertManeuverRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.AlertManeuver msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavGetWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.GetWayPoints msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavSendLocationRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SendLocation msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavShowConstantTBTRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.ShowConstantTBT msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavStartAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartAudioStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)BuildDefaults.buildDefaultMessage(type, corrId);
			}
			else
			{
				tmpObj.setId(corrId);
			}

            sendRpc(tmpObj);
        }

		public override void onNavStartStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StartStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)BuildDefaults.buildDefaultMessage(type, corrId);
			}
			else
			{
				tmpObj.setId(corrId);
			}

            sendRpc(tmpObj);
        }

		public override void onNavStopAudioStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopAudioStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)BuildDefaults.buildDefaultMessage(type, corrId);
			}
			else
			{
				tmpObj.setId(corrId);
			}

            sendRpc(tmpObj);
        }

		public override void onNavStopStreamRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.StopStream msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream);
                tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)BuildDefaults.buildDefaultMessage(type, corrId);
			}
			else
			{
				tmpObj.setId(corrId);
			}

            sendRpc(tmpObj);
        }

		public override void onNavSubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.SubscribeWayPoints msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavUnsubscribeWayPointsRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onNavUpdateTurnListRequest(HmiApiLib.Controllers.Navigation.IncomingRequests.UpdateTurnList msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoDiagnosticMessageRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.DiagnosticMessage msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoGetDTCsRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetDTCs msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoGetVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetVehicleData msg)
		{
            if (appSetting.getVIGetVehicleData())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleData);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVehicleInfoGetVehicleTypeRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.GetVehicleType msg)
		{
            if (appSetting.getVIGetVehicleType())
            {
				int corrId = msg.getId();

				HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType();
				tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
				if (null == tmpObj)
				{
                    Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetVehicleType);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
				}
				else
				{
					tmpObj.setId(corrId);
					sendRpc(tmpObj);
				}
            }
		}

		public override void onVehicleInfoReadDIDRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.ReadDID msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoSubscribeVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.SubscribeVehicleData msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onVehicleInfoUnsubscribeVehicleDataRequest(HmiApiLib.Controllers.VehicleInfo.IncomingRequests.UnsubscribeVehicleData msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcPutfileNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnPutFile msg)
		{
			Dictionary<string, Bitmap> tmpMapping = new Dictionary<string, Bitmap>();
			string storedFileName = HttpUtility.getStoredFileName(msg.getSyncFileName());
			int appId = HttpUtility.getAppId(storedFileName);
			string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);
			string fileName = HttpUtility.getFileName(storedFileName);

			Stream inputStream = HttpUtility.downloadFile(storedFileName);

			String state = Android.OS.Environment.ExternalStorageState;

			if (Android.OS.Environment.MediaMounted == state)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";
				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (!sharpHmiAndroidDir.Exists())
				{
					sharpHmiAndroidDir.Mkdir();
				}

				Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
				if (!appStorageDir.Exists())
				{
					appStorageDir.Mkdir();
				}

				Java.IO.File myFile = new Java.IO.File(appStorageDir, fileName);

				if (!myFile.Exists())
				{
					try
					{
						myFile.CreateNewFile();
						FileOutputStream fileOutStream = new FileOutputStream(myFile);

						byte[] buffer = new byte[1024];
						int len = 0;
						while ((len = inputStream.Read(buffer, 0, buffer.Length)) > 0)
						{
							fileOutStream.Write(buffer, 0, len);
						}
						fileOutStream.Close();
					}
					catch (Exception ex)
					{
					}
				}
			}

			if (appIdPutfileList.ContainsKey(appId))
			{
				if (!appIdPutfileList[appId].Contains(msg.getSyncFileName()))
				{
					appIdPutfileList[appId].Add(msg.getSyncFileName());
				}
			}
			else
			{
				List<string> tmpPutFileList = new List<string>();
				tmpPutFileList.Add(msg.getSyncFileName());
				appIdPutfileList.Add(appId, tmpPutFileList);
			}

		}

		public override void onBcAllowDeviceToConnectRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.AllowDeviceToConnect msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcDialNumberRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.DialNumber msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcGetSystemInfoRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.GetSystemInfo msg)
		{
            if (appSetting.getBCGetSystemInfo())
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo();
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
		}
		public override void onBcGetSystemTimeRequest(GetSystemTime msg)
		{
            {
                int corrId = msg.getId();

                HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime();
                tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
                if (null == tmpObj)
                {
                    Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.GetSystemTime);
                    sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
                }
                else
                {
                    tmpObj.setId(corrId);
                    sendRpc(tmpObj);
                }
            }
		}



		public override void onBcPolicyUpdateRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.PolicyUpdate msg)
		{
			int corrId = msg.getId();

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcSystemRequestRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.SystemRequest msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public override void onBcUpdateAppListRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.UpdateAppList msg)
		{
			int corrId = msg.getId();
			
			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}
		public override void onUiRecordStartNotification(HmiApiLib.Controllers.UI.IncomingNotifications.OnRecordStart msg)
		{
			
		}
		public override void onBcUpdateDeviceListRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.UpdateDeviceList msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}

		public Stream getPutfile(string syncFileName)
		{
			Stream inputStreamObj = null;
			if (Android.OS.Environment.MediaMounted == Android.OS.Environment.ExternalStorageState)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";

				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (sharpHmiAndroidDir.Exists())
				{
					string storedFileName = HttpUtility.getStoredFileName(syncFileName);
					string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);

					Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
					if (appStorageDir.Exists())
					{
						string fileName = HttpUtility.getFileName(storedFileName);

						Java.IO.File myFile = new Java.IO.File(appStorageDir, fileName);
						if (myFile.Exists())
						{
							StreamReader inputStream = new StreamReader(appRootDirPath + appStorageDirectoryName + "/" + fileName);
							inputStreamObj = inputStream.BaseStream;
						}
					}
				}
			}
			return inputStreamObj;
		}

		public void deletePutfileDirectory(string syncFileName)
		{
			if (Android.OS.Environment.MediaMounted == Android.OS.Environment.ExternalStorageState)
			{
				string appRootDirPath = Android.OS.Environment.ExternalStorageDirectory.Path
											+ "/Sharp Hmi Android/";

				Java.IO.File sharpHmiAndroidDir = new Java.IO.File(appRootDirPath);
				if (sharpHmiAndroidDir.Exists())
				{
					string storedFileName = HttpUtility.getStoredFileName(syncFileName);
					string appStorageDirectoryName = HttpUtility.getAppStorageDirectory(storedFileName);

					Java.IO.File appStorageDir = new Java.IO.File(appRootDirPath + appStorageDirectoryName + "/");
					if (appStorageDir.Exists() && appStorageDir.IsDirectory)
					{
						Java.IO.File[] filesInFolder = appStorageDir.ListFiles();

						bool filedeleted = false;
						for (int i = 0; i < filesInFolder.Length; i++)
						{
							Java.IO.File file = filesInFolder[i];
							filedeleted = file.Delete();
						}
						filedeleted = appStorageDir.Delete();
					}
				}
			}
		}

        public override void onNavOnAudioDataStreamingNotification(OnAudioDataStreaming msg)
        {
            if (msg != null && msg.getAvailable() != null)
            {
                this.isAudioStreaming = (bool)msg.getAvailable();

                if ((bool)msg.getAvailable())
                {
                    initializeAudioPlayer();
                }
                else
                {
                    releaseAudioPlayer();
                }
            }
        }

        public override void onNavOnVideoDataStreamingNotification(OnVideoDataStreaming msg)
        {
            if(msg != null && msg.getAvailable() != null){

                this.isVideoStreaming = (bool)msg.getAvailable();

                if (this.isVideoStreaming)
                {
                    initializeVideoPlayer();

                    if(appUiCallback != null && !userMovedAwayFromMediaProj) {
                        handler.Post(new Java.Lang.Runnable(() => ((MainActivity)appUiCallback).setVlcPlayerFragment()));
                    }
                } else {
                    releaseVideoPlayer();
                }

                if (vlcPlayerCallback != null)
                {
                    handler.Post(new Java.Lang.Runnable(() => vlcPlayerCallback.OnVideoStreaming()));
                }
            }
        }

        public override void onNavOnWayPointChangeNotification(OnWayPointChange msg)
        {

        }

        public override void onBcOnResumeAudioSourceNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnResumeAudioSource msg)
        {

        }

        public override void onBcOnSDLPersistenceCompleteNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnSDLPersistenceComplete msg)
        {

        }

        public override void onBcOnFileRemovedNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnFileRemoved msg)
        {

        }

        public override void onBcOnSDLCloseNotification(HmiApiLib.Controllers.BasicCommunication.IncomingNotifications.OnSDLClose msg)
        {

        }

        public override void onBcDecryptCertificateRequest(HmiApiLib.Controllers.BasicCommunication.IncomingRequests.DecryptCertificate msg)
        {

        }

        public override void onSDLActivateAppResponse(HmiApiLib.Controllers.SDL.IncomingResponses.ActivateApp msg)
        {

        }

        public override void OnSDLOnAppPermissionChangedNotification(OnAppPermissionChanged msg)
        {

        }

        public override void OnSDLOnSDLConsentNeededNotification(OnSDLConsentNeeded msg)
        {

        }

        public override void OnSDLOnStatusUpdateNotification(OnStatusUpdate msg)
        {

        }

        public override void OnSDLOnSystemErrorNotification(OnSystemError msg)
        {

        }

        public override void OnSDLAddStatisticsInfoNotification(AddStatisticsInfo msg)
        {

        }

        public override void OnSDLOnDeviceStateChangedNotification(OnDeviceStateChanged msg)
        {

        }

        public override void onRcGetCapabilitiesRequest(HmiApiLib.Controllers.RC.IncomingRequests.GetCapabilities msg)
        {
			int corrId = msg.getId();

            HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetCapabilities);
                sendRpc(BuildDefaults.buildDefaultMessage(type,corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcGetInteriorVehicleDataRequest(HmiApiLib.Controllers.RC.IncomingRequests.GetInteriorVehicleData msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcGetInteriorVehicleDataConsentRequest(HmiApiLib.Controllers.RC.IncomingRequests.GetInteriorVehicleDataConsent msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }
		public override void onRcReleaseInteriorVehicleDataConsentRequest(HmiApiLib.Controllers.RC.IncomingRequests.ReleaseInteriorVehicleDataConsent msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent);
				sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}
		public override void onUiCreateWindowRequest(HmiApiLib.Controllers.UI.IncomingRequests.CreateWindow msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.CreateWindow);
				sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}
		public override void onUiDeleteWindowRequest(HmiApiLib.Controllers.UI.IncomingRequests.DeleteWindow msg)
		{
			int corrId = msg.getId();

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.DeleteWindow);
				sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
		}
		public override void onRcIsReadyRequest(HmiApiLib.Controllers.RC.IncomingRequests.IsReady msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.IsReady tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.IsReady();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.IsReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.IsReady>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.IsReady);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onRcSetInteriorVehicleDataRequest(HmiApiLib.Controllers.RC.IncomingRequests.SetInteriorVehicleData msg)
        {
			int corrId = msg.getId();

			HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
                Type type = typeof(HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onButtonsButtonPressRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.ButtonPress msg)
        {
			int corrId = msg.getId();

            HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
			if (null == tmpObj)
			{
				sendRpc(BuildRpc.buildButtonsButtonPressResponse(corrId, HmiApiLib.Common.Enums.Result.SUCCESS));
			}
			else
			{
				tmpObj.setId(corrId);
				sendRpc(tmpObj);
			}
        }

        public override void onSDLGetListOfPermissionsResponse(GetListOfPermissions msg)
        {

        }

        public override void onSDLGetStatusUpdateResponse(GetStatusUpdate msg)
        {

        }

        public override void onSDLGetURLSResponse(GetURLS msg)
        {

        }

        public override void onSDLGetUserFriendlyMessageResponse(GetUserFriendlyMessage msg)
        {

        }

        public override void onSDLUpdateSDLResponse(UpdateSDL msg)
        {

        }

        public override void onButtonsGetCapabilitiesRequest(HmiApiLib.Controllers.Buttons.IncomingRequests.GetCapabilities msg, InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Buttons.OutgoingResponses.GetCapabilities);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onRcOnRCStatusNotification(OnRCStatus msg)
        {

        }

        public override void onNavSetVideoConfigRequest(SetVideoConfig msg)
        {
            int corrId = msg.getId();

            HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.Navigation.OutgoingResponses.SetVideoConfig);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }

        public override void onUiShowAppMenuRequest(ShowAppMenu msg)
        {
            int corrId = msg.getId();
            int appID = (int)msg.getAppId();
            HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.ShowAppMenu);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }

        }

        public override void onUiCloseApplicationRequest(CloseApplication msg)
        {
            int corrId = msg.getId();
            int appID = (int)msg.getAppId();
            HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication>(SharpHmiApplication.getInstance().ApplicationContext, tmpObj.getMethod());
            if (null == tmpObj)
            {
                Type type = typeof(HmiApiLib.Controllers.UI.OutgoingResponses.CloseApplication);
                sendRpc(BuildDefaults.buildDefaultMessage(type, corrId));
            }
            else
            {
                tmpObj.setId(corrId);
                sendRpc(tmpObj);
            }
        }
    }
}