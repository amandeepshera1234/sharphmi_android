﻿using System;
using Android.Content;
using Android.Support.V7.Preferences;
using HmiApiLib;
using static SharpHmiAndroid.AppInstanceManager;

namespace SharpHmiAndroid
{
	public class AppSetting
	{
        private String sServerAddress = null;
        private String sServerPort = null;
        private String sVideoStreamingPort = null;
        private String sAudioStreamingPort = null;
		private static Context appContext = null;
		private ISharedPreferences prefs = null;
        private InitialConnectionCommandConfig initialConnectionCommandConfig;
        private SelectionMode selectedMode = SelectionMode.NONE;

		public AppSetting(Context appCon)
		{
			appContext = appCon;
			prefs = PreferenceManager.GetDefaultSharedPreferences(appContext);
		}

        public SelectionMode getSelectedMode()
        {
			if (prefs != null)
                selectedMode = (SelectionMode)prefs.GetInt(Const.PREFS_KEY_SELECTION_MODE, Const.PREFS_DEFAULT_SELECTION_MODE);

			return selectedMode;
        }

		public void setSelectedMode(SelectionMode mode)
		{
			selectedMode = mode;
		}

        public String getServerAddress()
		{
			if (prefs != null)
            if (sServerAddress == null)
                sServerAddress = prefs.GetString(Const.PREFS_KEY_SERVER_ADDRESS,Const.PREFS_DEFAULT_SERVER_ADDRESS);

            return sServerAddress;
		}

        public void setServerAddress(String sVal)
		{
            sServerAddress = sVal;
		}

        public string getServerPort()
		{
			if (prefs != null)
			{
                if (sServerPort == null)
				{
                    int iServerPort = prefs.GetInt(Const.PREFS_KEY_SERVER_PORT, Const.PREFS_DEFAULT_SERVER_PORT);
                    sServerPort = iServerPort.ToString();
				}
			}

            return sServerPort;
		}

        public void setServerPort(string sVal)
		{
            sServerPort = sVal;
		}

        public string getVideoStreamingPort()
        {
            if (prefs != null)
            {
                if (sVideoStreamingPort == null)
                {
                    int iVideoStreamingPort = prefs.GetInt(Const.PREFS_KEY_VIDEO_STREAMING_PORT, Const.PREFS_DEFAULT_VIDEO_STREAMING_PORT);
                    sVideoStreamingPort = iVideoStreamingPort.ToString();
                }
            }

            return sVideoStreamingPort;
        }

        public void setVideoStreamingPort(string sVal)
        {
            sVideoStreamingPort = sVal;
        }

        public string getAudioStreamingPort()
        {
            if (prefs != null)
            {
                if (sAudioStreamingPort == null)
                {
                    int iAudioStreamingPort = prefs.GetInt(Const.PREFS_KEY_AUDIO_STREAMING_PORT, Const.PREFS_DEFAULT_AUDIO_STREAMING_PORT);
                    sAudioStreamingPort = iAudioStreamingPort.ToString();
                }
            }

            return sAudioStreamingPort;
        }

        public void setAudioStreamingPort(string sVal)
        {
            sAudioStreamingPort = sVal;
        }

		public InitialConnectionCommandConfig getInitialConnectionCommandConfig()
		{
			if (prefs != null)
			{
				if (initialConnectionCommandConfig == null)
				{
                    String str = prefs.GetString(Const.PREFS_KEY_INITIAL_CONFIG, null);
                    if (str != null)
                    {
                        initialConnectionCommandConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<InitialConnectionCommandConfig>(str);
                    }
				}
			}
			return initialConnectionCommandConfig;
		}

		public void setInitialConnectionCommandConfig(InitialConnectionCommandConfig sVal)
		{
			initialConnectionCommandConfig = sVal;
		}

		Boolean BCMixAudioSupport;
		public Boolean getBCMixAudioSupport()
		{
			if (prefs != null)
				BCMixAudioSupport = prefs.GetBoolean(Const.PREFS_KEY_BC_MIX_AUDIO_SUPPORTED, Const.PREFS_DEFAULT_BC_MIX_AUDIO_SUPPORTED);

			return BCMixAudioSupport;
		}
		public void setBCMixAudioSupport(Boolean sVal)
		{
			BCMixAudioSupport = sVal;
		}

		Boolean ButtonGetCapabilities;
		public Boolean getButtonGetCapabilities()
		{
			if (prefs != null)
				ButtonGetCapabilities = prefs.GetBoolean(Const.PREFS_KEY_BUTTONS_GET_CAPABILITIES, Const.PREFS_DEFAULT_BUTTONS_GET_CAPABILITIES);

			return ButtonGetCapabilities;
		}
		public void setButtonGetCapabilities(Boolean sVal)
		{
			ButtonGetCapabilities = sVal;
		}

		Boolean NavigationIsReady;
		public Boolean getNavigationIsReady()
		{
			if (prefs != null)
				NavigationIsReady = prefs.GetBoolean(Const.PREFS_KEY_NAVIGATION_IS_READY, Const.PREFS_DEFAULT_NAVIGATION_IS_READY);

			return NavigationIsReady;
		}
		public void setNavigationIsReady(Boolean sVal)
		{
			NavigationIsReady = sVal;
		}

		Boolean RCGetCapabilities;
		public Boolean getRCGetCapabilities()
		{
			if (prefs != null)
				RCGetCapabilities = prefs.GetBoolean(Const.PREFS_KEY_RC_GET_CAPABILITIES, Const.PREFS_DEFAULT_RC_GET_CAPABILITIES);

			return RCGetCapabilities;
		}
		public void setRCGetCapabilities(Boolean sVal)
		{
			RCGetCapabilities = sVal;
		}

		Boolean RCIsReady;
		public Boolean getRCIsReady()
		{
			if (prefs != null)
				RCIsReady = prefs.GetBoolean(Const.PREFS_KEY_RC_IS_READY, Const.PREFS_DEFAULT_RC_IS_READY);

			return RCIsReady;
		}
		public void setRCIsReady(Boolean sVal)
		{
			RCIsReady = sVal;
		}

		Boolean TTSGetCapabilities;
		public Boolean getTTSGetCapabilities()
		{
			if (prefs != null)
				TTSGetCapabilities = prefs.GetBoolean(Const.PREFS_KEY_TTS_GET_CAPABILITIES, Const.PREFS_DEFAULT_TTS_GET_CAPABILITIES);

			return TTSGetCapabilities;
		}
		public void setTTSGetCapabilities(Boolean sVal)
		{
			TTSGetCapabilities = sVal;
		}

		Boolean TTSGetLanguage;
		public Boolean getTTSGetLanguage()
		{
			if (prefs != null)
				TTSGetLanguage = prefs.GetBoolean(Const.PREFS_KEY_TTS_GET_LANGUAGE, Const.PREFS_DEFAULT_TTS_GET_LANGUAGE);

			return TTSGetLanguage;
		}
		public void setTTSGetLanguage(Boolean sVal)
		{
			TTSGetLanguage = sVal;
		}

		Boolean TTSGetSupportedLanguage;
		public Boolean getTTSGetSupportedLanguage()
		{
			if (prefs != null)
				TTSGetSupportedLanguage = prefs.GetBoolean(Const.PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE, Const.PREFS_DEFAULT_TTS_GET_SUPPORTED_LANGUAGE);

			return TTSGetSupportedLanguage;
		}
		public void setTTSGetSupportedLanguage(Boolean sVal)
		{
			TTSGetSupportedLanguage = sVal;
		}

		Boolean TTSIsReady;
		public Boolean getTTSIsReady()
		{
			if (prefs != null)
				TTSIsReady = prefs.GetBoolean(Const.PREFS_KEY_TTS_IS_READY, Const.PREFS_DEFAULT_TTS_IS_READY);

			return TTSIsReady;
		}
		public void setTTSIsReady(Boolean sVal)
		{
			TTSIsReady = sVal;
		}

		Boolean UIGetCapabilities;
		public Boolean getUIGetCapabilities()
		{
			if (prefs != null)
				UIGetCapabilities = prefs.GetBoolean(Const.PREFS_KEY_UI_GET_CAPABILITIES, Const.PREFS_DEFAULT_UI_GET_CAPABILITIES);

			return UIGetCapabilities;
		}
		public void setUIGetCapabilities(Boolean sVal)
		{
			UIGetCapabilities = sVal;
		}

		Boolean UIGetLanguage;
		public Boolean getUIGetLanguage()
		{
			if (prefs != null)
				UIGetLanguage = prefs.GetBoolean(Const.PREFS_KEY_UI_GET_LANGUAGE, Const.PREFS_DEFAULT_UI_GET_LANGUAGE);

			return UIGetLanguage;
		}
		public void setUIGetLanguage(Boolean sVal)
		{
			UIGetLanguage = sVal;
		}

		Boolean UIGetSupportedLanguage;
		public Boolean getUIGetSupportedLanguage()
		{
			if (prefs != null)
				UIGetSupportedLanguage = prefs.GetBoolean(Const.PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE, Const.PREFS_DEFAULT_UI_GET_SUPPORTED_LANGUAGE);

			return UIGetSupportedLanguage;
		}
		public void setUIGetSupportedLanguage(Boolean sVal)
		{
			UIGetSupportedLanguage = sVal;
		}

		Boolean UIIsReady;
		public Boolean getUIIsReady()
		{
			if (prefs != null)
				UIIsReady = prefs.GetBoolean(Const.PREFS_KEY_UI_IS_READY, Const.PREFS_DEFAULT_UI_IS_READY);

			return UIIsReady;
		}
		public void setUIIsReady(Boolean sVal)
		{
			UIIsReady = sVal;
		}

		Boolean VIGetVehicleData;
		public Boolean getVIGetVehicleData()
		{
			if (prefs != null)
				VIGetVehicleData = prefs.GetBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_DATA, Const.PREFS_DEFAULT_VI_GET_VEHICLE_DATA);

			return VIGetVehicleData;
		}
		public void setVIGetVehicleData(Boolean sVal)
		{
			VIGetVehicleData = sVal;
		}

		Boolean VIGetVehicleType;
		public Boolean getVIGetVehicleType()
		{
			if (prefs != null)
				VIGetVehicleType = prefs.GetBoolean(Const.PREFS_KEY_VI_GET_VEHICLE_TYPE, Const.PREFS_DEFAULT_VI_GET_VEHICLE_TYPE);

			return VIGetVehicleType;
		}
		public void setVIGetVehicleType(Boolean sVal)
		{
			VIGetVehicleType = sVal;
		}

		Boolean VIIsReady;
		public Boolean getVIIsReady()
		{
			if (prefs != null)
				VIIsReady = prefs.GetBoolean(Const.PREFS_KEY_VI_IS_READY, Const.PREFS_DEFAULT_VI_IS_READY);

			return VIIsReady;
		}
		public void setVIIsReady(Boolean sVal)
		{
			VIIsReady = sVal;
		}

		Boolean VRGetCapabilities;
		public Boolean getVRGetCapabilities()
		{
			if (prefs != null)
				VRGetCapabilities = prefs.GetBoolean(Const.PREFS_KEY_VR_GET_CAPABILITIES, Const.PREFS_DEFAULT_VR_GET_CAPABILITIES);

			return VRGetCapabilities;
		}
		public void setVRGetCapabilities(Boolean sVal)
		{
			VRGetCapabilities = sVal;
		}

		Boolean VRGetLanguage;
		public Boolean getVRGetLanguage()
		{
			if (prefs != null)
				VRGetLanguage = prefs.GetBoolean(Const.PREFS_KEY_VR_GET_LANGUAGE, Const.PREFS_DEFAULT_VR_GET_LANGUAGE);

			return VRGetLanguage;
		}
		public void setVRGetLanguage(Boolean sVal)
		{
			VRGetLanguage = sVal;
		}

		Boolean VRGetSupportedLanguage;
		public Boolean getVRGetSupportedLanguage()
		{
			if (prefs != null)
				VRGetSupportedLanguage = prefs.GetBoolean(Const.PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE, Const.PREFS_DEFAULT_VR_GET_SUPPORTED_LANGUAGE);

			return VRGetSupportedLanguage;
		}
		public void setVRGetSupportedLanguage(Boolean sVal)
		{
			VRGetSupportedLanguage = sVal;
		}

		Boolean VRIsReady;
		public Boolean getVRIsReady()
		{
			if (prefs != null)
				VRIsReady = prefs.GetBoolean(Const.PREFS_KEY_VR_IS_READY, Const.PREFS_DEFAULT_VR_IS_READY);

			return VRIsReady;
		}
		public void setVRIsReady(Boolean sVal)
		{
			VRIsReady = sVal;
		}

        Boolean BCGetSystemInfo;
        public Boolean getBCGetSystemInfo()
        {
            if (prefs != null)
                BCGetSystemInfo = prefs.GetBoolean(Const.PREFS_KEY_BC_GET_SYSTEM_INFO, Const.PREFS_DEFAULT_BC_GET_SYSTEM_INFO);

            return BCGetSystemInfo;
        }
        public void setBCGetSystemInfo(Boolean sVal)
        {
            BCGetSystemInfo = sVal;
        }

        Boolean enableRC;
        public Boolean isRCEnabled()
        {
            if (prefs != null)
                enableRC = prefs.GetBoolean(Const.PREFS_KEY_IS_RC_ENABLED, Const.PREFS_DEFAULT_IS_RC_ENABLED);

            return enableRC;
        }
        public void setRCEnabled(Boolean sVal)
        {
            enableRC = sVal;
        }
    }
}
