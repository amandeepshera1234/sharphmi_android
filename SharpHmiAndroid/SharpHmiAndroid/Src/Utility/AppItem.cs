﻿using System;
using Android.Graphics;

namespace SharpHmiAndroid
{
	public class AppItem
	{
		private string AppName;
		private int AppID;
		private Bitmap appIcon;
		private int WindowId;
		public void setAppIcon(Bitmap appIcon)
		{
			this.appIcon = appIcon;
		}

		public Bitmap getAppIcon()
		{
			return appIcon;
		}

		public string getAppName()
		{
			return AppName;
		}

		public int getAppID()
		{
			return AppID;
		}
		public int getWindowID()
		{
			return WindowId;
		}
		public AppItem(string appName, int appID)
		//public AppItem(string appName, int appID, int windowId)

		{
			AppName = appName;
			AppID = appID;
			//WindowId = windowId;
		}
	}
}